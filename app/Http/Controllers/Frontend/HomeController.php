<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Route;
Use App\Models\WebsiteContent;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class HomeController extends FrontendController
{
   	public function index() {
      $getData = WebsiteContent::first();

      return view('admin.websiteContent.landing',compact('getData'));
    }
}
