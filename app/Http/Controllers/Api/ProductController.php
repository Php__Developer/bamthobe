<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomerAppointment;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Models\ProductDetail;

class ProductController extends Controller
{
    public function getproducts()
    {
        $products = Product::get();
        $i=0;
        foreach ($products as $product) {
            $data[$i]['id'] = $product['id'];
            $data[$i]['product_code'] = $product['product_code'];
            $data[$i]['image'] = url('public/uploads/catalogues').'/'.$product['product_image'];
            $data[$i]['name'] = $product['product_name'];
            $data[$i]['type'] = $product['product_type'];
            $data[$i]['product_details'] = $product['product_details'];
            $data[$i]['product_description'] = $product['product_description'];
            $data[$i]['product_length'] = $product['product_length'];
            $data[$i]['product_quantity'] = $product['product_quantity'];
            $data[$i]['product_price'] = 'SAR '.$product['product_price'];
          $i++;
        }
        return response()->json(['result'=>$data, 'status'=>200,'message'=>'Product data']);
    }

    public function getproductsdetails(Request $request)
    {
      $validator = Validator::make($request->all(), [  
            'product_id' => 'required'
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        $productdetails =  Product::where(['id' => $request['product_id']])->get();
        $Products = [];
        foreach ($productdetails as $value) {
            $pro["product_name"] = $value["product_name"];
            $pro["product_type"] = $value["product_type"];
            $pro["product_details"] = $value["product_details"];
            $pro["product_description"] = $value["product_description"];
            $pro["product_length"] = $value["product_length"];
            $pro["product_price"] = $value["product_price"];
            $pro["product_image"] = url('public/uploads/catalogues').'/'.$value["product_image"];
        $Products[] = $pro;
      }
      return response()->json(['result'=>$Products, 'status'=>200,'message'=>'Product details']);
      
    }

}
