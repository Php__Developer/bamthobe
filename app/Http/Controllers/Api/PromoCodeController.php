<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
Use App\Models\PromoCode;
use App\Models\Loyalty;
use App\Models\Order;

class PromoCodeController extends Controller
{
    public function getPromo(Request $request){
        $validator = Validator::make($request->all(),[
            'coupon' => 'required',
            'customer_id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>$validator->errors(),'status'=>400]);            
        }

        $usedCoupon = Order::where('promo', $request->coupon)->where('customer_id', $request->customer_id)->first();
        if($usedCoupon){
            return response()->json(['status'=>404, 'message' => 'Coupon already used..!']);
        }
        //dd($usedCoupon);
        $promo = PromoCode::where('promo','like', "{$request->coupon}")
                ->where('status',1)->get();
        if($promo->isEmpty()){
            $promo = Loyalty::select('user_id','points as amount', 'reedem_code as promo')->where('reedem_code','like', "{$request->coupon}")
                ->where('user_id',$request->customer_id)->get();
        }
         //dd($promo);
        if($promo->isNotEmpty()){

            return response()->json(['result'=>$promo, 'status'=>200, 'message' => 'Retrieve coupon..!']);
        }else{

            return response()->json(['status'=>404, 'message' => 'Coupon not found..!']);
        }
    }

    public function businessgetPromo(Request $request){
        $validator = Validator::make($request->all(),[
            'coupon' => 'required',
            'customer_id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>$validator->errors(),'status'=>400]);            
        }

        $usedCoupon = Business_order::where('promo', $request->coupon)->where('customer_id', $request->customer_id)->first();
        if($usedCoupon){
            return response()->json(['status'=>404, 'message' => 'Coupon already used..!']);
        }else{
            return response()->json(['result'=>$promo, 'status'=>200, 'message' => 'Retrieve coupon..!']);
        }
        //dd($usedCoupon);
        $promo = PromoCode::where('promo','like', "{$request->coupon}")
                ->where('status',1)->get();
        if($promo->isEmpty()){
            $promo = Loyalty::select('user_id','points as amount', 'reedem_code as promo')->where('reedem_code','like', "{$request->coupon}")
                ->where('user_id',$request->customer_id)->get();
        }
         //dd($promo);
        if($promo->isNotEmpty()){

            return response()->json(['result'=>$promo, 'status'=>200, 'message' => 'Retrieve coupon..!']);
        }else{

            return response()->json(['status'=>404, 'message' => 'Coupon not found..!']);
        }
    }
}
