<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Loyalty;
use App\Customer;
use App\Models\LoyaltyTerms;
use Illuminate\Support\Facades\Validator;

class LoyaltyController extends Controller
{
    public function loyalty(Request $request){
        //die('f');
    	$validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        /*$availablePoints = Loyalty::select('points')
        ->where('user_id',$request->user_id)
        ->where('reedem_code', '=', '')
        ->get();
        $a = 0;
        foreach ($availablePoints as $points) {
            $getpoints = $points["points"];
            $a += $getpoints;
        }*/
        $customer = Customer::select('total_points')
          ->where('id', '=', $request->user_id)
          ->first();
          $getTotalpoints = $customer['total_points'];
        $totalPoints = $getTotalpoints;
        $totatcountedprice=$totalPoints/300;
        $totalamount=round($totatcountedprice, 2);
    	$loyalty = Loyalty::select('id','transaction_type','points','order_id','reedem_code','created_at','updated_at')
        ->where('user_id',$request->user_id)
        ->get();
        $loyaltyArray = [];
        $loyaltyDetails = [];
        foreach ($loyalty as $value) {
            $loyaltyDetails['id'] = $value['id'];
            $loyaltyDetails["transaction_type"] = $value["transaction_type"];
            $loyaltyDetails["points"] = $value["points"];
            $loyaltyDetails["order_id"] = $value["order_id"];
            $loyaltyDetails["reedem_code"] = $value["reedem_code"];
            if($value["updated_at"]=="0000-00-00 00:00:00")
            {
                $stamp = strtotime($value["created_at"]);
                $loyaltyDetails["action_date"] = $stamp*1000;
            }
            else
            {
                $stamp = strtotime($value["updated_at"]);
                $loyaltyDetails["action_date"] = $stamp*1000;
            }
            $loyaltyArray[] = $loyaltyDetails;
        }
        $LoyaltyTerms = LoyaltyTerms::Select('content')->get();
    	return response()->json(['totalPoints'=>$totalPoints, 'totalAmount'=>$totalamount, 'history'=>$loyaltyArray, 'terms&conditions'=>$LoyaltyTerms, 'status'=>200, 'message'=>'Loyalty Details']);
    }

    public function redeemPoints(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'points' => 'required',
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        $customer = Customer::select('total_points')
          ->where('id', '=', $request->user_id)
          ->first();
        $getTotalpoints = $customer['total_points'];
        $data['user_id'] = $request->user_id;
        $data['points'] = $request->points;
        $usingPoints = $request->points;
        $RedeemCode = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
        $data['reedem_code'] = $RedeemCode;
        $data['transaction_type'] = "Reedem";
        $code = rand(10000, 99999);
        $data['order_id'] = 'Not Use';
        $leftPoints = $getTotalpoints - $usingPoints;
        $redeem = Loyalty::create($data);
        if($redeem){
            Customer::where('id',$request->user_id)->update(['total_points' => $leftPoints]);
            return response()->json(['result'=>$redeem, 'status'=>200,'message'=>'Your redemption is successfully done.']);
        }
        
    }
}
