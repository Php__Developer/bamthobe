<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use App\Models\Admin;
use App\Models\ContactUsQuery;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUsQueryMail;
use App\Customer;

class ContactUsController extends Controller
{
    public function ContactUs(){
  	    $contactUs = Admin::Select('phone','email','address','lat','longs')->get();
		    return response()->json(['result'=>$contactUs, 'status'=>200, 'message' => 'Admin details']);
    }

    public function sendQuery(Request $request){
		$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				'title' => 'required',
				'message' => 'required'
		]);
		if ($validator->fails()) 
		{
		  return response()->json(['message'=>$validator->errors(),'status'=>401]);
		}
		else{
			$customer = Customer::select('name','email')->where('id',$request['user_id'])->first();
			$data['name'] = $customer['name'];
			$data['email'] = $customer['email'];
			$data['title'] = $request->title;
			$data['message'] = $request->message;
			$query = ContactUsQuery::create($data);
			if($query){
				Mail::to('php.mspl@gmail.com')->send(new ContactUsQueryMail($data));
			    return response()->json(['status'=>200,'message'=>'We have recieved your query.We will contact you in next 24-48 hours.']);
			}
			else{
				return response()->json(['status'=>400,'message'=>'Sorry, Try again please.']);
			}
		}
	}  
}
