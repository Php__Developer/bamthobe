<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use App\Models\tax;
use App\Models\OrderitemComments;
use App\Models\Product;
use App\Models\deliveryCharges;
use App\Models\addMoreAddress;
use App\Models\OtherUsers;
use App\Models\ModelPricing;
use DB;

class OrderController extends Controller
{
    public function getOrderHistory(Request $request)
    {
        $saveArray = $request->all();
    	$validator = Validator::make($request->all(), [  
          'user_id' => 'required'
        ]);
        if ($validator->fails()) 
        {			
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
        $orderDetail = Order::where(['customer_id' => $saveArray['user_id']])->get();
        $orderArray = [];
        foreach($orderDetail as $order){
            $orderDetails['id'] = $order['id'];
            $orderDetails['order_unique_id'] = $order['order_unique_id'];
            $orderDetails['quantity'] = $order['quantity'];
            $orderDetails["order_total"]=$order["total_amount"];
            $orderDetails['down_payment'] = $order['amount_to_pay'];
            $orderDetails['price_remaining'] = $order['price_remaining'];
            $stamp = strtotime($order["created_at"]);
            $orderDetails["order_date"] = $stamp*1000;
            if($order["delivered_at"] == null){
                $stamp1 = strtotime($order["expected_delivery_time"]);
            }else{
                $stamp1 = strtotime($order["delivered_at"]);
            }
            $orderDetails["completion_date"] = $stamp1*1000;
            $orderDetails['status'] = $order['status'];
            $orderArray[] = $orderDetails;
        }
        return response()->json([
            'result'=>$orderArray,
            'status'=>200,
            'message'=>'Orders History'
        ]);
    }
}
    public function orderdetail(Request $request){
        $validator = Validator::make($request->all(), [  
               'order_unique_id' => 'required',
           ]);
           if ($validator->fails()) {           
                       return response()->json(['message'=>$validator->errors(),'status'=>401]);
            }
            $id = $request->order_unique_id;
            $details = Order::where(['order_unique_id' => $id])->groupBy('order_unique_id')->get();
            $orderdetail = [];
            foreach ($details as $key) {
                  $detailsArray["order_unique_id"]=$key["order_unique_id"];
                  $getquantity = Order::where('order_unique_id',$key['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    $order_amount = 0;
                    $orderTotalTax = 0;
                    foreach ($getquantity as $value) {
                        $qua = $value['quantity'];
                        $quantity += $qua;
                        $order_amount +=$value->total_price;
                        $orderTotalTax +=$value->tax;
                    }
                  $detailsArray['quantity'] = $quantity;
                  $detailsArray["status"]=$key["status"];
                  $stamp = strtotime($key["created_at"]);
                  $detailsArray["order_date"] = $stamp*1000;
                  $stamp1 = strtotime($key["delivered_at"]);
                  //$detailsArray["completion_date"] = $stamp1*1000;
                  $detailsArray["completion_date"] = strtotime($key->expected_delivery_time) * 1000;
                  //$detailsArray["order_amount"]=$key["total_price"];
                  $detailsArray["order_amount"]=$order_amount;
                  $detailsArray["order_tax"]=$orderTotalTax;
                  $detailsArray["order_total"]=$key["total_amount"];
                  if($key->promo_amount){
                    $detailsArray["total_after_discount"]=$key["total_amount"] - $key["promo_amount"];
                  }
                  $detailsArray["down_payment"]=$key["amount_to_pay"];
                  $detailsArray["price_paid"]=$key["price_paid"];
                  $detailsArray["price_remaining"]=$key["price_remaining"];
                  $detailsArray["customer_id"]=$key["customer_unique_id"];
                  $detailsArray["customer_name"]=$key["customer_name"];
                  $order_unique_id = $detailsArray["order_unique_id"];

                  $customer = Customer::where(['customer_unique_id' => $key->customer_unique_id])->first();
                  $detailsArray['account_type'] = $customer->account_type;                  

                  $products= Order::where(['order_unique_id' => $order_unique_id])->with('ProductModel')->get();
                  $productArray = [];
                  $i=0;
                  foreach($products as $product) {
                    //$product_id=$product["product_id"];
                    $data['order_product_id'] = $product->product_id;
                    $data['order_type'] = $product->order_type;
                    //$pro = Product::where(['id' => $product_id])->first();
                    $data['id'] = $product->product_id;
                    $data['product_code'] = $product->product_code;
                    $data['product_image'] = url('/public/uploads/orderSketch').'/'.$product->sketch;
                    //$data['product_image'] = url('/public/uploads/catalogues').'/'.$pro['product_image'];
                    $data['product_name'] = $product->product_name;
                    $data['model'] = $product->ProductModel['name'];
                    $data['product_length'] = $product->product_length;
                    $data['product_quantity'] = $product->quantity;
                    $data['product_price'] = 'SAR '.$product->total_price;
                    $data['defective'] =  '';
                    if($product->status == '5'){
                      $data['defective'] = 'yes';
                    }else{
                      $data['defective'] = 'no';
                    }

                    $productArray[] = $data;
                  } 
                  $detailsArray["products"] = $productArray;
                  
                }
                $orderdetail[] = $detailsArray;
                return response()->json(['result'=>$orderdetail, 'status'=>200, 'message' => 'Orders Details']);

    } 


    public function deliverydetails(Request $request){
        $validator = Validator::make($request->all(), [  
               'order_unique_id' => 'required',
           ]);
           if ($validator->fails()) {           
                       return response()->json(['message'=>$validator->errors(),'status'=>401]);
            }
            $id = $request->order_unique_id;
            $details = Order::where(['order_unique_id' => $id])
            ->groupBy('order_unique_id')
            ->get();
            $orderdetail = [];
            foreach ($details as $key) {
                  $detailsArray["order_unique_id"]=$key["order_unique_id"];
                  $detailsArray["customer_name"]=$key["customer_name"];
                  $detailsArray["customer_unique_id"]=$key["customer_unique_id"];
                  $customer_unique_id = $detailsArray["customer_unique_id"];
                  $detailsArray["order_total"]=$key["total_amount"];
                  $detailsArray['down_payment'] = $key['amount_to_pay'];
                  $detailsArray['price_remaining'] = $key['price_remaining'];
                  $getquantity = Order::where('order_unique_id',$key['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $value) {
                        $qua = $value['quantity'];
                        $quantity += $qua;
                    }
                  $detailsArray['quantity'] = $quantity;
                  $detailsArray["status"]=$key["status"];
                  $stamp = strtotime($key["created_at"]);
                  $detailsArray["order_date"] = $stamp*1000;
                  //$stamp1 = strtotime($key["delivered_at"]);
                  $stamp1 = strtotime($key["expected_delivery_time"]);
                  $detailsArray["completion_date"] = $stamp1*1000;
                  $Customer_address = Customer::where(['customer_unique_id' => $customer_unique_id])->first();
                  $deliveryAddress = addMoreAddress::where(['customer_unique_id' => $customer_unique_id])
                                  ->where('customer_id',$Customer_address->id)
                                  ->orderBy('id', 'DESC')->first();
                  
                  if(isset($deliveryAddress->address2)){
                    $detailsArray["customer_default_address"] = $deliveryAddress['address2'];
                    $detailsArray["customer_default_lat"] = $deliveryAddress['lat2'];
                    $detailsArray["customer_default_longs"] = $deliveryAddress['longs2'];
                  }else{    
                    $detailsArray["customer_default_address"] = $Customer_address['address'];
                    $detailsArray["customer_default_lat"] = $Customer_address['lat'];
                    $detailsArray["customer_default_longs"] = $Customer_address['longs'];
                  }
                  $detailsArray["customer_id"] = $Customer_address['id'];
                  $delivery_charges = tax::select('delivery_charges')->first();
                  $detailsArray["delivery_charges"] = $delivery_charges["delivery_charges"];
                  $orderdetail[] = $detailsArray;
                } 
                
                return response()->json(['result'=>$orderdetail, 'status'=>200, 'message' => 'Delivery Details']);

    }


    public function deliveryadd_address(Request $request){
        $validator = Validator::make($request->all(), [  
               'user_id' => 'required',
               'address' => 'required',
               'order_unique_id' => 'required'
           ]);
           if ($validator->fails()) {           
                       return response()->json(['message'=>$validator->errors(),'status'=>401]);
            }
            $id = $request->user_id;
            $Customer = Customer::where(['id' => $id])->first();
            $data['customer_unique_id'] = $Customer['customer_unique_id'];
            $data['customer_id'] = $id;
            $data['address2'] = $request->address;
            if($request->lat){
              $data['lat2'] = $request->lat;
            }else{
              $data['lat2'] = 0;
            }
            if($request->longs){
              $data['longs2'] = $request->longs;
            }else{
              $data['longs2'] = 0;
            }
            $delivery_charges = tax::select('delivery_charges')->first();

            $getOrderTotal = Order::where('order_unique_id',$request->order_unique_id)->get()->first();
            if($getOrderTotal->delivery_charges == 0 ){

              $totalOrderAmountWithDelivery = $getOrderTotal->total_amount+$delivery_charges->delivery_charges;
              $remainingOrderAmountWithDelivery = $getOrderTotal->price_remaining+$delivery_charges->delivery_charges;
            }else{
              $totalOrderAmountWithDelivery = $getOrderTotal->total_amount;
              $remainingOrderAmountWithDelivery = $getOrderTotal->price_remaining;
            }


            $updateDeliverCharge = Order::where('order_unique_id',$request->order_unique_id)
                                  ->update(['delivery' => '2','delivery_charges' => $delivery_charges->delivery_charges,'total_amount' => $totalOrderAmountWithDelivery, 'price_remaining' => $remainingOrderAmountWithDelivery]);

            $addAddress = addMoreAddress::create($data);
                return response()->json(['result'=>$addAddress, 'status'=>200, 'message' => 'Added new Address']);

    }

    public function reorder(Request $request){
      $saveArray = $request->all();
      $validator = Validator::make($request->all(), [  
        //'user_id' => 'required',
        'order_unique_id'=>'required',
        'product_id'=>'required' 
      ]);
      if ($validator->fails()) {			
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
      }
      else{
          $order = Order::where('order_unique_id',$request['order_unique_id'])
          //->where('customer_id',$request['user_id'])
          //->where('order_by',$request['user_id'])
          ->where('product_id',$request['product_id'])
          ->first();

          if ($order) {
                if($order['behaviour']=='External'){
                  return response()->json(['message'=>'Sorry, Please try with other internal order.','status'=>401]);
                }
                else{
                    $data=array();
                    $lastOrder = Order::select('id')
                    ->orderBy('id', 'desc')
                    ->first();
                    if($lastOrder){
                      $order_unique_id = date('y').sprintf("%06d", $lastOrder->id+1);
                    }else{
                        $order_unique_id = date('y').sprintf("%06d", 1);
                    }

                    $data['order_unique_id'] = $order_unique_id;
                    $data['customer_id']=$order['customer_id'];
                    $data['customer_unique_id']=$order['customer_unique_id'];
                    //echo json_encode($order['customer_unique_id']); die;
                    $data['customer_name']=$order['customer_name'];

                    $data['product_name']=$order['product_name'];
                    $data['product_length']=$order['product_length'];
                    $data['quantity']=$order['quantity'];
                    $data['behaviour']=$order['behaviour'];
                    $data['sketch']=$order['sketch'];

                    $data['comments']=$order['comments'];

                    $parts = explode('-', $order['product_id']);
                    $onlyProId = $parts[1];

                    $Getproduct = Product::select('*')
                    ->where('id',$onlyProId)->first();

                    $internalProductId = Order::max('id') + 1;
                    $createdProductId = $internalProductId.'-'.$Getproduct['id'];
                    $data['product_id'] = $createdProductId;

                    if($Getproduct){
                      $reOrderLenght = $order['product_length'] * $order['quantity'];
                      if($Getproduct->product_length >= $reOrderLenght){

                          $currentQuantity = $Getproduct->product_length - $reOrderLenght;
                          $updateQuantity = Product::where('id',$onlyProId)
                              ->update(['product_length' => $currentQuantity]);
                      }else{
                          return response()->json(['status'=>204, 'message' => 'Sorry, Product quantity not available.']);                        
                      }
                    }else{
                      return response()->json(['status'=>204, 'message' => 'Sorry, Product is not available.']);  
                    }

                    if(isset($order['comments'])){
                        $data['comments'] = $order['comments'];

                        //Insert comment on the basis of product
                        $commentData['comment'] = $order['comments'];
                        $commentData['user_id'] = $order['order_by'];

                        $commentData['order_product_id'] = $createdProductId;

                        $commentData['order_unique_id'] = $order_unique_id;
                        //dd($commentData);
                        $comment = OrderitemComments::create($commentData);

                    }else{
                        $data['comments'] = NULL;
                    }
                    $modelPrice = ModelPricing::where('id',$order['model'])->get()->first();

                    $productPrice = ($Getproduct['product_price']*$reOrderLenght)+$modelPrice->price;
                    $gettax = tax::first();
                    $tax = $gettax['tax_rate'];
                    $productTax = ($productPrice*$tax)/100;

                    $data['total_price']=$productPrice;
                    $data['product_code']=$Getproduct->product_code;
                    $data['tax']=$productTax;

                    if($order->delivery == 2){
                      $deliveryCharges = $gettax['delivery_charges'];
                    }else{
                      $deliveryCharges = 0.00;
                    }

                    $data['delivery_charges']=$deliveryCharges;

                    $data['total_amount']=$productPrice+$productTax+$deliveryCharges;
                    //$data['price_paid']='SAR 0.00;';
                    $data['price_remaining']=$data['total_amount'];
                    $data['discount_code']= NULL;
                    $data['amount_to_pay']= '';
                    $data['payment_method']=NULL;
                    $data['order_by']=$order['order_by'];
                    $data['model']=$order['model'];

                    $getActiveTailor = OtherUsers::where('type','2')->get();

                    $days = $order['quantity']/(4*$getActiveTailor->count());
                    $addBufferDay = round($days)+$gettax->buffer_days;
                    $today=date('d-m-Y');
                    $delivery_date=date('d-m-Y', strtotime($today. ' +'.$addBufferDay.'  days'));

                    $data['expected_delivery_time']=$delivery_date;
                    $data['status']='4';

                    $newOrder = Order::insertGetId($data); 
                  return response()->json(['status'=>200,'message'=>'Your Order successfully placed.']);
                }
          }
          else{
              return response()->json(['message'=>'Sorry,Order Not Found,try again.','status'=>404]);
          }
      }
    }

}

