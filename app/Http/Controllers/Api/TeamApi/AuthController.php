<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use Hash;
use App\Mail\ForgetPasswordMail;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
	public function login(Request $request){
		$validator = Validator::make($request->all(), [
				'email' => 'required',
				'password' => 'required',
				'device_token' => 'required'
		]);
		if ($validator->fails()) 
		{
		  return response()->json(['message'=>$validator->errors(),'status'=>401]);
		}
		$teamuser = Salesman::where('email', '=', $request->email)
		  ->first();
		if($teamuser)
		{
		    if(Hash::check($request->password,$teamuser['password'])){
		    	$device_token =$request->device_token;
		   		$resp= Salesman::where('id', $teamuser['id'])->update(['device_token'=>$device_token]);
                $image = public_path('/uploads/salesmans/'.$teamuser['image_name']);
		    return response()->json([
		    	'user_id' => $teamuser->id,
		    	'title' => $teamuser->title, 
		    	'email'=> $teamuser->email,
		    	'description'=> $teamuser->description, 
		    	'phone' => $teamuser->phone, 
		    	'branch' => $teamuser->branch, 
		    	'monthly_target' => $teamuser->monthly_target, 
		    	'type' => $teamuser->type,
		    	'image' => $image,
		    	'status'=>200, 
		    	'register_status' => 'Logged in successfully.!'
		    ]);
		    }
		    else 
		       {
			    return response()->json(['status'=>400,'message'=>'Email or Password is incorrect']);
		    }
	    }
		else 
		   {
			return response()->json(['status'=>404,'message'=>'User does not exists']);
		   }     	
	}

	public function forgetpassword(Request $request){
		$validator = Validator::make($request->all(), [
				'email' => 'required'
		]);
		if ($validator->fails()) 
		{
		  return response()->json(['message'=>$validator->errors(),'status'=>401]);
		}
		$teamuser = Salesman::where('email', $request['email'])->first();
		if($teamuser){
			$userpassword = rand(100000, 999999);
			$pass['password'] = $userpassword;
			$teamuser->password = Hash::make($userpassword);
			$teamuser->save();
			Mail::to($request['email'])->send(new ForgetPasswordMail($pass));
			return response()->json(['status'=>200,'message'=>'Please check mail, A new password sent in your mailbox.']);
		}
		else{
			return response()->json(['status'=>404,'message'=>'This email does not exists']);
		}
	}

	public function changePassword(Request $request){
		$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				'old_password' => 'required',
				'new_password' => 'required'
		]);
		if ($validator->fails()) 
		{
		  return response()->json(['message'=>$validator->errors(),'status'=>401]);
		}else{
			$getUser = Salesman::where('id',$request['user_id'])->first();
			//dd($getUser);
			if(!empty($getUser))
			{
				if(Hash::check($request->old_password,$getUser['password'])){
					$NewPassword = Hash::make($request->new_password);
				    $updPassword = Salesman::where('id',$request['user_id'])
				        ->update(['password' => $NewPassword]);
				    if(!empty($updPassword))
			        {
			    	    return response()->json(['status'=>200,'message'=>'Password updated successfully..!']);
			        }
			        else{
			    	    return response()->json(['status'=>400,'message'=>'Password not updated..!']);
			        }
			    }
			    else{
			    	return response()->json(['status'=>404,'message'=>'Old Password did not match..!']);
			    }
			}
			else{
				return response()->json(['status'=>404,'message'=>'User did not match..!']);
			}
		}
	}

}