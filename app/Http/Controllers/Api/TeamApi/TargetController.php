<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use App\Models\Trackorder;
use App\Customer;
use App\Models\tax;
use App\Models\deliveryCharges;
use App\Models\addMoreAddress;
use App\Models\Salesman;
use App\Models\Product;
use App\Models\Assignments;
use App\Models\OtherUsers;
use DB;

class TargetController extends Controller
{
	public function tailorsDailyTarget(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
        	$gettailorsid = Assignments::join('other_users', 'assignments.tailor_id', '=', 'other_users.id')
        	    ->whereDate('assignments.tailor_assigned_at', \Carbon\Carbon::today())
        	    ->groupBy('assignments.tailor_id')
        	    ->get();
        	if($gettailorsid->count() >0)
        	{
        		$array = [];
        		foreach($gettailorsid as $key) {
        			$value['tailor_id'] = $key['tailor_id'];
        			$value['tailor_name'] = $key['name'];
        			$gettodaysOrders = Assignments::where('tailor_id',$value['tailor_id'])
        			    ->whereDate('tailor_assigned_at', \Carbon\Carbon::today())
        			    ->get();
        			if($gettodaysOrders->count() >0)
        			{
        				$total_items = 0;
        				foreach($gettodaysOrders as $order) {
        					$order_unique_id = $order['order_unique_id'];
        					$getProductsQuantity = Order::where('order_unique_id',$order_unique_id)
        					    ->get();
        					$quantity = 0;
                            foreach($getProductsQuantity as $productquantity) {
                                $qua = $productquantity['quantity'];
                                $quantity += $qua;
                            }
                            $total_items += $quantity;
        				}
        				$getCompletedOrders = Assignments::where('tailor_id',$value['tailor_id'])
        			    ->whereDate('tailor_assigned_at', \Carbon\Carbon::today())
        			    ->where('tailor_status','2')
        			    ->get();
        			    $completed_items = 0;
        			    foreach ($getCompletedOrders as $completedorder) {
        			    	$completeorderId = $completedorder['order_unique_id'];
        			    	$completedorderQua = Order::where('order_unique_id',$completeorderId)
        					    ->get();
        					$completequantity = 0;
        					foreach ($completedorderQua as $completequa) {
                                $comqua = $completequa['quantity'];
                                $completequantity += $comqua;
                            }
                            $completed_items += $completequantity;
        			    }
        			    $getReassignedOrders = Assignments::where('tailor_id',$value['tailor_id'])
        			    ->whereDate('tailor_assigned_at', \Carbon\Carbon::today())
        			    ->where('tailor_status','3')
        			    ->get();
        			    $re_assigned_items = 0;
        			    foreach ($getReassignedOrders as $reassigndorder) {
        			    	$reassignorderId = $reassigndorder['order_unique_id'];
        			    	$reassignorderQua = Order::where('order_unique_id',$reassignorderId)
        					    ->get();
        					$reassignquantity = 0;
        					foreach ($reassignorderQua as $reassignqua) {
                                $requa = $reassignqua['quantity'];
                                $reassignquantity += $requa;
                            }
                            $re_assigned_items += $reassignquantity;
        			    }

        			    $data['total_items'] = $total_items;
        			    $data['completed_items'] = $completed_items;
        			    $data['pending_items'] = $total_items - $completed_items;
        			    $data['re_assigned_items'] = $re_assigned_items;
        			}
        		    else{
        		    	return response()->json(['status'=>404, 'message' => 'Did not find order..!']);
        		    }
        		    $value['total_items'] = $data['total_items'];
        		    $value['completed_items'] = $data['completed_items'];
        		    $value['pending_items'] = $data['pending_items'];
        		    $value['re_assigned_items'] = $data['re_assigned_items'];
        			$array[] = $value;
        		}
        		return response()->json(['result'=>$array, 'status'=>200, 'message' => 'Tailors Target..!']);
        	}
        	else{
        		return response()->json(['status'=>404, 'message' => 'Did not find today target..!']);
        	}
        }
    }

    public function tailorMonthlyTarget(Request $request){
        $validator = Validator::make($request->all(), [  
            'tailor_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
        	$GetMonthlyTarget = OtherUsers::where('id',$request['tailor_id'])->where('type',2)
        	    ->first();
        	$data['tailor_name'] = $GetMonthlyTarget['name'];
        	$data['monthly_target'] = $GetMonthlyTarget['monthly_target'];
        	$data['month_year'] = date('M-Y');
        	$getCompletedOrders = Assignments::where('tailor_id',$request['tailor_id'])
                        	    ->whereMonth('tailor_completion_date', \Carbon\Carbon::now()->month)
                        		->where('tailor_status','2')
                        	    ->get();
        	if($getCompletedOrders->count() >0)
        	{
        		$completed_items = 0;
                $reassigned_items = 0;
                foreach ($getCompletedOrders as $completedorder) {
                    //quantity calculation for reassigned orders.
                    if($completedorder->old_tailor_id){
                        $reassignedOrderQua = Order::where('order_unique_id',$completedorder->order_unique_id)
                                            ->get();
                        $reassignedquantity = 0;
                        foreach ($reassignedOrderQua as $reassignedqua) {
                            $reassQua = $reassignedqua['quantity'];
                            $reassignedquantity += $reassQua;
                        }
                        $reassigned_items += $reassignedquantity;                        

                    }
                    //For completed order on quantity basis.
        	        $completeorderId = $completedorder['order_unique_id'];
                    $completedorderQua = Order::where('order_unique_id',$completeorderId)
        		    ->get();
        		    $completequantity = 0;
        	        foreach ($completedorderQua as $completequa) {
                        $comqua = $completequa['quantity'];
                        $completequantity += $comqua;
                    }
                $completed_items += $completequantity;
        	    }
        	}
        	else{
        		$completed_items = 0;
                $reassigned_items = 0;
        	}
        	$data['completed_items'] = $completed_items;
            $data['reassigned_items'] = $reassigned_items;
        	return response()->json(['result'=>$data, 'status'=>200, 'message' => 'Tailor Monthly Performance..!']);
        }
    }

    public function managerDailyTarget(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
        	//$GetmytodayOrders = Assignments::whereDate('created_at', \Carbon\Carbon::today())
            $GetmytodayOrders = Assignments::where('status','2')->where('tailor_status','0')
        	    ->get();
            if($GetmytodayOrders->count() >0)
            {
            	$total_items = 0;
            	foreach ($GetmytodayOrders as $todayOrders) {
            		$todayOrderId = $todayOrders['order_unique_id'];
            		$getTodaysTotalItems = Order::where('order_unique_id',$todayOrderId)
        		        ->get();
        		    $totalquantity = 0;
        	        foreach ($getTodaysTotalItems as $todayquantity) {
                        $todaytotal = $todayquantity['quantity'];
                        $totalquantity += $todaytotal;
                    }
            		$total_items += $totalquantity;
            	}
            	//$GetUnassigned = Assignments::whereDate('created_at', \Carbon\Carbon::today())
                $GetAssigned = Assignments::where('status','2')
        	        ->where('tailor_status','1')
        	        ->get();
        	    if($GetAssigned->count() >0)
        	    {
        	    	$total_assigned = 0;
            	    foreach ($GetAssigned as $assignedOrders) {
            		    $AssignedOrderId = $assignedOrders['order_unique_id'];
            		    $getAllAssigned = Order::where('order_unique_id',$AssignedOrderId)
        		            ->get();
        		        $assignedquantity = 0;
        	            foreach ($getAllAssigned as $assignquantity) {
                            $Assignedtotal = $assignquantity['quantity'];
                            $assignedquantity += $Assignedtotal;
                        }
            		$total_assigned += $assignedquantity;
            	    }
            	    $assigned_Items = $total_assigned;
        	    }
        	    else{
        	    	$assigned_Items = 0;
        	    }
        	    //$GetDefective = Assignments::whereDate('created_at', \Carbon\Carbon::today())
                $GetDefective = Assignments::where('status','2')
        	        ->where('order_status','0')
        	        ->get();
        	    if($GetDefective->count() >0)
        	    {
        	    	$total_Defective = 0;
            	    foreach ($GetDefective as $DefectiveOrders) {
            		    $DefectiveOrderId = $DefectiveOrders['order_unique_id'];
            		    $getTodaysDefective = Order::where('order_unique_id',$DefectiveOrderId)
        		            ->get();
        		        $Defectivequantity = 0;
        	            foreach ($getTodaysDefective as $Defectivequa) {
                            $Defectivetotal = $Defectivequa['quantity'];
                            $Defectivequantity += $Defectivetotal;
                        }
            		$total_Defective += $Defectivequantity;
            	    }
            	    $Defective_Items = $total_Defective;
        	    }
        	    else{
        	    	$Defective_Items = 0;
        	    }
        	    $GetCompletedItems = Assignments::whereDate('qc_passed_at', \Carbon\Carbon::today())
        	        ->where('status','2')
        	        ->where('qc_status','2')
        	        ->where('order_status','1')
        	        ->get();
        	    if($GetCompletedItems->count() >0)
        	    {
        	    	$total_Completed = 0;
            	    foreach ($GetCompletedItems as $todayCompletedOrders) {
            		    $todayCompletedId = $todayCompletedOrders['order_unique_id'];
            		    $getTodaysCompleted = Order::where('order_unique_id',$todayCompletedId)
        		            ->get();
        		        $Completedquantity = 0;
        	            foreach ($getTodaysCompleted as $Completedqua) {
                            $Completedtotal = $Completedqua['quantity'];
                            $Completedquantity += $Completedtotal;
                        }
            		$total_Completed += $Completedquantity;
            	    }
            	    $Total_Completed_today = $total_Completed;
        	    }
        	    else{
        	    	$Total_Completed_today = 0;
        	    }
            	$data['total_items'] = $total_items + $Defective_Items + $Total_Completed_today + $assigned_Items;
            	$data['unassigned_items'] = $total_items;
            	$data['assigned_items'] = $assigned_Items;
            	$data['deffective_items'] = $Defective_Items;
            	$data['completed_items'] = $Total_Completed_today;
            	return response()->json(['result'=>$data, 'status'=>200, 'message' => 'Manager Daily Performance..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Today Orders not found..!']);
            }
        }
    }

    public function cutterDailyTarget(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
        	$GetmytodayOrders = Assignments::whereDate('created_at', \Carbon\Carbon::today())
        	    //->where('status','1')
        	    ->where('cutter_id',$request['user_id'])
        	    ->get();
            $GetDailyTarget = Salesman::where('id',$request['user_id'])->first();
            if($GetmytodayOrders->count() >0)
            {
            	$total_items = 0;
            	foreach ($GetmytodayOrders as $todayOrders) {
            		$todayOrderId = $todayOrders['order_unique_id'];
            		$getTodaysTotalItems = Order::where('order_unique_id',$todayOrderId)
        		        ->get();
        		    $totalquantity = 0;
        	        foreach ($getTodaysTotalItems as $todayquantity) {
                        $todaytotal = $todayquantity['quantity'];
                        $totalquantity += $todaytotal;
                    }
            		$total_items += $totalquantity;
            	}
            	$todaysCompletedOrders = Assignments::whereDate('created_at', \Carbon\Carbon::today())
        	    ->where('status','2')
        	    ->where('cutter_id',$request['user_id'])
        	    ->get(); 
        	    if($todaysCompletedOrders->count() >0)
        	    {
        	    	$QuantityTotalCompleted = 0;
        	    	foreach ($todaysCompletedOrders as $completedOrder) {
        	    		$completeOrderId = $completedOrder['order_unique_id'];
        	    		$getCompletedQuantity = Order::where('order_unique_id',$completeOrderId)
        		        ->get();
        		        $totalCompletedQua = 0;
        		        foreach ($getCompletedQuantity as $completedQuantity) {
        		        	$todayCompleted = $completedQuantity['quantity'];
        		        	$totalCompletedQua += $todayCompleted;
        		        }
        		        $QuantityTotalCompleted += $totalCompletedQua;
        	    	}
        	    }
        	    else{
        	    	$QuantityTotalCompleted = 0;
        	    }
            	$data['target'] = $GetDailyTarget->monthly_target;
                $data['assigned'] = $total_items;
            	$data['completed'] = $QuantityTotalCompleted;
            	$data['pending'] = $total_items - $QuantityTotalCompleted;
            	return response()->json(['result'=>$data, 'status'=>200, 'message' => 'Cutter Today Performance..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Today Orders not found..!']);
            }
        }
    }

    public function salesmanMonthlyTarget(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
        	$GetMonthlyTarget = Salesman::where('id',$request['user_id'])->first();
        	if(!empty($GetMonthlyTarget)){
        		$data['monthly_target'] = $GetMonthlyTarget['monthly_target'];
        		// $GetAchievedTarget = Assignments::where('user_id',$request['user_id'])
        		//     ->whereMonth('created_at', \Carbon\Carbon::now()->month)
        		//     ->get();
        		// if($GetAchievedTarget->count() >0)
        		// {
        		// 	$achievedTarget = 0;
        		// 	foreach ($GetAchievedTarget as $achieved) {
        		// 		$achievedId = $achieved['order_unique_id'];
        		// 		$getAchievedQuantity = Order::where('order_unique_id',$achievedId)
        		//         ->get();
        		//         $totalAchievedQuantity = 0;
        		//         foreach ($getAchievedQuantity as $achievQuantity) {
        		//         	$todayAchieved = $achievQuantity['quantity'];
        		//         	$totalAchievedQuantity += $todayAchieved;
        		//         }
        		// 	    $achievedTarget += $totalAchievedQuantity;
        		// 	}
        		// }
        		// else{
        		// 	$achievedTarget = 0;
        		// }
        		$GetTotalOrdersOfMonth = Order::whereMonth('created_at', \Carbon\Carbon::now()->month)
                ->where('order_by', $request->user_id)
                ->where('status','!=','0')
                ->get();
                $achievedTarget = 0;
                foreach ($GetTotalOrdersOfMonth as $value) {
                    $achievedTarget += $value->total_price;
                }
        		$TotalOrdersOfMonth = $GetTotalOrdersOfMonth->unique('order_unique_id')->count();
        		$data['achieved_target'] = $achievedTarget;

                if($data['monthly_target'] == $achievedTarget || $achievedTarget > $data['monthly_target']){
        		    $data['pending'] = 0;
                }else{
                    $data['pending'] = $data['monthly_target'] - $achievedTarget;
                }

        		$data['no_of_orders'] = $TotalOrdersOfMonth;
        		return response()->json(['result'=>$data, 'status'=>200, 'message' => 'Salesman Monthly Performance..!']);
        	}
        	else{
        		return response()->json(['status'=>404, 'message' => 'Target not found..!']);
        	}
        }
    }

}