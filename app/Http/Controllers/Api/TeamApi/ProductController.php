<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use App\Customer;
use App\Models\Product;


class ProductController extends Controller
{
	public function viewSketch(Request $request){
	    $validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
	        'product_id' => 'required'		
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$getsketch = Product::select('product_image')
			    ->where('id',$request['product_id'])
			    ->first();
			if(!empty($getsketch)){
				$result['product_image'] = url('public/uploads/catalogues').'/'.$getsketch['product_image'];
			return response()->json(['result'=>$result, 'status'=>200,'message'=>'Product sketch.']);
			}
			else{
				return response()->json(['status'=>404,'message'=>'Sketch not found..!']);
			}
		}
	}
	public function getProductslist(){
		$products = Product::where('product_length', '>', '0')->get();
		if($products->count() >0)
		{
			$productsArray = [];
			foreach ($products as $pro) {
			    $array['id'] = $pro['id'];
			    $array['product_name'] = $pro['product_name'];
			    $array['product_code'] = $pro['product_code'];
			    $array['product_type'] = $pro['product_type'];
			    $array['product_details'] = $pro['product_details'];
			    $array['product_description'] = $pro['product_description'];
			    $array['product_length'] = $pro['product_length'];
			    $array['product_quantity'] = $pro['product_quantity'];
			    $array['product_price'] = $pro['product_price'];
			    $array['product_image'] = url('public/uploads/catalogues').'/'.$pro['product_image'];
			    $productsArray[] = $array;
			}
			return response()->json(['result'=>$productsArray, 'status'=>200, 'message' => 'Products list..!']);
		}
		else{
			return response()->json(['status'=>404, 'message' => 'Products not found..!']);
		}
	}

	public function getBusinessProductslist(){
		$products = Product::where('product_length', '>', '0')->get();
		if($products->count() >0)
		{
			$productsArray = [];
			foreach ($products as $pro) {
			    $array['id'] = $pro['id'];
			    $array['product_name'] = $pro['product_name'];
			    $array['product_code'] = $pro['product_code'];
			    $array['product_type'] = $pro['product_type'];
			    $array['product_details'] = $pro['product_details'];
			    $array['product_description'] = $pro['product_description'];
			    $array['product_length'] = $pro['product_length'];
			    $array['product_quantity'] = $pro['product_quantity'];
			    $array['product_price'] = $pro['product_price'];
			    $array['product_image'] = url('public/uploads/catalogues').'/'.$pro['product_image'];
			    $productsArray[] = $array;
			}
			return response()->json(['result'=>$productsArray, 'status'=>200, 'message' => 'Products list..!']);
		}
		else{
			return response()->json(['status'=>404, 'message' => 'Products not found..!']);
		}
	}
}