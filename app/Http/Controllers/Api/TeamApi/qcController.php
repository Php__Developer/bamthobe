<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Salesman;

class qcController extends Controller
{
	public function getQC(){
		$qcs = Salesman::where('type','5')->where('status','1')->get();
		if($qcs->count() >0)
		{
			$qcArray = [];
            foreach($qcs as $qc){
            	$val['id'] = $qc['id'];
                $val['qc_image'] = url('public/uploads/quality').'/'.$qc['image_name'];
                $val['qc_name'] = $qc['title'];
                $val['qc_email'] = $qc['email'];
                $val['description'] = $qc['description'];
                $val['status'] = $qc['status'];
                $val['phone'] = $qc['phone'];
                $val['branch'] = $qc['branch'];
                $val['monthly_target'] = $qc['monthly_target'];
                $qcArray[] = $val;
            }
            return response()->json(['result'=>$qcArray, 'status'=>200, 'message' => 'Qc..']);
		}
		else{
			return response()->json(['status'=>404, 'message' => 'Qc not found..!']);
		}
	}
}