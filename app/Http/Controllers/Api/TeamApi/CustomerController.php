<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\CustomerAppointment;
use App\Models\Order;
use App\Models\Salesman;
use App\Customer;

class CustomerController extends Controller
{
	public function createcustomer(Request $request)
    {
		$saveArray = $request->all();
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'name' => 'required', 
			//'email' => 'required',
			'account_type' => 'required',
			'phone'=> 'required'	
		]);
		if ($validator->fails()) {			
			return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
            $user = Salesman::select('title')
            ->where('id','=',$request['user_id'])->first();
            $user_name = $user['title'];

			$Customer = Customer::where('phone',$request['phone'])->first();
            if ($Customer) {
            	$cust_id = $Customer->customer_unique_id;
            	$cust_name = $Customer->name;
            	return response()->json(['message'=>'Phone Number Is Used For Customer ID '.$cust_id.' & Name '. $cust_name,'status'=>406]);
            }

            $customer_email = Customer::where('email',$request['email'])->first();
            if($customer_email && $request->email != null){
            	return response()->json(['message'=>'Email Already Used For An Customer','status'=>406]);
            }
            
            else{
            	$data=array();
            	$lastcustomer = Customer::select('id')
            	->orderBy('id', 'desc')
		  	    ->first();
		  	    $lastid = $lastcustomer['id'];
		  	    $newid = $lastid+1;
		  	    $countid = strlen($newid);
		  	    if($countid=='1')
		  	    {
		  	    	$madeid = '000'.$newid;
		  	    }
		  	    else if($countid=='2')
		  	    {
		  	    	$madeid = '00'.$newid;
		  	    }
		  	    else if($countid=='3')
		  	    {
		  	    	$madeid = '0'.$newid;
		  	    }
		  	    else{
		  	    	$madeid = $newid;
		  	    }
		  	    $idcode = date('y-m').'-'.$madeid;
            	$data['customer_unique_id'] = $idcode;
		        $data['name']=$request->name;
		        $data['email']=$request->email;
		        $data['address']=$request->address;
		        $data['phone']=$request->phone;
		        $data['lat']=$request->lat;
		        $data['longs']=$request->longs;
		        $data['created_by']=$user_name;
		        $data['account_type']=$request->account_type;
		        $newcustomer = Customer::insertGetId($data); 
		        return response()->json(['result'=>$newcustomer, 'uniqueId'=>$data['customer_unique_id'],'status'=>200,'message'=>'Customer created successfully..!']);
            }
		}
	}

	public function editcustomer(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'customer_unique_id' => 'required'	
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
             $Customer = Customer::where('customer_unique_id',$request['customer_unique_id'])
			->first();
			if($Customer->count() >0)
			{
				return response()->json(['result'=>$Customer, 'status'=>200,'message'=>'Edit Customer..']);
			}
		}
    }

	public function updatecustomer(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'customer_unique_id' => 'required',
			'name' => 'required', 
			'email' => 'required',
			'account_type' => 'required',	
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$customer_unique_id=$request->customer_unique_id;
			$name = $request->name;
            $email = $request->email;
            $account_type = $request->account_type;
            $customer = Customer::where('customer_unique_id',$customer_unique_id)->first();
            $customer->name = $name;
            $customer->email = $email;
            $customer->account_type = $account_type;
            $customer->update($request->all());
			return response()->json(['status'=>200,'message'=>'Customer Profile Updated Successfully.']);
		}
    }
}