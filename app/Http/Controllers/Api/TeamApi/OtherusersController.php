<?php

namespace App\Http\Controllers\Api\TeamApi;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OtherUsers;
use Illuminate\Support\Facades\Validator;

class OtherusersController extends Controller
{
    public function getTailorsList()
    {
    	$getTailorsLists = OtherUsers::where('type','2')->where('status','1')->get();
    	if($getTailorsLists->count() >0)
    	{
    		$tailorArray = [];
    		foreach ($getTailorsLists as $tailors) {
    			$array['id'] = $tailors['id'];
    			$array['name'] = $tailors['name'];
    			$array['email'] = $tailors['email'];
    			$array['phone'] = $tailors['phone'];
    			$array['status'] = $tailors['status'];
    			$array['image_name'] = url('public/uploads/tailor').'/'.$tailors['image_name'];
    			$tailorArray[]=$array;
    		}
    		return response()->json(['result'=>$tailorArray, 'status'=>200, 'message' => 'Tailers..!']);
    	}
    	else{
    		return response()->json(['status'=>404, 'message' => 'Tailer not found..!']);
    	}
	}
}