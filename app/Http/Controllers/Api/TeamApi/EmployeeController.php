<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Models\Salesman;
use App\Models\Salary;
use App\Models\OtherUsers;
use App\Models\Teamtype;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class EmployeeController extends Controller
{
	public function getEmployeeList(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'employee_type' => 'required'         
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
        	if($request['employee_type']=='1')
        	{
        		$getemployees = Salesman::where('type','4')->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['title'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
        			    $value['employee_image'] = url('public/uploads/cutter').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }
        	}
        	else if($request['employee_type']=='2')
        	{
        		$getemployees = OtherUsers::where('type','2')->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['name'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
        			    $value['employee_image'] = url('public/uploads/tailor').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }
        	}
        	else if($request['employee_type']=='3')
        	{
        		$getemployees = Salesman::where('type','5')->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['title'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
        			    $value['employee_image'] = url('public/uploads/quality').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }
        	}
        	else if($request['employee_type']=='4')
        	{
        		$getemployees = OtherUsers::where('type','1')->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['name'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
        			    $value['employee_image'] = url('public/uploads/other').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }
        	}
        }
    }

    public function editCutter(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'employee_id' => 'required',        
            'employee_name' => 'required',        
            'employee_phone' => 'required',        
            'employee_status' => 'required',
            'monthly_target'  => 'required',
            'branch'  => 'required'
            //'employee_image' => 'required'        
        ]);
        if ($validator->fails()) {          
                $getemployees = Salesman::where('type','4')
                    ->where('id',$request['employee_id'])->with('teamtype')
                    ->get();

        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['title'];
                        $value['employee_role'] = $employee['teamtype']['typename'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
                        $value['target'] = $employee['monthly_target'];
                        $value['branch'] = $employee['branch'];
        			    $value['employee_image'] = url('public/uploads/cutter').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee details..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }     
        }else{
        	if($request['employee_image'])
        	{
        		$image = $request->file('employee_image');
                $data_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/cutter');
                $image->move($destinationPath, $data_image);

                $updateDetails = Salesman::where('type','4')->where('id',$request['employee_id'])
                ->update([
                        'title' => $request->employee_name,
                        'phone' => $request->employee_phone,
                        'status' => $request->employee_status,
                        'image_name' => $data_image,
                        'monthly_target' => $request->monthly_target,
                        'branch' => $request->branch
                ]);
        	}else{
            	$updateDetails = Salesman::where('type','4')->where('id',$request['employee_id'])
            	    ->update([
                            'title' => $request->employee_name,
                            'phone' => $request->employee_phone,
                            'status' => $request->employee_status,
                            'monthly_target' => $request->monthly_target,
                            'branch' => $request->branch
                            //'image_name' => $data_image
            	    ]);
            }
        	if(!empty($updateDetails))
        	{
        		return response()->json(['status'=>200, 'message' => 'Details updated successfully..!']);
        	}
        	else{
        		return response()->json(['status'=>404, 'message' => 'Details not updated, Try again..!']);
        	}
        }
    }

    public function editTailor(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'employee_id' => 'required',        
            'employee_name' => 'required',        
            'employee_phone' => 'required',        
            'employee_status' => 'required',
            'monthly_target'  => 'required',
            'branch'  => 'required'       
            //'employee_image' => 'required'        
        ]);
        if ($validator->fails()) {     
                $getemployees = OtherUsers::where('type','2')
                    ->where('id',$request['employee_id'])->with('teamtype')
                    ->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['name'];
                        $value['employee_role'] = $employee['teamtype']['typename'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
                        $value['target'] = $employee['monthly_target'];
                        $value['branch'] = $employee['branch']?$employee['branch']:'';
        			    $value['employee_image'] = url('public/uploads/tailor').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee details..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }     
        }else{
        	if($request['employee_image'])
        	{
        		$image = $request->file('employee_image');
                $data_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/tailor');
                $image->move($destinationPath, $data_image);

                $updateDetails = OtherUsers::where('type','2')->where('id',$request['employee_id'])
                ->update([
                        'name' => $request->employee_name,
                        'phone' => $request->employee_phone,
                        'status' => $request->employee_status,
                        'image_name' => $data_image,
                        'monthly_target' => $request->monthly_target,
                        // 'branch' => $request->branch
                ]);
        	}else{
            	$updateDetails = OtherUsers::where('type','2')->where('id',$request['employee_id'])
            	    ->update([
                            'name' => $request->employee_name,
                            'phone' => $request->employee_phone,
                            'status' => $request->employee_status,
                            'monthly_target' => $request->monthly_target,
                            // 'branch' => $request->branch
                            //'image_name' => $data_image
            	    ]);
            }
        	if(!empty($updateDetails))
        	{
        		return response()->json(['status'=>200, 'message' => 'Details updated successfully..!']);
        	}
        	else{
        		return response()->json(['status'=>404, 'message' => 'Details not updated, Try again..!']);
        	}
        }
    }

    public function editQC(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'employee_id' => 'required',        
            'employee_name' => 'required',        
            'employee_phone' => 'required',        
            'employee_status' => 'required',
            'monthly_target'  => 'required',
            'branch'  => 'required'         
            //'employee_image' => 'required'        
        ]);
        if ($validator->fails()) {          
                $getemployees = Salesman::where('type','5')
                    ->where('id',$request['employee_id'])->with('teamtype')
                    ->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['title'];
                        $value['employee_role'] = $employee['teamtype']['typename'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
                        $value['target'] = $employee['monthly_target'];
                        $value['branch'] = $employee['branch']?$employee['branch']:'';
        			    $value['employee_image'] = url('public/uploads/quality').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee details..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }     
        }else{
        	if($request['employee_image'])
        	{
        		$image = $request->file('employee_image');
                $data_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/quality');
                $image->move($destinationPath, $data_image);

                $updateDetails = Salesman::where('type','5')->where('id',$request['employee_id'])
                ->update([
                        'title' => $request->employee_name,
                        'phone' => $request->employee_phone,
                        'status' => $request->employee_status,
                        'image_name' => $data_image,
                        'monthly_target' => $request->monthly_target,
                        // 'branch' => $request->branch
                ]);
        	}else{

        	$updateDetails = Salesman::where('type','5')->where('id',$request['employee_id'])
        	    ->update([
                        'title' => $request->employee_name,
                        'phone' => $request->employee_phone,
                        'status' => $request->employee_status,
                        'monthly_target' => $request->monthly_target,
                        // 'branch' => $request->branch
                        //'image_name' => $data_image
        	    ]);
            }
        	if(!empty($updateDetails))
        	{
        		return response()->json(['status'=>200, 'message' => 'Details updated successfully..!']);
        	}
        	else{
        		return response()->json(['status'=>404, 'message' => 'Details not updated, Try again..!']);
        	}
        }
    }

    public function editPacker(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'employee_id' => 'required',        
            'employee_name' => 'required',        
            'employee_phone' => 'required',        
            'employee_status' => 'required',
            'monthly_target'  => 'required',
            'branch'  => 'required'      
            //'employee_image' => 'required'        
        ]);
        if ($validator->fails()) {          
                $getemployees = OtherUsers::where('type','1')
                    ->where('id',$request['employee_id'])->with('teamtype')
                    ->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['name'];
                        $value['employee_role'] = $employee['teamtype']['typename'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
                        $value['target'] = $employee['monthly_target'];
                        $value['branch'] = $employee['branch']?$employee['branch']:'';
        			    $value['employee_image'] = url('public/uploads/other').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee details..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }     
        }
        else{
        	if($request['employee_image'])
        	{
        		$image = $request->file('employee_image');
                $data_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/other');
                $image->move($destinationPath, $data_image);

            $updateDetails = OtherUsers::where('type','1')->where('id',$request['employee_id'])
                ->update([
                        'name' => $request->employee_name,
                        'phone' => $request->employee_phone,
                        'status' => $request->employee_status,
                        'image_name' => $data_image,
                        'monthly_target' => $request->monthly_target,
                        // 'branch' => $request->branch
                ]);                
        	}else{

        	$updateDetails = OtherUsers::where('type','1')->where('id',$request['employee_id'])
        	    ->update([
                        'name' => $request->employee_name,
                        'phone' => $request->employee_phone,
                        'status' => $request->employee_status,
                        'monthly_target' => $request->monthly_target,
                        // 'branch' => $request->branch
                        //'image_name' => $data_image
        	    ]);
            }
        	if(!empty($updateDetails))
        	{
        		return response()->json(['status'=>200, 'message' => 'Details updated successfully..!']);
        	}
        	else{
        		return response()->json(['status'=>404, 'message' => 'Details not updated, Try again..!']);
        	}
        }
    }

    public function EmployeeRole(){
        $getRole = Teamtype::where('id','!=','3')->where('id','!=','6')->where('id','!=','7')->get();
        return response()->json(['result'=>$getRole, 'status'=>200, 'message' => 'Employee Roles..!']);
    }

    public function createEmployee(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',        
            'employee_name' => 'required',        
            'employee_email' => 'required',        
            'employee_phone' => 'required',        
            'employee_role' => 'required',         
            'employee_image' => 'required',
            'monthly_target'  => 'required',
            'branch'  => 'required'
        ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $lastEmp = Salary::orderBy('id', 'desc')->first();

        	if($request['employee_role']=='1'){
                if($request['employee_image']){
                    $image = $request->file('employee_image');
                    $data_image = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/other');
                    $image->move($destinationPath, $data_image);
        	    }
                $data['name'] = $request->employee_name;
                $data['email'] = $request->employee_email;
                $data['phone'] = $request->employee_phone;
                $data['type'] = $request->employee_role;
                $data['monthly_target'] = $request->monthly_target;
                $data['branch'] = $request->branch;
                $data['image_name'] = $data_image;
                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request->employee_name;
                $data['emp_id'] = $empID;

                $employee = OtherUsers::create($data);
                if(!empty($employee))
                {
                    Salary::create($salaryRequest);
            	    return response()->json(['status'=>200, 'message' => 'Employee added successfully..!']);
                }
                else{
            	    return response()->json(['status'=>401, 'message' => 'Sorry! Employee not added, Please try again..!']);
                }
        	}
        	else if($request['employee_role']=='2'){
        		if($request['employee_image'])
        	    {
        		    $image = $request->file('employee_image');
                    $data_image = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/tailor');
                    $image->move($destinationPath, $data_image);
        	    }
                $data['name'] = $request->employee_name;
                $data['email'] = $request->employee_email;
                $data['phone'] = $request->employee_phone;
                $data['type'] = $request->employee_role;
                $data['monthly_target'] = $request->monthly_target;
                $data['branch'] = $request->branch;
                $data['image_name'] = $data_image;
                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                } 
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request->employee_name;
                $data['emp_id'] = $empID;

                $employee = OtherUsers::create($data);
                if(!empty($employee))
                {
                    Salary::create($salaryRequest);                    
            	    return response()->json(['status'=>200, 'message' => 'Employee added successfully..!']);
                }
                else{
            	    return response()->json(['status'=>401, 'message' => 'Sorry! Employee not added, Please try again..!']);
                }
        	}
        	else if($request['employee_role']=='3'){
        		if($request['employee_image'])
        	    {
        		    $image = $request->file('employee_image');
                    $data_image = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/salesmans');
                    $image->move($destinationPath, $data_image);
        	    }
                $data['title'] = $request->employee_name;
                $data['email'] = $request->employee_email;
                $data['phone'] = $request->employee_phone;
                $data['type'] = $request->employee_role;
                $data['monthly_target'] = $request->monthly_target;
                $data['branch'] = $request->branch;
                $data['image_name'] = $data_image;
                $passwordcreate = mt_rand(1000, 9999);
                $data['password'] = \Hash::make($passwordcreate);
                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }                 
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request->employee_name;
                $data['emp_id'] = $empID;

                $employee = Salesman::create($data);
                if(!empty($employee))
                {
                    Salary::create($salaryRequest);

                	$user['email']= $data['email'];
                    $user['Dpassword'] = $passwordcreate;
                    Mail::to($data['email'])->send(new WelcomeMail($user));
            	    return response()->json(['status'=>200, 'message' => 'Employee added successfully..!']);
                }
                else{
            	    return response()->json(['status'=>401, 'message' => 'Sorry! Employee not added, Please try again..!']);
                }
        	}
        	else if($request['employee_role']=='4'){
        		if($request['employee_image'])
        	    {
        		    $image = $request->file('employee_image');
                    $data_image = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/cutter');
                    $image->move($destinationPath, $data_image);
        	    }
                $data['title'] = $request->employee_name;
                $data['email'] = $request->employee_email;
                $data['phone'] = $request->employee_phone;
                $data['type'] = $request->employee_role;
                $data['monthly_target'] = $request->monthly_target;
                $data['branch'] = $request->branch;
                $data['image_name'] = $data_image;
                $passwordcreate = mt_rand(1000, 9999);
                $data['password'] = \Hash::make($passwordcreate);
                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }  
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request->employee_name;
                $data['emp_id'] = $empID;

                $employee = Salesman::create($data);
                if(!empty($employee))
                {
                    Salary::create($salaryRequest);

                	$user['email']= $data['email'];
                    $user['Dpassword'] = $passwordcreate;
                    Mail::to($data['email'])->send(new WelcomeMail($user));
            	    return response()->json(['status'=>200, 'message' => 'Employee added successfully..!']);
                }
                else{
            	    return response()->json(['status'=>401, 'message' => 'Sorry! Employee not added, Please try again..!']);
                }
        	}
        	else if($request['employee_role']=='5'){
        		if($request['employee_image'])
        	    {
        		    $image = $request->file('employee_image');
                    $data_image = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/quality');
                    $image->move($destinationPath, $data_image);
        	    }
                $data['title'] = $request->employee_name;
                $data['email'] = $request->employee_email;
                $data['phone'] = $request->employee_phone;
                $data['type'] = $request->employee_role;
                $data['monthly_target'] = $request->monthly_target;
                $data['branch'] = $request->branch;
                $data['image_name'] = $data_image;
                $passwordcreate = mt_rand(1000, 9999);
                $data['password'] = \Hash::make($passwordcreate);
                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }  
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request->employee_name;
                $data['emp_id'] = $empID;

                $employee = Salesman::create($data);
                if(!empty($employee))
                {
                    Salary::create($salaryRequest);

                	$user['email']= $data['email'];
                    $user['Dpassword'] = $passwordcreate;
                    Mail::to($data['email'])->send(new WelcomeMail($user));
            	    return response()->json(['status'=>200, 'message' => 'Employee added successfully..!']);
                }
                else{
            	    return response()->json(['status'=>401, 'message' => 'Sorry! Employee not added, Please try again..!']);
                }
        	}
        	else if($request['employee_role']=='6'){
                if($request['employee_image'])
        	    {
        		    $image = $request->file('employee_image');
                    $data_image = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/tailor');
                    $image->move($destinationPath, $data_image);
        	    }
                $data['title'] = $request->employee_name;
                $data['email'] = $request->employee_email;
                $data['phone'] = $request->employee_phone;
                $data['type'] = $request->employee_role;
                $data['monthly_target'] = $request->monthly_target;
                $data['branch'] = $request->branch;
                $data['image_name'] = $data_image;
                $passwordcreate = mt_rand(1000, 9999);
                $data['password'] = \Hash::make($passwordcreate);
                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request->employee_name;
                $data['emp_id'] = $empID;                

                $employee = Salesman::create($data);
                if(!empty($employee))
                {
                    Salary::create($salaryRequest);

                	$user['email']= $data['email'];
                    $user['Dpassword'] = $passwordcreate;
                    Mail::to($data['email'])->send(new WelcomeMail($user));
            	    return response()->json(['status'=>200, 'message' => 'Employee added successfully..!']);
                }
                else{
            	    return response()->json(['status'=>401, 'message' => 'Sorry! Employee not added, Please try again..!']);
                }
        	}
        	else if($request['employee_role']=='7'){
        		if($request['employee_image'])
        	    {
        		    $image = $request->file('employee_image');
                    $data_image = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/salesmans');
                    $image->move($destinationPath, $data_image);
        	    }
                $data['title'] = $request->employee_name;
                $data['email'] = $request->employee_email;
                $data['phone'] = $request->employee_phone;
                $data['type'] = $request->employee_role;
                $data['monthly_target'] = $request->monthly_target;
                $data['branch'] = $request->branch;
                $data['image_name'] = $data_image;
                $passwordcreate = mt_rand(1000, 9999);
                $data['password'] = \Hash::make($passwordcreate);
                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }  
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request->employee_name;
                $data['emp_id'] = $empID;

                $employee = Salesman::create($data);
                if(!empty($employee))
                {
                    Salary::create($salaryRequest);

                	$user['email']= $data['email'];
                    $user['Dpassword'] = $passwordcreate;
                    Mail::to($data['email'])->send(new WelcomeMail($user));
            	    return response()->json(['status'=>200, 'message' => 'Employee added successfully..!']);
                }
                else{
            	    return response()->json(['status'=>401, 'message' => 'Sorry! Employee not added, Please try again..!']);
                }
        	}
        	
        }
    }

    public function searchEmployee(Request $request){
        $team = Teamtype::where('typename',$request['serach_key'])->first();
        if($team){
            $loginEmployees = Salesman::where('type', '!=', '3')
                                        ->where('type', '!=', '6')
                                        ->where('type', '!=', '7')
                                        ->where('type', 'like', $team->id)
                                        ->get();
            $withoutloginEmployees = OtherUsers::where('type', 'like', $team->id)->get();

        }else{
            $loginEmployees = Salesman::where('type', '!=', '3')
                                        ->where('type', '!=', '6')
                                        ->where('type', '!=', '7')
                                        ->where('title', 'like', "%{$request->serach_key}%")
                            		    ->orwhere('emp_id', 'like', "%{$request->serach_key}%")
                            		    ->orwhere('phone', 'like', "%{$request->serach_key}%")
                            		    ->get();
    		$withoutloginEmployees = OtherUsers::where('name', 'like', "%{$request->serach_key}%")
                                    		    ->orwhere('emp_id', 'like', "%{$request->serach_key}%")
                                    		    ->orwhere('phone', 'like', "%{$request->serach_key}%")
                                    		    ->get();
        }
		$getEmployees = $loginEmployees->merge($withoutloginEmployees);
		if($getEmployees->count() >0)
		{
			$employeeArray = [];
			foreach($getEmployees as $Employee)
			{
				$employeeType = $Employee['type'];
                //if($Employee->status == '1'){
    				if($employeeType=='1'){
                        $value['employee_id'] = $Employee['id'];
    				    $value['employee_name'] = $Employee['name'];
    				    $value['employee_email'] = $Employee['email'];
    				    $value['employee_role'] = "Packer";
    				    $value['employee_phone'] = $Employee['phone'];
    				}
    				else if($employeeType=='2'){
                        $value['employee_id'] = $Employee['id'];
    				    $value['employee_name'] = $Employee['name'];
    				    $value['employee_email'] = $Employee['email'];
    				    $value['employee_role'] = "Tailor";
    				    $value['employee_phone'] = $Employee['phone'];
    				}
    				else if($employeeType=='3'){
                        $value['employee_id'] = $Employee['id'];
    				    $value['employee_name'] = $Employee['title'];
    				    $value['employee_email'] = $Employee['email'];
    				    $value['employee_role'] = "Salesman";
    				    $value['employee_phone'] = $Employee['phone'];
    				}
    				else if($employeeType=='4'){
                        $value['employee_id'] = $Employee['id'];
    				    $value['employee_name'] = $Employee['title'];
    				    $value['employee_email'] = $Employee['email'];
    				    $value['employee_role'] = "Cutter";
    				    $value['employee_phone'] = $Employee['phone'];
    				}
    				else if($employeeType=='5'){
                        $value['employee_id'] = $Employee['id'];
    				    $value['employee_name'] = $Employee['title'];
    				    $value['employee_email'] = $Employee['email'];
    				    $value['employee_role'] = "Quality Checker";
    				    $value['employee_phone'] = $Employee['phone'];
    				}
    				else if($employeeType=='6'){
                        $value['employee_id'] = $Employee['id'];
    				    $value['employee_name'] = $Employee['title'];
    				    $value['employee_email'] = $Employee['email'];
    				    $value['employee_role'] = "Manager";
    				    $value['employee_phone'] = $Employee['phone'];
    				}
    				else if($employeeType=='7'){
                        $value['employee_id'] = $Employee['id'];
    				    $value['employee_name'] = $Employee['title'];
    				    $value['employee_email'] = $Employee['email'];
    				    $value['employee_role'] = "Branch Manager";
    				    $value['employee_phone'] = $Employee['phone'];
    				}
    				$employeeArray[] = $value;
                //}
			}
			return response()->json(['result' => $employeeArray, 'status'=>200, 'message' => 'Employee..!']);
		}
		else{
			return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
		}
    }

    public function editSalesmans(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'employee_id' => 'required',        
            'employee_name' => 'required',        
            'employee_phone' => 'required',        
            'employee_status' => 'required'        
            //'employee_image' => 'required'        
        ]);
        if ($validator->fails()) {          
                $getemployees = Salesman::where('id',$request['employee_id'])
                    ->get();
        		if($getemployees->count() >0)
        	    {
        		    $EmployeeArray = [];
        		    foreach ($getemployees as $employee) {
        			    $value['employee_id'] = $employee['id'];
        			    $value['employee_name'] = $employee['title'];
        			    $value['employee_email'] = $employee['email'];
        			    $value['employee_phone'] = $employee['phone'];
        			    $value['employee_status'] = $employee['status'];
        			    $value['employee_image'] = url('public/uploads/salesmans').'/'.$employee['image_name'];
        			    $EmployeeArray[] = $value;
        		    }
        		    return response()->json(['result'=>$EmployeeArray, 'status'=>200, 'message' => 'Employee details..!']);
        	    }
        	    else{
        		    return response()->json(['status'=>404, 'message' => 'Employee not found..!']);
        	    }     
        }else{
        	if($request['employee_image'])
        	{
        		$image = $request->file('employee_image');
                $data_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/salesmans');
                $image->move($destinationPath, $data_image);

                $updateDetails = Salesman::where('id',$request['employee_id'])
                ->update([
                        'title' => $request->employee_name,
                        'phone' => $request->employee_phone,
                        'status' => $request->employee_status,
                        'image_name' => $data_image
                ]);
        	}else{
            	$updateDetails = Salesman::where('id',$request['employee_id'])
            	    ->update([
                            'title' => $request->employee_name,
                            'phone' => $request->employee_phone,
                            'status' => $request->employee_status
                            //'image_name' => $data_image
            	    ]);
            }
        	if(!empty($updateDetails))
        	{
        		return response()->json(['status'=>200, 'message' => 'Details updated successfully..!']);
        	}
        	else{
        		return response()->json(['status'=>404, 'message' => 'Details not updated, Try again..!']);
        	}
        }
    }
}