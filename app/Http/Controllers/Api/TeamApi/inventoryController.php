<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Inventory;

class inventoryController extends Controller
{
	public function requestInventory(Request $request){
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'product_id' => 'required',	
			'comment' => 'required'	
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$data['user_id'] = $request->user_id;
			$data['product_id'] = $request->product_id;
			$data['comment'] = $request->comment;
			$createRequest = Inventory::create($data);
			if($createRequest->count() >0)
            {
            	return response()->json(['status'=>200, 'message' => 'Request Confirmed..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
            }
		}
	}
}