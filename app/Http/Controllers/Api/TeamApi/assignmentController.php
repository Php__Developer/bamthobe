<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Models\Order;
use App\Models\Business_order;
use App\Models\Product;
use App\Models\Assignments;
use App\Models\Salesman;
use App\Models\OtherUsers;
use App\Models\Trackorder;

class assignmentController extends Controller
{
	public function assignedUnassigned(Request $request){
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'assignment' => 'required'				
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$Orders = Order::where('assignment',$request['assignment'])
                            ->where('status','!=','0')
			->groupBy('order_unique_id')
			->get();
			if($Orders->count() >0)
			{
				$orderArray = [];
                foreach($Orders as $order){
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $val['customer_name'] = $order['customer_name'];
                    $val['customer_unique_id'] = $order['customer_unique_id'];
                    $stamp = strtotime($order['created_at']);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $val['total_amount'] = 'SR '.$order['total_amount'];
                    $val['amount_to_pay'] = 'SR '.$order['amount_to_pay'];
                    $val['price_remaining'] = 'SR '.$order['price_remaining'];
                    $val['assignment'] = $order['assignment'];
                    if($val['assignment']=='1')
                    {
                    	$getcutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                    	->where('assignments.order_unique_id',$val['order_unique_id'])
                    	->first();
                    	$val['cutter_id'] = $getcutter['id'];
                    	$val['cutter_name'] = $getcutter['title'];
                    }
                    $val['status'] = $order['status'];
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders']);
			}
			else{
				return response()->json(['status'=>404, 'message' => 'Order not found..!']);
			}
		}
	}

	public function createAssignment(Request $request){
	    $validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'order_unique_id' => 'required',
			'cutter_id' => 'required'				
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$data = [];
			$data['user_id'] = $request->user_id;
			$data['order_unique_id'] = $request->order_unique_id;
			$data['cutter_id'] = $request->cutter_id;
			$assignment = Assignments::create($data);
			if($assignment->count() >0)
            {
            	$assignmentStatus = Order::where('order_unique_id',$request['order_unique_id'])
            	                    ->update(['assignment' => '1']);
                $tracking = Trackorder::where('order_unique_id',$request['order_unique_id'])
                                    ->update(['with_cutter' => '1']);
            	return response()->json(['status'=>200, 'message' => 'Cutter assigned successfully..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
            }
		}  		
	}

	public function getQcAssignedOrders(Request $request){
	    $validator = Validator::make($request->all(), [ 
		    'user_id' => 'required'				
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
            $getassignedOrders = Assignments::join('orders','assignments.order_unique_id', '=', 'orders.order_unique_id')
                ->where('assignments.qc_id',$request['user_id'])
                ->where('assignments.qc_status','1')
                ->Orwhere('assignments.qc_status','3')
                ->where('assignments.qc_id',$request['user_id'])
                ->groupBy('orders.order_unique_id')
                ->get();
            if($getassignedOrders->count() >0)
            {
            	$orderArray = [];
                foreach($getassignedOrders as $order){
                	$val['order_unique_id'] = $order['order_unique_id'];
                	$stamp = strtotime($order['created_at']);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $getquantity = Order::where('order_unique_id',$val['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $value) {
                        $qua = $value['quantity'];
                        $quantity += $qua;

                        $val['customer_unique_id'] = $value->customer_unique_id;
                        $val['customer_name'] = $value->customer_name;
                        $getCustomer = Customer::where('customer_unique_id',$value->customer_unique_id)->first();
                        //dd($getCustomer);
                        $val['account_type'] = $getCustomer->account_type;
                    }
                    $val['quantity'] = $quantity.' Items';
                    $cutter_id = $order['cutter_id'];
                	$getcutter = Salesman::where('id', $cutter_id)->first();
                	$val['cutter_id'] = $getcutter['id'];
                    $val['cutter_name'] = $getcutter['title'];
                    $val['tailor_id'] = $order['tailor_id'];
                    $gettailor = OtherUsers::where('id',$val['tailor_id'])->first();
                    $val['tailor_name'] = $gettailor['name'];
                	$orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Your new Orders..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'No order found..!']);
            }
		}
	}

    public function assignTailorbymngr(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'order_unique_id' => 'required',
            'tailor_id' => 'required',
            'date_time' => 'required'         
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getAssigned = Assignments::where('order_unique_id',$request['order_unique_id'])
                ->where('status','2')->first();
            $tailor_id = $getAssigned['tailor_id'];
            $tailor_assigned_at = $getAssigned['tailor_assigned_at'];
            $tailor_status = $getAssigned['tailor_status'];
            if(empty($tailor_id) && empty($tailor_assigned_at) && $tailor_status=='0')
            {
                $assignment = Assignments::where('order_unique_id',$request['order_unique_id'])
                                    ->update([
                                        'tailor_id' => $request->tailor_id,
                                        'tailor_status' => '1',
                                        'tailor_assigned_at' => $request->date_time,
                                    ]);
                if(!empty($assignment))
                {
                    $tracking = Trackorder::where('order_unique_id',$request['order_unique_id'])
                                    ->update(['with_tailor' => '1']);
                    return response()->json(['status'=>200, 'message' => 'Tailor assigned successfully..!']);
                }
                else{
                    return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
                }
            }
            else{
                $assignment = Assignments::where('order_unique_id',$request['order_unique_id'])
                                    ->update([
                                        'tailor_id' => $request->tailor_id,
                                        'tailor_status' => '3',
                                        'tailor_assigned_at' => $request->date_time,
                                    ]);
                if(!empty($assignment))
                {
                    $tracking = Trackorder::where('order_unique_id',$request['order_unique_id'])
                                    ->update(['with_tailor' => '1']);
                    return response()->json(['status'=>200, 'message' => 'Tailor reassigned successfully..!']);
                }
                else{
                    return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
                }
            } 
        }       
    }

    public function assignQcbymngr(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'order_unique_id' => 'required',
            'qc_id' => 'required',
            'date_time' => 'required'         
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getAssigned = Assignments::where('order_unique_id',$request['order_unique_id'])
                ->where('status','2')->first();
            $qc_assigned_at = $getAssigned['qc_assigned_at'];
            $qc_status = $getAssigned['qc_status'];

            if(empty($qc_assigned_at) && $qc_status=='0')
            {
                $assignment = Assignments::where('order_unique_id',$request['order_unique_id'])
                                        ->update([
                                            'qc_id' => $request->qc_id,
                                            'qc_status' => '1',
                                            'tailor_status' => '2',
                                            'qc_assigned_at' => $request->date_time,
                                            'tailor_completion_date' => $request->date_time,
                                        ]);
                if(!empty($assignment))
                {
                    $tracking = Trackorder::where('order_unique_id',$request['order_unique_id'])
                                        ->update(['with_qa' => '1']);
                    return response()->json(['status'=>200, 'message' => 'Qc assigned successfully..!']);
                }
                else{
                    return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
                }
            }else{
                $assignment = Assignments::where('order_unique_id',$request['order_unique_id'])
                                        ->update([
                                            'qc_id' => $request->qc_id,
                                            'qc_status' => '3',
                                            'tailor_status' => '2',
                                            'qc_assigned_at' => $request->date_time,
                                            'tailor_completion_date' => $request->date_time,
                                        ]);
                if(!empty($assignment))
                {
                    $tracking = Trackorder::where('order_unique_id',$request['order_unique_id'])
                                        ->update(['with_qa' => '1']);
                    return response()->json(['status'=>200, 'message' => 'Qc reassigned successfully..!']);
                }
                else{
                    return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
                }                
            }
        }       
    }

    public function businessassignedUnassigned(Request $request){
        $validator = Validator::make($request->all(), [ 
            'customer_id' => 'required', 
            'type' => 'required',             
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            if ($request->type == '1') {
                // dd("Assign");
                $Orders = Business_order::where('customer_id',$request['customer_id'])
                ->where('status','=','1')
                // ->groupBy('order_unique_id')
                ->get();
            }elseif ($request->type == '0') {
                // dd("UnAssign");
                $Orders = Business_order::where('customer_id',$request['customer_id'])
                ->where('status','=','0')
                // ->groupBy('order_unique_id')
                ->get();
            }elseif ($request->type == '2') {
                // dd("Confirm");
                $Orders = Business_order::where('customer_id',$request['customer_id'])
                ->where('status','=','2')
                // ->groupBy('order_unique_id')
                ->get();
            }elseif ($request->type == '3') {
                // dd("Completed");
                $Orders = Business_order::where('customer_id',$request['customer_id'])
                ->where('status','=','3')
                // ->groupBy('order_unique_id')
                ->get();
            }
            if($Orders->count() >0)
            {
                $orderArray = [];
                foreach($Orders as $order){
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $val['customer_name'] = $order['customer_name'];
                    $val['customer_id'] = $order['customer_id'];
                    $val['customer_unique_id'] = $order['customer_unique_id'];
                    $stamp = strtotime($order['created_at']);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $val['total_amount'] = 'SR '.$order['total_amount'];
                    $val['amount_to_pay'] = 'SR '.$order['amount_to_pay'];
                    $val['price_remaining'] = 'SR '.$order['price_remaining'];
                    $val['assignment'] = $order['assignment'];
                    // if($val['assignment']=='1')
                    // {
                    //     $getcutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                    //     ->where('assignments.order_unique_id',$val['order_unique_id'])
                    //     ->first();
                    //     $val['cutter_id'] = $getcutter['id'];
                    //     $val['cutter_name'] = $getcutter['title'];
                    // }
                    $val['status'] = $order['status'];
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found..!']);
            }
        }
    }

    public function businessfmorders(Request $request){
        $validator = Validator::make($request->all(), [ 
            // 'customer_id' => 'required'          
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $orders = Business_order::where('status','=','1')
            ->orWhere('status','=','2')
            ->get();
            // print_r(json_encode($orders));die();
            if($orders->count() >0){
                $orderArray = [];
                foreach($orders as $order){
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $val['customer_name'] = $order['customer_name'];
                    $val['customer_id'] = $order['customer_id'];
                    $val['customer_unique_id'] = $order['customer_unique_id'];
                    $stamp = strtotime($order['created_at']);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $val['total_amount'] = 'SR '.$order['total_amount'];
                    $val['amount_to_pay'] = 'SR '.$order['amount_to_pay'];
                    $val['price_remaining'] = 'SR '.$order['price_remaining'];
                    $val['assignment'] = $order['assignment'];
                    $val['status'] = $order['status'];
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found..!']);
            }
        }
    }

    public function businesscompletedorders(Request $request){
        $validator = Validator::make($request->all(), [ 
            'customer_id' => 'required'          
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $orders = Business_order::where('customer_id',$request['customer_id'])
            ->where('status','=','3')
            ->get();
            // print_r(json_encode($orders));die();
            if($orders->count() >0){
                $orderArray = [];
                foreach($orders as $order){
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $val['customer_name'] = $order['customer_name'];
                    $val['customer_id'] = $order['customer_id'];
                    $val['customer_unique_id'] = $order['customer_unique_id'];
                    $stamp = strtotime($order['created_at']);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $val['total_amount'] = 'SR '.$order['total_amount'];
                    $val['amount_to_pay'] = 'SR '.$order['amount_to_pay'];
                    $val['price_remaining'] = 'SR '.$order['price_remaining'];
                    $val['assignment'] = $order['assignment'];
                    $val['status'] = $order['status'];
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found..!']);
            }
        }
    }

    public function businesscreateAssignment(Request $request){
        $validator = Validator::make($request->all(), [ 
            'customer_id' => 'required', 
            'order_unique_id' => 'required',
            // 'cutter_id' => 'required'               
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $data = [];
            $data['user_id'] = $request->customer_id;
            $data['order_unique_id'] = $request->order_unique_id;
            $data['cutter_id'] = '0';
            $assignment = Assignments::create($data);
            if($assignment->count() >0)
            {
                $assignmentStatus = Business_order::where('order_unique_id',$request['order_unique_id'])
                                    ->update(['assignment' => '2']);
                $tracking = Trackorder::where('order_unique_id',$request['order_unique_id'])
                                    ->update(['with_cutter' => '1']);
                return response()->json(['status'=>200, 'message' => 'Cutter assigned successfully..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
            }
        }       
    }
}