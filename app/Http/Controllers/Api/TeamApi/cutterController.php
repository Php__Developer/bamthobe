<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Salesman;

class cutterController extends Controller
{
	public function getCutters(){
		$cutterss = Salesman::where('type','4')->where('status','1')->get();
		if($cutterss->count() >0)
		{
			$cutterArray = [];
            foreach($cutterss as $cutter){
            	$val['id'] = $cutter['id'];
                $val['cutter_image'] = url('public/uploads/cutter').'/'.$cutter['image_name'];
                $val['cutter_name'] = $cutter['title'];
                $val['cutter_email'] = $cutter['email'];
                $val['description'] = $cutter['description'];
                $val['status'] = $cutter['status'];
                $val['phone'] = $cutter['phone'];
                $val['branch'] = $cutter['branch'];
                $val['monthly_target'] = $cutter['monthly_target'];
                $cutterArray[] = $val;
            }
            return response()->json(['result'=>$cutterArray, 'status'=>200, 'message' => 'Cutters']);
		}
		else{
			return response()->json(['status'=>404, 'message' => 'Cutters not found..!']);
		}
	}
}