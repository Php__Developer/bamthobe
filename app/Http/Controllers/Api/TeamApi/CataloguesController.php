<?php

namespace App\Http\Controllers\Api\TeamApi;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomerAppointment;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Inventory_sample;

class CataloguesController extends Controller
{

    public function getinventry(Request $request){
        $validator = Validator::make($request->all(), [
            'type' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors(),'status'=>400]);
        }
        else{
        if($request->type == '1'){
            $type = ['product_type' => '1'];
            $ptype = 'Summer';
        }elseif ($request->type == '2') {
            $type = ['product_type' => '2'];
            $ptype = 'Winter';
        }elseif ($request->type == '3') {
            $type = ['product_type' => '3'];
            $ptype = 'Other';
        }
        $products = Product::where($type)->orderBy('created_at','Desc')->get();
        $inventArray = array();
        foreach ($products as $product) {
            $data['id'] = $product['id'];
            $data['product_code'] = $product['product_code'];
            $data['image'] = url('public/uploads/catalogues').'/'.$product['product_image'];
            $data['product_name'] = $product['product_name'];
            $data['product_type'] = $ptype;
            $data['product_color'] = $product['product_color'];
            $data['product_length'] = $product['product_length'];
            $inventArray[] = $data;
        }
        return response()->json([
            'status'=>200,
            'message'=>'Inventory data',
            'total'=>$products->count(),
            'result'=>$inventArray,
        ]);
        }
    }

    public function postinventry(Request $request){
        $validator = Validator::make($request->all(), [
            'request_no' => 'required',
            'product_id' => 'required',
            'place' => 'required',
            'salesman_id' => 'required',
            'type' => 'required',
            'name' => 'required',
            'color' => 'required',
            'length' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors(),'status'=>400]);
        }
        else{
            $data['request'] = $request->request_no;
            $data['product_id'] = $request->product_id;
            $data['place'] = $request->place;
            $data['salesman_id'] = $request->salesman_id;
            $data['type'] = $request->type;
            $data['name'] = $request->name;
            $data['color'] = $request->color;
            $data['length'] = $request->length;
            $order = Inventory_sample::create($data);
            return response()->json([
                'status'=>200,
                'message'=>'Sample ready',
                'result'=>$order,
            ]);
        }
    }

    public function createdInventories(Request $request)
    {
        $inventories =  Inventory_sample::orderBy('created_at','DESC')->get();
        $inventory = [];
        foreach ($inventories as $value) {
            $pcode = Product::where('id',$value["product_id"])->first();
            $pro["inventory_id"] = $value["id"];
            $pro["request"] = $value["request"];
            $pro["name"] = $value["name"];
            $pro["color"] = $value["color"];
            $pro["length"] = $value["length"];
            $pro["status"] = $value["status"];
            $pro["place"] = $value["place"];
            $pro["branch"] = "1";
            $pro["product_code"] = $pcode['product_code']?$pcode['product_code']:'';
            $pro["product_type"] = $pcode['product_type']?$pcode['product_type']:'';
            $pro["product_id"] = $value["product_id"];
            // $pro["product_image"] = url('public/uploads/catalogues').'/'.$value["product_image"];
            $inventory[] = $pro;
        }
        return response()->json(['result'=>$inventory, 'status'=>200,'message'=>'All Inventories']);
    }

    public function inventoriesActions(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inventory_id' => 'required',
            'act' => 'required', #A & R = Accept Reject
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors(),'status'=>400]);
        }
        else{
            $checkSample = Inventory_sample::where('id', $request['inventory_id'])->first();
            if ($request->act == 'A' && $checkSample['status'] == 'requested') {
                $resp= Inventory_sample::where('id', $request['inventory_id'])
                ->update(['status' => 'accepted']);
                if ($resp) {
                    return response()->json(['status'=>200,'message'=>'sample accepted successfully']); 
                }else{
                    return response()->json(['status'=>200,'message'=>'something went wrong']);
                }
            }elseif ($request->act == 'R' && $checkSample['status'] == 'requested') {
                $comment = $request->comment;
                $resp= Inventory_sample::where('id', $request['inventory_id'])
                ->update(['status' => 'rejected'],['comment' => $comment]);
                if ($resp) {
                    return response()->json(['status'=>200,'message'=>'sample rejected successfully']);
                }else{
                    return response()->json(['status'=>200,'message'=>'something went wrong']);
                }
            }else{
                return response()->json(['status'=>200,'message'=>'already accepted or rejected']);
            }
        }
    }

    public function fetchInventory(Request $request){
        $validator = Validator::make($request->all(), [
            'inventory_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors(),'status'=>400]);
        }
        else{
            $inventories =  Inventory_sample::where('id',$request->inventory_id)->first();
            if ($inventories) {
                $productDetail = Product::where('id',$inventories->product_id)->first();
                $invenArray = array(
                    'product_id' => $inventories->product_id,
                    'product_name' => $productDetail['product_name']?$productDetail['product_name']:'',
                    'product_type' => $productDetail['product_type']?$productDetail['product_type']:'',
                    'product_code' => $productDetail['product_code']?$productDetail['product_code']:'',
                    'product_length' => $productDetail['product_length']?$productDetail['product_length']:'',
                    'product_price' => $productDetail['product_price']?$productDetail['product_price']:'',
                    'salesman_id' => $inventories->salesman_id,
                    'request' => $inventories->request,
                    'length' => $inventories->length,
                    'color' => $inventories->color,
                    'name' => $inventories->name,
                    'status' => $inventories->status,
                    'place' => $inventories->place,
                    'type' => $inventories->type,
                );
                return response()->json([
                    'status'=>200,
                    'message' => 'data fetched!',
                    'inventoryArr' => $invenArray
                ]);
            }else{
                return response()->json(['status'=>400,'message'=>'no data']);
            }
        }
    }

}
