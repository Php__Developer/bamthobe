<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use App\Models\Trackorder;
use App\Customer;
use App\Models\tax;
use App\Models\deliveryCharges;
use App\Models\addMoreAddress;
use App\Models\Salesman;
use App\Models\Product;
use App\Models\Assignments;
use App\Models\OtherUsers;
use App\Models\OrderitemComments;
use App\Models\Comment;
use App\Models\Loyalty;
use Carbon\Carbon;

class OrderController extends Controller
{
	public function checkCustomer(Request $request){
		$customer = Customer::where('customer_unique_id',$request['customer_unique_id'])
		            ->orwhere('phone',$request['customer_phone'])
		            ->first();
		if(!empty($customer)){
		        return response()->json(['result'=>$customer, 'status'=>200, 'message' => 'Customer is correct']);	
		}
		else{
			return response()->json(['status'=>404, 'message' => 'Customer not found..!']);
		}
	}
	public function createorder(Request $request){
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'customer_unique_id' => 'required',	
			'customer_name' => 'required',		
			'customer_phone' => 'required'		
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$lastorder = Order::select('id')
            ->orderBy('id', 'desc')
            ->first();
            $lastid = $lastorder['id'];
            $newid = $lastid+1;
            $countid = strlen($newid);
            if($countid=='1')
            {
                $madeid = '00'.$newid;
            }
            else if($countid=='2')
            {
                $madeid = '0'.$newid;
            }
            else{
                $madeid = $newid;
            }
            $idcode = date('md').$madeid;
            $data['order_unique_id'] = 'SO-'.$idcode;
            $data['order_by'] = $request->user_id;
            $data['customer_unique_id'] = $request->customer_unique_id;
            $getid = Customer::where('customer_unique_id',$request['customer_unique_id'])->first();
            $data['customer_id'] = $getid['id'];
            $data['customer_name'] = $request->customer_name;
            $order = Order::create($data);
            return response()->json(['result'=>$order, 'status'=>200, 'message' => 'New Order']);
		}
	}
	public function orderitem(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required', #salesman id here
			'customer_unique_id' => 'required',
			'product' => 'required'            		
		]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors(),'status'=>400]);
        }
		else{
            $ph = Customer::where('customer_unique_id',$request->customer_unique_id)->first();
            $phone =$ph->phone;

            if(isset($request->order_unique_id)){
                // Get order details when order update
                $getOrderDetails = Order::where('customer_unique_id',$request->customer_unique_id)
                ->where('order_unique_id',$request->order_unique_id)
                ->get();
                // $ph = Customer::where('customer_unique_id',$request->customer_unique_id)->first();
                // $phone =$ph->phone;
                if($getOrderDetails->isNotEmpty()){
                //get pre order quantity and update to product
                    $preOrderQuantity = 0;
                    foreach ($getOrderDetails as $value) {
                        if($value->behaviour == 'Internal'){
                            $getquantity = Product::select('product_length')
                            ->where('id',$value->product_id)->first();
                            $orderLenght = $value->product_length * $value->quantity;
                            $currentQuantity = $getquantity['product_length'] + $orderLenght;
                            $updateQuantity = Product::where('id',$value->product_id)
                            ->update(['product_length' => $currentQuantity]);
                        }
                        //Delete previous entery from order
                        $delOreder = Order::where('id',$value->id)->delete();
                    }
                    $preOrderDetails = $getOrderDetails->first();
                    //print_r($preOrderDetails['price_remaining']);die;
                    $image = $request->file('product_image');
                    $products = json_decode($request['product'], true);
                    if(!empty($products)){
                        // print_r($products);die();
                        $i=0;
                        foreach($products as $pro){
                            $customProductId = 'E-'.uniqid();
                            $detail = array();
                            $internalProductId = Order::max('id') + 1;
                            $detail['order_unique_id'] = $preOrderDetails->order_unique_id;
                            $detail['order_by'] = $preOrderDetails->order_by;
                            $detail['customer_id'] = $preOrderDetails->customer_id;
                            $detail['customer_unique_id'] = $preOrderDetails->customer_unique_id;
                            $detail['customer_name'] = $preOrderDetails->customer_name;
                            $detail['product_length'] = $pro['product_length'];
                            $detail['order_type'] = $pro['order_type'];
                            $detail['quantity'] = $pro['quantity'];
                            $detail['behaviour'] = $pro['behaviour'];
                            $detail['total_price'] = $pro['total_price'];
                            if($detail['behaviour']=='Internal'){
                                $detail['product_name'] = $pro['product_name'];
                                $detail['product_id'] = $internalProductId.'-'.$pro['id'];
                                $detail['product_code'] = $pro['product_code'];
                                $getquantity = Product::select('product_length')
                                ->where('id',$pro['id'])->first();
                                $orderLenght = $pro['product_length'] * $pro['quantity'];
                                if($getquantity->product_length >= $orderLenght){
                                    $currentQuantity = $getquantity->product_length - $orderLenght;
                                    $updateQuantity = Product::where('id',$pro['id'])
                                    ->update(['product_length' => $currentQuantity]);
                                }else{
                                    return response()->json(['status'=>204, 'message' => 'Sorry, Product quantity not available.']);
                                }
                            }else{
                                $detail['product_id'] = $customProductId;
                            }
                            $gettax = tax::first();
                            $priceTotal = $pro['total_price'];
                            $tax = $gettax['tax_rate'];
                            $detail['tax'] = ($priceTotal*$tax)/100;
                            $detail['delivery_charges'] = "0.00";

                            if(isset($pro['comments'])){
                            $detail['comments'] = $pro['comments'];

                            //Insert comment on the basis of product
                            $commentData['comment'] = $pro['comments'];
                            $commentData['user_id'] = $preOrderDetails->order_by;
                            if($detail['behaviour']=='Internal'){
                                $commentData['order_product_id'] = $internalProductId.'-'.$pro['id'];
                            }else{
                                $commentData['order_product_id'] = $customProductId;
                            }
                            $commentData['order_unique_id'] = $preOrderDetails->order_unique_id;
                            //dd($commentData);
                            $comment = OrderitemComments::create($commentData);

                            }else{
                                $detail['comments'] = NULL;
                            }

                            $detail['model'] = $pro['model'];
                            if(!empty($image[$i]))
                            {
                                $detail['sketch'] = $this->uploadimages($image[$i]); 
                            }
                            else
                            {
                                $detail['sketch'] = "no-image.png";
                            }
                            //dd($detail);
                            $createOrder = Order::create($detail);
                            $i++; 
                        }
                       
                        return response()->json(['result'=>$createOrder, 'status'=>200, 'message' => 'Order updated successfully..!']);
                    }
                    else{
                        return response()->json(['status'=>204, 'message' => 'Sorry, No product in order!Please try again..']);
                    }
                }else{
                    return response()->json(['status'=>404, 'message' => 'Sorry, Order not found..']);
                }


            }else{
                //Create new order
    			$getdetails = Customer::where('customer_unique_id',$request['customer_unique_id'])->first();

    			$lastOrder = Order::select('id')
                ->orderBy('id', 'desc')
                ->first();
                if($lastOrder){
                    $order_unique_id = date('y').sprintf("%06d", $lastOrder->id+1);
                }else{
                    $order_unique_id = date('y').sprintf("%06d", 1);
                }


    			$order_by = $request->user_id;
    			$customer_id = $getdetails['id'];
    			$customer_unique_id = $request->customer_unique_id;
    			$customer_name = $getdetails['name'];
                //$total_countedprice = $request->total_price;
                $image = $request->file('product_image');
    			$products = json_decode($request['product'], true);
                //print_r($products);die;
    			if(!empty($products)){
                    $orderdetails = [];
                    $i=0;
    			    foreach($products as $pro){
                        // print_r($pro);die();
                        $externalProductId = 'E-'.uniqid();
                        $detail = array();
                        $internalProductId = Order::max('id') + 1;

    				    $detail['order_unique_id'] = $order_unique_id;
    				    $detail['order_by'] = $order_by;
    				    $detail['customer_id'] = $customer_id;
    				    $detail['customer_unique_id'] = $customer_unique_id;
    				    $detail['customer_name'] = $customer_name;
    				    $detail['product_length'] = $pro['product_length'];
                        $detail['order_type'] = $pro['order_type'];
    				    $detail['quantity'] = $pro['quantity'];
                        $detail['behaviour'] = $pro['behaviour'];
    				    $detail['total_price'] = $pro['total_price'];
                        if($detail['behaviour']=='Internal'){
                            $detail['product_name'] = $pro['product_name'];
                            $detail['product_id'] = $internalProductId.'-'.$pro['id'];
                            // print_r($detail['product_id']);die();
                            $detail['product_code'] = $pro['product_code'];

                                $getquantity = Product::select('product_length')
                                    ->where('id',$pro['id'])->first();
                                    $orderLenght = $pro['product_length'] * $pro['quantity'];
                                    //dd($getquantity);
                                if($getquantity->product_length >= $orderLenght){

                                    $currentQuantity = $getquantity->product_length - $orderLenght;
                                    $updateQuantity = Product::where('id',$pro['id'])
                                        ->update(['product_length' => $currentQuantity]);
                                }else{
                                    return response()->json(['status'=>204, 'message' => 'Sorry, Product quantity not available.']);
                                }
                        }else{
                            $detail['product_id'] = $externalProductId;
                        }
                        $gettax = tax::first();
                        $priceTotal = $pro['total_price'];
                        $tax = $gettax['tax_rate'];
                        $detail['tax'] = ($priceTotal*$tax)/100;
                        $detail['delivery_charges'] = "0.00";

                        if(isset($pro['comments'])){
                            $detail['comments'] = $pro['comments'];

                            //Insert comment on the basis of product
                            $commentData['comment'] = $pro['comments'];
                            $commentData['user_id'] = $order_by;
                            if($detail['behaviour']=='Internal'){
                                $commentData['order_product_id'] = $internalProductId.'-'.$pro['id'];
                            }else{
                                $commentData['order_product_id'] = $externalProductId;
                            }
                            $commentData['order_unique_id'] = $order_unique_id;
                            //dd($commentData);
                            $comment = OrderitemComments::create($commentData);

                        }else{
                            $detail['comments'] = NULL;
                        }

                        $detail['model'] = $pro['model'];
                        if(!empty($image[$i]))
                        {
                            $detail['sketch'] = $this->uploadimages($image[$i],$order_unique_id); 
                        }
                        else
                        {
                            $detail['sketch'] = "no-image.png";
                        }
                        //dd($detail);
                        $createOrder = Order::create($detail);
                        $i++;
                    }
                    
    			    return response()->json(['result'=>$createOrder, 'status'=>200, 'message' => 'New Order created successfully..!']);
    			}
    			else{
    				return response()->json(['status'=>204, 'message' => 'Sorry, Order not created!Please try again..']);
    			}
            }
		}
	}
    
    public function uploadimages($image, $order_unique_id){

            $name = $order_unique_id.'-'.rand(5,100000);
            $sketch = $name.'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/orderSketch');
            $image->move($destinationPath, $sketch);
            return $sketch;
    }

    public function editOrder(Request $request){

        $validator = Validator::make($request->all(), [ 
            'order_unique_id' => 'required', 
            'customer_unique_id' => 'required',               
        ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }else{

            $getOrder = Order::where('customer_unique_id',$request->customer_unique_id)
                                ->where('order_unique_id',$request->order_unique_id)
                                ->get();
        }
        return response()->json(['result'=>$getOrder, 'status'=>200, 'message' => 'Get created order successfully..!']);
    }

    public function uploadimagestest(Request $request){
        $files = $request->file('product_image');
        $i=0;
        $ImageArray = [];
        foreach ($files as $image) {
            $sketch = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/orderSketch');
            $image->move($destinationPath, $sketch);
           $ImageArray[] = $sketch;
           $i++;
        }
        //dd($ImageArray);
    }

	public function orderAcknoledgement(Request $request){
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 
			'customer_unique_id' => 'required',	
			'order_unique_id' => 'required'				
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$lastorder = Order::where('customer_unique_id',$request['customer_unique_id'])
                            //->where('order_by',$request['user_id'])
                            ->where('order_unique_id',$request['order_unique_id'])//->with('ProductModel')
                            ->first();
            //dd($lastorder);
            if($lastorder->count() >0)
            {
            	$data['order_unique_id'] = $lastorder['order_unique_id'];
                $customerdetails = Customer::where('customer_unique_id',$request['customer_unique_id'])
                ->first();
                $data['customer_unique_id'] = $customerdetails['customer_unique_id'];
                $data['customer_name'] = $customerdetails['name'];
                $data['customer_id'] = $customerdetails['id'];

                if($lastorder->delivery == 1){
                    //Select Branch for delivery
                    $orderBy = Salesman::where('id',$request['user_id'])->with('branchdata')->first();
                    //dd($orderBy->branchdata->branch);
                    $data['customer_address'] = $orderBy->branchdata->branch;

                }else{
                    //Select Home for delivery
                    if(!empty($customerdetails['delivery_address']))
                    {
                        $data['customer_address'] = $customerdetails['delivery_address'];
                    }
                    else{
                        $data['customer_address'] = $customerdetails['address'];
                    }                    
                }

                //Calculate delivery date
                $undeliveredOrders = Order::where('status', '!=', '3')->get();
                $totalQuantity = 0;
                foreach ($undeliveredOrders as $value) {
                    $key = $value['quantity'];
                    $totalQuantity += $key;
                }

                $getActiveTailor = OtherUsers::where('type','2')->get();
                $get_buffer_day = tax::first();

                $days = $totalQuantity/(4*$getActiveTailor->count());
                $addBufferDay = round($days)+$get_buffer_day->buffer_days;
                $today=date('d-m-Y');
                $delivery_date=date('d-m-Y', strtotime($today. ' +'.$addBufferDay.'  days'));
                $data['expected_delivery_time'] = $delivery_date;
                //End calculate delivery date


            	$getitemsInorder = Order::where('customer_unique_id',$request['customer_unique_id'])
                            //->where('order_by',$request['user_id'])
                            ->where('order_unique_id',$request['order_unique_id'])
                            ->get();
                $quantity = 0;
                $totalAmmount = 0;
                $totalTax = 0;
                foreach ($getitemsInorder as $value) {
                    $key = $value['quantity'];
                    $quantity += $key;
                    $totalAmmount += $value->total_price;
                    $totalTax += $value->tax;
                }

                $data['quantity'] = $quantity.' Items';
                $data['total_price'] = 'SAR '.$totalAmmount;
                $data['tax'] = 'SAR '.$totalTax;
                $data['delivery_charges'] = 'SAR '.$lastorder['delivery_charges'];
 
                $total_amount= $totalTax + $totalAmmount + $lastorder['delivery_charges'];
                $data['total_amount'] = 'SAR '.$total_amount;
                $updAmount = Order::where('order_unique_id',$request['order_unique_id'])
                                ->update([
                                    'total_amount' => $total_amount,
                                    'expected_delivery_time' => $delivery_date
                                ]);
                $gettotalAmount = Order::where('order_unique_id',$request['order_unique_id'])
                ->first();
                $data['total_amount_after_discount'] = 'SAR '.$gettotalAmount['total_amount'];
            	return response()->json(['result'=>$data, 'status'=>200, 'message' => 'Order acknowledgement..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Order not found']);
            }  
		}
	}

	public function orderconfirm(Request $request){
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required', 	
			'order_unique_id' => 'required',
            'amount_to_pay' => 'required',
            'price_remaining' => 'required',
            'payment_method' => 'required',         
            //'order_type' => 'required',
            'delivery_address' => 'required'		
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$lastorder = Order::where('order_unique_id',$request['order_unique_id'])
                            //->where('order_by',$request['user_id'])
                            ->first();
            $customer=Customer::where('id',$lastorder['customer_id'])->first();
            if(!empty($lastorder))
            {
                //$order_type = $request->order_type;
                $total_amount = $lastorder['total_amount'];
                $customer_unique_id = $lastorder['customer_unique_id'];
                $amount_to_pay = $request->amount_to_pay;
                $price_remaining = $request->price_remaining;
                $payment_method = $request->payment_method;
                $orderBy = $request->user_id;

                if(isset($request->coupon) && isset($request->coupon_amount)){
                    $promo = $request->coupon;
                    $promo_amount = $request->coupon_amount;
                    $total_is = $amount_to_pay + $price_remaining + $promo_amount;
                }else{
                    $promo = NULL;
                    $promo_amount = NULL;
                    $total_is = $amount_to_pay + $price_remaining;
                }

                if($total_is==$total_amount)
                {
                    $order = Order::where('order_unique_id',$request['order_unique_id'])
                                    ->update([
                                        'amount_to_pay' => $amount_to_pay,
                                        'price_remaining' => $price_remaining,
                                        'payment_method' => $payment_method,
                                        'promo' => $promo,
                                        'promo_amount' => $promo_amount,
                                        'status' => '1',
                                        'order_by' => $orderBy,
                                    ]);
                    if(!empty($order)){
                        $insert['order_unique_id'] = $request->order_unique_id;
                        $insert['order_placed'] = '1';
                        $insert['order_confirmed'] = '1';
                        $statusUpload = Trackorder::create($insert);
                        $last_order_date = date('d-m-Y');
                        $updatecustomer = Customer::where('customer_unique_id',$customer_unique_id)->update([
                            'delivery_address' => $request->delivery_address,
                            'last_order_date' => $last_order_date
                            ]);
                    }
                    //Point used
                    if($request->coupon){
                        $loyalty=Loyalty::where('reedem_code', $request->coupon)->first(); // fetch the Loyalty
                        if($loyalty){

                            Loyalty::where('id',$loyalty->id)->update([
                                'order_id' => $request->order_unique_id]);
                            $loyalty->delete(); //delete the fetched Loyalty
                        }
                    }

                    //Add loyalty Points
                    $orderForLoyalty = Order::where('order_unique_id',$request['order_unique_id'])->get();
                    $customerForLoyalty = Customer::where('customer_unique_id',$customer_unique_id)->first();
                    $pointsInt = 0;
                    $pointsExt = 0;
                    foreach ($orderForLoyalty as $value) {

                        if($value['behaviour']=='Internal'){
                            $pointsInt += $value->total_price*2.5;
                        }else{
                            $pointsExt += $value->total_price*2;
                        }
                        $points = $pointsInt+$pointsExt+$customerForLoyalty->total_points;
                    }

                    $data['user_id'] = $customerForLoyalty->id;
                    $data['points'] = $pointsInt+$pointsExt;
                    //$data['reedem_code'] = $RedeemCode;
                    $data['transaction_type'] = "Credit";
                    $data['order_id'] = $request->order_unique_id;

                    $creditPoints = Loyalty::create($data);
                    if($creditPoints){
                        Customer::where('customer_unique_id',$customer_unique_id)
                                ->update(['total_points' => $points]);
                    }
                    //End loyalty Points
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('POST', 'http://www.oursms.net/api/sendsms.php', [
                        'form_params' => [
                        'username' => 'bamthobe',
                        'password' => 'bam123',
                        'message' => 'عميلنا'.$lastorder['customer_name'].'رقم طلبك'.$request->order_unique_id.'المبلغ الإجمالي' .$total_is.'المبلغ المتبقي'.$price_remaining.'الشروط والأحكام'.asset('/terms&condition'),
                         'numbers' => "+966".$customer['phone'],
                        'sender' => 'BAM',
                        'unicode' => 'E',
                        'return' => 'full',
                        ]
                    ]);
                    return response()->json(['status'=>200, 'message' => 'Order Confirmed..!']);
                }
                else{
                    return response()->json(['status'=>400, 'message' => 'Order Not Confirmed ammount not matched..!']);
                }
            	
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Order not found']);
            }  
		}
	}
    
    public function getorders(Request $request){
        $validator = Validator::make($request->all(), [ 
            'status' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            if($request['status']=='2'){
                $Orders = Order::where('status','1')
                        ->orwhere('status','2')
                        ->groupBy('order_unique_id')
                        ->get();
            }
            else if($request['status']=='3'){
                $Orders = Order::where('status','3')
                        ->groupBy('order_unique_id')
                        ->get();
            }
            else if($request['status']=='4'){
                $Orders = Order::where('status','4')
                        ->groupBy('order_unique_id')
                        ->get();
            }
            else if($request['status']=='5'){
                $Orders = Order::where('status','5')
                        ->groupBy('order_unique_id')
                        ->get();
            }
            if($Orders->count() >0){
                $orderArray = [];
                foreach($Orders as $order){
                    $val['id'] = $order['id'];
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $val['customer_name'] = $order['customer_name'];
                    $val['customer_unique_id'] = $order['customer_unique_id'];
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $val['total_amount'] = 'SR '.$order['total_amount'];
                    $val['amount_to_pay'] = 'SR '.$order['amount_to_pay'];
                    $val['price_remaining'] = 'SR '.$order['price_remaining'];
                    $val['status'] = $order['status'];
                    $val['delivery'] = $order['delivery'];
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found']);
            }
        }
    }
    public function getsearchorders(Request $request){
        $validator = Validator::make($request->all(), [ 
            'status' => 'required',
            'order_unique_id' => 'required',
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            if($request['status']=='2'){
                $Orders = Order::where('order_unique_id',$request->order_unique_id)
                ->where('status','1')
                ->orwhere('status','2')
                // ->groupBy('order_unique_id')
                ->first();
            }
            else if($request['status']=='3'){
                $Orders = Order::where('order_unique_id',$request->order_unique_id)
                ->where('status','3')
                ->first();
            }
            else if($request['status']=='4'){
                $Orders = Order::where('order_unique_id',$request->order_unique_id)
                ->where('status','4')
                ->first();
            }
            else if($request['status']=='5'){
                $Orders = Order::where('order_unique_id',$request->order_unique_id)
                ->where('status','5')
                ->first();
            }

            if($Orders->count() >0){
            $stamp1 = strtotime($Orders['expected_delivery_time']);
            $orderArray = array(
                'id'=>$Orders['id'],
                'order_unique_id'=>$Orders['order_unique_id'],
                'customer_name'=>$Orders['customer_name'],
                'customer_unique_id'=>$Orders['customer_unique_id'],
                'order_unique_id'=>$Orders['order_unique_id'],
                'customer_name'=>$Orders['customer_name'],
                'expected_delivery_time'=>$stamp1*1000,
                'total_amount' => 'SR '.$Orders['total_amount'],
                'amount_to_pay' => 'SR '.$Orders['amount_to_pay'],
                'price_remaining' => 'SR '.$Orders['price_remaining'],
                'status' => $Orders['status'],
                'delivery' => $Orders['delivery'],
            );
            return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found']);
            }
        }
    }

	public function getneworders(Request $request){
			$getorders = Assignments::join('orders','assignments.order_unique_id', '=', 'orders.order_unique_id')        ->select('*')->where('assignments.status','1')
                        ->join('customers', 'customers.customer_unique_id', '=', 'orders.customer_unique_id')
                        ->select('customers.account_type','orders.*','assignments.assigned_at')
                        ->where('assignments.cutter_id',$request->cutter_id)
                        ->groupBy('orders.order_unique_id')->orderBy('customers.account_type', 'desc')
                        ->get();
                        
		    if($getorders->count() >0){
		    	$orderArray = [];
                foreach($getorders as $order){
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $getquantity = Order::where('order_unique_id',$val['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $value) {
                        $qua = $value['quantity'];
                        $quantity += $qua;

                        $val['customer_unique_id'] = $value->customer_unique_id;
                        $val['customer_name'] = $value->customer_name;
                        //$getCustomer = Customer::where('customer_unique_id',$value->customer_unique_id)->first();
                        //dd($getCustomer);
                        //$val['account_type'] = $getCustomer->account_type;                        
                    }
                    $val['account_type'] = $order->account_type; 
                    $val['quantity'] = $quantity.' Items';
                    $stamp = strtotime($order["created_at"]);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $stamp2 = strtotime($order['assigned_at']);
                    $val['assigned_at'] = $stamp2*1000;
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'New Orders']);
		    }
		    else{
		    	return response()->json(['status'=>404, 'message' => 'New order not found']);
		    }
	}
    
    public function orderHistory(Request $request){
    	$validator = Validator::make($request->all(), [  
			'user_id' => 'required',
            'customer_unique_id' => 'required'
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$getorders = Order::where('customer_unique_id', $request['customer_unique_id'])
			->groupBy('order_unique_id')
		    ->get();
		    //dd($getorders);
		    if($getorders->count() >0){
		    	$orderArray = [];
                foreach($getorders as $order){
                    $orderDetails['id'] = $order['id'];
                    $orderDetails['order_unique_id'] = $order['order_unique_id'];
                    $orderDetails['quantity'] = $order['quantity'];
                    $orderDetails["order_total"]=$order["total_amount"];
                    $orderDetails['down_payment'] = $order['amount_to_pay'];
                    $orderDetails['price_remaining'] = $order['price_remaining'];
                    $stamp = strtotime($order["created_at"]);
                    $orderDetails["order_date"] = $stamp*1000;
                    if($order["delivered_at"] == null){
                        $stamp1 = strtotime($order["expected_delivery_time"]);
                    }else{
                        $stamp1 = strtotime($order["delivered_at"]);
                    }
                    $orderDetails["completion_date"] = $stamp1*1000;
                    $orderDetails['status'] = $order['status'];
                    $orderArray[] = $orderDetails;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders']);
		    }
		    else{
		    	return response()->json(['status'=>404, 'message' => 'Order not found']);
		    }
		}	
	}

	public function trackOrder(Request $request){
    	$validator = Validator::make($request->all(), [  
            'order_unique_id' => 'required'
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        $id = $request->order_unique_id;
    	$track = Trackorder::join('orders','tracking_delivery.order_unique_id', '=', 'orders.order_unique_id')->select('*')->where('tracking_delivery.order_unique_id',$id)
        ->groupBy('orders.order_unique_id')
        ->get();
        $trackArray = [];
        foreach ($track as $value) {
            $trackDetails["order_unique_id"] = $value["order_unique_id"];
            $trackDetails["customer_unique_id"] = $value["customer_unique_id"];
            $trackDetails["customer_name"] = $value["customer_name"];
            $trackDetails["quantity"] = $value["quantity"];
            //$stamp1 = strtotime($value["delivered_at"]);
            $stamp1 = strtotime($value["expected_delivery_time"]);
            $trackDetails["completion_date"] = $stamp1*1000;
            $trackDetails["order_placed"] = $value["order_placed"];
            $trackDetails["order_confirmed"] = $value["order_confirmed"];
            $trackDetails["with_cutter"] = $value["with_cutter"];
            $trackDetails["with_tailor"] = $value["with_tailor"];
            $trackDetails["with_qa"] = $value["with_qa"];
            $trackDetails["redy_for_delivery"] = $value["redy_for_delivery"];
            $trackDetails["delivered"] = $value["delivered"];
            $trackArray[] = $trackDetails;
        }   
    	return response()->json(['result'=>$trackArray, 'status'=>200, 'message'=>'Tracking Details']);
    }

    public function searchOrder(Request $request){
        $phoneNumber = Customer::where('phone', 'like', "{$request->customer_name}")->first();

        if(isset($phoneNumber->customer_unique_id)){
            $order = Order::where('customer_unique_id', 'like', "{$phoneNumber->customer_unique_id}")
                            ->where('status','!=','0')
                            ->groupBy('order_unique_id')
                            ->get();
        }else{

            $order = Order::where('customer_unique_id', 'like', "{$request->customer_name}")
                            ->where('status','!=','0')            
                //->where('customer_unique_id', 'like', "%{$request->customer_unique_id}%")
                ->Orwhere('order_unique_id', 'like', "{$request->customer_name}")
                            ->where('status','!=','0')                  
                //->where('product_code', 'like', "%{$request->product_code}%")
                ->groupBy('order_unique_id')
                ->get();
        }
        if($order->count() >0){
            $orderArray = [];
            foreach ($order as $value) {
                $getCustomer = Customer::where('customer_unique_id',$value->customer_unique_id)->first();

                $orderDetails["order_unique_id"] = $value["order_unique_id"];
                $orderDetails["customer_unique_id"] = $value["customer_unique_id"];
                $orderDetails["customer_name"] = $value["customer_name"];
                $orderDetails["customer_phone"] = $getCustomer->phone;
                $orderDetails["quantity"] = $value["quantity"];
                $stamp1 = strtotime($value["created_at"]);
                $orderDetails["created_at"] = $stamp1*1000;
                $stamp = strtotime($value["expected_delivery_time"]);
                $orderDetails["expected_delivery_time"] = $stamp*1000;
                $orderDetails["total_amount"] = $value["total_amount"];
                $orderArray[] = $orderDetails;
            }
            return response()->json(['result'=>$orderArray, 'status'=>200, 'message'=>'Orders']);
        }   
        else{
            return response()->json(['status'=>404, 'message'=>'Order not found..!']);
        }
    }

    public function getcuttersCompletedorders(Request $request){
            $getorders = Trackorder::join('orders','tracking_delivery.order_unique_id', '=', 'orders.order_unique_id')
                        ->join('assignments','tracking_delivery.order_unique_id', '=', 'assignments.order_unique_id')
                        ->select('orders.*','tracking_delivery.with_cutter','assignments.cutter_id','assignments.status','assignments.cutter_completion_date')
                        ->where('tracking_delivery.with_cutter','1')
                        ->where('assignments.cutter_id',$request->cutter_id)
                        ->where('assignments.status',2)
                        ->whereDate('assignments.cutter_completion_date', '=', date('Y-m-d'))
                        ->groupBy('orders.order_unique_id')
                        ->get();
            //dd($getorders);
            if($getorders->count() >0){
                $orderArray = [];
                foreach($getorders as $order){
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $getquantity = Order::where('order_unique_id',$val['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $value) {
                        $qua = $value['quantity'];
                        $quantity += $qua;


                        $val['customer_unique_id'] = $value->customer_unique_id;
                        $val['customer_name'] = $value->customer_name;
                        $getCustomer = Customer::where('customer_unique_id',$value->customer_unique_id)->first();
                        //dd($getCustomer);
                        $val['account_type'] = $getCustomer->account_type;                        
                    }
                    $val['quantity'] = $quantity.' Items';
                    $stamp = strtotime($order["created_at"]);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['cutter_completion_date']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Completed Orders']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found']);
            }
    }

    public function comleteorderbyCutter(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'order_unique_id' => 'required',
            'date_time' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $completed = Trackorder::where('order_unique_id',$request['order_unique_id'])
                ->update([
                        'with_tailor' => '0',
                        'cutter_id' => $request->user_id,
                        ]);
            if(!empty($completed)){
                $updassignmentStatus = Assignments::where('order_unique_id',$request['order_unique_id'])->update([
                    'status' => '2',
                    'cutter_completion_date' => $request->date_time,
                ]);
                return response()->json(['status'=>200, 'message' => 'Completed Order']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not completed..!']);
            }
        }
    }

    public function sendDelivery(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'order_unique_id' => 'required',
            'delivery' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $completed = Order::where('order_unique_id',$request['order_unique_id'])
                ->update([
                        'delivery' => $request->delivery
                        ]);
            if(!empty($completed)){
                $delivery = $request->delivery;
                if($delivery=='2')
                {
                    $getcharges = tax::first();
                    $charges = $getcharges['delivery_charges'];
                    $addcharges = Order::where('order_unique_id',$request['order_unique_id'])
                        ->update([
                                'delivery_charges' => $charges
                            ]);
                }
                else if($delivery=='1')
                {
                    $addcharges = Order::where('order_unique_id',$request['order_unique_id'])
                        ->update([
                                'delivery_charges' => '0.00'
                            ]);
                }
                return response()->json(['status'=>200, 'message' => 'Delivery set.']);
            }
            else{
                return response()->json(['status'=>200, 'message' => 'Order not found..!']);
            }
        }
    }

    public function getDelivery(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'delivery_status' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getorderDelivery = Order::where('delivery',$request['delivery_status'])
                                ->has('AssignmentData')->with('TrackOrder')
                                ->where('status','!=', '0')
                                //->where('status','!=', '5')
                                ->groupBy('order_unique_id')
                                ->get();

            if($getorderDelivery->count() >0)
            {
                $orderArray = [];
                foreach($getorderDelivery as $order){
                    //if($order->AssignmentData->order_status == '1'){
                    if($order->AssignmentData->qc_passed_at != NULL && $order->AssignmentData->order_status == '1' && $order->TrackOrder->delivered == 0){
                        $value['order_unique_id'] = $order['order_unique_id'];
                        $value['customer_unique_id'] = $order['customer_unique_id'];
                        $value['customer_name'] = $order['customer_name'];
                        $product_id = $order['product_id'];
                        $value['quantity'] = $order['quantity'];
                        $value['total_price'] = $order['total_price'];
                        $value['tax'] = $order['tax'];
                        $value['delivery_charges'] = $order['delivery_charges'];
                        $value['total_amount'] = $order['total_amount'];
                        $value['discount_code'] = $order['discount_code'];
                        $value['amount_to_pay'] = $order['amount_to_pay'];
                        $value['price_remaining'] = $order['price_remaining'];      
                        $value['expected_delivery_time'] = $order['expected_delivery_time'];      
                        $value['assignment'] = $order['assignment'];      
                        $value['delivery'] = $order['delivery'];
                        $getAddress = Customer::where('customer_unique_id',$order['customer_unique_id'])
                                        ->first();
                        $value['address'] = $getAddress['delivery_address'];     
                        $value['phone'] = $getAddress['phone'];     
                        $orderArray[] = $value;
                    }
                }
                return response()->json(['result'=>$orderArray, 'message'=>'Orders', 'status'=>200]); 
            }
            else{
                return response()->json(['message'=>'Orders not found..!', 'status'=>404]);
            }
        }
    }

    public function orderDelivery(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'order_unique_id' => 'required',
            'delivery_time' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $date = $request->delivery_time;
            $amount_to_pay = $request->amount_to_pay;
            $payment_method = $request->payment_method;
            $getRemaining = Order::where('order_unique_id',$request['order_unique_id'])
                ->first();
            $remaining_price = $getRemaining['price_remaining'];
            $paid_ammount = $getRemaining['amount_to_pay'];
            $ammount_paid = $amount_to_pay + $paid_ammount;
            $totalRemaining_now = $remaining_price - $amount_to_pay;
            $orderDelivery = Order::where('order_unique_id',$request['order_unique_id'])
                        ->update([
                                'price_remaining' => $totalRemaining_now,
                                'amount_to_pay' => $ammount_paid,
                                'delivered_at' => $date,
                                'status' => '3'
                            ]);
            $phone = Customer::where('customer_unique_id',$getRemaining['customer_unique_id'])->value('phone');
            if(!empty($orderDelivery))
            {
                $updTracking = Trackorder::where('order_unique_id',$request['order_unique_id'])
                        ->update([
                                'delivered' => '1',
                                'driverSalesman_id' => $request->user_id
                            ]);
                        $cus_name = $getRemaining['customer_name'];
                        $orderID = $request['order_unique_id'];
                        $mess = 'طلب معرف الطلب الخاص بك هو :';
                        $concateString = $cus_name . $mess . $orderID;
                        $client = new \GuzzleHttp\Client();
                        $res = $client->request('POST', 'http://www.oursms.net/api/sendsms.php', [
                        'form_params' => [
                        'username' => 'bamthobe',
                        'password' => 'bam123',
                        'message' => $concateString.
                        'ثيابك جاهزة للإستلام ملبوس العافية
                         بام للخياطةالرجالية
                         0566885235',
                        'numbers' => "+966".$phone,
                        'sender' => 'BAM',
                        'unicode' => 'E',
                        'return' => 'full',
                        ]
                    ]);
                return response()->json(['message'=>'Order deliverd successfully..!', 'status'=>200]); 
            }
            else{
                return response()->json(['message'=>'Orders not found..!', 'status'=>404]);
            }
        }
    }

    public function getDeliveredOrders(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            if($request['delivery_status']=='1'){
                $getDeliveredOrders = Order::where('delivery','1')
                ->where('status','3')
                ->groupBy('order_unique_id')
                ->get();
            }
            else if($request['delivery_status']=='2'){
                $getDeliveredOrders = Order::where('delivery','2')
                ->where('status','3')
                ->groupBy('order_unique_id')
                ->get();
            }
            else{
                $getDeliveredOrders = Order::where('status','3')
                ->groupBy('order_unique_id')
                ->get();
            }
            if($getDeliveredOrders->count() >0)
            {
                $orderArray = [];
                foreach($getDeliveredOrders as $order){
                    $value['order_unique_id'] = $order['order_unique_id'];
                    $value['customer_unique_id'] = $order['customer_unique_id'];
                    $value['customer_name'] = $order['customer_name'];
                    $product_id = $order['product_id'];
                    $value['quantity'] = $order['quantity'];
                    $value['total_price'] = $order['total_price'];
                    $value['tax'] = $order['tax'];
                    $value['delivery_charges'] = $order['delivery_charges'];
                    $value['total_amount'] = $order['total_amount'];
                    $value['discount_code'] = $order['discount_code'];
                    $value['amount_to_pay'] = $order['amount_to_pay'];
                    $value['price_remaining'] = $order['price_remaining'];
                    $stamp = strtotime($order["delivered_at"]);
                    $value['delivered_on'] = $stamp*1000;      
                    $value['assignment'] = $order['assignment'];      
                    $value['delivery'] = $order['delivery'];
                    $getAddress = Customer::where('customer_unique_id',$order['customer_unique_id'])
                                    ->first();
                    $value['address'] = $getAddress['delivery_address'];     
                    $orderArray[] = $value;
                }
                return response()->json(['result'=>$orderArray, 'message'=>'Delivered Orders..!', 'status'=>200]); 
            }
            else{
                return response()->json(['message'=>'Orders not found..!', 'status'=>404]);
            }
        }
    }

    public function passorderByqc(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'order_unique_id' => 'required',
            'date_time' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $UpdateAssignment = Assignments::where('order_unique_id',$request['order_unique_id'])
                ->where('qc_id',$request['user_id'])
                ->update([
                    'qc_status' => '2',
                    'qc_passed_at' => $request->date_time,
                    'order_status' => '1'
                ]);
            if(!empty($UpdateAssignment)){
                $completed = Trackorder::where('order_unique_id',$request['order_unique_id'])
                ->update([
                        'redy_for_delivery' => '1',
                        'qc_id' => $request->user_id,
                    ]);
                return response()->json(['message'=>'You passed Order..!', 'status'=>200]); 
            }
            else{
                return response()->json(['message'=>'Sorry! Try again..', 'status'=>404]);
            }
        }
    }

    public function getQcCompletedOrders(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getassignedOrders = Assignments::join('orders','assignments.order_unique_id', '=', 'orders.order_unique_id')
                ->where('assignments.qc_id',$request['user_id'])
                ->where('assignments.qc_status','2')
                ->whereDate('assignments.qc_passed_at', date('Y-m-d'))                
                ->groupBy('orders.order_unique_id')
                ->get();
            if($getassignedOrders->count() >0)
            {
                $orderArray = [];
                foreach($getassignedOrders as $order){
                    $val['order_unique_id'] = $order['order_unique_id'];
                    $stamp = strtotime($order['created_at']);
                    $val['created_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $val['expected_delivery_time'] = $stamp1*1000;
                    $val['qc_status'] = $order->order_status;
                    $getquantity = Order::where('order_unique_id',$val['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $value) {
                        $qua = $value['quantity'];
                        $quantity += $qua;

                        $val['customer_unique_id'] = $value->customer_unique_id;
                        $val['customer_name'] = $value->customer_name;
                        $getCustomer = Customer::where('customer_unique_id',$value->customer_unique_id)->first();
                        //dd($getCustomer);
                        $val['account_type'] = $getCustomer->account_type;                        
                    }
                    $val['quantity'] = $quantity.' Items';
                    $cutter_id = $order['cutter_id'];
                    $getcutter = Salesman::where('id', $cutter_id)->first();
                    $val['cutter_id'] = $getcutter['id'];
                    $val['cutter_name'] = $getcutter['title'];
                    $val['tailor_id'] = $order['tailor_id'];
                    $gettailor = OtherUsers::where('id',$val['tailor_id'])->first();
                    $val['tailor_name'] = $gettailor['name'];
                    $orderArray[] = $val;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Your Completed Orders..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'No order found today..!']);
            }
        }
    }

    public function managerNeworders(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'status' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            //Unassigned order
            if($request->status == 0){
                $getnewOrders = Order::join('assignments','orders.order_unique_id', '=', 'assignments.order_unique_id')
                    ->where('assignments.status','2')
                    ->where('assignments.tailor_status','0')
                    ->where('assignments.qc_status','0')
                    ->groupBy('orders.order_unique_id')
                    ->get();
                if($getnewOrders->count() >0)
                {
                    $orderArray = [];
                    foreach($getnewOrders as $order){
                        $value['order_unique_id'] = $order['order_unique_id'];
                        $stamp = strtotime($order['cutter_completion_date']);
                        $value['cutter_completion_date'] = $stamp*1000;
                        // $stampes = strtotime($order['tailor_completion_date']);
                        // $value['tailor_completion_date'] = $stampes*1000;
                        $stamp1 = strtotime($order['expected_delivery_time']);
                        $value['expected_delivery_time'] = $stamp1*1000;
                        $value['order_status'] = $order['order_status'];
                        $order_type = $order['order_type'];
                        $customer_unique_id = $order['customer_unique_id'];
                        $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                            ->first();
                        $account_type = $getcustomer['account_type'];
                        $value['account_type'] = $account_type;
                        // if($order_type=='2' && $account_type='2' && $account_type='1')
                        // { $value['order_type'] = "2"; }
                        // if($order_type=='1' && $account_type='2')
                        // { $value['order_type'] = "3"; }
                        // if($order_type=='1' && $account_type='1')
                        // { $value['order_type'] = "1"; }
                        $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                            ->get();
                        $quantity = 0;
                        foreach ($getquantity as $val) {
                            $qua = $val['quantity'];
                            $quantity += $qua;
                        }
                        $value['quantity'] = $quantity.' Items';
                        $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();
                        $value['cutter_assigned'] = $getassignedCutter['title'];

                        $commentCount = Comment::where('order_unique_id',$order->order_unique_id)->count();
                        $value['orderCommentCount'] = $commentCount;
                        // $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                        //     ->where('assignments.order_unique_id',$value['order_unique_id'])
                        //     ->where('assignments.tailor_status','1')
                        //     ->orwhere('assignments.tailor_status','3')
                        //     ->first();
                        // $value['tailor_id'] = $getassignedTailor['id'];
                        // $value['tailor_name'] = $getassignedTailor['name'];
                        // $value['tailor_status'] = $getassignedTailor['tailor_status'];
                        // $stamp2 = strtotime($getassignedTailor['tailor_assigned_at']);
                        // $value['tailor_assigned_at'] = $stamp2*1000;
                        $orderArray[] = $value;
                    }
                    return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'New Orders..!']);
                }else{
                    return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
                }
            }
            //With Tailor order
            if($request->status == 1){
                $getnewOrders = Order::join('assignments','orders.order_unique_id', '=', 'assignments.order_unique_id')
                    ->where('assignments.status','2')
                    ->where('assignments.tailor_status','1')
                    ->where('assignments.qc_status','0')
                    ->Orwhere('assignments.tailor_status','3')
                    ->groupBy('orders.order_unique_id')
                    ->get();
                if($getnewOrders->count() >0)
                {
                    $orderArray = [];
                    foreach($getnewOrders as $order){
                        $value['order_unique_id'] = $order['order_unique_id'];
                        $stamp = strtotime($order['cutter_completion_date']);
                        $value['cutter_completion_date'] = $stamp*1000;
                        $stampes = strtotime($order['tailor_completion_date']);
                        $value['tailor_completion_date'] = $stampes*1000;
                        $stamp1 = strtotime($order['expected_delivery_time']);
                        $value['expected_delivery_time'] = $stamp1*1000;
                        $value['order_status'] = $order['order_status'];
                        $order_type = $order['order_type'];
                        $customer_unique_id = $order['customer_unique_id'];
                        $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                            ->first();
                        $account_type = $getcustomer['account_type'];
                        $value['account_type'] = $account_type;

                        $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                            ->get();
                        $quantity = 0;
                        foreach ($getquantity as $val) {
                            $qua = $val['quantity'];
                            $quantity += $qua;
                        }
                        $value['quantity'] = $quantity.' Items';
                        $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();
                        $value['cutter_assigned'] = $getassignedCutter['title'];

                        $commentCount = Comment::where('order_unique_id',$order->order_unique_id)->count();
                        $value['orderCommentCount'] = $commentCount;
                        
                        $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();

                        $value['tailor_id'] = $order['id'];
                        $value['tailor_name'] = $getassignedTailor['name'];
                        $value['tailor_status'] = $order['tailor_status'];
                        $stamp2 = strtotime($order['tailor_assigned_at']);
                        $value['tailor_assigned_at'] = $stamp2*1000;
                        $orderArray[] = $value;
                    }
                    return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'New Orders..!']);
                }else{
                    return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
                }
            }
            //QC order
            if($request->status == 2){
                $getnewOrders = Order::join('assignments','orders.order_unique_id', '=', 'assignments.order_unique_id')
                    ->where('assignments.status','2')
                    ->where('assignments.tailor_status','2')
                    ->where('assignments.qc_status','1')
                    ->Orwhere('assignments.qc_status','3')
                    ->groupBy('orders.order_unique_id')
                    ->get();
                if($getnewOrders->count() >0)
                {
                    $orderArray = [];
                    foreach($getnewOrders as $order){
                        $value['order_unique_id'] = $order['order_unique_id'];
                        $stamp = strtotime($order['cutter_completion_date']);
                        $value['cutter_completion_date'] = $stamp*1000;
                        $stampes = strtotime($order['tailor_completion_date']);
                        $value['tailor_completion_date'] = $stampes*1000;
                        $stamp1 = strtotime($order['expected_delivery_time']);
                        $value['expected_delivery_time'] = $stamp1*1000;
                        $value['order_status'] = $order['order_status'];
                        $order_type = $order['order_type'];
                        $customer_unique_id = $order['customer_unique_id'];
                        $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                            ->first();
                        $account_type = $getcustomer['account_type'];
                        $value['account_type'] = $account_type;

                        $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                            ->get();
                        $quantity = 0;
                        foreach ($getquantity as $val) {
                            $qua = $val['quantity'];
                            $quantity += $qua;
                        }
                        $value['quantity'] = $quantity.' Items';
                        $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();
                        $value['cutter_assigned'] = $getassignedCutter['title'];

                        $commentCount = Comment::where('order_unique_id',$order->order_unique_id)->count();
                        $value['orderCommentCount'] = $commentCount;
                        
                        $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();

                        $value['tailor_id'] = $order['id'];
                        $value['tailor_name'] = $getassignedTailor['name'];
                        $value['tailor_status'] = $order['tailor_status'];
                        $stamp2 = strtotime($order['tailor_assigned_at']);
                        $value['tailor_assigned_at'] = $stamp2*1000;
                        $orderArray[] = $value;
                    }
                    return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'New Orders..!']);
                }else{
                    return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
                }
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
            }
        }
    }

    public function managerCompletedorders(Request $request){
        // dd("ff");
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $currentdate = date('Y-m-d');
            
            $getnewOrders = Order::join('assignments','orders.order_unique_id', '=', 'assignments.order_unique_id')
                ->where('assignments.qc_status','2')
                ->where('orders.status','!=','3')
                ->whereDate('orders.created_at', '>', Carbon::now()->subDays(60))
                ->groupBy('orders.order_unique_id')
                ->orderBy('orders.created_at','DESC')
                ->get();
                // print_r(json_encode($getnewOrders));die();
            if($getnewOrders->count() >0)
            {
                $orderArray = [];
                foreach($getnewOrders as $order){
                    $value['order_unique_id'] = $order['order_unique_id'];
                    $getorderDate = Order::where('order_unique_id',$value['order_unique_id'])
                        ->groupBy('order_unique_id')->first();
                    $stampfirst = strtotime($getorderDate['created_at']);
                    $value['order_date'] = $stampfirst*1000;
                    $stamp = strtotime($order['qc_passed_at']);
                    $value['qc_passed_at'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $value['expected_delivery_time'] = $stamp1*1000;
                    $order_type = $order['order_type'];
                    $customer_unique_id = $order['customer_unique_id'];
                    $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                        ->first();
                    $account_type = $getcustomer['account_type'];
                    $value['account_type'] = $account_type;
                    // if($order_type=='2' && $account_type='2' && $account_type='1')
                    // { $value['order_type'] = "2"; }
                    // if($order_type=='1' && $account_type='2')
                    // { $value['order_type'] = "3"; }
                    // if($order_type=='1' && $account_type='1')
                    // { $value['order_type'] = "1"; }
                    $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $val) {
                        $qua = $val['quantity'];
                        $quantity += $qua;
                    }
                    $value['quantity'] = $quantity.' Items';
                    $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                        ->where('assignments.order_unique_id',$value['order_unique_id'])
                        ->where('assignments.status','2')
                        ->first();
                    $value['cutter_assigned'] = $getassignedCutter['title'];
                    $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                        ->select('other_users.id','other_users.name','assignments.tailor_assigned_at')
                        ->where('assignments.order_unique_id',$value['order_unique_id'])
                        ->where('assignments.tailor_status','!=','1')
                        ->orwhere('assignments.tailor_status','!=','3')
                        ->first();
                    $value['tailor_id'] = $getassignedTailor['id'];
                    $value['tailor_assigned'] = $getassignedTailor['name'];
                    $stamp2 = strtotime($getassignedTailor['tailor_assigned_at']);
                    $value['tailor_assigned_at'] = $stamp2*1000;
                    $value['qc_passed'] = "Yes";
                    $value['created_at'] = $order['created_at'];
                    $orderArray[] = $value;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Completed Orders..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found..!']);
            }
        }
    }

    public function searchCompletedorders(Request $request){
        $validator = Validator::make($request->all(), [  
            'order_unique_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getnewOrders = Order::join('assignments','orders.order_unique_id', '=', 'assignments.order_unique_id')
            ->where('assignments.order_unique_id',$request->order_unique_id)
            ->where('assignments.qc_status','2')
            ->first();
            if($getnewOrders)
            {
                $getorderDate = Order::where('order_unique_id',$request->order_unique_id)
                ->groupBy('order_unique_id')->first();
                $stampfirst = strtotime($getorderDate['created_at']);
                $stamp = strtotime($getnewOrders['qc_passed_at']);
                $stamp1 = strtotime($getnewOrders['expected_delivery_time']);
                $order_type = $getnewOrders['order_type'];
                $customer_unique_id = $getnewOrders['customer_unique_id'];
                $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                ->first();
                $account_type = $getcustomer['account_type'];
                $getquantity = Order::where('order_unique_id',$getnewOrders['order_unique_id'])
                ->get();
                $quantity = 0;
                foreach ($getquantity as $val) {
                    $qua = $val['quantity'];
                    $quantity += $qua;
                }
                $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                 ->where('assignments.order_unique_id',$getnewOrders['order_unique_id'])
                ->where('assignments.status','2')
                ->first();
                $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                ->select('other_users.id','other_users.name','assignments.tailor_assigned_at')
                ->where('assignments.order_unique_id',$getnewOrders['order_unique_id'])
                ->where('assignments.tailor_status','!=','1')
                ->orwhere('assignments.tailor_status','!=','3')
                ->first();
                $stamp2 = strtotime($getassignedTailor['tailor_assigned_at']);
                $orderArray = [
                    'order_unique_id' => $getnewOrders['order_unique_id'],
                    'order_date' => $stampfirst*1000,
                    'qc_passed_at' => $stamp*1000,
                    'expected_delivery_time' => $stamp1*1000,
                    'account_type' => $account_type,
                    'quantity' => $quantity.' Items',
                    'cutter_assigned' => $getassignedCutter['title'],
                    'tailor_id' => $getassignedTailor['id'],
                    'tailor_assigned_at' => $stamp2*1000,
                    'qc_passed' => "Yes"
                ];
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Completed Orders..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Order not found..!']);
            }
        }
    }

    public function updateOrderStatusbyQC(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'order_unique_id' => 'required',
            'date_time' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $gettailorId = Assignments::select('tailor_id')
                ->where('order_unique_id',$request['order_unique_id'])
                ->first();
            $old_tailor_id = $gettailorId['tailor_id'];
            $updateOrderStatus = Assignments::where('order_unique_id',$request['order_unique_id'])
                ->update([
                    'order_status' => '0',
                    'qc_status' => '2',
                    'qc_passed_at' => $request->date_time,
                    'old_tailor_id' => $old_tailor_id
                ]);
            if(!empty($updateOrderStatus))
            {
                return response()->json(['status'=>200, 'message' => 'Status Updated Successfully..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Status not Updated..!']);
            }
        }
    }

    public function getDefectiveOrdersbyMngr(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'status' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{

            if($request->status == 0){
                //Unassigned order for manager when defective order tracked
                $GetDefectiveOrders = Assignments::join('orders', 'assignments.order_unique_id', '=', 'orders.order_unique_id')
                    ->where('assignments.order_status','0')
                    ->where('assignments.tailor_status','2')
                    ->where('assignments.qc_status','2')
                    ->orWhere('orders.status','5')
                    ->groupBy('orders.order_unique_id')
                    ->get();
                if($GetDefectiveOrders->count() >0)
                {
                    $orderArray = [];
                    foreach($GetDefectiveOrders as $order){
                        $value['order_unique_id'] = $order['order_unique_id'];
                        $stamp = strtotime($order['cutter_completion_date']);
                        $value['cutter_completion_date'] = $stamp*1000;
                        $stamp1 = strtotime($order['expected_delivery_time']);
                        $value['expected_delivery_time'] = $stamp1*1000;
                        $qcComplete = strtotime($order['qc_passed_at']);
                        $value['cq_completion_date'] = $qcComplete*1000;
                        $order_type = $order['order_type'];
                        $customer_unique_id = $order['customer_unique_id'];
                        $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                            ->first();
                        $account_type = $getcustomer['account_type'];
                        $value['account_type'] = $account_type;
                        // if($order_type=='2' && $account_type='2' && $account_type='1')
                        // { $value['order_type'] = "2"; }
                        // if($order_type=='1' && $account_type='2')
                        // { $value['order_type'] = "3"; }
                        // if($order_type=='1' && $account_type='1')
                        // { $value['order_type'] = "1"; }
                        $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                            ->get();
                        $quantity = 0;
                        foreach ($getquantity as $val) {
                            $qua = $val['quantity'];
                            $quantity += $qua;
                        }
                        $value['quantity'] = $quantity.' Items';
                        $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();
                        $value['cutter_assigned'] = $getassignedCutter['title'];
                        $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->first();
                        $value['tailor_id'] = $getassignedTailor['id'];
                        $value['tailor_name'] = $getassignedTailor['name'];
                        $value['tailor_status'] = $getassignedTailor['tailor_status'];
                        if(!empty($order['old_tailor_id']))
                        {
                            $getOldtailerdetails = Assignments::join('other_users','assignments.old_tailor_id', '=', 'other_users.id')
                                ->where('assignments.order_unique_id',$value['order_unique_id'])
                                ->first();
                            $value['old_tailor_id'] = $getOldtailerdetails['id'];
                            $value['old_tailor_name'] = $getOldtailerdetails['name'];
                        }
                        $orderArray[] = $value;
                    }
                    return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Defective Orders..!']);
                }
                else{
                    return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
                }                
            }
            if($request->status == 1){
                //With tailor order
                $GetDefectiveOrders = Assignments::join('orders', 'assignments.order_unique_id', '=', 'orders.order_unique_id')
                    ->where('assignments.order_status','0')
                    ->where('assignments.tailor_status','3')
                    ->groupBy('orders.order_unique_id')
                    ->get();
                if($GetDefectiveOrders->count() >0)
                {
                    $orderArray = [];
                    foreach($GetDefectiveOrders as $order){
                        $value['order_unique_id'] = $order['order_unique_id'];
                        $stamp = strtotime($order['cutter_completion_date']);
                        $value['cutter_completion_date'] = $stamp*1000;
                        $stamp1 = strtotime($order['expected_delivery_time']);
                        $value['expected_delivery_time'] = $stamp1*1000;
                        $order_type = $order['order_type'];
                        $customer_unique_id = $order['customer_unique_id'];
                        $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                            ->first();
                        $account_type = $getcustomer['account_type'];
                        $value['account_type'] = $account_type;
                        // if($order_type=='2' && $account_type='2' && $account_type='1')
                        // { $value['order_type'] = "2"; }
                        // if($order_type=='1' && $account_type='2')
                        // { $value['order_type'] = "3"; }
                        // if($order_type=='1' && $account_type='1')
                        // { $value['order_type'] = "1"; }
                        $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                            ->get();
                        $quantity = 0;
                        foreach ($getquantity as $val) {
                            $qua = $val['quantity'];
                            $quantity += $qua;
                        }
                        $value['quantity'] = $quantity.' Items';
                        $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();
                        $value['cutter_assigned'] = $getassignedCutter['title'];
                        $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->first();
                        $value['tailor_id'] = $getassignedTailor['id'];
                        $value['tailor_name'] = $getassignedTailor['name'];
                        $value['tailor_status'] = $getassignedTailor['tailor_status'];
                        if(!empty($order['old_tailor_id']))
                        {
                            $getOldtailerdetails = Assignments::join('other_users','assignments.old_tailor_id', '=', 'other_users.id')
                                ->where('assignments.order_unique_id',$value['order_unique_id'])
                                ->first();
                            $value['old_tailor_id'] = $getOldtailerdetails['id'];
                            $value['old_tailor_name'] = $getOldtailerdetails['name'];
                        }
                        $orderArray[] = $value;
                    }
                    return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Defective Orders..!']);
                }
                else{
                    return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
                }
            }
            if($request->status == 2){
                //QC order
                $GetDefectiveOrders = Assignments::join('orders', 'assignments.order_unique_id', '=', 'orders.order_unique_id')
                    ->where('assignments.order_status','0')
                    ->where('assignments.qc_status','3')
                    ->groupBy('orders.order_unique_id')
                    ->get();
                if($GetDefectiveOrders->count() >0)
                {
                    $orderArray = [];
                    foreach($GetDefectiveOrders as $order){
                        $value['order_unique_id'] = $order['order_unique_id'];
                        $stamp = strtotime($order['cutter_completion_date']);
                        $value['cutter_completion_date'] = $stamp*1000;
                        $stamp1 = strtotime($order['expected_delivery_time']);
                        $value['expected_delivery_time'] = $stamp1*1000;
                        $order_type = $order['order_type'];
                        $customer_unique_id = $order['customer_unique_id'];
                        $getcustomer = Customer::where('customer_unique_id',$customer_unique_id)
                            ->first();
                        $account_type = $getcustomer['account_type'];
                        $value['account_type'] = $account_type;
                        // if($order_type=='2' && $account_type='2' && $account_type='1')
                        // { $value['order_type'] = "2"; }
                        // if($order_type=='1' && $account_type='2')
                        // { $value['order_type'] = "3"; }
                        // if($order_type=='1' && $account_type='1')
                        // { $value['order_type'] = "1"; }
                        $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                            ->get();
                        $quantity = 0;
                        foreach ($getquantity as $val) {
                            $qua = $val['quantity'];
                            $quantity += $qua;
                        }
                        $value['quantity'] = $quantity.' Items';
                        $getassignedCutter = Assignments::join('salesmans','assignments.cutter_id', '=', 'salesmans.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->where('assignments.status','2')
                            ->first();
                        $value['cutter_assigned'] = $getassignedCutter['title'];
                        $getassignedTailor = Assignments::join('other_users','assignments.tailor_id', '=', 'other_users.id')
                            ->where('assignments.order_unique_id',$value['order_unique_id'])
                            ->first();
                        $value['tailor_id'] = $getassignedTailor['id'];
                        $value['tailor_name'] = $getassignedTailor['name'];
                        $value['tailor_status'] = $getassignedTailor['tailor_status'];
                        if(!empty($order['old_tailor_id']))
                        {
                            $getOldtailerdetails = Assignments::join('other_users','assignments.old_tailor_id', '=', 'other_users.id')
                                ->where('assignments.order_unique_id',$value['order_unique_id'])
                                ->first();
                            $value['old_tailor_id'] = $getOldtailerdetails['id'];
                            $value['old_tailor_name'] = $getOldtailerdetails['name'];
                        }
                        $orderArray[] = $value;
                    }
                    return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Defective Orders..!']);
                }
                else{
                    return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
                }
            }            
        }
    }

    public function getDeliveredOrdersbyMngr(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getDeliveredOrders = Order::join('assignments', 'orders.order_unique_id', '=', 'assignments.order_unique_id')
                ->select('orders.order_unique_id','orders.customer_unique_id','orders.created_at','orders.expected_delivery_time','orders.order_type','orders.delivered_at','assignments.cutter_id','assignments.tailor_id','assignments.tailor_id')
                ->where('orders.status','3')
                ->whereDate('orders.created_at', '>', Carbon::now()->subDays(60))
                ->groupBy('orders.order_unique_id')
                ->orderBy('orders.created_at','DESC')
                ->get();
            if($getDeliveredOrders->count() >0)
            {
                $orderArray = [];
                foreach ($getDeliveredOrders as $order) {
                    $value['order_unique_id'] = $order['order_unique_id'];
                    $stampfirst = strtotime($order['created_at']);
                    $value['order_date'] = $stampfirst*1000;
                    $stamp = strtotime($order['expected_delivery_time']);
                    $value['expected_delivery_time'] = $stamp*1000;
                    //$value['order_type'] = $order['order_type'];
                    $getcustomer = Customer::where('customer_unique_id',$order->customer_unique_id)
                        ->first();
                    $value['account_type'] = $getcustomer->account_type;

                    $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $val) {
                        $qua = $val['quantity'];
                        $quantity += $qua;
                    }
                    $value['quantity'] = $quantity.' Items';
                    $cutter_id = $order['cutter_id'];
                    
                    $getCutter = Salesman::where('type','4')->where('id',$cutter_id)->first();
                    $value['cutter_assigned'] = $getCutter['title'];

                    $tailor_id = $order['tailor_id'];
                    $getTailor = OtherUsers::where('type','2')->where('id',$tailor_id)->first();
                    $value['tailor_assigned'] = $getTailor['name'];

                    $deliveredAt = strtotime($order['delivered_at']);
                    $value['delivered_at'] = $deliveredAt*1000;

                    $value['qc_passed'] = "Yes";
                    $value['created_at'] = $order['created_at'];
                    $orderArray[] = $value;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Delivered Orders..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
            }
        }
    }

    public function searchDeliveredOrdersbyMngr(Request $request){
        $validator = Validator::make($request->all(), [  
            'order_unique_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getDeliveredOrders = Order::join('assignments', 'orders.order_unique_id', '=', 'assignments.order_unique_id')
                ->select('orders.order_unique_id','orders.customer_unique_id','orders.created_at','orders.expected_delivery_time','orders.order_type','orders.delivered_at','assignments.cutter_id','assignments.tailor_id','assignments.tailor_id')
                ->where('assignments.order_unique_id',$request->order_unique_id)
                ->where('orders.status','3')
                // ->groupBy('orders.order_unique_id')
                ->first();
                // print_r(json_encode($getDeliveredOrders));die();
            if($getDeliveredOrders)
            {
                $stampfirst = strtotime($getDeliveredOrders['created_at']);
                $stamp = strtotime($getDeliveredOrders['expected_delivery_time']);
                $getcustomer = Customer::where('customer_unique_id',$getDeliveredOrders->customer_unique_id)->first();
                $getquantity = Order::where('order_unique_id',$getDeliveredOrders['order_unique_id'])->get();
                $quantity = 0;
                foreach ($getquantity as $val) {
                    $qua = $val['quantity'];
                    $quantity += $qua;
                }
                $cutter_id = $getDeliveredOrders['cutter_id'];
                $getCutter = Salesman::where('type','4')->where('id',$cutter_id)->first();
                $tailor_id = $getDeliveredOrders['tailor_id'];
                $getTailor = OtherUsers::where('type','2')->where('id',$tailor_id)->first();
                $deliveredAt = strtotime($getDeliveredOrders['delivered_at']);
                $orderArray = array(
                    'order_unique_id' => $getDeliveredOrders['order_unique_id'],
                    'order_date' => $stampfirst*1000,
                    'expected_delivery_time' => $stamp*1000,
                    'account_type' => $getcustomer->account_type,
                    'quantity' => $quantity.' Items',
                    'cutter_assigned' => $getCutter['title'],
                    'tailor_assigned' => $getTailor['name'],
                    'delivered_at' => $deliveredAt*1000,
                    'qc_passed' => "Yes",
                );
                /*$orderArray = [];
                foreach ($getDeliveredOrders as $order) {
                    $value['order_unique_id'] = $order['order_unique_id'];
                    $stampfirst = strtotime($order['created_at']);
                    $value['order_date'] = $stampfirst*1000;
                    $stamp = strtotime($order['expected_delivery_time']);
                    $value['expected_delivery_time'] = $stamp*1000;
                    //$value['order_type'] = $order['order_type'];
                    $getcustomer = Customer::where('customer_unique_id',$order->customer_unique_id)
                        ->first();
                    $value['account_type'] = $getcustomer->account_type;

                    $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $val) {
                        $qua = $val['quantity'];
                        $quantity += $qua;
                    }
                    $value['quantity'] = $quantity.' Items';
                    $cutter_id = $order['cutter_id'];
                    
                    $getCutter = Salesman::where('type','4')->where('id',$cutter_id)->first();
                    $value['cutter_assigned'] = $getCutter['title'];

                    $tailor_id = $order['tailor_id'];
                    $getTailor = OtherUsers::where('type','2')->where('id',$tailor_id)->first();
                    $value['tailor_assigned'] = $getTailor['name'];

                    $deliveredAt = strtotime($order['delivered_at']);
                    $value['delivered_at'] = $deliveredAt*1000;

                    $value['qc_passed'] = "Yes";
                    $orderArray[] = $value;
                }*/
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Delivered Orders..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
            }
        }
    }

    public function confirmReorderbySalesman(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'order_unique_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $confirmReorder = Order::where('order_unique_id',$request['order_unique_id'])
                ->update(['status' => '2']);
            if(!empty($confirmReorder))
            {

                    //Add loyalty Points
                    $orderForLoyalty = Order::where('order_unique_id',$request['order_unique_id'])->get();
                    $customerForLoyalty = Customer::where('customer_unique_id',$orderForLoyalty->first()->customer_unique_id)->first();
                    $pointsInt = 0;
                    $pointsExt = 0;
                    foreach ($orderForLoyalty as $value) {

                        if($value['behaviour']=='Internal'){
                            $pointsInt += $value->total_price*2.5;
                        }else{
                            $pointsExt += $value->total_price*2;
                        }
                        $points = $pointsInt+$pointsExt+$customerForLoyalty->total_points;
                    }

                    $data['user_id'] = $customerForLoyalty->id;
                    $data['points'] = $pointsInt+$pointsExt;
                    //$data['reedem_code'] = $RedeemCode;
                    $data['transaction_type'] = "Credit";
                    $data['order_id'] = $request->order_unique_id;

                    $creditPoints = Loyalty::create($data);
                    if($creditPoints){
                        Customer::where('customer_unique_id',$orderForLoyalty->first()->customer_unique_id)
                                ->update(['total_points' => $points]);
                    }
                    //End loyalty Points

                return response()->json(['status'=>200, 'message' => 'Order Confirmed Successfully..!']);
            }
            else{
                return response()->json(['status'=>401, 'message' => 'Order not confirmed..!']);
            }
        }
    }

    public function markAsDefective(Request $request){
        $validator = Validator::make($request->all(), [  
            'defective_by_id' => 'required',
            'order_unique_id' => 'required',
            'defective_by_role' => 'required',
            'reason' => 'required',
            'product_id' => 'required'
        ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        $getOrder = Order::where('order_unique_id', $request->order_unique_id)
                        ->where('product_id',$request->product_id)->get();
        if($getOrder[0]->delivered_at){
            //Count day's from delivery.
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $getOrder[0]->delivered_at);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', date('Y-m-d h:m:s'));
            $diff_in_days = $to->diffInDays($from);
        }else{
            return response()->json(['status'=>401, 'message' => 'Order is not delivered']);
        }
        if($diff_in_days <= 90){
            foreach ($getOrder as $value) {

                if($request->reason == 'Tailoring Defect'){
                    $assignment = Assignments::where('order_unique_id',$request->order_unique_id)->first();
                    if($assignment->tailor_id){
                        $tailor = OtherUsers::where('id',$assignment->tailor_id)->first();
                        $defectiveOrder = 1;
                        if($tailor){
                            $defectiveOrder += $tailor->defective_order;
                            OtherUsers::where('id',$assignment->tailor_id)->update(['defective_order' => $defectiveOrder]);
                        }else{
                            return response()->json(['status'=>401, 'message' => 'Tailor Not Found']);                            
                        }
                    }else{
                        return response()->json(['status'=>401, 'message' => 'Tailor Not Assigned']);
                    }
                }else if($request->reason == 'Measurement issue'){
                    $salesman = Salesman::where('id',$getOrder[0]->order_by)->first();
                    if($salesman){
                        $defectiveOrder = 1;
                        $defectiveOrder += $salesman->defective_order;
                        Salesman::where('id',$salesman->id)->update(['defective_order' => $defectiveOrder]);
                    }else{
                        return response()->json(['status'=>401, 'message' => 'Salesman Not Found']);
                    }
                }


                $markDefective = Order::where('id',$value->id)
                    ->update(['defective_by_id' => $request->defective_by_id,'defective_by_role'=>$request->defective_by_role,'reason'=>$request->reason,'status' => '5','comment'=>$request->comment]);

            }

            if(!empty($markDefective)){
                return response()->json(['status'=>200, 'message' => 'Order Mark As Defective Successfully..!']);
            }else{
                return response()->json(['status'=>401, 'message' => 'Failed To Mark As Defective']);
            }
            
        }else{
            return response()->json(['status'=>401, 'message' => 'Please Contact To Salesman For Inquiry']);            
        }
    }
    function getSalesmanOrders(Request $request){
        $validator = Validator::make($request->all(), [  
            'salesman_id' => 'required'
        ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        //$is_salesman_id = Salesman::where('type',3)->orWhere('type',7)->find($request->salesman_id);
        $is_salesman_id = Salesman::where('type',3)->where('id',$request->salesman_id)
                                    ->orWhere('type',7)->where('id',$request->salesman_id)->first();

        if($is_salesman_id){
            $allOrders = Order::where('order_by',$is_salesman_id->id)->groupBy('order_unique_id')->orderBy('id', 'desc')->get();
            foreach ($allOrders as &$value) {
                $value['order_created_date'] = strtotime($value->created_at)*1000;
            }

            if($allOrders->isNotEmpty()){
                return response()->json(['status'=>200, 'result'=>$allOrders, 'message' => 'Get All Orders']); 
            }else{
                return response()->json(['status'=>404, 'message' => 'Order Not Found']);                 
            }
        }else{
           return response()->json(['status'=>404, 'message' => 'Salesman Not Found']); 
        }
    }

}