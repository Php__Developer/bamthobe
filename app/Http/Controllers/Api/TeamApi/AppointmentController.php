<?php

namespace App\Http\Controllers\Api\TeamApi;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomerAppointment;
use App\Models\Order;
use App\Models\appointmentComments;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
	public function getAppointments(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',	
		    'status' => 'required'	
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			if($request['status']=='1')
			{
			    $appointments = CustomerAppointment::where('status',$request['status'])
			    				->where('accepted_by',$request->user_id)
			    				->Orwhere('accepted_by',0)
			    				->where('status',$request['status'])
			    				->get();
			}
			else if($request['status']=='2')
			{
				$appointments = CustomerAppointment::where('status',$request['status'])
				->where('visit_by',$request['user_id'])->get();
			}
			else if($request['status']=='0')
			{
				$appointments = CustomerAppointment::where('status',$request['status'])
				->where('visit_by',$request['user_id'])->get();
			}
			if($appointments->count() >0){
				foreach ($appointments as &$value) {
					$value['created_date'] = strtotime($value->created_at)*1000;

				}
			    return response()->json(['result'=>$appointments, 'status'=>200, 'message' => 'Appointments']);
		    }
		    else{
		    	return response()->json(['status'=>404, 'message' => 'No Appointment found..!']);
		    }
		}
    }

    public function acceptedAppointment(Request $request){
    	$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',
		    'appointment_id' => 'required'
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		CustomerAppointment::where('id',$request['appointment_id'])
                            ->update(['accepted_by' => $request->user_id]);

		return response()->json(['status'=>200, 'message' => 'Appointment Accepted.']);
    }

    public function visitToAppointment(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',
		    'appointment_id' => 'required'
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$appointments = CustomerAppointment::where('id',$request['appointment_id'])
                            ->update([
                                    'visit_by' => $request->user_id,
                                    'status' => '2',
                                ]);
			if(!empty($appointments)){
			    return response()->json(['result'=>$appointments, 'status'=>200, 'message' => 'Appointment Booked']);
		    }
		    else{
		    	return response()->json(['status'=>404, 'message' => 'No Appointment found..!']);
		    }
		}
    }

    public function cancleAppointment(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',
		    'appointment_id' => 'required',
		    'reason' => 'required'
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$appointments = CustomerAppointment::where('id',$request['appointment_id'])
                            ->update([
                                    'visit_by' => $request->user_id,
                                    'status' => '0',
                                    'reason' => $request->reason,
                                ]);
			if(!empty($appointments)){
			    return response()->json(['result'=>$appointments, 'status'=>200, 'message' => 'Appointment Cacelled..!']);
		    }
		    else{
		    	return response()->json(['status'=>404, 'message' => 'No Appointment found..!']);
		    }
		}
    }

    public function addComment(Request $request){
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',
		    'appointment_id' => 'required', 
			'comment' => 'required'				
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
            if($request->file('comment_image')){
				$image = $request->file('comment_image');
	            $data['comment_image'] = time().'.'.$image->getClientOriginalExtension();
	            $destinationPath = public_path('/uploads/appointmentComments');
	            $image->move($destinationPath, $data['comment_image']);            	
            }			
			$data['user_id'] = $request->user_id;
			$data['appointment_id'] = $request->appointment_id;
            $data['comment'] = $request->comment;
			$comment = appointmentComments::create($data);
			if($comment->count() >0)
            {
            	return response()->json(['status'=>200, 'message' => 'Comment successfully done..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
            }
		}
	}

	public function getComments(Request $request){
	    $validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',
		    'appointment_id' => 'required'			
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
            $getcomments = appointmentComments::where('appointment_id',$request['appointment_id'])
                ->get();
            if($getcomments->count() >0)
            {
                $commentArray = [];
                foreach ($getcomments as $value) {
                	$key['id'] = $value['id'];
                	$key['user_id'] = $value['user_id'];
                	$key['appointment_id'] = $value['appointment_id'];
                	$key['comment'] = $value['comment'];
                	$key['comment_image'] = url('public/uploads/appointmentComments').'/'.$value['comment_image'];
                	$stamp1 = strtotime($value['created_at']);
                    $key['created_at'] = $stamp1*1000;
                	$commentArray[] = $key;
                }
                return response()->json(['result' =>$commentArray, 'status'=>200, 'message' => 'Comments..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'No comment found..!']);
            }
		}		
	}
}