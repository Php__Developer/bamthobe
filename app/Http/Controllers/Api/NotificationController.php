<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Order;	

class NotificationController extends Controller
{
    public function notification(){
    	$notification = Notification::select('id','title','message','created_at','updated_at')->get();
    	return response()->json(['result'=>$notification, 'status'=>200]);
	}


	public function teamNotification(){
    	$noti = Notification::select('id','title','message','created_at','updated_at')->get();
    	$orderStatus = Order::select('order_unique_id','status')->where('order_by',2)->get();
    	$notification =array('noti'=>$noti,'orderStatus'=>$orderStatus);
    	return response()->json(['result'=>$notification, 'status'=>200]);
	}
}
