<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotion;

class PromotionController extends Controller
{
 
 public function getPromotions(){
 	$promotions = Promotion::get();
 	$promotionArray = [];
            foreach($promotions as $promo){
                $promoval['title'] = $promo['title'];
                $promoval['description'] = $promo['description'];
                $stamp = strtotime($promo['valid_till']);
                $promoval['valid_till'] = $stamp*1000;
                $promotionArray[] = $promoval;
        }
        return response()->json(['result'=>$promotionArray, 'status'=>200, 'message' => 'Promotions Messages']);
    }
}
