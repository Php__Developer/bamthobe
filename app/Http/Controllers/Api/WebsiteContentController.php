<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WebsiteContent;

class WebsiteContentController extends Controller
{
    public function termsAndCondition(){
		$websiteContent = WebsiteContent::Select('title','content')->where('title','Terms and Conditions')->first();
		return response()->json(['result'=>$websiteContent, 'status'=>200]);

	}

	public function aboutUs(){
		$websiteContent = WebsiteContent::Select('title','content')->where('title','About Us')->first();
		return response()->json(['result'=>$websiteContent, 'status'=>200]);

	}

	public function privacyPolicy(){
		$websiteContent = WebsiteContent::Select('title','content')->where('title','Privacy policy')->first();
		return response()->json(['result'=>$websiteContent, 'status'=>200]);

	}
}
