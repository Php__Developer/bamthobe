<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;


class PaymentController extends AdminController
{
    
    public function __construct()
    {
        $model = new Order();
        $this->tableName =  $model->table;
        $this->ModuleName = 'orders';
    }

    public function index()
    {
        $orders = Order::where('status','!=','0')->groupBy('order_unique_id')->get();

        $totalAmount = 0;
        $totalPaidAmount = 0;
        $totalRemainingAmount = 0;

        foreach ($orders as $value) {
            $totalAmount += $value->total_amount;
            $totalPaidAmount += $value->amount_to_pay;
            $totalRemainingAmount += $value->price_remaining;
        }
        return view('admin.payments.index', compact(['totalAmount','totalPaidAmount','totalRemainingAmount']));
    }
}