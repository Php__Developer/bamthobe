<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\User;
use App\Customer;
use App\Models\Order;
use App\Models\Business_order;
use App\Models\Assignments;
use App\Models\Trackorder;
use App\Models\tax;
use App\Models\Branch;
use App\Models\Factory;
use Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Edujugon\PushNotification\PushNotification;

class DashBoardController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;
    
	// public function __construct(){
 //        $this->middleware('auth:admin');
 //    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $id = $request->input('id');
        #All Orders
        if ($id == '') {
            //Delay order
            $bufferDays = tax::select('buffer_days')->first();
            $bufferDay = $bufferDays->buffer_days;
            $in = array('0','3','4','5');
            $orders = Order::whereNotIn('status',$in)
            ->groupBy('order_unique_id')
            ->where('expected_delivery_time','>=',date('Y-m-d'))
            ->get();
            // $delayOrder = 0;
            //     foreach ($orders as $order) {
            //         $expectedDate = date('Y-m-d',strtotime($order->expected_delivery_time));
            //         $internalExpected = date('Y-m-d', strtotime($expectedDate. ' - '.$bufferDay.' days'));
            //         $today = date('Y-m-d');
            //         if($today == $internalExpected){
            //             $count = 1;
            //             $delayOrder += $count;
            //         }
            // }
            $delayOrder = count($orders);

            //Branch waise placed order
            $branchePlaceOrders = Branch::with(['orderWithSale' => function($query) {
                $query->groupBy('orders.order_unique_id');
            }])->get();
            $PlaceOrder =array();
            foreach ($branchePlaceOrders as $branch) {
                $orderCount = $branch->orderWithSale->count();
                $PlaceOrder[$branch->branch] = $orderCount;
            }
            $braPlaceOrder = array_sum($PlaceOrder);

            // //Total place order
            // $orders = Order::where('status','!=','0')
            // ->with('orderBy')
            // ->groupBy('order_unique_id')
            // ->get();
            // $totalPlaceOrders = $orders->count();
            
            //Total b2b orders
            $borders = Business_order::where('status','!=','0')
            // ->whereDate('created_at', '=', date('Y-m-d'))
            ->with('orderBy')
            ->groupBy('order_unique_id')
            ->get();
            $bbOrders = $borders->count();


            //Ready to assign
            $OrderReadyToAssign = Order::where('assignment',0)->where('status','!=','0')->get();
            $totalReadyToAssign = $OrderReadyToAssign->count();

            //Completed order
            $GetTotalOrderCompleted = Assignments::where('order_status', '1')->where('qc_status','2')->get();
            $totalOrderCompleted = $GetTotalOrderCompleted->count();

            //Defective order
            $defectByCustomer = Order::where('status','5')->groupBy('order_unique_id')->get();
            $defectByQc = Assignments::where('order_status', '0')->where('qc_status', '2')->get();
            $totalDefective = $defectByCustomer->count() + $defectByQc->count();
            
            //With cutter
            $cutterOrder = Assignments::where('status', '1')->get();
            $withCutter = $cutterOrder->count();

            //Revenue
            $orderRev = Order::where('status','!=','0')
            // ->whereDate('created_at', Carbon::today())
            ->groupBy('order_unique_id')->get();
            $revenue = 0;
            $cash = 0;
            $credit = 0;
            foreach($orderRev as $order){
                $revenue += $order->total_amount;
                $cash += $order->amount_to_pay;
                $credit += $order->price_remaining;
            }

            //In factory
            $factory = Trackorder::where('with_tailor', '1')
            ->where('with_qa', '0')
            ->orwhere('with_qa', '1')
            ->where('redy_for_delivery', '0')
            ->get();
            $orderInFactory = $factory->count();

            //Branch Revenue
            $branchWithOrderRev = Branch::with(['orderWithSale' => function($query) {
                $query->groupBy('orders.order_unique_id');
            }])->get();

            if($branchWithOrderRev->count() >0)
            { 
                foreach ($branchWithOrderRev as &$branch) {
                    $totalRevenue = 0;
                    foreach ($branch->orderWithSale as $order) {
                        $total_amount = $order['total_amount'];
                        $totalRevenue += $total_amount;
                    }
                   $branch['total_branch_rev'] =  $totalRevenue;
                }
            }
            else{
                $branchWithOrderRev = array();
            }
            //incomebranch
               $orders = Order::where('status','!=','0')
              ->groupBy('order_unique_id')
             ->get();
            
            $incomebranch = $orders->count();

            //Realeased Orders
            $orders = Order::where('status','!=','0')
            ->with('releasedAssignments')
            ->groupBy('order_unique_id')
            ->get();
            // print_r(json_encode($orders));die();
            $realeased = $orders->count();

            $data['id'] = $id;
            $data['bbOrders'] = $bbOrders;
            $data['realeased'] = $realeased;
            $data['incomebranch'] = $incomebranch;
            $data['braPlaceOrder'] = $braPlaceOrder;
            $data['totalReadyToAssign'] = $totalReadyToAssign;
            $data['totalOrderCompleted'] = $totalOrderCompleted;
            $data['totalDefective'] = $totalDefective;
            $data['withCutter'] = $withCutter;
            $data['branchWithOrderRev'] = $branchWithOrderRev;
            $data['revenue'] = $revenue;
            $data['cash'] = $cash;
            $data['credit'] = $credit;
            $data['orderInFactory'] = $orderInFactory;
            $data['delayOrder'] = $delayOrder;

            return view('admin.Dashboard.dashboard')->with('data', $data);
        }
        #Daily Orders
        elseif ($id == 1) {
            //Delay order
            $bufferDays = tax::select('buffer_days')->first();
            $bufferDay = $bufferDays->buffer_days;
            $in = array('0','3','4','5');
            $orders = Order::whereNotIn('status',$in)
            ->groupBy('order_unique_id')
            ->where('expected_delivery_time','>=',date('Y-m-d'))
            // ->whereDate('expected_delivery_time', '>=', date('Y-m-d'))
            ->where('created_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 1 DAY)'))
            ->get();
            $delayOrder = count($orders);

            //Total place order
            $orders = Order::where('status','!=','0')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->with('orderBy')
            ->groupBy('order_unique_id')
            ->get();
            $totalPlaceOrders = $orders->count();
            
            //Total b2b orders
            $borders = Business_order::where('status','!=','0')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->with('orderBy')
            ->groupBy('order_unique_id')
            ->get();
            $bbOrders = $borders->count();
            //Branch waise placed order
            $branchePlaceOrders = Branch::with(['orderWithSale' => function($query) {
                $query->groupBy('orders.order_unique_id');
            }])->get();
            $PlaceOrder =array();
            foreach ($branchePlaceOrders as $branch) {
                $orderCount = $branch->orderWithSale->count();
                $PlaceOrder[$branch->branch] = $orderCount;
            }
            $braPlaceOrder = array_sum($PlaceOrder);

            //Ready to assign
            $OrderReadyToAssign = Order::where('assignment',0)->where('status','!=','0')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->get();
            $totalReadyToAssign = $OrderReadyToAssign->count();

            //Completed order
            $GetTotalOrderCompleted = Assignments::where('order_status', '1')
            ->where('qc_status','2')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->get();
            $totalOrderCompleted = $GetTotalOrderCompleted->count();

            //Defective order
            $defectByCustomer = Order::where('status','5')
            ->groupBy('order_unique_id')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->get();
            $defectByQc = Assignments::where('order_status', '0')
            ->where('qc_status', '2')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->get();
            $totalDefective = $defectByCustomer->count() + $defectByQc->count();
            
            //With cutter
            $cutterOrder = Assignments::where('status', '1')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->get();
            $withCutter = $cutterOrder->count();

            //Revenue
            $orderRev = Order::where('status','!=','0')
            ->whereDate('created_at', '=', date('Y-m-d'))
            // ->whereDate('created_at', Carbon::today())
            ->groupBy('order_unique_id')
            ->get();
            $revenue = 0;
            $cash = 0;
            $credit = 0;
                foreach($orderRev as $order){
                    $revenue += $order->total_amount;
                    $cash += $order->amount_to_pay;
                    $credit += $order->price_remaining;
                }

            //In factory
            $factory = Trackorder::where('with_tailor', '1')
            ->where('with_qa', '0')
            ->orwhere('with_qa', '1')
            ->where('redy_for_delivery', '0')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->get();
            $orderInFactory = $factory->count();

            //Branch Revenue
            $branchWithOrderRev = Branch::with(['orderWithSale' => function($query) {
                $query->whereDate('orders.created_at', Carbon::today())->groupBy('orders.order_unique_id');
            }])->get();

            if($branchWithOrderRev->count() >0)
            { 
                foreach ($branchWithOrderRev as &$branch) {
                    $totalRevenue = 0;
                    foreach ($branch->orderWithSale as $order) {
                        $total_amount = $order['total_amount'];
                        $totalRevenue += $total_amount;
                    }
                   $branch['total_branch_rev'] =  $totalRevenue;
                }
            }
            else{
                $branchWithOrderRev = array();
            }

            $orders = Order::where('status','!=','0')
            ->with('releasedAssignments')
            ->groupBy('order_unique_id')
            ->get();
            // print_r(json_encode($orders));die();
            $realeased = $orders->count();

            $data['bbOrders'] = $bbOrders;
            $data['totalPlaceOrders'] = $totalPlaceOrders;
            $data['realeased'] = $realeased;
            $data['braPlaceOrder'] = $braPlaceOrder;
            $data['totalReadyToAssign'] = $totalReadyToAssign;
            $data['totalOrderCompleted'] = $totalOrderCompleted;
            $data['totalDefective'] = $totalDefective;
            $data['withCutter'] = $withCutter;
            $data['branchWithOrderRev'] = $branchWithOrderRev;
            $data['revenue'] = $revenue;
            $data['cash'] = $cash;
            $data['credit'] = $credit;
            $data['orderInFactory'] = $orderInFactory;
            $data['delayOrder'] = $delayOrder;
            // print_r($data['braPlaceOrder']);die();
            return view('admin.Dashboard.dashboard')->with('data', $data);
        }
        #Weekly Orders
        elseif ($id == 2) {
            //Delay order
            $bufferDays = tax::select('buffer_days')->first();
            $bufferDay = $bufferDays->buffer_days;
            $in = array('0','3','4','5');
            $orders = Order::whereNotIn('status',$in)
            ->groupBy('order_unique_id')
            ->where('expected_delivery_time','>=',date('Y-m-d'))
            // ->whereDate('expected_delivery_time', '>=', date('Y-m-d'))
            ->where('created_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 1 WEEK)'))
            ->get();
            // print_r(count($orders));die();
            // $delayOrder = 0;
            //     foreach ($orders as $order) {
            //         $expectedDate = date('Y-m-d',strtotime($order->expected_delivery_time));
            //         $internalExpected = date('Y-m-d', strtotime($expectedDate. ' - '.$bufferDay.' days'));
            //         $today = date('Y-m-d');
            //         if($today == $internalExpected){
            //             $count = 1;
            //             $delayOrder += $count;
            //         }
            // }
            $delayOrder = count($orders);
            $today = strtotime(date('Y-m-d'));
            $weeklydate = strtotime("-6 day", $today);
            $start = date('Y-m-d', $today);
            $end = date('Y-m-d', $weeklydate);
            // print_r($start);die();
            //Total place order
            $orders = Order::where('status','!=','0')
            ->whereBetween('created_at', [$start, $end])
            // ->whereDate('created_at', '>=', $week)
            ->with('orderBy')
            ->groupBy('order_unique_id')
            ->get();
            // print_r($orders);die();
            $totalPlaceOrders = $orders->count();
            
            $borders = Business_order::where('status','!=','0')
            ->whereBetween('created_at', [$start, $end])
            ->with('orderBy')
            ->groupBy('order_unique_id')
            ->get();
            $bbOrders = $borders->count();

            //Branch waise placed order
            $branchePlaceOrders = Branch::with(['orderWithSale' => function($query) {
                $query->groupBy('orders.order_unique_id');
            }])->get();
            $PlaceOrder =array();
            foreach ($branchePlaceOrders as $branch) {
                $orderCount = $branch->orderWithSale->count();
                $PlaceOrder[$branch->branch] = $orderCount;
            }
            $braPlaceOrder = array_sum($PlaceOrder);

            //Ready to assign
            $OrderReadyToAssign = Order::where('assignment',0)->where('status','!=','0')
            ->whereBetween('created_at', [$start, $end])
            ->get();
            $totalReadyToAssign = $OrderReadyToAssign->count();

            //Completed order
            $GetTotalOrderCompleted = Assignments::where('order_status', '1')
            ->where('qc_status','2')
            ->whereBetween('created_at', [$start, $end])
            ->get();
            $totalOrderCompleted = $GetTotalOrderCompleted->count();

            //Defective order
            $defectByCustomer = Order::where('status','5')
            ->groupBy('order_unique_id')
            ->whereBetween('created_at', [$start, $end])
            ->get();
            $defectByQc = Assignments::where('order_status', '0')
            ->where('qc_status', '2')
            ->whereBetween('created_at', [$start, $end])
            ->get();
            $totalDefective = $defectByCustomer->count() + $defectByQc->count();
            
            //With cutter
            $cutterOrder = Assignments::where('status', '1')
            ->whereBetween('created_at', [$start, $end])
            ->get();
            $withCutter = $cutterOrder->count();

            //Revenue
            $orderRev = Order::where('status','!=','0')
            ->whereBetween('created_at', [$start, $end])
            // ->whereDate('created_at', Carbon::today())
            ->groupBy('order_unique_id')
            ->get();
            $revenue = 0;
            $cash = 0;
            $credit = 0;
                foreach($orderRev as $order){
                    $revenue += $order->total_amount;
                    $cash += $order->amount_to_pay;
                    $credit += $order->price_remaining;
                }

            //In factory
            $factory = Trackorder::where('with_tailor', '1')
            ->where('with_qa', '0')
            ->orwhere('with_qa', '1')
            ->where('redy_for_delivery', '0')
            ->whereBetween('created_at', [$start, $end])
            ->get();
            $orderInFactory = $factory->count();

            //Branch Revenue
            $branchWithOrderRev = Branch::with(['orderWithSale' => function($query) {
                $currentDate = \Carbon\Carbon::now();
                $nowDate = $currentDate->subDays($currentDate->dayOfWeek + 1);
                $query->whereBetween('orders.created_at', [$currentDate, $nowDate])->groupBy('orders.order_unique_id');
            }])->get();

            if($branchWithOrderRev->count() >0)
            { 
                foreach ($branchWithOrderRev as &$branch) {
                    $totalRevenue = 0;
                    foreach ($branch->orderWithSale as $order) {
                        $total_amount = $order['total_amount'];
                        $totalRevenue += $total_amount;
                    }
                   $branch['total_branch_rev'] =  $totalRevenue;
                }
            }
            else{
                $branchWithOrderRev = array();
            }
              //incomebranch
               $orders = Order::where('status','!=','0')
              ->groupBy('order_unique_id')
             ->get();
            
            $incomebranch = $orders->count();

            $orders = Order::where('status','!=','0')
            ->with('releasedAssignments')
            ->groupBy('order_unique_id')
            ->get();
            // print_r(json_encode($orders));die();
            $realeased = $orders->count();

            $data['bbOrders'] = $bbOrders;
            $data['totalPlaceOrders'] = $totalPlaceOrders;
            $data['braPlaceOrder'] = $braPlaceOrder;
            $data['totalReadyToAssign'] = $totalReadyToAssign;
            $data['totalOrderCompleted'] = $totalOrderCompleted;
            $data['totalDefective'] = $totalDefective;
            $data['withCutter'] = $withCutter;
            $data['branchWithOrderRev'] = $branchWithOrderRev;
            $data['revenue'] = $revenue;
            $data['cash'] = $cash;
            $data['credit'] = $credit;
            $data['orderInFactory'] = $orderInFactory;
            $data['delayOrder'] = $delayOrder;
            $data['realeased'] = $realeased;
            $data['incomebranch'] = $incomebranch;
            // print_r(json_encode($data));die();
            return view('admin.Dashboard.dashboard')->with('data', $data);
        }
        #Monthly Orders
        elseif ($id == 3) {
            //Delay order
            $bufferDays = tax::select('buffer_days')->first();
            $bufferDay = $bufferDays->buffer_days;
            $in = array('0','3','4','5');
            $orders = Order::whereNotIn('status',$in)
            ->groupBy('order_unique_id')
            ->where('expected_delivery_time','>=',date('Y-m-d'))
            // ->whereDate('expected_delivery_time', '<=', date('Y-m-d'))
            ->where('created_at', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 1 MONTH)'))
            ->get();
            $delayOrder = count($orders);

            //Total place order
            $orders = Order::where('status','!=','0')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->with('orderBy')
            ->groupBy('order_unique_id')
            ->get();
            // print_r($orders);die();
            $totalPlaceOrders = $orders->count();
            
            $borders = Business_order::where('status','!=','0')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->with('orderBy')
            ->groupBy('order_unique_id')
            ->get();
            $bbOrders = $borders->count();

            //Branch waise placed order
            $branchePlaceOrders = Branch::with(['orderWithSale' => function($query) {
                $query->groupBy('orders.order_unique_id');
            }])->get();
            $PlaceOrder =array();
            foreach ($branchePlaceOrders as $branch) {
                $orderCount = $branch->orderWithSale->count();
                $PlaceOrder[$branch->branch] = $orderCount;
            }
            $braPlaceOrder = array_sum($PlaceOrder);

            //Ready to assign
            $OrderReadyToAssign = Order::where('assignment',0)
            ->where('status','!=','0')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->get();
            $totalReadyToAssign = $OrderReadyToAssign->count();

            //Completed order
            $GetTotalOrderCompleted = Assignments::where('order_status', '1')
            ->where('qc_status','2')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->get();
            $totalOrderCompleted = $GetTotalOrderCompleted->count();

            //Defective order
            $defectByCustomer = Order::where('status','5')
            ->groupBy('order_unique_id')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->get();
            $defectByQc = Assignments::where('order_status', '0')
            ->where('qc_status', '2')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->get();
            $totalDefective = $defectByCustomer->count() + $defectByQc->count();
            
            //With cutter
            $cutterOrder = Assignments::where('status', '1')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->get();
            $withCutter = $cutterOrder->count();

            //Revenue
            $orderRev = Order::where('status','!=','0')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            // ->whereDate('created_at', Carbon::today())
            ->groupBy('order_unique_id')
            ->get();
            $revenue = 0;
            $cash = 0;
            $credit = 0;
                foreach($orderRev as $order){
                    $revenue += $order->total_amount;
                    $cash += $order->amount_to_pay;
                    $credit += $order->price_remaining;
                }

            //In factory
            $factory = Trackorder::where('with_tailor', '1')
            ->where('with_qa', '0')
            ->orwhere('with_qa', '1')
            ->where('redy_for_delivery', '0')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->get();
            $orderInFactory = $factory->count();

            //Branch Revenue
            $branchWithOrderRev = Branch::with(['orderWithSale' => function($query) {
                $query->whereDate('orders.created_at','<=', Carbon::today()->subMonth()->month)
                ->groupBy('orders.order_unique_id');
            }])->get();

            if($branchWithOrderRev->count() >0)
            { 
                foreach ($branchWithOrderRev as &$branch) {
                    $totalRevenue = 0;
                    foreach ($branch->orderWithSale as $order) {
                        $total_amount = $order['total_amount'];
                        $totalRevenue += $total_amount;
                    }
                   $branch['total_branch_rev'] =  $totalRevenue;
                }
            }
            else{
                $branchWithOrderRev = array();
            }
              //incomebranch
               $orders = Order::where('status','!=','0')
              ->groupBy('order_unique_id')
             ->get();
            
            $incomebranch = $orders->count();

            #released order
            $orders = Order::where('status','!=','0')
            ->with('releasedAssignments')
            ->groupBy('order_unique_id')
            ->get();
            $realeased = $orders->count();

            $data['bbOrders'] = $bbOrders;
            $data['totalPlaceOrders'] = $totalPlaceOrders;
            $data['braPlaceOrder'] = $braPlaceOrder;
            $data['totalReadyToAssign'] = $totalReadyToAssign;
            $data['totalOrderCompleted'] = $totalOrderCompleted;
            $data['totalDefective'] = $totalDefective;
            $data['withCutter'] = $withCutter;
            $data['branchWithOrderRev'] = $branchWithOrderRev;
            $data['revenue'] = $revenue;
            $data['cash'] = $cash;
            $data['credit'] = $credit;
            $data['orderInFactory'] = $orderInFactory;
            $data['delayOrder'] = $delayOrder;
            $data['realeased'] = $realeased;
            $data['incomebranch'] = $incomebranch;

            // print_r(json_encode($data));die();
            return view('admin.Dashboard.dashboard')->with('data', $data);
        }
    }
    
}