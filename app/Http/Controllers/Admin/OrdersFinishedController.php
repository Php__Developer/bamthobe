<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Models\Order;
use App\Models\Comment;

class OrdersFinishedController extends AdminController
{
    
    public function __construct()
    {
        $model = new Order();
        $this->tableName =  $model->table;
        $this->ModuleName = 'orders';
    }

    public function index()
    {
        return view('admin.orders.finished');  
    }

    public function table_data(Request $request)
    {
        $orders = Order::where('status','3')
                        ->where('price_remaining','0')->get();

        $datatables = Datatables::of($orders)

        ->editColumn('status',function ($orders){
            $status = '<span class="label label-success">Finished</span>';

              $html = $status;
              return $html;
        })
        ->editColumn('order_by',function ($orders){
            if(isset($orders->orderBy)){
                $html = $orders->orderBy->title;
            }else{
                $html ='--';
            }

              return $html;
        })

        ->editColumn('delivered_at',function ($orders){
            if($orders->delivered_at){
              $html = formatDate($orders->delivered_at);
            }else{
                $html = '--';
            }

            return $html;
        })

        ->editColumn('branch',function ($orders){
            if(isset($orders->orderBy['branchdata'])){
                $html = $orders->orderBy->branchdata['branch'];
            }else{
                $html = '--';
            }
            return $html;
        })
        ->addColumn('action',function ($orders){
            $html='<a target="_blank" class="btn btn-primary" title="View Details" href="'.route('admin.orderDetails', ["orderId" => $orders->order_unique_id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            return $html;
        })
        ->editColumn('created_at',function($orders)
        {
            $html = formatDate($orders->created_at);
            return $html;
        })
        ->rawColumns(['order_unique_id','customer_unique_id','order_by','quantity','order_date','status', 'action']);
        return $datatables->make(true);
    }

}
