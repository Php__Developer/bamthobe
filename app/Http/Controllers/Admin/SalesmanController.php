<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Payroll;
use App\Models\Salary;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class SalesmanController extends AdminController
{
    
    public function __construct()
    {
        $model = new Salesman();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Salesman';
    }

    public function index()
    {
        return view('admin.salesmans.index');  
    }

    public function table_data(Request $request)
    {
        //$salesmans = Salesman::Select('*');
        $salesmans = Salesman::with('teamtype')
                                ->where('type','3')->orwhere('type','7')
                                ->get();
                        //dd('$salesmans');
        $datatables = Datatables::of($salesmans)
        ->editColumn('image_name',function($salesman){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($salesman->image_name){
                $html .= '<a href="'.asset('uploads/salesmans/'.$salesman->image_name).'"
                data-lightbox="image-'.$salesman->id.'" data-title=""><img src="'.asset('uploads/salesmans/'.$salesman->image_name).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$salesman->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('title',function ($salesman){
              $html = str_limit($salesman->title,20,'....');
              return $html;
        })
        ->editColumn('monthly_target',function ($salesman){
              $html = number_format($salesman->monthly_target,2, ".", ",");
              return $html;
        })
        ->editColumn('type',function ($salesman){
              $html = str_limit($salesman->teamtype->typename,20,'....');
              return $html;
        })
        ->editColumn('branch',function ($salesman){
              $html = $salesman->branchdata['branch'];
              return $html;
        })
        ->editColumn('evaluation',function ($salesman){

            $allEvaluation = Payroll::where('emp_id',$salesman->emp_id)->get();
            $totalEvaluation =0;

            if($allEvaluation->isNotEmpty()){
                $count = $allEvaluation->count();
                
                foreach ($allEvaluation as $evaluation) {
                   $totalEvaluation += $evaluation->evaluation;
                }

                $html = $totalEvaluation/$count;
            }else{
               $html = $totalEvaluation; 
            }

              return $html;
        })
        ->editColumn('status',function ($salesman){
            $html = '<label class="switch" id="changeStatus" module-id="'.$salesman->id.'">';
            if($salesman->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($salesman){
            $html='<a class="btn btn-primary" value="'.$salesman->id.'" href="'.route('salesmans.edit',$salesman->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
            '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$salesman->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($salesman)
        {
            $html = formatDate($salesman->created_at);
            return $html;
        })
        ->editColumn('updated_at',function($salesman)
        {
            $html = formatDate($salesman->updated_at);
            return $html;
        })
        ->rawColumns(['image_name','title','description','status', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create salesman
     */
    public function create()
    {
        // $userType = Teamtype::
        //             where('id','3')
        //             ->orwhere('id','7')
        //             ->pluck('typename','id');

        $branchs = Branch::pluck('branch','id');

        $app = app();
        $salesman = $app->make('stdClass');
        $salesman->id = -1;
        $salesman->image_name = '';
        $salesman->email = '';
        $salesman->title = '';
        $salesman->description = '';
        $salesman->status=1;
        $salesman->phone = '';
        $salesman->branch = '';
        $salesman->monthly_target = '';
        $salesman->type = '';
        $salesman->igama_renewal_date = '';
        return view('admin.salesmans.edit')->with('heading','Create')->with('salesman',$salesman)
                //->with('userType',$userType)
                ->with('branchs',$branchs);
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id  = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Salesman::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($salesman_id) {
        if(!empty($salesman_id) && is_numeric($salesman_id)) {
            $value = Salesman::find($salesman_id);
            // $userType = Teamtype::where('id','3')
            // ->orwhere('id','7')->pluck('typename','id');

            $branchs = Branch::pluck('branch','id');

            $id = $value['id'];
            if( $id == $salesman_id ) {
               $salesman = DB::table($this->tableName)->select('*')->where('id',$salesman_id)->first();
             return view('admin.salesmans.edit')->with('salesman',$salesman)->with('heading','Edit')
                    //->with('userType',$userType)
                    ->with('branchs',$branchs);
            } else {
                return redirect()->route('salesmans.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('salesmans.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$salesman_id) {
        //print_r($request->all());die;
        if($salesman_id>0)
            {
                request()->validate([
                    'title'         => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('salesmans.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image_name');
                        $data['image_name'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/salesmans');
                        $checkPrevious = Salesman::where('id', $salesman_id)->first()->image_name;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image_name']);
                    }
                }
                $data['title'] = $request->get('title');
                $data['email'] = $request->get('email');
                $data['phone'] = $request->get('phone');
                $data['description']  = $request->get('description');
                $data['status']       = $request->get('status');
                $data['branch'] = $request->get('branch');
                $data['monthly_target'] = $request->get('monthly_target');
                $data['igama_renewal_date'] = $request->get('igama_renewal_date');
                //$data['type'] = $request->get('type');
                    if(Salesman::where('id', $salesman_id)->update($data)) {
                        return redirect()->route('salesmans.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('salesmans.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    'title'         => 'required',
                    'email'         =>'required|email',
                    'phone'         =>'required|numeric',
                  
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('image_name')) {
                        $mimetype = $request->file('image_name')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('salesmans.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('image_name');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/salesmans');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['image_name'] = $imageName;
                $request = $request->input();

                //Create dynamic emp_id
                $lastEmp = Salary::orderBy('id', 'desc')->first();                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request['title'];
                $request['emp_id'] = $empID;


                $request['type'] = 3;         

                //create password
                $passwordcreate = mt_rand(1000, 9999);
                $request['password'] = \Hash::make($passwordcreate);
                // $client = new \GuzzleHttp\Client();
                // $res = $client->request('POST', 'http://www.oursms.net/api/sendsms.php', [
                //     'form_params' => [
                //         'username' => 'bamthobe',
                //         'password' => 'bam123',
                //         'message' => $passwordcreate,
                //         'numbers' => "+966".$request['phone'],
                //         'sender' => 'BAM',
                //         'unicode' => 'E',
                //         'return' => 'full',
                //     ]
                // ]);
                //print_r($res);die;

                Salesman::create($request);
                Salary::create($salaryRequest);

                //Welcome mail
                $user['email']= $request['email'];
                $user['Dpassword'] = $passwordcreate;
                Mail::to($request['email'])->send(new WelcomeMail($user));
                // End welcome mail

                return redirect()->route('salesmans.index')->with('Success', 'Created Successfully');      
            }
        }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/salesmans');
                $deleteImg = Salesman::where('id', $id)->first()->image_name;
                if(Salesman::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('salesmans.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        $id   = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }
}
