<?php

namespace App\Http\Controllers\Admin;

Use App\Models\Faq;
Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class FaqController extends AdminController
{

    public function __construct()
    {
        $model = new Faq();
        $this->tableName = $model->table;
        $this->ModuleName = 'FAQ';
    }

	public function index() {
        return view('admin.faq.index');
    }

    public function table_data() {
        $faq = Faq::select('*');
        return Datatables::of($faq)
            ->editColumn('status',function ($faq){
                $html = '<label class="switch">';
                if($faq->status){
                    $html .= '<input onchange="changefaqStatus('.$faq->id.',0)" type="checkbox" checked="checked">';
                } else {
                    $html .= '<input type="checkbox" onchange="changefaqStatus('.$faq->id.',1)">';
                }
                $html .= '<span class="lever round"></span></label>';
                return $html;
            })
            ->addColumn('action',function ($faq){
                $html='<a class="btn btn-primary" title="Edit"  href="'.route('faq.edit',$faq->id).'" ><i class="fa fa-pencil" aria-hidden="true"></i></a>'.                       
                    '<button class="btn btn-danger btn-delete" id="deletefaq" title="Delete" value="'.$faq->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                return $html;
               })
            ->editColumn('question',function($faq){
            	$html = str_limit($faq->question, $limit = 20, $end = '...');
            	return $html;	
            })
            ->editColumn('answer',function($faq){
            	$html = str_limit($faq->answer, $limit = 50, $end = '...');
            	return $html;
            })
            ->editColumn('created_at',function($faq){
                $html = formatDate($faq->created_at);
                return $html;
            })
            ->editColumn('updated_at',function($faq){
                $html = formatDate($faq->updated_at);
                return $html;
            })
            ->escapeColumns([])
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    /*
     * For Add
     */
    public function create(Request $request){
        
        $app = app();
        $faq = $app->make('stdClass');
        $faq->id = -1;
        $faq->question='';
        $faq->answer='';
        $faq->status=1;
        $faq->sort_order='';
        $faq->faq_categories_id='';
        $faq_categories = DB::table('faq_categories')->select('*')->get(); 
        return view('admin.faq.edit',compact('faq','faq_categories'))->with('heading','Create');
    }

    /*
     * Change status
     */
    public function changeStatus(Request $request) {
        $faq_id   = $request->get('faq_id');
        if($faq_id && is_numeric(($faq_id))) {
            if(Faq::where('id', $faq_id)->update(array('status' => $request->get('status')))) {
                echo json_encode(array('status' => 'success', 'message' => trans('message.statusUpdated')));exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
    }

    /*
     * Get FAQ for Edit
     */
    public function edit($faq_id) {
        if(!empty($faq_id) && is_numeric($faq_id)) {
            $value = Faq::find($faq_id);
            $id = $value['id'];
            if( $id == $faq_id ) {
                $faq = DB::table($this->tableName)->select('*')->where('id',$faq_id)->first(); 
                 $faq_categories = DB::table('faq_categories')->select('*')->get(); 
                return view('admin.faq.edit',compact('faq','faq_categories'))->with('heading','Edit');
            } else {
                return redirect()->route('faq.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('faq.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Add/Update FAQ
     */
    public function update(Request $request,$faq_id) {
        if($faq_id!=-1) {// For edit 
            request()->validate([
                'question' => 'required',
                'answer' => 'required',
                'status' => 'required',
                'sort_order' => 'required',            
            ]);
            $request = $request->all();
            unset($request['_token']);
            unset($request['_method']);
            try {
                DB::table($this->tableName)
                    ->where('id', $faq_id)
                    ->update($request);
                return redirect()->route('faq.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
            }  catch (\Exception $ex) {
                dd($ex);
            }
        } else {// For create 
            $v = Validator::make($request->all(), [
                'question' => 'required',
                'answer' => 'required',
                'status' => 'required',
                'sort_order' => 'required',            
            ]);
            $request = $request->input();      
            Faq::create($request);
            return redirect()->route('faq.index')->with('success', $this->ModuleName.trans('message.AddedMsg'));
        }
    }

    /*
     * Delete FAQ
     */
    public function destroy($id) {
        Faq::destroy($id);
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
    }

    public function show()
    {
        return redirect()->route('faq.index')->with('error',trans('message.invalidId'));
    }

}