<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Models\Order;
use App\Models\Assignments;
use App\Models\Comment;

class DeliveredController extends AdminController
{
    
    public function __construct()
    {
        $model = new Order();
        $this->tableName =  $model->table;
        $this->ModuleName = 'orders';
    }

    public function index()
    {
        return view('admin.subdashboard.delivery');  
    }

    public function table_data(Request $request)
    {
        $orders = Order::where('status', '=', '3')
        ->get();

        $datatables = Datatables::of($orders)
        ->editColumn('completion_time',function ($orders){
         $completion = Assignments::where('order_unique_id',$orders->order_unique_id)->first();
            $html = $completion['tailor_completion_date'];
            return $html;
        })

        ->addColumn('waiting_time',function ($orders){
            $html = ($orders->delivered_at);
            return $html;
        })

        ->addColumn('action',function ($orders){
            $html='<a target="_blank" class="btn btn-primary" title="View Details" href="'.route('admin.orderDetails', ["orderId" => $orders->order_unique_id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a><button class="btn btn-danger btn-delete delete-records" value="'.$orders->order_unique_id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })

        ->editColumn('created_at',function($orders)
        {
            $html = formatDate($orders->created_at);
            return $html;
        })
        
        ->rawColumns(['order_unique_id','product_id','completion_time','delivery_time','waiting_time', 'action']);
        return $datatables->make(true);
    }
    /*
     * Show Record
    */

    public function details($orderId)
    {
        if(!empty($orderId)) {
            $order = Order::where('order_unique_id', $orderId)->get();
            if( !empty($order) ) {
                
                return view('admin.orders.details', compact('order'))->with('heading', 'Details');
            } else {
                return redirect()->route('orders.index')->with('error', "orders does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }

    public function addComment()
    {
        $input = request()->all();
        Comment::create($input);
        return response()->json(['success'=>'Comment added.']);

    }

    public function destroy($id) {
        $product = Order::where('order_unique_id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }

  


}
