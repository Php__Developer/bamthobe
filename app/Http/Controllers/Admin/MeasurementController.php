<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Measurement;

class MeasurementController extends AdminController
{
    
    public function __construct()
    {
        $model = new Measurement();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Measurement';
    }

    public function index()
    {
        return view('admin.Measurements.index');  
    }

    public function table_data(Request $request)
    {
        $measurement = Measurement::select('*');
        // dd($modelpricing);
        $datatables = Datatables::of($measurement)
        ->editColumn('image',function($measurement){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($measurement->image){
                $html .= '<a href="'.asset('uploads/measurements/'.$measurement->image).'"
                data-lightbox="image-'.$measurement->id.'" data-title=""><img src="'.asset('uploads/measurements/'.$measurement->image).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$measurement->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('price',function($measurement){
            $html = number_format($measurement->price,2, ".", ",");
            return $html;
        })
        ->editColumn('created_at',function($measurement)
        {
            $html = formatDate($measurement->created_at);
            return $html;
        })
        ->editColumn('updated_at',function($measurement)
        {
            $html = formatDate($measurement->updated_at);
            return $html;
        })
        ->addColumn('action',function ($measurement){
            $html='<a class="btn btn-primary" value="'.$measurement->id.'" href="'.route('measurement.edit',$measurement->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.'<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$measurement->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->rawColumns(['name','image','price','action']);
        return $datatables->make(true);
    }
   
    /**
     * Create Model
     */
    public function create()
    {
        $app = app();
        $measurement = $app->make('stdClass');
        $measurement->id = -1;
        $measurement->name = '';
        $measurement->image = '';
        $measurement->price = '';

        return view('admin.Measurements.edit')->with('heading','Create')->with('measurement',$measurement);
    }

    /**
     Edit Record
    **/

    public function edit($measurement_id) {
        // dd($measurement_id);
        if(!empty($measurement_id) && is_numeric($measurement_id)) {
            $value = Measurement::find($measurement_id);
            $id = $value['id'];
            if( $id == $measurement_id ) {
               $measurement = DB::table($this->tableName)->select('*')->where('id',$measurement_id)->first();
             return view('admin.Measurements.edit')->with('measurement',$measurement)->with('heading','Edit');
            } else {
                return redirect()->route('measurement.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('measurement.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$measurement_id) {
        if($measurement_id>0)
            {
                request()->validate([
                    'name'         => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image')) {
                    $mimetype = $request->file('image')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('measurement.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image');
                        $data['image'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/measurements');
                        $checkPrevious = Measurement::where('id', $measurement_id)->first()->image;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image']);
                    }
                }
                $data['name'] = $request->get('name');
                $data['price'] = $request->get('price');
                
                    if(Measurement::where('id', $measurement_id)->update($data)) {
                        return redirect()->route('measurement.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('measurement.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    'name'         => 'required',
                    'price'         =>'required',
                    'image'         =>'required'
                  
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('image')) {
                        $mimetype = $request->file('image')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('measurement.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('image');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/measurements');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['image'] = $imageName;
                $request = $request->input();

                Measurement::create($request);

                return redirect()->route('measurement.index')->with('Success', 'Created Successfully');      
            }
        }

    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/measurements');
                $deleteImg = Measurement::where('id', $id)->first()->image;
                if(Measurement::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
  
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('measurement.index')->with('error',trans('message.invalidId'));
    }


}
