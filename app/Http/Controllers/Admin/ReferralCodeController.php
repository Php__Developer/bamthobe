<?php

namespace App\Http\Controllers\Admin;

Use App\Models\ReferralCode;
Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;

class ReferralCodeController extends AdminController
{
     public function __construct()
    {
        $model = new ReferralCode();
        $this->tableName =  $model->table;
        $this->ModuleName = 'ReferralCode';
    }

    public function index()
    {
       return view('admin.referralCodes.index');
    }



    public function table_data(Request $request){
        $referralcodes  = ReferralCode::select('*');
        $datatables = Datatables::of($referralcodes)
            
             ->editColumn('name',function ($referralCode){
               $html = $referralCode->name;
               return $html;
            })
             ->editColumn('email',function ($referralCode){
               $html = $referralCode->email;
               return $html;
            })
             ->editColumn('code',function ($referralCode){
               $html = $referralCode->code;
               return $html;
            })
             
           ->editColumn('status',function ($referralCode){
                $html = '<label class="switch" id="changeStatus" module-id="'.$referralCode->id.'">';
                if($referralCode->status){
                    $html .= '<input type="checkbox" checked="checked">';
                } else {
                    $html .= '<input type="checkbox">';
                }
                $html .= '<span class="lever round"></span></label>';
                return $html;
            })
            ->addColumn('action',function ($referralCode){
                $html='<a class="btn btn-primary" value="'.$referralCode->id.'" href="'.route('referralCodes.edit',$referralCode->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <button class="btn btn-danger btn-delete delete-records" value="'.$referralCode->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                return $html;
            })
            ->editColumn('created_at',function($referralCode)
            {
                $html = formatDate($referralCode->created_at);
                return $html;
            })
         
            ->rawColumns(['name','email','code','status', 'action']);
            return $datatables->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){

        $app = app();
        $referralCode = $app->make('stdClass');
        $referralCode->id = -1;
        $referralCode->name = '';
        $referralCode->email = '';
        $referralCode->code = 'GT'.random_number(8);
        $referralCode->status=1;
       
        return view('admin.referralCodes.edit')->with('heading','Create')->with('referralCode',$referralCode);
    }



     /*
     * Change Status
     */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(ReferralCode::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => trans('message.statusUpdated')));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }

    /*
     * Edit Record
     */
    public function edit($referralCode_id) {
        if(!empty($referralCode_id) && is_numeric($referralCode_id)) {
            $value = ReferralCode::find($referralCode_id);
            $id = $value['id'];
            if( $id == $referralCode_id ) {
               $referralCode = DB::table($this->tableName)->select('*')->where('id',$referralCode_id)->first();
             return view('admin.referralCodes.edit')->with('referralCode',$referralCode)->with('heading','Edit');
            } else {
                return redirect()->route('referralCodes.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('referralCodes.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Update Record
     */
    public function update(Request $request,$referralCode_id) {
        if($referralCode_id>0)
            {
                request()->validate([
                    'name'         => 'required',
                    'email'        => 'required',
                    'code' =>         'required|unique:referral_codes,code,'. $referralCode_id .''
                ]);
                $data['name']         = $request->get('name');
                $data['email']        = $request->get('email');
                $data['code']         = $request->get('code');
                $data['status']       = $request->get('status');
                    if(ReferralCode::where('id', $referralCode_id)->update($data)) {
                        return redirect()->route('referralCodes.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('referralCodes.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else {
                $v = Validator::make($request->all(), [
                'name'         => 'required',
                'email'        => 'required',
                'code'         => 'required|unique:referral_codes,code'      
                ]);
                if ($v->fails()) {
            return back()->with('errors', $v->errors())->withInput();
        }else{

        
             $request = $request->input();
                ReferralCode::create($request);
                return redirect()->route('referralCodes.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg')); 
                }     
            }
         }
        /*
        * Delete record
        */
        public function destroy($id) {
        ReferralCode::destroy($id);
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }   
  

        /*
         * Check code existing
         */

        // public function checkCode() {
        //     $userid = Input::get('userid');
        //     $code = Input::get('code');
        //     if($code != "") {
        //         if($userid == '') {
        //             $checkExist = ReferralCode::where('code', '=', $code)->first();

        //         } else {
        //             $checkExist = ReferralCode::where([['code', '=', $code], ['id', '!=', $userid]])->first();
                    
        //         }
        //         if ($checkExist === null) {
        //             echo "true";
        //         } else {
        //             echo "false";
        //         }
        //     }
        // }



}
