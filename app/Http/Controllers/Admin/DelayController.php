<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Models\Order;
use App\Models\Comment;
use App\Models\Assignments;
use App\Models\tax;
use Carbon\Carbon;

class DelayController extends AdminController
{
    
    public function __construct()
    {
        $model = new Order();
        $this->tableName =  $model->table;
        $this->ModuleName = 'orders';
    }

    public function index()
    {
        return view('admin.Dashboard.Delay.index');  
    }

    public function table_data(Request $request)
    {
        $mytime = \Carbon\Carbon::now();
        $date = $mytime->toDateTimeString();
        $bufferDays = tax::select('buffer_days')->first();
        $bufferDay = $bufferDays->buffer_days;
        $in = array('0','3','4','5');
        $orders = Order::whereNotIn('status',$in)
        ->where('expected_delivery_time','>=',date('Y-m-d'))
        ->groupBy('order_unique_id')
        // ->whereDate('expected_delivery_time', '>=', date('Y-m-d'))
        // ->where('created_at', date('Y-m-d'))
        ->get();
        // $delayOrder = 0;
        // foreach ($orders as $order) {
        //     $expectedDate = date('Y-m-d',strtotime($order->expected_delivery_time));
        //     $internalExpected = date('Y-m-d', strtotime($expectedDate. ' - '.$bufferDay.' days'));
        //     $today = date('Y-m-d');
        //     if($today == $internalExpected){
        //         $count = 1;
        //         $delayOrder += $count;
        //     }
        // }
        $datatables = Datatables::of($orders)

        ->editColumn('status',function ($orders){
            //0=Order Not Placed,1=Received, 2=Pending, 3=Delivered, 4=Reorder, 5=Defective
            $orderStatus = Assignments::where('order_unique_id',$orders['order_unique_id'])->first();
            if ($orderStatus['tailor_status'] == '0') {
                $status = '<span class="label label-danger">With Tailor</span>';
            }elseif ($orderStatus['qc_status'] == '0') {
                $status = '<span class="label label-danger">With QC</span>';
            }elseif ($orderStatus['qc_passed_at'] !== '') {
                $status = '<span class="label label-danger">Salesman</span>';
            }
            // if($orderStatus->tailor_status == '0')
            //     $status = '<span class="label label-danger">With Tailor</span>';
            // if($orderStatus->tailor_status == '2')
            //     $status = '<span class="label label-danger">Completed From Tailor</span>';
            // if($orders->status == '3')
            //     $status = '<span class="label label-danger">Delivered & Delay</span>';
            // if($orders->status == '4')
            //     $status = '<span class="label label-info">Reorder</span>';
            // if($orders->status == '5')
            //     $status = '<span class="label label-danger">Defective</span>';
            $html = $status;
            return $html;
        })

        ->editColumn('delivered_at',function ($orders){
            if($orders->delivered_at){
              $html = formatDate($orders->delivered_at);
            }else{
                $html = '--';
            }

            return $html;
        })

        ->editColumn('branch',function ($orders){
            if(isset($orders->orderBy['branchdata'])){
                $html = $orders->orderBy->branchdata['branch'];
            }else{
                $html = '--';
            }
            return $html;
        })

        ->addColumn('action',function ($orders){
            $html='<a target="_blank" class="btn btn-primary" title="View Details" href="'.route('delay.orderDetails', ["orderId" => $orders->order_unique_id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a><button class="btn btn-danger btn-delete delete-records" value="'.$orders->order_unique_id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($orders)
        {
            $html = formatDate($orders->created_at);
            return $html;
        })
        ->rawColumns(['order_unique_id','customer_unique_id','order_by','quantity','order_date','status', 'action']);
        return $datatables->make(true);
    }

    /*
     * Show Record
    */

    public function orderDetails($orderId)
    {
        // dd($orderId);
        if(!empty($orderId)) {
            $order = Order::where('order_unique_id', $orderId)->get();
            // print_r(json_encode($order));die();
            if( !empty($order) ) {
                
                return view('admin.Dashboard.Delay.details', compact('order'))->with('heading', 'Details');
            } else {
                return redirect()->route('orders.index')->with('error', "orders does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }

    public function addComment()
    {
        $input = request()->all();
        Comment::create($input);
        return response()->json(['success'=>'Comment added.']);

    }

    public function destroy($id) {
        $product = Order::where('order_unique_id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }

    public function show()
    {
        return redirect()->route('delay.index')->with('error',trans('message.invalidId'));
    }

}
