<?php

namespace App\Http\Controllers\Admin;

use DB;
use Route;
Use App\Models\ContentManagement;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class ContentManagementController extends AdminController
{

    public function __construct()
    {
        $model = new ContentManagement();
        $this->tableName =  $model->table;
    }

    //Terms and Conditions
   	public function index() {
    	$terms_body_display = DB::table($this->tableName)->where('id',1)->first();
        return view('admin.terms.index')->with('terms_body_display',$terms_body_display);
    }
    public function storeterms(Request $request) {
        $v = Validator::make($request->all(), [
            'title' =>'required',
            'content' => 'required',                       
        ]);
        $content = $request->get('content');
        $title = $request->get('title');
        DB::table($this->tableName)->where('id',1)->update(['title'=>$title, 'content' => $content]);
        return redirect(route('admin.terms'))->with('success', $title.trans('message.UpdatedMsg'));
    }
    public function editTerms() {
       $extractData = DB::table($this->tableName)->where('id',1)->get();
       return $extractData;
    }

    //Private Policy
    public function privateIndex() {
        $private_body_display = DB::table($this->tableName)->where('id',2)->first();
        return view('admin.privatePolicy.index')->with('private_body_display',$private_body_display);
    }
    public function storePrivatePolicy() {
        $v = Validator::make(request()->all(), [
            'title' =>'required',
            'content' => 'required',                       
        ]);
        $content=request()->get('content');
        $title=request()->get('title');
        DB::table($this->tableName)->where('id',2)->update(['title'=>$title, 'content' => $content]);
        return redirect(route('admin.privacy_policy'))->with('success', $title.trans('message.UpdatedMsg'));
    }
    public function editPrivate() {
       $extract = DB::table($this->tableName)->where('id',2)->get();
       return $extract;
    }

    //aboutUs
    public function aboutIndex() {
        $about_body_display = DB::table($this->tableName)->where('id',3)->first();
        return view('admin.aboutUs.index')->with('about_body_display',$about_body_display);
    }
    public function storeAbout(Request $request) {
        $v = Validator::make($request->all(), [
            'title' =>'required',
            'content' => 'required',
        ]);
        $content=$request->get('content');
        $title=$request->get('title');
        DB::table($this->tableName)->where('id',3)->update(['title'=>$title, 'content' => $content]);
        return redirect(route('admin.about_us'))->with('success', $title.trans('message.UpdatedMsg'));
    }
    public function editAbout() {
       $extractData = DB::table($this->tableName)->where('id',3)->get();
       return $extractData;
    }

     //donationVsbid
    public function donationVsBidIndex() {
        $donationVsBid_body_display = DB::table($this->tableName)->where('id',4)->first();
        return view('admin.donationVsBid.index')->with('donationVsBid_body_display',$donationVsBid_body_display);
    }
    public function storedonationVsBid(Request $request) {
        $v = Validator::make($request->all(), [
            'title' =>'required',
            'content' => 'required',
        ]);
        $content=$request->get('content');
        $title=$request->get('title');
        DB::table($this->tableName)->where('id',4)->update(['title'=>$title, 'content' => $content]);
        return redirect(route('admin.donationVsBid'))->with('success', $title.trans('message.UpdatedMsg'));
    }
    public function editdonationVsBid() {
       $extractData = DB::table($this->tableName)->where('id',4)->get();
       return $extractData;
    }

}