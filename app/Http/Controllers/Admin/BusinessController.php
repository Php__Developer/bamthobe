<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Business;
use App\Models\Business_order;

class BusinessController extends AdminController
{
    
    public function __construct()
    {
        $model = new Business();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Business';
    }

    public function index()
    {
        return view('admin.Business.index');  
    }

    public function table_data(Request $request)
    {
       
        $business = Business::select('*');
        $datatables = Datatables::of($business)

        ->editColumn('customer_status', function ($business) {
                $html = '<label class="switch">';
                if ($business->customer_status) {
                    $html .= '<input class="customer_status-business" onchange="changeCustomerStatus(' . $business->id . ', 0)" type="checkbox" checked="checked">';
                } else {
                    $html .= '<input class="customer_status-business" onchange="changeCustomerStatus(' . $business->id . ', 1)" type="checkbox">';
                }
                $html .= '<span class="lever round"></span></label>';
                return $html;
            })

        ->addColumn('action', function ($business) {
            $html = '<a target="_blank" class="btn btn-primary" title="View Details" href="' . route('admin.customerProfile', ["businessID" => $business->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>';
                return $html;
            })

        ->addColumn('total_orders', function ($business) {
            $orderCount = Business_order::where('customer_id',$business->id)
            ->where('order_compete','1')
            ->count();
            $html = $orderCount;
            return $html;
        })
        ->addColumn('last_payment', function ($business) {
            if ($business->last_payment == NULL) {
                $last = 'Not Paid Yet';
            }else{
                $last = formatDate($business->last_payment);
            }
            $html = $last;
            return $html;
        })
        ->editColumn('created_at',function($business)
        {
            $html = formatDate($business->created_at);
            return $html;
        })
        ->editColumn('updated_at',function($business)
        {
            $html = formatDate($business->updated_at);
            return $html;
        })
        ->addColumn('action',function ($business){
            $html='<a class="btn btn-primary" title="Edit Customer" value="'.$business->id.'" href="'.route('business.edit',$business->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><a target="_blank" class="btn btn-primary" title="View Details" href="'.route('business.orderDetails', ["orderId" => $business->id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a><a target="_blank" class="btn btn-primary" title="Pay & Complete Order" href="'.route('business.orderPay', ["orderId" => $business->id]).'"><i class="fa fa-money" aria-hidden="true"></i></a>';
            if($business->last_payment != NULL){
                $html .= '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$business->id.'">
                <i class="fa fa-trash" aria-hidden="true"></i></button>';
            }
            return $html;
        })
        ->rawColumns(['contact_number','username','shopname','responsible_name','shop_email','shop_location','customer_status','summer_service_price','winter_sewing_price','piping','washing','botana','embroidery','zipper','sleeping_dress','other','action']);
        return $datatables->make(true);
    }

    public function details($orderId)
    {
        // dd($orderId);
        if(!empty($orderId)) {
            $order = Business_order::where('customer_id', $orderId)->get();
            if( !empty($order) ) {
                $customerDetail = Business::where('id', $orderId)->first();
                return view('admin.Business.details', compact('order','customerDetail'))->with('heading', 'Details');
            } else {
                return redirect()->route('orders.index')->with('error', "orders does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }

    public function pay($orderId,$type = 'stream')
    {
        // dd($orderId);
        if(!empty($orderId)) {

            $orderuniqueID = Business::where('id',$orderId)->first();

            $order = Business_order::where('order_unique_id', $orderuniqueID['unique_id'])->get();
            $complete_order = Business_order::where('customer_id', $orderId)->update(array('order_compete' => '2'));

            $pdf = app('dompdf.wrapper')->loadView('admin.Business.order-pdf', ['order' => $order,'orderuniqueID'=>$orderuniqueID]);
            if($type == 'stream') {
                return $pdf->stream('invoice.pdf');
            } else {
                return $pdf->download('invoice.pdf');
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }
   
    /**
     * Create Model
     */
    public function create()
    {
        $app = app();
        $business = $app->make('stdClass');
        $business->id = -1;
        $business->contact_number = '';
        $business->username = '';
        $business->shopname = '';
        $business->responsible_name = '';
        $business->shop_email = '';
        $business->shop_location = '';
        $business->password = '';
        $business->summer_service_price = '';
        $business->winter_sewing_price = '';
        $business->piping = '';
        $business->washing = '';
        $business->botana = '';
        $business->embroidery = '';
        $business->zipper = '';
        $business->sleeping_dress = '';
        $business->other = '';
        $business->other_price = '';

        return view('admin.Business.edit')->with('heading','Create')->with('business',$business);
        // print_r($business);die();
    }

    /**
     Edit Record
    **/

    public function edit($business_id) {
        // dd($business_id);
        if(!empty($business_id) && is_numeric($business_id)) {
            $value = Business::find($business_id);
            $id = $value['id'];
            if( $id == $business_id ) {
               $business_id = DB::table($this->tableName)->select('*')->where('id',$business_id)->first();
             return view('admin.Business.edit')->with('business',$business_id)->with('heading','Edit');
            } else {
                return redirect()->route('business.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('business.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$business_id) {
        if($business_id>0)
            {
                $request->validate([
                    'contact_number'         => 'required|numeric|digits:10',
                    'username'         =>'required',
                    'shopname'         =>'required',
                    'responsible_name'         => 'required',
                    'shop_email'         =>'required',
                    'shop_location'         =>'required',
                    'summer_service_price'         =>'nullable|numeric|between:0,99.99',
                    'winter_sewing_price'         =>'nullable|numeric|between:0,99.99',
                    'piping'         =>'nullable|numeric|between:0,99.99',
                    'washing'         =>'nullable|numeric|between:0,99.99',
                    'botana'         =>'nullable|numeric|between:0,99.99',
                    'embroidery'         =>'nullable|numeric|between:0,99.99',
                    'zipper'         =>'nullable|numeric|between:0,99.99',
                    'sleeping_dress'         =>'nullable|numeric|between:0,99.99',
                    'other'         =>'nullable',
                    'other_price'         =>'nullable|numeric|between:0,99.99'
                ]);
                $data = array();
                $data['contact_number'] = $request->get('contact_number');
                $data['username'] = $request->get('username');
                $data['shopname'] = $request->get('shopname');
                $data['responsible_name'] = $request->get('responsible_name');
                $data['shop_email'] = $request->get('shop_email');
                $data['shop_location'] = $request->get('shop_location');
                $data['summer_service_price'] = $request->get('summer_service_price');
                $data['winter_sewing_price'] = $request->get('winter_sewing_price');
                $data['piping'] = $request->get('piping');
                $data['washing'] = $request->get('washing');
                $data['botana'] = $request->get('botana');
                $data['embroidery'] = $request->get('embroidery');
                $data['zipper'] = $request->get('zipper');
                $data['sleeping_dress'] = $request->get('sleeping_dress');
                $data['other'] = $request->get('other');
                $data['other_price'] = $request->get('other_price');

                if(Business::where('id', $business_id)->update($data)) {
                    return redirect()->route('business.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                } else {
                    return redirect()->route('business.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                }
                }else { //for Create
                    $request->validate([
                        'contact_number'         => 'required|numeric|digits:10',
                        'username'         =>'required',
                        'shopname'         =>'required',
                        'responsible_name'         => 'required',
                        'shop_email'         =>'required',
                        'shop_location'         =>'required',
                        'summer_service_price'         =>'nullable|numeric|between:0,99.99',
                        'winter_sewing_price'         =>'nullable|numeric|between:0,99.99',
                        'piping'         =>'nullable|numeric|between:0,99.99',
                        'washing'         =>'nullable|numeric|between:0,99.99',
                        'botana'         =>'nullable|numeric|between:0,99.99',
                        'embroidery'         =>'nullable|numeric|between:0,99.99',
                        'zipper'         =>'nullable|numeric|between:0,99.99',
                        'sleeping_dress'         =>'nullable|numeric|between:0,99.99',
                        'other'         =>'nullable',
                        'other_price'         =>'nullable|numeric|between:0,99.99'
                    ]);
                    // $v = Validator::make($request->all(), [
                    
                    // ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('image')) {
                        $mimetype = $request->file('image')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('business.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('image');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/business');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['image'] = $imageName;
                $request = $request->input();
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $pin = mt_rand(1000000, 9999999) . mt_rand(1000000, 9999999) . $characters[rand(0, strlen($characters) - 1)];
                $string = str_shuffle($pin);
                // $limit = 6;
                // $unique_id = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
                $request['password'] = $pin;
                $lastcustomer = Business::select('id')->first();
                $lastid = $lastcustomer['id'];
                $newid = $lastid+1;
                $countid = strlen($newid);
                if($countid=='1')
                {
                    $madeid = '000'.$newid;
                }
                else if($countid=='2')
                {
                    $madeid = '00'.$newid;
                }
                else if($countid=='3')
                {
                    $madeid = '0'.$newid;
                }
                else{
                    $madeid = $newid;
                }
                $idcode = date('y-m').'-'.$madeid;
                $request['unique_id'] = $idcode;
                Business::create($request);

                /*send sms to user*/
                /**/
                $name = $request['username'];
                $shopname = $request['shopname'];
                $mess = ' We are glad to have you as part of BAM family please download BAM Team application Username : ';
                $applink = ' https://play.google.com/store/apps/details?id=e.vlogic.bamapp';
                $cred = $name.' Password : '.$pin;
                $concateString = $name . $mess . $cred . $applink;
                $client = new \GuzzleHttp\Client();
                $res = $client->request('POST', 'http://www.oursms.net/api/sendsms.php', [
                    'form_params' => [
                        'username' => 'bamthobe',
                        'password' => 'bam123',
                        'message' => $concateString,
                        'numbers' => "+966".$request['contact_number'],
                        'sender' => 'BAM',
                        'unicode' => 'E',
                        'return' => 'full',
                    ]
                ]);

                /*send sms to user*/

                /*sending mail to user*/

                $name = $request['username'];
                $email = $request['shop_email'];
                // print_r($email);die();
                $title = 'Your credentials of registration';
                $content = $name.' is username and password is '.$pin;

                // \Mail::queue('contactUsMail', array('confirmation_code' => $user->confirmation_code, 'username' => $user->username), function($message) use($user) {
                // $message->to($user->email,$user->firstname . ' ' . $user->lastname)
                //     ->subject('CableLog.com - Verify your email address');
                // });

                \Mail::send('contactUsMail', ['name' => $name, 'email' => '', 'title' => $title, 'content' => $content], function ($message) use($email) {
                    $message->to($email)->subject('Credentials Mail');
                });

                /*sending mail to user*/
                return redirect()->route('business.index')->with('Success', 'Created Successfully');      
            }
        }

    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        // dd($business_id);
        if ($id && is_numeric(($id))) {
            if (Business::where('id', $id)->update(array('customer_status' => $request->get('status')))) {
                echo json_encode(array('status' => 'success', 'message' => 'Status changed'));
                exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));
                exit;
            }
        }
        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));
        exit;
    }

    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/business');
                $deleteImg = Business::where('id', $id)->first()->image;
                if(Business::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
  
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('business.index')->with('error',trans('message.invalidId'));
    }


}
