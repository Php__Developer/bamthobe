<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Models\Order;
use App\Models\Assignments;
use App\Models\Comment;

class DefectiveController extends AdminController
{
    
    public function __construct()
    {
        $model = new Order();
        $this->tableName =  $model->table;
        $this->ModuleName = 'orders';
    }

    public function index()
    {
        return view('admin.subdashboard.defective');
    }

    public function table_data(Request $request)
    {
        $orders = Order::where('status','5')
        ->get();
        $datatables = Datatables::of($orders)

        ->editColumn('branch',function ($orders){
            if(isset($orders->orderBy['branchdata'])){
                $html = $orders->orderBy->branchdata['branch'];
            }else{
                $html = '<span class="label label-danger">No Data</span>';
            }
            return $html;
        })

        ->addColumn('salesman',function ($orders){
            if(isset($orders->orderBy['branchdata'])){
                $branchID = $orders->orderBy['branchdata']['branch_id'];
                $manager = Branch::where('branch_id',$branchID)->select('salesman_id')->first();
                $salesman = Salesman::where('id',$manager['salesman_id'])->first();
                $html = $salesman['title'];
            }else{
                $html = '<span class="label label-danger">No Data</span>';
            }
            return $html;
        })

        ->addColumn('tailor_id',function ($orders){
            $sewing = Assignments::where('order_unique_id',$orders->order_unique_id)->first();
            // $html = $sewing['tailor_id'];
            $salesman = Salesman::where('type',$sewing['tailor_id'])->first();
            if ($salesman) {
                $html = $salesman['title'];
            } else{
                $html = '<span class="label label-danger">No Data</span>';
            }
            return $html;
        })

        ->addColumn('cutter_id',function ($orders){

            $cutter = Assignments::where('order_unique_id',$orders->order_unique_id)->first();
            // $salesman = $cutter['cutter_id'];
            $salesman = Salesman::where('type',$cutter['cutter_id'])->first();
            // print_r($salesman);die();
            if ($salesman) {
                $html = $salesman['title'];
            } else{
                $html = '<span class="label label-danger">No Data</span>';
            }
            return $html;
        })

        ->addColumn('qc_id',function ($orders){
            $sewing = Assignments::where('order_unique_id',$orders->order_unique_id)->first();
            $salesman = Salesman::where('id',$sewing['qc_id'])->first();
            if ($salesman) {
                $html = $salesman['title'];
            } else{
                $html = '<span class="label label-danger">No Data</span>';
            }
            return $html;
        })

        ->addColumn('tailor_id',function ($orders){
            $sewing = Assignments::where('order_unique_id',$orders->order_unique_id)->first();
            $salesman = Salesman::where('type',$sewing['tailor_id'])->first();
            if ($salesman) {
                $html = $salesman['title'];
            } else{
                $html = '<span class="label label-danger">No Data</span>';
            }
            return $html;
        })

        ->addColumn('defective_type',function ($orders){

            $defective = Order::where('order_unique_id',$orders->order_unique_id)->first();
            $html = $defective['defective_by_role'];
            return $html;
        })

        ->addColumn('accountable',function ($orders){

            // if(isset($orders->orderBy)){
            //     $html = $orders->orderBy;
            // }else{
            //     $html ='3';
            // }
            $html ='3';
            return $html;
        })
        
        ->addColumn('assign_time',function ($orders){
                
            // if(isset($orders->orderBy)){
            //     $html = $orders->orderBy;
            // }else{
            //     $html ='3';
            // }

            $html ='3';
            return $html;
        })
        
        ->addColumn('assigned_staff',function ($orders){
            // if(isset($orders->orderBy)){
            //     $html = $orders->orderBy;
            // }else{
            //     $html ='3';
            // }
            $html ='3';
            return $html;
        })

        ->addColumn('waiting_time',function ($orders){
            // if(isset($orders->orderBy)){
            //     $html = $orders->orderBy;
            // }else{
            //     $html ='3';
            // }
            $html ='3';
            return $html;
        })

        ->addColumn('action',function ($orders){
            $html='<a target="_blank" class="btn btn-primary" title="View Details" href="'.route('admin.orderDetails', ["orderId" => $orders->order_unique_id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a><button class="btn btn-danger btn-delete delete-records" value="'.$orders->order_unique_id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })

        ->editColumn('created_at',function($orders)
        {
            $html = formatDate($orders->created_at);
            return $html;
        })

        ->rawColumns(['order_unique_id','product_id','branch','salesman','cutter_id','tailor_id','qc_id','status', 'action']);
        return $datatables->make(true);
    }
    /*
     * Show Record
     */
    public function details($orderId)
    {
        if(!empty($orderId)) {
            $order = Order::where('order_unique_id', $orderId)->get();
            if( !empty($order) ) {
                
                return view('admin.orders.details', compact('order'))->with('heading', 'Details');
            } else {
                return redirect()->route('orders.index')->with('error', "orders does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }

    public function addComment()
    {
        $input = request()->all();
        Comment::create($input);
        return response()->json(['success'=>'Comment added.']);

    }

    public function destroy($id) {
        $product = Order::where('order_unique_id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }

  


}
