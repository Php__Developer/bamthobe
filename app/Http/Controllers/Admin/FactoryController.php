<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
Use App\Models\Factory;
use Yajra\Datatables\Datatables;
Use App\Models\OtherUsers;
use App\Models\Salesman;

class FactoryController extends AdminController
{

    public function __construct()
    {
        $model = new Factory();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Factory';
    }

    public function index() {
        $manager = Salesman::where('type',6)
                    ->where('factory_id',0)
                    ->pluck('title','id');
        return view('admin.factories.index', compact('manager'));
    }

    public function table_data() {
        $factories = Factory::select('*');
        return Datatables::of($factories)
        	->editColumn('status',function ($factories){
                $html = '<label class="switch" id="changeStatus" module-id="'.$factories->id.'">';
       
                if($factories->status){
                    $html .= '<input type="checkbox" checked="checked">';
                } else {
                    $html .= '<input type="checkbox">';
                }
                $html .= '<span class="lever round" onchange="changeStatus('.$factories->id.','.$factories->status.')" ></span></label>';
                return $html;
            })
            ->addColumn('action',function ($factories){
                $html='<a class="btn btn-primary" title="Edit" onclick="showEditModel(\''.$factories->id.'\');" value="'.$factories->id.'" module-id="'.$factories->id.'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
            '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$factories->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                return $html;
           	})
           	->editColumn('created_at',function($factories)
            {
                $html = formatDate($factories->created_at);
                return $html;
            })

            ->editColumn('manager',function($factories)
            {
                $getManager = Salesman::where('id',$factories->manager_id)->first();

                if($getManager){
                    $html = $getManager->title;
                }else{
                    $html = '--';
                }
                return $html;
            })
            ->editColumn('updated_at',function($factories)
            {
                $html = formatDate($factories->updated_at);
                return $html;
            })
           	->rawColumns(['status', 'action'])
            ->make(true);
    }

    /*
     * Add Record
     */
    public function store(Request $request) {
        if($request->ajax()) {
            $validatedData = $request->validate([
                'name'    => 'required',
                'address'  => 'required',
                'phone'  => 'required',
                'status'  => 'required'
            ]);
            $data = array();
            unset($request['_token']);
            unset($request['_method']);
            unset($request['factoryFormAction']);
            $data['name'] = $request->get('name');
            $data['address'] = $request->get('address');
            $data['status'] = $request->get('status');
            $data['phone'] = $request->get('phone');
            $data['manager_id'] = $request->get('manager_id');

            $factory = Factory::orderBy('id', 'DESC')->get()->first();
            if($factory){
                $factoryID = sprintf("%02d", $factory->id+1);
            }else{
                $factoryID = sprintf("%02d", 1);
            }
            $data['fac_id'] = 'FAC'.$factoryID;


            if($fact = Factory::create($data)) {

                Salesman::where('id', $request['manager_id'])->update(['factory_id'=> $fact->id]);

                echo json_encode(array('status' => 'success', 'message' => $this->ModuleName.trans('message.AddedMsg')));exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
    }

    /*
     * Delete Record
     */
    public function destroy(Request $request,$id) {
    	if($request->ajax()) {
    		if($id && is_numeric($id)) {
		        if(Factory::destroy($id)) {
		        	echo json_encode(array('status' => 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')));exit;
		        } else {
		        	echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
		        }
		    } else {
		    	echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
		    }
	    }
    }

    /*
     * Update Record
     */
   	public function update(Request $request,$factoryID) {
    	if($request->ajax()) {
    		if($factoryID && is_numeric($factoryID)) {
	    		$validatedData = $request->validate([
					'name'    => 'required',
                    'address'  => 'required',
                    'status'  => 'required',
                    'phone'  => 'required',
                    'manager_id'  => 'required'
				]);
				unset($request['_token']);
	        	unset($request['_method']);
	        	unset($request['materialFormAction']);
                $request = $request->all();
                $data['name'] = $request['name'];
                $data['address'] = $request['address'];
                $data['phone'] = $request['phone'];
                $data['manager_id'] = $request['manager_id'];
                $data['status'] = $request['status'];

                //check old manager and new manager id
                $factory = Factory::find($factoryID);

                if($factory->manager_id != $request['manager_id']){
                    Salesman::where('id', $factory->manager_id)->update(['factory_id'=>'0']);
                }
                
                try { 
                    factory::where('id', $factoryID)->update($data);
                    Salesman::where('id', $request['manager_id'])->update(['factory_id'=> $factoryID]);                    
                } catch(\Illuminate\Database\QueryException $ex){ 
                    echo json_encode(array('status' => 'error', 'message' => $ex->getMessage()));exit;
                }
                echo json_encode(array('status' => 'success', 'message' => $this->ModuleName.trans('message.UpdatedMsg')));exit;
	        }
	        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
    	}
    }

    /*
     * Get Record
     */
   	public function getEditRecord(Request $request) {
        $result = Factory::select('*')->find($request->get('id'));

        $selectedManager = Salesman::where('type',6)->where('factory_id',$request->get('id'))->first();        
        $manager = Salesman::where('type',6)->where('factory_id',0)->pluck('title','id');
        if($selectedManager){
            $manager->put($selectedManager->id,$selectedManager->title.' (Current Manager)');
        }
        $result['manager'] = $manager;

        if($result->first()){
            echo json_encode(array('status'=> 'success','result'=>$result));exit;
        }
        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
    }

    /*
     * Change status
     */
    public function changeStatus(Request $request) {
    	if($request->ajax()) {
    		$id = $request->get('id');
    		if($id && is_numeric(($id))) {
		       	if(Factory::where('id', $id)->update(array('status' => $request->get('status')))) {
		        	echo json_encode(array('status' => 'success', 'message' => trans('message.statusUpdated')));exit;
		        } else {
		        	echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
		        }
		    }
		    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
    	}
    }

}
