<?php

namespace App\Http\Controllers\Admin;

Use App\Models\Charity;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Admin;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class CharityController extends AdminController
{
    public function __construct()
    {
        $model = new Charity();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Charity';
    }

    public function index() 
    {
        return view('admin.charities.index');
    }

    public function table_data(Request $request){
        $charities  = Charity::select('*');
        $datatables = Datatables::of($charities)
			->editColumn('logo',function ($charity){
				$html = '<div class="serviceImg" style="width:35px;height:35px">';
            	if($charity->logo){
                	$html .= '<a href="'.asset('uploads/charities/'.$charity->logo).'" data-lightbox="image-'.$charity->id.'" data-title=""><img src="'.asset('uploads/charities/'.$charity->logo).'"></a>';
            	} else {
                	$html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$charity->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
            	return $html;
            })
             ->editColumn('title',function ($charity){
               $html = str_limit($charity->title,20,'....');
               return $html;
            })
             ->editColumn('address',function ($charity){
                $html =  str_limit($charity->address,20,'....');
                return $html;
             })
             ->editColumn('link',function ($charity){
                $html = '<a target="_blank" href="'.$charity->link.'">'.str_limit($charity->link, $limit = 10, $end = '...').'</a>';
                return $html;
            })
             ->editColumn('telephone',function ($charity){
                $html =  str_limit($charity->telephone,20,'....');
                return $html;
            })
             ->editColumn('fax',function ($charity){
                $html =  str_limit($charity->fax,20,'....');
                return $html;
            })
             ->editColumn('email',function ($charity){
                $html =  str_limit($charity->email,10,'....');
                return $html;
             })
           ->editColumn('status',function ($charity){
           		$html = '<label class="switch" id="changeStatus" module-id="'.$charity->id.'">';
            	if($charity->status){
                	$html .= '<input type="checkbox" checked="checked">';
            	} else {
                	$html .= '<input type="checkbox">';
            	}
            	$html .= '<span class="lever round"></span></label>';
            	return $html;
			})
			->addColumn('action',function ($charity){
            	$html='<a class="btn btn-primary" value="'.$charity->id.'" href="'.route('charities.edit',$charity->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <button class="btn btn-danger btn-delete delete-records" value="'.$charity->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            	return $html;
			})
			->editColumn('created_at',function($charity)
            {
                $html = formatDate($charity->created_at);
                return $html;
            })
            ->editColumn('updated_at',function($charity)
            {
                $html = formatDate($charity->updated_at);
                return $html;
            })
			->rawColumns(['logo','title','address','link','telephone','fax','email','description','status', 'action']);
    	return $datatables->make(true);
    }

    /*
     * For Add
     */
    public function create(Request $request){
        $app = app();
        $charity = $app->make('stdClass');
        $charity->id = -1;
        $charity->title = '';
        $charity->logo = '';
        $charity->address = '';
        $charity->link = '';
        $charity->telephone='';
        $charity->fax='';
        $charity->email = '';
        $charity->password = '';
        $charity->tmp_name = '';
        $charity->description = '';
        $charity->status=1;
        return view('admin.charities.edit')->with('heading','Create')->with('charity',$charity);
    }
     /*
     * Change Status
     */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Charity::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'Status changed'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }

    /*
     * Edit Record
     */
    public function edit($charity_id) {
        if(!empty($charity_id) && is_numeric($charity_id)) {
            $value = Charity::find($charity_id);
            $id = $value['id'];
            if( $id == $charity_id ) {
                $charity = DB::table($this->tableName)->leftJoin('admins', 'charities.id', '=', 'admins.charity_id')->select('charities.*', 'admins.tmp_name as tmp_name')->where('charities.id',$charity_id)->first();
                return view('admin.charities.edit')->with('charity',$charity)->with('heading','Edit');
            } else {
                return redirect()->route('charities.index')->with('error','Updated successfully');
            }
        } else {
            return redirect()->route('charities.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Update Record
     */
    public function update(Request $request, $charity_id) {
        if($charity_id > 0) {// Edit
            $validator = Validator::make($request->all(), [
                'title'         => 'required',
                'email' => 'required|email|unique:charities,email,'. $charity_id .'',
                'password' => 'min:6',
                'status'        => 'required',
                'description'   => 'required'
            ]);
            if ($validator->fails()) {
                return back()->with('errors', $validator->errors())->withInput();
            } else {
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('logo')) {
                    $mimetype = $request->file('logo')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('charities.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('logo');
                        $data['logo'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/charities');
                        $checkPrevious = Charity::where('id', $charity_id)->first()->logo;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['logo']);
                    }
                }
                $data['title']        = $request->get('title');
                $data['address']      = $request->get('address');
                $data['link']         = $request->get('link');
                $data['telephone']    = $request->get('telephone');
                $data['fax']          = $request->get('fax');
                $data['email']        = $request->get('email');
                $data['description']  = $request->get('description');
                $data['status']       = $request->get('status');
                if(Charity::where('id', $charity_id)->update($data)) {
                    // save as admin
                    $charityAdmin = array(
                        'name' => $data['title'],
                        'email' => $data['email'],
                        'password' => bcrypt($request->get('password')),
                        'tmp_name' => $request->get('password'),
                        'charity_id' => $charity_id,
                    );
                    try {
                        Admin::updateOrCreate( [ 'charity_id' => $charity_id ] , $charityAdmin );
                    } catch(\Exception $ex) {

                    }
                    // save as admin
                    return redirect()->route('charities.index')->with('success', 'success');
                } else {
                    return redirect()->route('charities.index')->with('error', 'Error');
                }
            }
        } else {// Add
            $validator = Validator::make($request->all(), [
                'title'         => 'required',
                'email' => 'required|email|unique:charities,email|unique:admins,email',
                'password' => 'required|min:6',
                'status'        => 'required',
                'description'   => 'required'      
            ]);
            if ($validator->fails()) {
                return back()->with('errors', $validator->errors())->withInput();
            } else {
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                $imageName = NULL;
                if($request->hasFile('logo')) {
                    $mimetype = $request->file('logo')->getClientMimeType();
                     if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('charities.edit')->with('success', 'Please select a valid image type');
                        exit;
                    } else{
                        $image = $request->file('logo');
                        $imageName = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/charities');
                        $image->move($destinationPath, $imageName);
                    }
                }
                $request['logo'] = $imageName;
                $request = $request->input();
                $charity = Charity::create($request);
                // save as admin
                $charityAdmin = array(
                    'name' => $request['title'],
                    'email' => $request['email'],
                    'password' => bcrypt($request['password']),
                    'tmp_name' => $request['password'],
                    'charity_id' => $charity->id,
                );
                Admin::updateOrCreate( [ 'charity_id' => $charity->id ], $charityAdmin );
                // save as admin
                return redirect()->route('charities.index')->with('success', 'Updated Successfully');
            }
        }
   }

    /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
    	if($request->ajax()) {
    		if($id && is_numeric($id)) {
    			$destinationPath = public_path('/uploads/charities');
				$deleteImg = Charity::where('id', $id)->first()->logo;
		        if(Charity::destroy($id)) {
                    Admin::where('charity_id', '=', $id)->delete();
		        	if(isset($deleteImg) && !empty($deleteImg)) {
						File::delete($destinationPath.'/'.$deleteImg);
					}
		        	echo json_encode(array('status' => 'success', 'message' => 'Deleted'));exit;
		        } else {
		        	echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
		        }
		    } else {
		    	echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
		    }
	    }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('charities.index')->with('error',trans('message.invalidId'));
    }
}
