<?php

namespace App\Http\Controllers\Admin;

use DB;
use Route;
Use App\Models\WebsiteContent;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class WebsiteContentController extends AdminController
{
   	public function index() {
		$websiteContent = WebsiteContent::first();

		return view('admin.websiteContent.index',compact('websiteContent'));
    }

    public function storeWebsiteContent(Request $request){
    	
    	$request = $request->all();
      		$already_exist = WebsiteContent::where('title',$request['title'])->first();
      		if($already_exist){
            $data = array();
            $data['app_store'] = $request['apple'];
            $data['play_store'] = $request['google'];
            $data['content'] = $request['how_it_works'];
            $data['address'] = $request['lets_talk'];

      			WebsiteContent::where('id',$already_exist->id)->update($data);
      		} else {
      			return redirect('admin/webpage/edit_website_content')->with('error','Something went wrong');
      		}
        return redirect('admin/webpage')->with('success','Website content has been updated successfully');
    }


    public function privacy() {
    //$websiteContent = WebsiteContent::first();

    return view('admin.websiteContent.privacypolicy');
    }
}
