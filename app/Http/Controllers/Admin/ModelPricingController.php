<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\ModelPricing;

class ModelPricingController extends AdminController
{
    
    public function __construct()
    {
        $model = new ModelPricing();
        $this->tableName =  $model->table;
        $this->ModuleName = 'ModelPricing';
    }

    public function index()
    {
        return view('admin.ModelPricing.index');  
    }

    public function table_data(Request $request)
    {
        $modelpricing = ModelPricing::select('*');
        $datatables = Datatables::of($modelpricing)
        ->editColumn('image',function($modelpricing){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($modelpricing->image){
                $html .= '<a href="'.asset('uploads/modelpricing/'.$modelpricing->image).'"
                data-lightbox="image-'.$modelpricing->id.'" data-title=""><img src="'.asset('uploads/modelpricing/'.$modelpricing->image).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$modelpricing->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('price',function($modelpricing){
            $html = number_format($modelpricing->price,2, ".", ",");
            return $html;
        })
        ->editColumn('created_at',function($modelpricing)
        {
            $html = formatDate($modelpricing->created_at);
            return $html;
        })
        ->editColumn('updated_at',function($modelpricing)
        {
            $html = formatDate($modelpricing->updated_at);
            return $html;
        })
        ->addColumn('action',function ($modelpricing){
            $html='<a class="btn btn-primary" value="'.$modelpricing->id.'" href="'.route('modelpricing.edit',$modelpricing->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.'<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$modelpricing->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->rawColumns(['name','image','price','action']);
        return $datatables->make(true);
    }
   
    /**
     * Create Model
     */
    public function create()
    {
        $app = app();
        $modelpricing = $app->make('stdClass');
        $modelpricing->id = -1;
        $modelpricing->name = '';
        $modelpricing->image = '';
        $modelpricing->price = '';

        return view('admin.ModelPricing.edit')->with('heading','Create')->with('modelpricing',$modelpricing);
    }
    /**
     Edit Record
     */
    public function edit($modelpricing_id) {
        if(!empty($modelpricing_id) && is_numeric($modelpricing_id)) {
            $value = ModelPricing::find($modelpricing_id);
            $id = $value['id'];
            if( $id == $modelpricing_id ) {
               $modelpricing = DB::table($this->tableName)->select('*')->where('id',$modelpricing_id)->first();
             return view('admin.ModelPricing.edit')->with('modelpricing',$modelpricing)->with('heading','Edit');
             //->with('userType',$userType);
            } else {
                return redirect()->route('modelpricing.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('modelpricing.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$modelpricing_id) {
        if($modelpricing_id>0)
            {
                request()->validate([
                    'name'         => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image')) {
                    $mimetype = $request->file('image')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('modelpricing.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image');
                        $data['image'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/modelpricing');
                        $checkPrevious = ModelPricing::where('id', $modelpricing_id)->first()->image;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image']);
                    }
                }
                $data['name'] = $request->get('name');
                $data['price'] = $request->get('price');
                
                    if(ModelPricing::where('id', $modelpricing_id)->update($data)) {
                        return redirect()->route('modelpricing.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('modelpricing.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    'name'         => 'required',
                    'price'         =>'required',
                    'image'         =>'required'
                  
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('image')) {
                        $mimetype = $request->file('image')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('modelpricing.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('image');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/modelpricing');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['image'] = $imageName;
                $request = $request->input();

                ModelPricing::create($request);

                return redirect()->route('modelpricing.index')->with('Success', 'Created Successfully');      
            }
        }

    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/modelpricing');
                $deleteImg = ModelPricing::where('id', $id)->first()->image;
                if(ModelPricing::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
  
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('modelpricing.index')->with('error',trans('message.invalidId'));
    }


}
