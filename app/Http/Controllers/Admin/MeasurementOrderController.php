<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Models\Order;
use App\Models\Comment;
use App\Models\Business_order;

class MeasurementOrderController extends AdminController 
{
    
    public function __construct()
    {
        $model = new Order();
        $this->tableName =  $model->table;
        $this->ModuleName = 'orders';
    }

    public function index()
    {
        return view('admin.subdashboard.measurementorder');  
    }

    public function table_data(Request $request)
    {
        $orders = Order::where('status',  '2')
        // ->where('status', '!=', '3')
        // ->where('price_remaining', '!=','0')
        // ->groupBy('order_unique_id')
        ->get();
        // print_r(json_encode($orders));die();
        $datatables = Datatables::of($orders)

        // ->editColumn('status',function ($orders){
        //     //0=Order Not Placed,1=Received, 2=Pending, 3=Delivered, 4=Reorder, 5=Defective
        //     if($orders->status == '1')
        //         $status = '<span class="label label-primary">Received</span>';
        //     if($orders->status == '2')
        //         $status = '<span class="label label-warning">Pending</span>';
        //     if($orders->status == '3')
        //         $status = '<span class="label label-success">Delivered</span>';
        //     if($orders->status == '4')
        //         $status = '<span class="label label-info">Reorder</span>';
        //     if($orders->status == '5')
        //         $status = '<span class="label label-danger">Defective</span>';
        //     // if($orders->status == '0')
        //     //     $status = 'Order Not Placed';

        //       $html = $status;
        //       return $html;
        // })
        ->addColumn('sewing_id',function ($orders){
             $sewing = Business_order::where('order_unique_id',$orders->order_unique_id)->first();
           $html = $sewing['id'];
            return $html;
        })

        ->editColumn('delivered_at',function ($orders){
            if($orders->delivered_at){
              $html = formatDate($orders->delivered_at);
            }else{
                $html = '--';
            }

            return $html;
        })

        ->editColumn('branch',function ($orders){
            if(isset($orders->orderBy['branchdata'])){
                $html = $orders->orderBy->branchdata['branch'];
            }else{
                $html = '--';
            }
            return $html;
        })
        // ->editColumn('quantity',function ($orders){
        //     $allOrder = Order::where('order_unique_id',$orders->order_unique_id)->get();
        //     $quantity = 0;
        //     foreach ($allOrder as $order) {
        //         $orderQty = $order->quantity;
        //         $quantity += $orderQty;
        //     }
        //       $html = $quantity;
        //       return $html;
        // })

        ->addColumn('action',function ($orders){
            $html='<a target="_blank" class="btn btn-primary" title="View Details" href="'.route('admin.orderDetails', ["orderId" => $orders->order_unique_id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a><button class="btn btn-danger btn-delete delete-records" value="'.$orders->order_unique_id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($orders)
        {
            $html = formatDate($orders->created_at);
            return $html;
        })
        ->rawColumns(['order_unique_id','sewing_id','created_at','status', 'action']);
        return $datatables->make(true);
    }
    /*
     * Show Record
     */
    public function details($orderId)
    {
        if(!empty($orderId)) {
            $order = Order::where('order_unique_id', $orderId)->get();
            if( !empty($order) ) {
                
                return view('admin.orders.details', compact('order'))->with('heading', 'Details');
            } else {
                return redirect()->route('orders.index')->with('error', "orders does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }

    public function addComment()
    {
        $input = request()->all();
        Comment::create($input);
        return response()->json(['success'=>'Comment added.']);

    }

    public function destroy($id) {
        $product = Order::where('order_unique_id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }

  


}
