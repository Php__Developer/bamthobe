<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Uniform;


class UniformController extends AdminController
{
    
    public function __construct()
    {
        $model = new Uniform();
        $this->tableName =  $model->table;
        $this->ModuleName = 'uniform';
    }

    public function index()
    {
        // dd("dd");
        return view('admin.uniform.index');  
    }

    public function table_data(Request $request)
    {
        $uniform = Uniform::select('*');
        // dd($modelpricing);
        $datatables = Datatables::of($uniform) 

        ->editColumn('customer_status', function ($uniform) {
                $html = '<label class="switch">';
                if ($uniform->customer_status) {
                    $html .= '<input class="customer_status-uniform" onchange="changeCustomerStatus(' . $uniform->id . ', 0)" type="checkbox" checked="checked">';
                } else {
                    $html .= '<input class="customer_status-uniform" onchange="changeCustomerStatus(' . $uniform->id . ', 1)" type="checkbox">';
                }
                $html .= '<span class="lever round"></span></label>';
                return $html;
            })

        ->addColumn('action', function ($uniform) {
            $html = '<a target="_blank" class="btn btn-primary" title="View Details" href="' . route('admin.customerProfile', ["uniformID" => $uniform->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i>';
                return $html;
            })
            ->addColumn('total_orders', function ($uniform) {
            $html = $uniform->total_orders;
            return $html;
        })
        ->addColumn('last_payment', function ($uniform) {
            if ($uniform->last_payment == NULL) {
                $last = 'Not Paid Yet';
            }else{
                $last = formatDate($uniform->last_payment);
            }
            $html = $last;
            return $html;
        })
        ->editColumn('created_at',function($uniform)
        {
            $html = formatDate($uniform->created_at);
            return $html;
        })
        ->editColumn('updated_at',function($uniform)
        {
            $html = formatDate($uniform->updated_at);
            return $html;
        })
        ->addColumn('action',function ($uniform){
          $html=' <a class="btn btn-primary" title="Edit Customer" value="'.$uniform->id.'" href="'.route('uniform.edit',$uniform->id).'">
            <i class="fa fa-pencil" aria-hidden="true"></i>
              <a target="_blank" class="btn btn-primary" title="View Details" href="'.route('uniform.orderDetails', ["orderId" => $uniform->id]).'">
            <i class="fa fa-eye" aria-hidden="true"></i></a>
            

            <a target="_blank" class="btn btn-primary" title="Pay" href="'.route('uniform.orderPay', ["orderId" => $uniform->id]).'">
            <i class="fa fa-money" aria-hidden="true"></i></a>

            </a>'.'<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$uniform->id.'">
            <i class="fa fa-trash" aria-hidden="true"></i>
            </button>';
            return $html;
        })
        ->rawColumns(['contact_number','username','password','shopname','responsible_name','shop_email','shop_location','order_status','customer_status','unique_id','quantity','picture','completion_date','price','comments','documents','action']);
        return $datatables->make(true);
    }

    public function details($orderId)
    {
        if(!empty($orderId)) {
            $order = Uniform::where('id', $orderId)->get();
            if( !empty($order) ) {
                $customerDetail = Uniform::where('id', $orderId)->first();
                return view('admin.uniform.details', compact('order','customerDetail'))->with('heading', 'Details');
            } else {
                return redirect()->route('orders.index')->with('error', "orders does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }

    public function pay($orderId,$type = 'stream')
    {
        // dd($orderId);
        if(!empty($orderId)) {

            $orderuniqueID = Uniform::where('id',$orderId)->first();

            $order = Uniform::where('order_unique_id', $orderuniqueID['unique_id'])->get();
            $complete_order = Uniform::where('order_unique_id', $orderuniqueID['unique_id'])->update(array('order_compete' => '2'));

            $pdf = app('dompdf.wrapper')->loadView('admin.uniform.order-pdf', ['order' => $order,'orderuniqueID'=>$orderuniqueID]);
            if($type == 'stream') {
                return $pdf->stream('invoice.pdf');
            } else {
                return $pdf->download('invoice.pdf');
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }
   
    /**
     * Create Model
     */
    public function create()
    {
        $app = app();
        $uniform = $app->make('stdClass');
        $uniform->id = -1;
        $uniform->contact_number = '';
        $uniform->username = '';
        $uniform->shopname = '';
        $uniform->responsible_name = '';
        $uniform->shop_email = '';
        $uniform->shop_location = '';
        $uniform->password = '';
        $uniform->quantity = '';
        $uniform->picture = '';
        $uniform->completion_date = '';
        $uniform->price = '';
        $uniform->comments = '';
        $uniform->documents = '';
      
        

        return view('admin.uniform.edit')->with('heading','Create')->with('uniform',$uniform);
        // print_r($business);die();
    }

    /**
     Edit Record
    **/

    public function edit($uniform_id) {
        // dd($uniform_id);
        if(!empty($uniform_id) && is_numeric($uniform_id)) {
            $value = Uniform::find($uniform_id);
            $id = $value['id'];
            if( $id == $uniform_id ) {
               $uniform_id = DB::table($this->tableName)->select('*')->where('id',$uniform_id)->first();
             return view('admin.uniform.edit')->with('uniform',$uniform_id)->with('heading','Edit');
            } else {
                return redirect()->route('uniform.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('uniform.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$uniform_id) {
        if($uniform_id>0)
            {
                request()->validate([
                    'contact_number'         => 'required',
                    'username'         => 'required',
                    'shopname'         => 'required',
                    'responsible_name'         => 'required',
                    'shop_email'         => 'required',
                    'shop_location'         => 'required'
                 ]);
                $data = array();
                 $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('picture')) {
                    $mimetype = $request->file('picture')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('uniform.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('picture');
                        $data['picture'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/uniform');
                        $checkPrevious = Uniform::where('id', $uniform_id)->first()->image;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['picture']);
                    }
                }
                $data['contact_number'] = $request->get('contact_number');
                $data['username'] = $request->get('username');
                $data['shopname'] = $request->get('shopname');
                $data['responsible_name'] = $request->get('responsible_name');
                $data['shop_email'] = $request->get('shop_email');
                $data['shop_location'] = $request->get('shop_location');
                $data['quantity'] = $request->get('quantity');
                $data['picture'] = $request->get('picture');
                $data['completion_date'] = $request->get('completion_date');
                $data['price'] = $request->get('price');
                $data['comments'] = $request->get('comments');
                $data['documents'] = $request->get('documents');
              
                
                    if(Uniform::where('id', $uniform_id)->update($data)) {
                        return redirect()->route('uniform.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('uniform.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $request->validate([
                        'contact_number'         => 'required',
                        'username'         =>'required',
                        'shopname'         =>'required',
                        'responsible_name'         => 'required',
                        'shop_email'         =>'required',
                        'shop_location'         =>'required',
                        'price'         =>'nullable|numeric|between:0,99.99',
                        'quantity'         =>'nullable|numeric|between:0,999'
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('picture')) {
                        $mimetype = $request->file('picture')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            $image = $request->file('picture');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/uniform');
                            $image->move($destinationPath, $imageName);
                            // return redirect()->route('uniform.edit')->with('success', 'Please select a valid picture type');
                            // exit;
                            
                        } else{
                            $image = $request->file('picture');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/uniform');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName1 = NULL;
                    if($request->hasFile('documents')) {
                        $mimetype = $request->file('documents')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            $image = $request->file('documents');
                            $imageName1 = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/uniform');
                            $image->move($destinationPath, $imageName1);
                            // return redirect()->route('uniform.edit')->with('success', 'Please select a valid documents type');
                            // exit;
                            
                        } else{
                            $image = $request->file('documents');
                            $imageName1 = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/uniform');
                            $image->move($destinationPath, $imageName1);
                        }
                    }
                $request['picture'] = $imageName;
                $request['documents'] = $imageName1;
                $request = $request->input();
                // $unique_id = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
                $lastcustomer = Uniform::select('id')->orderBy('id','DESC')->first();
                $lastid = $lastcustomer['id'];
                // print_r($lastid);die();
                $newid = $lastid+1;
                $countid = strlen($newid);
                if($countid=='1')
                {
                    $madeid = '0000'.$newid;
                }
                else if($countid=='2')
                {
                    $madeid = '000'.$newid;
                }
                else if($countid=='3')
                {
                    $madeid = '00'.$newid;
                }
                else{
                    $madeid = $newid;
                }
                $idcode = date('y').$madeid;
                // print_r($idcode);die();
                $request['order_unique_id'] = $idcode;
                // dd($request);
                Uniform::create($request);
                /*sending mail to user*/

                // $name = $request['username'];
                // $email = $request['email'];
                // $title = 'Your credentials of registration';
                // $content = $name.'and password is'.$pin;

                // \Mail::send('contactUsMail', ['name' => $name, 'email' => $email, 'title' => $title, 'content' => $content], function ($message) {
                //     $message->to($email)->subject('Contact Us Email');
                // });

                /*sending mail to user*/
                return redirect()->route('uniform.index')->with('Success', 'Created Successfully');      
            }
        }

    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        // dd($business_id);
        if ($id && is_numeric(($id))) {
            if (Uniform::where('id', $id)->update(array('customer_status' => $request->get('status')))) {
                echo json_encode(array('status' => 'success', 'message' => 'Status changed'));
                exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));
                exit;
            }
        }
        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));
        exit;
    }

    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/uniform');
                $deleteImg = Uniform::where('id', $id)->first()->image;
                if(Uniform::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
  
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('uniform.index')->with('error',trans('message.invalidId'));
    }
   


}
