<?php

namespace App\Http\Controllers\Admin;

Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
Use App\Models\ContactUs;

class QueryController extends AdminController
{
      public function __construct()
      {
            $model = new ContactUs();
            $this->tableName =  $model->table;
            $this->ModuleName = 'ContactUs';
      }

      public function index()
      {
            return view('admin.query.index');
      }

      public function table_data(Request $request){
            $loyaltyPrograms  = ContactUs::select('*');
            $datatables = Datatables::of($loyaltyPrograms)
            ->editColumn('created_at',function($loyalty){
                  $html = formatDate($loyalty->created_at);
                  return $html;
            })
            ->rawColumns(['name','email','title','message', 'created_at']);
            return $datatables->make(true);
      }
}
