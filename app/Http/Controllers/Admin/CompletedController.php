<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route; 
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Models\Order;
use App\Models\Comment;
use App\Models\Assignments;

class CompletedController extends AdminController
{
    
    public function __construct()
    {
        $model = new Order();
        $this->tableName =  $model->table;
        $this->ModuleName = 'orders';
    }

    public function index()
    {
        return view('admin.subdashboard.complete');  
    }

    public function table_data(Request $request)
    {
        $orders = Order::where('status', '1')
        ->get();
        // print_r(json_encode($orders));die();
        $datatables = Datatables::of($orders)
        ->editColumn('order_by',function ($orders){
            if(isset($orders->orderBy)){
                $html = $orders->orderBy->title;
            }else{
                $html ='--';
            }

              return $html;
        })

        ->addColumn('action',function ($orders){
            $html='<a target="_blank" class="btn btn-primary" title="View Details" href="'.route('admin.orderDetails', ["orderId" => $orders->order_unique_id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a><button class="btn btn-danger btn-delete delete-records" value="'.$orders->order_unique_id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->addColumn('completion_time',function($orders)
        {
            $assign = Assignments::where('order_unique_id',$orders->order_unique_id)->first();
            $html = formatDate($assign['cutter_completion_date']);
            return $html;
        })
        ->rawColumns(['order_unique_id','status','completion_time','action']);
        return $datatables->make(true);
    }
    /*
     * Show Record
     */
    public function details($orderId)
    {
        if(!empty($orderId)) {
            $order = Order::where('order_unique_id', $orderId)->get();
            if( !empty($order) ) {
                
                return view('admin.orders.details', compact('order'))->with('heading', 'Details');
            } else {
                return redirect()->route('orders.index')->with('error', "orders does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }

    public function addComment()
    {
        $input = request()->all();
        Comment::create($input);
        return response()->json(['success'=>'Comment added.']);

    }

    public function destroy($id) {
        $product = Order::where('order_unique_id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }

  


}
