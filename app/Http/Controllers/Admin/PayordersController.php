<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Payorders;
use App\Models\Supplier;
use App\Models\Account;
use Session;

class PayordersController extends AdminController
{
    
    public function __construct()
    {
        $model = new Payorders();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Payorders';
    }

    public function index()
    {
        return view('admin.payorders.index');
    }

    public function createOrder(Request $request)
    {
        $lastPayorder = Payorders::orderBy('id', 'desc')->first();                
        if($lastPayorder){
            $refrence = sprintf("%03d", $lastPayorder->id+1);
        }else{
            $refrence = sprintf("%03d", 1);
        }

        $app = app();
        $payorders = $app->make('stdClass');
        $payorders->id = -1;
        $payorders->supplier_id = '';
        $payorders->date = '';
        $payorders->refrence_number =  'PO-'.date('ym').$refrence;
        $getSupplier = Supplier::pluck('supplier_name','id');
        return view('admin.payorders.add',compact('getSupplier'))->with('heading','Create')->with('payorders',$payorders);
        return view('admin.payorders.add');
    }

    public function createNextOrder(Request $request)
    {
        // dd($request->supplier_id);
        // $lastPayorder = Payorders::orderBy('id', 'desc')->first();                
        // if($lastPayorder){
        //     $refrence = sprintf("%03d", $lastPayorder->id+1);
        // }else{
        //     $refrence = sprintf("%03d", 1);
        // }

        $app = app();
        $payorders = $app->make('stdClass');
        $payorders->id = -1;
        $payorders->supplier_id = $request->supplier_id;
        $payorders->date = $request->date;
        $payorders->refrence_number =  $request->refrence_number;
        $payorders->product_code = '';
        $payorders->product_name = '';
        $payorders->quantity = '';
        $payorders->purchase_unit_price = '';
        $payorders->selling_unit_price = '';
        $payorders->purchase_amount = '';
        $payorders->paid_amount = '';
        $payorders->remaining_amount = '';
        $payorders->due_date = '';
        $payorders->note = '';
        $payorders->image = '';
        $payorders->tax = '';

        $getSupplier = $request->supplier_id;
        return view('admin.payorders.edit',compact('getSupplier'))->with('heading','Create')->with('payorders',$payorders);
    }

    public function table_data(Request $request)
    {
        $payorders = Payorders::groupBy('refrence_number');
        $datatables = Datatables::of($payorders)

        ->editColumn('supplier',function ($payorders){
            $getSupplier = Supplier::where('id',$payorders->supplier_id)->first();
            $html = $getSupplier['supplier_name'];
            return $html;
        })
        ->editColumn('supplier_email',function ($payorders){
            $getSupplier = Supplier::where('id',$payorders->supplier_id)->first();
            $html = $getSupplier['email'];
            return $html;
        })
        ->editColumn('date',function($payorders)
        {
            $html = formatDate($payorders->date);
            return $html;
        })
        ->editColumn('total',function($payorders)
        {
            $html = number_format($payorders->quantity*$payorders->purchase_unit_price,2, ".", ",");
            return $html;
        })
        ->editColumn('tax',function($payorders)
        {
            $html = number_format($payorders->purchase_amount-($payorders->quantity*$payorders->purchase_unit_price),2, ".", ",");
            return $html;
        })
        ->editColumn('purchase_amount',function($payorders)
        {
            $html = number_format($payorders->purchase_amount,2, ".", ",");
            return $html;
        })
        ->editColumn('paid_amount',function($payorders)
        {
            $payroll = Payorders::where('refrence_number', $payorders->refrence_number)->get();
            $paidAmount = 0;
            foreach ($payroll as $pay) {
                $paidAmount += $pay->paid_amount;
            }
            $html = number_format($paidAmount,2, ".", ",");
            return $html;
        })
        ->editColumn('remaining_amount',function($payorders)
        {
            $payroll = Payorders::where('refrence_number', $payorders->refrence_number)
                        ->orderBy('id', 'desc')->first();
                if($payroll->remaining_amount == 0){
                    $html = '<span class="label label-success">Settled</span>';
                }else{
                    $html = number_format($payroll->remaining_amount,2, ".", ",");
                }
            return $html;
        })
        ->editColumn('updated_at',function($payorders)
        {
            $html = formatDate($payorders->updated_at);
            return $html;
        })
        ->addColumn('action',function ($payorders){
            //$html='<a class="btn btn-primary" value="'.$payorders->id.'" href="'.route('payorders.edit',$payorders->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.'<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$payorders->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>'.'<a class="btn btn-primary" target="_blank" value="'.$payorders->id.'" href="'.route('admin.payOrderDetails',$payorders->refrence_number).'"><i class="fa fa-eye" aria-hidden="true"></i></a>';

            $payrolls = Payorders::where('refrence_number', $payorders->refrence_number)->get();
            foreach ($payrolls as $payroll) {
                if($payroll->remaining_amount == 0){
                    $html='<a class="btn btn-primary" target="_blank" value="'.$payorders->id.'" href="'.route('admin.payOrderDetails',$payorders->refrence_number).'" title="View receipt"><i class="fa fa-eye" aria-hidden="true"></i></a>';
                }else{
                    $html='<a class="btn btn-primary" value="'.$payorders->refrence_number.'" href="'.route('admin.payOrderSettle',$payorders->refrence_number).'"><i class="fa fa-money" aria-hidden="true" title="Payment"></i></a>'.
                        '<a class="btn btn-primary" target="_blank" value="'.$payorders->refrence_number.'" href="'.route('admin.payOrderDetails',$payorders->refrence_number).'" title="View receipt"><i class="fa fa-eye" aria-hidden="true"></i></a>';
                }
            }
            return $html;
        })

        ->rawColumns(['date','supplier','refrence_number','product_code','purchase_amount','paid_amount','remaining_amount','action']);
        return $datatables->make(true);
    }

    /**
     Edit Record
    */

    public function edit($payorders_id) {
        if(!empty($payorders_id) && is_numeric($payorders_id)) {
            $value = Payorders::find($payorders_id);
            $id = $value['id'];
            if( $id == $payorders_id ) {
               $getSupplier = Supplier::pluck('supplier_name','id');
               $payorders = DB::table($this->tableName)->select('*')->where('id',$payorders_id)->first();
             return view('admin.payorders.edit',compact('getSupplier'))->with('payorders',$payorders)->with('heading','Edit');
            } else {
                return redirect()->route('payorders.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('payorders.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Update Record
    */

    public function update(Request $request,$payorders_id){
        if($payorders_id>0){
            request()->validate([

            ]);
            $data = array();
            $imageName = NULL;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/payorders');
                $checkPrevious = Payorders::where('id', $payorders_id)->first()->image;
                if(isset($checkPrevious) && !empty($checkPrevious)) {
                    File::delete($destinationPath.'/'.$checkPrevious);
                }
                $image->move($destinationPath, $imageName);
                $data['image'] = $imageName;
            }

            $data['supplier_id'] = $request->get('supplier_id');
            $data['date'] = \Carbon\Carbon::parse($request->get('date'))->format('Y-m-d');
            $data['refrence_number'] = $request->get('refrence_number');
            $data['product_code'] = $request->get('product_code');
            $data['product_name'] = $request->get('product_name');
            $data['quantity'] = $request->get('quantity');
            $data['purchase_unit_price'] = $request->get('purchase_unit_price');
            $data['selling_unit_price'] = $request->get('selling_unit_price');
            $data['purchase_amount'] = $request->get('purchase_amount');
            $data['paid_amount'] = $request->get('paid_amount');
            $data['remaining_amount'] = $request->get('remaining_amount');
            $data['due_date'] = \Carbon\Carbon::parse($request->get('due_date'))->format('Y-m-d');
            $data['note'] = $request->get('note');
            $data['tax'] = $request->get('tax');
            if(Payorders::where('id', $payorders_id)->update($data)) {
                return redirect()->route('payorders.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
            } else{
                return redirect()->route('payorders.index')->with('error', $this->ModuleName.trans('message.networkErr'));
            }
            }else { //for Create
                // $saveArry = $request->all();
                $v = Validator::make($request->all(), [

                ]);
                $imageName = NULL;
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $imageName = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/payorders');
                    $image->move($destinationPath, $imageName);
                }
                $request['image'] = $imageName;

                $request['date'] = \Carbon\Carbon::parse($request->date)->format('Y-m-d');
                $request['due_date'] = \Carbon\Carbon::parse($request->due_date)->format('Y-m-d');
                $request = $request->input();
                $purchaseOrder = Payorders::create($request);
                if($purchaseOrder){
                    $requestAcc = array();
                    $requestAcc['category'] = 'Purchase Order';
                    $requestAcc['account_date'] = $request['date'];
                    $requestAcc['type'] = 0;
                    $requestAcc['amount'] = $request['purchase_amount'];
                    $requestAcc['comment'] = 'Auto Added From Purchase Order';
                    Account::create($requestAcc);
                }
                return redirect()->route('payorders.index')->with('Success', 'Created Successfully');      
            }
        }

    /*
     * Delete Record
    */

    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/payorders');
                $deleteImg = Payorders::where('id', $id)->first()->image_name;
                if(Payorders::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
    }

    /*
     * Show Record
    */

    public function show()
    {
        return redirect()->route('payorders.index')->with('error',trans('message.invalidId'));
    }

    public function details($payId,$type = 'stream')
    {
        $purchaseOrders = Payorders::where('refrence_number', $payId)->with('getSupplier')->get();
        if(!$purchaseOrders){
            return redirect()->route('payorders.index')->with('error',trans('message.invalidId'));
        }
        $pdf = app('dompdf.wrapper')->loadView('admin.payorders.payorder-pdf', ['purchaseOrders' => $purchaseOrders]);
        if($type == 'stream') {
            return $pdf->stream('invoice.pdf');
        } else {
            return $pdf->download('invoice.pdf');
        }
        // return view('admin.payorders.details', compact('purchaseOrders'));
    }
    
    public function settle($refId)
    {
        $lastPayorder = Payorders::where('refrence_number', $refId)->orderBy('id', 'desc')->first();                

        $app = app();
        $payorders = $app->make('stdClass');
        $payorders->id = $refId;
        $payorders->refrence_number =  $refId;
        $payorders->paid_amount = '';
        $payorders->remaining_amount = $lastPayorder->remaining_amount;

        return view('admin.payorders.settled')->with('heading','Sattled')->with('payorders',$payorders);
    }

    public function settled(Request $request)
    {
        $input = $request->all();
        $lastPayorder = Payorders::where('refrence_number', $input['refrence_number'])->orderBy('id', 'desc')->first();
        $data = array();

            $data['date'] = $lastPayorder->date;
            $data['refrence_number'] = $input['refrence_number'];
            $data['supplier_id'] = $lastPayorder->supplier_id;
            $data['product_code'] = $lastPayorder->product_code;
            $data['product_name'] = $lastPayorder->product_name;
            $data['quantity'] = $lastPayorder->quantity;
            $data['purchase_unit_price'] = $lastPayorder->purchase_unit_price;
            $data['selling_unit_price'] = $lastPayorder->selling_unit_price;
            $data['purchase_amount'] = $lastPayorder->purchase_amount;
            $data['paid_amount'] = $input['paid_amount'];
            $data['tax'] = $lastPayorder->tax;
            $data['remaining_amount'] = ($lastPayorder->remaining_amount-$input['paid_amount']);
            $data['due_date'] = date('Y-m-d');

        Payorders::create($data);

        return redirect()->route('payorders.index')->with('Success', 'Payment Added Successfully');
    }    
}
