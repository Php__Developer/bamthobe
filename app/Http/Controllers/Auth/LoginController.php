<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*
     * Login user
     */
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ]);
        if ($validator->fails()) {
            return back()->with('error', 'Incorrect email address or password.');
        } else {
            if (Auth::attempt([ 'email' => $request->email, 'password' => $request->password ], $request->remember)) {
                if(Session::has('customIntendedURL')) {
                    $customIntendedURL = Session::get('customIntendedURL');
                    Session::forget('customIntendedURL');
                    return redirect($customIntendedURL);
                }
                return redirect()->intended($this->redirectTo);
            }
            return back()->with('error', 'Incorrect email address or password.');
        }
    }

    /*
     * Logout user
     */
    public function logout(Request $request) {
        Auth::logout();
        Session::forget('auctionPaymentInfo');
        return redirect('/login');
    }

    /*
     * Override authenticated method to check user is verified/active
     */
    public function authenticated(Request $request, $user) {
        if($user->status == '1') {
            if(!$user->verified) {
                auth()->logout();
                return back()->with('error', 'Please check your email. We have sent you an activation code.');
            }
        } else {
            auth()->logout();
            return back()->with('error', trans('message.InactiveUserLogin'));
        }
        return redirect()->intended($this->redirectPath());
    }

    /*
     * Redirect the user to Provider
     */
    public function redirectToProvider($provider) {
        if($provider == 'linkedin') {
            $client_id = env('LINKEDIN_CLIENT_ID');
            $redirect_uri = env('LINKEDIN_REDIRECT');
            return redirect("https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=$client_id&redirect_uri=$redirect_uri&state=987924321&scope=r_liteprofile,r_emailaddress");
        }
        return Socialite::driver($provider)->redirect();
    }

    /*
     * Get user details from Provider
     */
    public function handleProviderCallback($provider, Request $request) {
        if($provider == 'linkedin') {
            $accessCode = $request['code'];
            $userSocial = $this->getLinkedinUser($accessCode);
        } else {
            $userSocial = Socialite::driver($provider)->user();
        }
        if(empty($userSocial->email) || empty($userSocial->name)) {
            return redirect('/login')->with('error', 'Unable to get your details. Please try again.');
        }
        $user = User::where(['email' => $userSocial->email])->first();
        if($user) {// Login
            if($user->status == '0') {
                return redirect('/login')->with('error', trans('message.InactiveUserLogin'));
            }
            if(!$user->verified) {
                $user->verified = 1;
                $user->verify_token = NULL;
                $user->save();
            }
        } else {// Register
            $createUser = new User;
            $createUser->name = $userSocial->name;
            $createUser->email = $userSocial->email;
            $createUser->verified = 1;
            $createUser->social_provider = $provider;
            if(Session::has('customIntendedURL')) {
                $createUser->terms_accepted = 1;
            }
            $createUser->save();
            $user = User::where(['email' => $createUser->email])->first();
        }
        if(!$user->terms_accepted) {
            return redirect('/login')->with('acceptTerms', true)->with('loginUser', $user);
        }
        Auth::login($user);
        if(Session::has('customIntendedURL')) {
            $customIntendedURL = Session::get('customIntendedURL');
            Session::forget('customIntendedURL');
            return redirect($customIntendedURL);
        }
        return redirect()->intended($this->redirectTo);
    }

    /*
     * Get Linkedin User
     */
    public function getLinkedinUser($accessCode) {
        $member = new \stdClass;
        $member->email = '';
        $member->name = '';
        $accessToken = $this->getAccessTokenLinkedin($accessCode);
        if(empty($accessToken)) {
            return $member;
        }
        //get email address
        $getEmailUrl = 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))';
        $getEmail = $this->getDataCurl($getEmailUrl, $accessToken);
        if(!empty($getEmail['elements'][0]['handle~']['emailAddress'])) {
            $member->email = $getEmail['elements'][0]['handle~']['emailAddress'];
        }
        //get email address
        //get user profile
        $getProfileUrl = 'https://api.linkedin.com/v2/me';
        $getProfile = $this->getDataCurl($getProfileUrl, $accessToken);
        $firstName = $lastName = '';
        if(!empty($getProfile['firstName']['preferredLocale']['country']) && !empty($getProfile['firstName']['preferredLocale']['language'])) {
            $language = $getProfile['firstName']['preferredLocale']['language'];
            $country = $getProfile['firstName']['preferredLocale']['country'];
            if(!empty($getProfile['firstName']['localized'][$language.'_'.$country])) {
                $firstName = $getProfile['firstName']['localized'][$language.'_'.$country];
            }
        }
        if(!empty($getProfile['lastName']['preferredLocale']['country']) && !empty($getProfile['lastName']['preferredLocale']['language'])) {
            $language = $getProfile['lastName']['preferredLocale']['language'];
            $country = $getProfile['lastName']['preferredLocale']['country'];
            if(!empty($getProfile['lastName']['localized'][$language.'_'.$country])) {
                $lastName = $getProfile['lastName']['localized'][$language.'_'.$country];
            }
        }
        $member->name = trim($firstName.' '.$lastName);
        //get user profile
        return $member;
    }

    /*
     * Get data from curl Linkedin
     */
    public function getDataCurl($url, $accessToken) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $headers = array();
        $headers[] = "Authorization: Bearer $accessToken";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return false;
        }
        curl_close($ch);
        return json_decode($result, TRUE);
    }

    /*
     * Get Access Token Linkedin
     */
    public function getAccessTokenLinkedin($accessCode) {
        $client_id = env('LINKEDIN_CLIENT_ID');
        $client_secret = env('LINKEDIN_CLIENT_SECRET');
        $redirect_uri = env('LINKEDIN_REDIRECT');
        $url = 'https://www.linkedin.com/oauth/v2/accessToken';
        $postfields='code='.$accessCode.'&client_id='.$client_id.'&client_secret='.$client_secret.'&redirect_uri='.$redirect_uri.'&grant_type=authorization_code';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 'Content-type: application/x-www-form-urlencoded');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $tokenData = curl_exec($ch);
        if (curl_errno($ch)) {
            return false;
        }
        curl_close($ch);
        $data = json_decode($tokenData);
        return $data->access_token;
    }

    /*
     * Accept Terms
     */
    public function acceptTerms(User $user, Request $request) {
        request()->validate([
            'terms_accepted' => 'required',
        ]);
        $request = $request->all();
        $request['terms_accepted'] = (isset($request['terms_accepted']))?1:0;
        unset($request['_token']);
        try {
            Auth::login($user);
            User::where('id', Auth::user()->id)->update($request);
            return redirect()->intended($this->redirectTo);
        } catch (\Exception $ex) {
            return redirect()->route('login')->with('error', trans('message.networkErr'));
        }
    }

}
