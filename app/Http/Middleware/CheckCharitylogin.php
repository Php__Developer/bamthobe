<?php

namespace App\Http\Middleware;

use Closure;

class CheckCharitylogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($request->user()->charity_id)) {
            return redirect()->route('admin.dashboard');
        }
        return $next($request);
    }
}
