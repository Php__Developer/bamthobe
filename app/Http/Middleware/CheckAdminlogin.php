<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminlogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty($request->user()->charity_id)) {
            return redirect()->route('charity.auctionDetails');
        }
        return $next($request);
    }
}
