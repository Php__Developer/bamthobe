<?php

namespace App\Providers;

use App\Models\WebsiteContent;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $data= WebsiteContent::all();
        View::share('data', $data);

        view()->composer('*', function($view)
        {   
            
            
            $notification_count = 0;
            if (Auth::check()) {
               $notification = DB::table('notifications')->where([['read',"false"],['user_id',Auth::user()->id ],])->get();
                $notification_count = $notification->count(); 
                $notifications = DB::table('notifications')->where('user_id',Auth::user()->id)->limit(10)->orderBy('created_at', 'desc')->get();
                  View::share('notifications', $notifications);

            }
              
                View::share('notification_count', $notification_count);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
