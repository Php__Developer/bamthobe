<?php

use App\Mail\UserOutbidded;
use App\Mail\AuctionResult;
use Twilio\Rest\Client;

/*
 * Date format
 */
function formatDate($date) {
	if(empty($date)) {
		return '';
	}
	$return = date('d M Y', strtotime($date));
	return $return;
}

/*
 * Date format
 */
function formatTime($date) {
	if(empty($date)) {
		return '';
	}
	$return = date('H:i', strtotime($date));
	return $return;
}

/*
 * Date Time format
 */
function formatDateTime($date) {
	if(empty($date)) {
		return '';
	}
	$return = date('d M Y H:i', strtotime($date));
	return $return;
}

/*
 * Feed Post date
 */
function postDate($date) {
	$date_format = 'h:i A';
	if(empty($date)) {
		return '';
	}

	$currentDateTime = date('Y-m-d');
	$created_date = date('Y-m-d',strtotime($date));
	$diff = abs(strtotime($currentDateTime) - strtotime($created_date));
	$years = floor($diff / (365*60*60*24));
	$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	$date = auctionTime($date);
	if($days == 0 && $months == 0 && $years == 0){
		return ' Today at ' . date($date_format, strtotime($date));
	} else if($days == 1 && $months == 0 && $years == 0){
		return ' Yesterday at ' . date($date_format, strtotime($date));
	} else if($days > 1 && $months == 0 && $years == 0){
		return $days. ' days ago at ' . date($date_format, strtotime($date));
	} else if($days > 0 && $months > 0 && $years == 0){
		return ($months > 1) ? $months.' months ago at ' . date($date_format, strtotime($date)) : $months.' month ago at ' . date($date_format, strtotime($date));
	} else if($days > 0 && $months > 0 && $years > 0){
		return ($years > 1) ? $years.' years ago at ' . date($date_format, strtotime($date)) : $years.' year ago at ' . date($date_format, strtotime($date));
	}
}

/*
 * Return country name from code
 */
function getCountryName($country) {
	if(empty($country)) {
		return '';
	}
    $countryName = CountryState::getCountryName($country);
    return $countryName;
}

/*
 * Return state name from code
 */
function getStateName($state, $country = null) {
	if(empty($state)) {
		return '';
	}
    $stateName = CountryState::getStateName($state, $country);
    return $stateName;
}



/*
 * Get Charity from ID
 */
function getCharityData($charityID) {
	if(empty($charityID)) {
		return '';
	}
	return DB::table('charities')->where('id', $charityID)->first();
}


/*
 * Get Currency Symbol
 */
function getCurrencySymbol($currencyName, $name='') {
	if(empty($currencyName)) {
		return '';
	}
	$symbol = '';
	if(strtolower($currencyName) == 'usd') {
		$symbol = '$';
	} elseif(strtolower($currencyName) == 'sgd') {
		$symbol = '$';
	} elseif(strtolower($currencyName) == 'cny') {
		$symbol = '¥';
	}
	return (!empty($name))?strtoupper(substr($currencyName, 0, -1)).$symbol:$symbol;
}




/*
 * Format Number
 */
function formatNumber($number) {
	if(empty($number)) {
		return 0;
	}
	$return = number_format($number,2);
	return $return;
}

/*
 * Format telephone
 */
function formatTelephone($phone) {
	if(empty($phone)) {
		return '';
	}
	$phonenum = preg_replace("/[^0-9]*/",'',$phone);
	if(strlen($phonenum) == 10) {
		$sArea = substr($phonenum,0,3);
		$sPrefix = substr($phonenum,3,3);
		$sNumber = substr($phonenum,6,4);
		$return = "(".$sArea.") ".$sPrefix."-".$sNumber;
		return $return;	
	} else {
	  	return $phone;
	}
}


function random_number($num = 10){
	$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $num; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}



/*
 * Get donation proceeds
 */
function auctionSupportProceeds($auctionID) {
	$proceeds = DB::table('donations')->select(DB::raw('SUM(amount) as amount'))->where('auction_id', $auctionID)->first();
	if(!empty($proceeds->amount)) {
		return $proceeds->amount;
	} else {
		return 0;
	}
}




/*
 * Get charity logo
 */
function getCharityLogo($charityID) {
	$charityLogo = DB::table('charities')->select('logo')->where('id', $charityID)->first();
	return $charityLogo->logo;
}



/*
 * Send SMS notification
 */
function sendSMS($country_code, $phone, $msg) {
    $TWILIO_ACCOUNT_SID = env('TWILIO_ACCOUNT_SID');
    $TWILIO_AUTH_TOKEN = env('TWILIO_AUTH_TOKEN');
    $TWILIO_PHONE_NUMBER = env('TWILIO_PHONE_NUMBER');
    $client = new Client($TWILIO_ACCOUNT_SID, $TWILIO_AUTH_TOKEN);
    try {
        $message = $client->messages->create(
            '+'.$country_code.$phone, // Text this number
            array(
                'from' => $TWILIO_PHONE_NUMBER, // From a valid Twilio number
                'body' => $msg
            )
        );
        $messageSID = $message->sid;
    } catch (Exception $e) {
        
    } catch(\Twilio\Exceptions\RestException $e) {
        
    }
}

/*
 * Save Notifications
 */
function saveNotification($userID, $msg) {
	try {
		$notification = array('user_id' => $userID, 'message' => $msg, 'created_at' => date('Y-m-d H:i:s'));
		DB::table('notifications')->insert($notification);
	} catch (Exception $e) {
        
    }
}

/*
 * Get Auction Route
 */
function getAuctionRoute($auctionSlug) {
	return env('APP_URL').'/auction/'.$auctionSlug.'/details';
}