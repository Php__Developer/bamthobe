<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class OrderDetail extends Model
{
    
    public $table = 'orders';
   
    protected $fillable = [
        'quantity','total_price','price_paid','price_remaining','delivered_at','status'
    ];

     public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}