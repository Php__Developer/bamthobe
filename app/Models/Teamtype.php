<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teamtype extends Model
{
    public $table = 'type';
    protected $fillable = [
        'typename'
    ];
}
