<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
     public $table = 'inventory';
   
    protected $fillable = [
        'user_id','product_id','comment'
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }    
    public function salesman()
    {
        return $this->belongsTo('App\Models\Salesman','user_id','id');
    }    
}
