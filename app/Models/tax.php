<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class tax extends Model
{
    
    public $table = 'taxes';
    protected $fillable = [
        'tax_rate',
        'buffer_days',
        'delivery_charges'
    ];
   
}