<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Product extends Model
{
    
    public $table = 'products';
   
    protected $fillable = [
        'product_type','product_name','product_details','product_description','product_length','product_price','product_image','product_code','product_color','selling_option'
    ];
    
    public function Product()
    {
        return $this->hasMany('App\Models\Order');
    }
}