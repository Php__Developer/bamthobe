<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = [
        'name', 'email', 'is_email_verified', 'phone', 'is_phone_verified','otp', 'status', 'verified', 'verify_token', 'address','created_by','account_type','last_order_date','total_order','deleted','device_token'
    ];
    
}
