<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Order extends Model
{
    
    public $table = 'orders';
   
    protected $fillable = [
        'order_unique_id','order_by','customer_id','customer_unique_id','customer_name','product_id','product_code','product_name','product_length','quantity','order_type','behaviour','sketch','comments','model','total_price','tax','total_amount','price_paid','price_remaining','discount_code','amount_to_pay','payment_method','delivery_charges','expected_delivery_time','assignment','delivery','delivered_at','deleted_at','status'
    ];
    public function Product()
    {
        return $this->belongsTo('App\Models\Product');
    }
    public function ProductModel(){
    	return $this -> belongsTo('App\Models\ModelPricing','model','id');
    }

    public function AssignmentData(){
        return $this -> belongsTo('App\Models\Assignments','order_unique_id','order_unique_id','cutter_id');
    }

    public function releasedAssignments(){
        return $this -> belongsTo('App\Models\Assignments','order_unique_id','order_unique_id','cutter_id')->where('qc_status','2');
    }

    public function TrackOrder(){
        return $this -> belongsTo('App\Models\Trackorder','order_unique_id','order_unique_id');
    }

    public function orderBy(){
        return $this -> belongsTo('App\Models\Salesman','order_by','id');
    }

    public function customerData(){
        return $this -> belongsTo('App\Models\Customer','customer_id','id');
    }

    public function getComments(){
        return $this -> hasMany('App\Models\Comment','order_unique_id','order_unique_id');
    }

    // public function commentBy(){
    //     return $this -> belongsTo('App\Models\Salesman','user_id','id');
    // }

}