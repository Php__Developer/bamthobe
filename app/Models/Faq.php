<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public $table = 'faqs';
    protected $primaryKey = 'id';
    protected $fillable = [
        'question', 'answer', 'status', 'sort_order','faq_categories_id'
    ];
}
