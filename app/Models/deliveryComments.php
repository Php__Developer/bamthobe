<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class deliveryComments extends Model
{
    public $table = 'delivery_comments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','order_unique_id','comment_image','comment'
    ];
}
