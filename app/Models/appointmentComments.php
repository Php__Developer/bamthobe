<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class appointmentComments extends Model
{
    public $table = 'appointment_comments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','appointment_id','comment_image','comment'
    ];
}
