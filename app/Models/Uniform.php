<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uniform extends Model
{
    public $table = 'business_uniforms';
    protected $primaryKey = 'id';

    protected $fillable = [
        'contact_number','username','password','shopname','responsible_name','shop_email','shop_location','order_status','customer_status','unique_id','quantity','picture','completion_date','price','comments','documents','order_unique_id'
    ];
}
