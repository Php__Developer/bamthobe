<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    public $table = 'business';
    protected $primaryKey = 'id';

    protected $fillable = [
        'contact_number','username','password','shopname','responsible_name','shop_email','shop_location','customer_status','total','unique_id','summer_service_price','winter_sewing_price','piping','washing','botana','embroidery','zipper','sleeping_dress','other','other_price','total_orders'
    ];
}
