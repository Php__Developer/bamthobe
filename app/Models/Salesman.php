<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
    public $table = 'salesmans';
    protected $primaryKey = 'id';

    protected $fillable = [
        'image_name', 'title','email', 'igama_renewal_date','description','status','phone','branch','monthly_target','type','defective_order','password','emp_id','factory_manager','device_token'
    ];
    public function teamtype(){
    	return $this -> belongsTo('App\Models\Teamtype','type','id');
    }

    public function getall()
    {
    	return $this->belongsTo('App\Models\OtherUsers');
    }
    public function branchdata()
    {
        return $this->belongsTo('App\Models\Branch','branch','id');
    }
    public function salary()
    {
        return $this->belongsTo('App\Models\Salary','emp_id','emp_id');
    }
}
