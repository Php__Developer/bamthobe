<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charity extends Model
{
    public $table = 'charities';
    protected $primaryKey = 'id';

    protected $fillable = [
       'title','logo','address','link','telephone','fax','email','description','status'
    ];
}