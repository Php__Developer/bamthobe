<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public $table = 'suppliers';
    protected $primaryKey = 'id';

    protected $fillable = [
        'image_name', 'supplier_name','company_name','email','address','status','primary_phone','secondary_phone'
    ];
}
