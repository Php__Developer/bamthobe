<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $table = 'accounts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'category','type','amount','comment', 'attachment', 'account_date'
    ];
    // public function teamtype(){
    // 	return $this -> belongsTo('App\Models\Teamtype','type','id');
    // }

    // public function getall()
    // {
    // 	return $this->belongsTo('App\Models\OtherUsers');
    // }
    // public function branchdata()
    // {
    //     return $this->belongsTo('App\Models\Branch','branch','id');
    // }
}
