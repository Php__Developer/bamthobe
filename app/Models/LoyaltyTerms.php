<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoyaltyTerms extends Model
{
    public $table = 'loyalty_terms';
   
    protected $fillable = [
        'content'
    ];
}
