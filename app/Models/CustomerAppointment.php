<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class CustomerAppointment extends Model
{
    
    public $table = 'customer_appointments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'customer_id','name','address','phone','appointment_datetime','tailor_id','visit_by','status','accepted_by','reason'
    ];


    // public function tailors()
    // {
    //     return $this->belongsToMany('App\Models\Tailor');
    // }
}