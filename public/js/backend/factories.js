var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
               $('.dataTables_paginate').show();
           }
       },
       columns: [
         { data: 'fac_id', name: 'fac_id', visible: true },
         { data: 'name', name: 'name'},
         { data: 'manager', name: 'manager'},
         { data: 'address', name: 'address' },
         { data: 'phone', name: 'phone', width: 100 },
         { data: 'created_at', name: 'created_at', width: 100 },
         { data: 'status', name: 'status', width: 80 },
         { data: 'action', name: 'action', orderable: false, searchable: false, width: 80 }
       ]
   });
    $('#ModuleModal').on('hidden.bs.modal', function () {
    	$(this).find('form').trigger('reset');
    	$('#changeModuleForm').find('textarea').text('');
    	$('.invalid-feedback').html('');
    });
    $('#show-add-form').on('click', function() {
      $('.change-on-edit').html('Add Factory');
      $('#factoryFormAction').val('add');
      $('#id').val('');
      $('#form-add-button').text('Add');
      $('#ModuleModal').modal();
  });
    $('#changeModuleForm').validate({
        debug: false,
        ignore:":not(:visible)",
        rules:{
            name: {
                required: true,
            }, 
            address : {
                required: true
                
            },
            status: {
                required: true
            }
        },
        messages:{
            name: {
                required: 'Please enter factory name.',
            },
            address:{
                required: 'Please enter address.'
              
            },
            status:{
                required: 'Please enter status.'
            }
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
           $(element).next('p').html(error);
       },
       highlight: function(element) { 
       },
       submitHandler: function(form) {
        var factoryFormAction = $.trim($('#factoryFormAction').val());
        if((factoryFormAction === 'add' || factoryFormAction === 'edit')) {
            $('#form-add-button').attr('disabled',true).css('cursor','wait');
            $('.show-loader').css('display','block');
            var form_data = $('#changeModuleForm').serialize();                
            var url       = $('#base_url').val()+'/factories';
            var id = $('#id').val();
            if(factoryFormAction === 'add') {
                var type = 'POST';
                var newUrl = url;
            } else {
                var type = 'POST';
                var newUrl = url + '/editFactory/' + id;
            }
            $.ajax({
                type: type,
                url: newUrl,
                data: new FormData($("#changeModuleForm")[0]),
                dataType: 'json',                    
                processData: false,
                contentType: false,
            })
            .done(function(response) {
                $('#form-add-button').attr('disabled',false).css('cursor','pointer');
                $('.show-loader').css('display','none');
                $('#ModuleModal').modal('hide');
                $('#changeModuleForm')[0].reset();
                $('#changeModuleForm').find('textarea').text('');
                $('#growls-default').html('');
                switch($.trim(response.status)) {
                    case 'error': 
                    $.growl.error({title: "Oops!", message:  response.message});
                    break;
                    case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                    break;
                    default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                    break;
                }
            })
            .fail(function(jqXHR, textStatus ) {
                $('#form-add-button').attr('disabled',false).css('cursor','pointer');
                $('.show-loader').css('display','none');
                if(jqXHR.status == '422') {
                    var errorObj = jqXHR.responseJSON.errors;
                    if(errorObj && typeof errorObj !== 'undefined') {
                        if (errorObj.hasOwnProperty("name")) {
                            $('#name').find('span').html(errorObj.name[0]).css('display','inline-block');
                        }
                        if (errorObj.hasOwnProperty("address")) {
                            $('#address').find('span').html(errorObj.address[0]).css('display','inline-block');
                        }
                        if (errorObj.hasOwnProperty("status")) {
                            $('#status').find('span').html(errorObj.status[0]).css('display','inline-block');
                        }
                    }
                } else {
                    $('#ModuleModal').modal('hide');
                    $('#changeModuleForm')[0].reset();
                    $('#changeModuleForm').find('textarea').text('');
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    }
});
$('body').on('click', '.delete-records',function() {
    var id = $(this).val();
    $('#confirm-factory-modal-delete-id').val(id);
    $('#confirm-factory-modal').modal();
});  

$('body').on('change','#changeStatus', function() {
   var checked = $('input', this).is(':checked');
   var id = $(this).attr('module-id');
   
   if(id && $.isNumeric(id)) {
       checked = ((checked) ? '1': '0');
       var url = $('#base_url').val() + '/factories/changeStatus';
       $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      })
       $.ajax({
          type: "POST",
          url: url,
          data: {id: id,status: checked},
          dataType: 'json',
          success: function (response) {
             
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
   }
});
$('.dataTables_filter input[type="search"]').on('keypress', function() {
    $('.dataTables_processing').css('visibility','hidden');  
}); 

});
function deleteRecord() {
	var delete_url = $('#base_url').val()+'/factories';
    var id = $("#confirm-factory-modal-delete-id").val();
    $("#confirm-factory-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
        	$('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-factory-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
             case 'success':
             dataTable.ajax.reload();
             $.growl.notice({title: "Success!", message:  response.message});
             break;
             case 'error':
             $.growl.error({title: "Oops!", message:  response.message});
             break
             default:
             $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
             break;

         }
     },
     error: function (response) {
        $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
        $('.show-loader').css('display','none');
        $('#confirm-factory-modal').modal('hide');
        $('#growls-default').html('');
        $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
    }
});
}
function showEditModel(id) {
    show_loader();
    var i =1;
    var edit_url = $('#base_url').val()+'/factories';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: 'post',
        url: edit_url + '/getEditRecord',
        data: {
            id:id
        },
        dataType:'JSON',
        success: function (response) {
          hide_loader();
          switch($.trim(response.status)) {
              case 'error': 
              $.growl.error({title: "Oops!", message:  response.message});
              break;
              case 'success':
              var id = response.result.id;
    					var name = response.result.name;
              var address = response.result.address;
              var phone = response.result.phone;
    					var status = response.result.status;
              var manager = response.result.manager_id;
              //create dynamic options for manager
              var editManager = response.result.manager;
              var option = '';
              $.each(editManager, function( k, v ) {
                 option += '<option value="'+ k + '">' + v + '</option>';
              });
              $("#manager_id option").remove();
              $("#manager_id").append(option);
              //End dynamic option manager
              $('.change-on-edit').html('Edit Factory');
              $('#factoryFormAction').val('edit');
              $('#id').val(id);
              $('#name').val(name);
              $('#address').val(address);
              $('#phone').val(phone);
              $('#manager_id').val(manager);
              $('#status').val(status);
              $('#form-add-button').text('Update');
              $('#ModuleModal').modal();
              break;
              default:
              $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
              break;
          }   
        }
    });
}