var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "asc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'order_unique_id', name: 'order_unique_id',width: 50 },
            { data: 'customer_unique_id', name: 'customer_unique_id',width: 80 },
            { data: 'branch', name: 'branch',width: 80 },
            { data: 'order_by', name: 'order_by',width: 30 },
            { data: 'created_at', name: 'order_date',width: 50 },
            { data: 'delivered_at', name: 'delivered_at',width: 10 },
            { data: 'status', name: 'status', width: 50 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 20 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           title: {
                 required: true
            },
        },
        messages:{
            title: {
                required: 'Please enter title.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });
})