var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'name', name:  'name', width: 60 },
            { data: 'email', name:  'email', width: 60 },
            { data: 'title', name: 'title', width:50},
            { data: 'message',name: 'message',width:  100 },
            { data: 'created_at', name: 'created_at', width: 50 },
            // { data: 'action', name: 'action', orderable: false, searchable: false, width: 60 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
})