var dataTable;
$(document).ready(function() {
    $('#dob').datetimepicker({
        format:'d M Y',
        timepicker: false
    });
  dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                 $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'branch_id', name: 'branch_id', visible: true, width: 20}, 
            { data: 'branch', name: 'branch',width: 70 },
            { data: 'phone', name: 'phone',width: 110 },
            { data: 'factory', name: 'factory',width:100 },
            { data: 'manager', name: 'manager',width:100 },
            { data: 'target', name: 'target',width: 70 },
            { data: 'achieve', name: 'achive',width: 70 },
            { data: 'status', name: 'status', width: 50, searchable: false },
            { data: 'created_at', name: 'created_at', width: 80 },
           // { data: 'updated_at', name: 'updated_at', width: 80 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 70 }
        ],
        columnDefs: [ { targets: '_all', defaultContent: '--' } ]
    });
    $(document).on('click','.delete-branch',function(){
        var faq_id = $(this).val();
        $('#DeleteBranch-delete-id').val(faq_id);
        $('#DeleteBranch').modal('show');
    });
    $('#branches-edit-form').validate({
        ignore: [],
        rules:{
            name: {
                required: true,
            }, 
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#base_url").val()+"/branches/checkEmail",
                    type: "GET",
                    cache: false,
                    data: { 'email':function(){return $('#email').val()}, 'branchid':function() { return $('#branch_id').val() } },
                    dataType: "json"
                }
            },
            
            phone: {
                number: true,
                maxlength: 12
            },
            status: {
              required: true
            }
        },
        messages:{
            name: {
                required: 'Please enter name.'
            },
            email:{
                required: 'Please enter email address.',
                email: 'Please enter a valid email address.',
                remote: 'This email address has already been taken.'
            },
            phone: {
                number: "Please enter numbers only.",
                maxlength: "Please enter no more than 12 digits."
            },
            status: {
              required: 'Please select status.'
            }
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
            $(element).next('p').html(error);
        },
    });
});
// $(document).on('change','#country',function() {
//     var countryCode = $('#country').val();
//     if(countryCode != '') {
//         $.ajax({
//             type: 'get',
//             url: $('#getStates').val(),
//             data: { country_code: countryCode },
//             success: function (response) {
//                 $('#state').html(response.state_html);
//             }
//         });
//     }
// });
function changePasswordbranches(branches_id) {   
    $("#branch_id").val(branches_id);
}
function changeBranchStatus(branch_id, status) {
    if(branch_id && $.isNumeric(status)) {
        var url = $('#base_url').val() + '/branches/changeStatus';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $.ajax({
            type: "POST",
            url: url,
            data: {branch_id: branch_id, status: status},
            dataType: 'json',
            success: function (response) {
                switch($.trim(response.status)) {
                    case 'success':
                        $.growl.notice({title: "Success!", message:  response.message});
                    break;
                    case 'error':
                        $.growl.error({title: "Oops!", message:  response.message});
                    break
                    default:
                        $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                    break;
                }
                dataTable.ajax.reload();
            },
            error: function (response) {
                $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                dataTable.ajax.reload();
            }
        });
    }
}
function DeleteBranch() {
    var delete_url = $('#base_url').val()+'/branches/delete';
    var branch_id = $("#DeleteBranch-delete-id").val();
    $("#DeleteBranch-delete-id").val('');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "POST",
        url: delete_url ,
        data: {branch_id: branch_id},
        dataType: 'json',
        success: function (data) {
            $('.show-loader').css('display','none');
            dataTable.ajax.reload();
            $('#growls-default').html('');
            switch($.trim(data.status)) {
                case 'success':
                    $.growl.notice({title: "Success!", message:  data.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  data.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;
            }
        },
        error: function (data) {
            $('.show-loader').css('display','none');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}

