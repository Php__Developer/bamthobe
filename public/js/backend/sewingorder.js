var dataTable;
$(document).ready(function() { 
    dataTable = $('#data-table').DataTable({
        processing: true, 
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "asc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'order_unique_id', name: 'order_unique_id',width: 50 },
            { data: 'product_id', name: 'item_number',width: 100 },
            { data: 'tailor_id', name: 'tailor_id',width: 80 },
            { data: 'branch', name: 'branch',width: 100 },
            { data: 'salesman', name: 'salesman',width: 80 },   
            { data: 'qc_id', name: 'qc',width: 80 },           
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 20 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           title: {
                 required: true
            },
        },
        messages:{
            title: {
                required: 'Please enter title.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });
});

$('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 

//For add comment by the admin
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-submit").click(function(e){
        e.preventDefault();
        var comment = $("input[name=comment]").val();
        var order_unique_id = $("input[name=order_unique_id]").val();
        var user_id = $("input[name=user_id]").val();
        var is_admin = $("input[name=is_admin]").val();

        if(comment == ''){
            alert("Please enter comment before submit");
        }
        else if(order_unique_id == ''){
            alert("Order id not found");            
        }
        else if(user_id == ''){
            alert("User id not found");            
        }
        $.ajax({
           type:'POST',
           url:'/admin/add/comment',
           data:{comment:comment, order_unique_id:order_unique_id, user_id:user_id, is_admin:is_admin},

           success:function(data){
              $(".success").show();
              $("input[name=comment]").val('');
              $('#commentModal').modal('hide');
              location.reload();
           }
        });
    });

    function deleteRecord() {
    var delete_url = $('#base_url').val()+'/orders';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}