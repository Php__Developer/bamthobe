var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'id', name: 'id', visible: false },
            { data: 'logo', name: 'logo', width: 40, orderable: false, searchable: false },
            { data: 'title', name: 'title',width: 50 },
            { data: 'address', name: 'address',width: 80 },
            { data: 'link', name: 'link',width: 10 },
            { data: 'telephone', name: 'telephone',width: 100 },
            { data: 'fax', name: 'fax' ,width: 40},
            { data: 'email', name: 'email',width: 40 },
            { data: 'status', name: 'status', width: 80 },
            { data: 'created_at', name: 'created_at', width: 100 },
            { data: 'updated_at', name: 'updated_at', width: 110 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 80 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });

    // $.validator.addMethod("custom_number", function(value, element) {
    //     return this.optional(element) || 
    //         value.match(/^[0-9,\+-]+$/);
    // }, "Please enter a valid number");

    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        errorPlacement: function(error, element) {
            if(element.attr("name") == "logo" ) {
                error.insertAfter('#charity-logo-err');
            } else {
                error.insertAfter(element);
            }
        },
        rules:{
            title: {
                required: true
            },
            address: {
               // required: true
            },
            link: {
                url: true
            },
            telephone: {
                // required: true,
                //number: true,
                //custom_number: true,
            },
            fax: {
                // required: true,
               // number: true,
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
            description: {
                required: true,
            },
            logo: {
                required: {
                    depends: function(element) {
                        return ($("#image-exist").val() == 'yes')?false:true;
                    }
                },
                extension: "png|jpeg|jpg|svg",
            }
        },
        messages:{
            title: {
                required: 'Please enter title.'
            },
            address: {
                //required: 'Please enter address.'
            },
            link: {
                url: 'Please enter a valid URL.'
            },
            telephone: {
                //required: 'Please enter telephone no.',
               // number:'Please enter numbers only.',
               // custom_number:'Please enter a valid number'
            },
            fax: {
               // required: 'Please enter fax no.',
               // number:'Please enter numbers only.'
            },
            email: {
               required: 'Please enter email.',
                email:    'Please enter valid email'
            },
            password: {
                required: 'Please enter password.',
                minlength: "Password must be 6 characters long.",
            },
            description: {
                required: 'Please enter description.'
            },
            logo: {
                required: 'Please select image.',
                extension: "Please select image file.",
            }
        },
         submitHandler: function(form) {
                form.submit();
            }
        });

    // $('#telephone').usPhoneFormat({
    //     format: '(xxx) xxx-xxxx',
    // });

    //  $('#fax').usPhoneFormat({
    //     format: '(xxx) xxx-xxxx',
    // });
    
    $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 
    $('body').on('change','#changeStatus', function() {
        var checked   = $('input', this).is(':checked');
        var id = $(this).attr('module-id');
        if(id && $.isNumeric(id)) {
            checked = ((checked) ? '1': '0');
            var url = $('#base_url').val() + '/charities/changeStatus';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: url,
                data: {id: id, status: checked},
                dataType: 'json',
                success: function (response) {
                    $('#growls-default').html('');
                    switch($.trim(response.status)) {
                        case 'success':
                            $.growl.notice({title: "Success!", message:  "Success"});
                        break;
                        case 'error':
                            $.growl.error({title: "Oops!", message:  response.message});
                        break
                        default:
                            $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                        break;

                    }
                },
                error: function (response) {
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });

    $("#logo").change(function(){
        var type = $(this).val();
        $('#charity-image').find('span').html('');
        switch(type.substring(type.lastIndexOf('.') + 1).toLowerCase()) {
            case 'png': case 'x-png': case 'gif': case 'jpeg': case 'pjpeg': case 'jpg': case 'svg': case 'svg+xml': 
                checkImageFile = true;
                readImageURL(this,'show-image-preview');
            break;
            default: 
                checkImageFile = false;
                //$('#service-image').find('span').html(langMsgs['imgErr']).css('display','inline-block');
            break;

        }
    });
})

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/charities';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  "Deleted"});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}


