var serviceTable, contactTable, faqTable, EmailTemplateTable, newsletterTable, techNewsletterTable, subscriberNewsletterTable, email_template_body_content, select_price, settings_validate;
var  autocomplete;

$(document).ready(function($) {
    var url = $('#url').val();
    var ajaxLoading = false;
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
	$('.menu').on('click', function(e) {
        e.preventDefault();
        if(!$(this).next('.dropwn-sidebar').hasClass('open')) {
        	$('.dropwn-sidebar').slideUp();
            $(this).next('.dropwn-sidebar').stop().slideToggle();
        	$('.dropwn-sidebar').removeClass('open');
            $('.menu').removeClass('arrow-down');
        	$(this).next('.dropwn-sidebar').addClass('open');
        	$(this).addClass('arrow-down');
        } else {
            $(this).next('.dropwn-sidebar').stop().slideToggle();
        	$(this).next('.dropwn-sidebar').removeClass('open');
        	$(this).removeClass('arrow-down');
        }
    });
    $(document).on('click','#deleteContactButton',function(){
        var product_id = $(this).val();
        $('#DeleteContact-delete-id').val(product_id);
        $('#DeleteContact').modal('show');
    });
    $(document).on('click','#deletefaq',function(){
        var faq_id = $(this).val();
        $('#DeleteFaq-delete-id').val(faq_id);
        $('#DeleteFaq').modal('show');
    });
    $('#changePasswordModel').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
        $('.invalid-feedback').html('');
    });

    $('#website-create-form').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           facebook: {
                 url: true
            },
            instagram: {
                 url: true
            },
            twitter: {
                 url: true
            }
        },
        messages:{
            facebook: {
                url: 'Please enter valid url.'
            },
            instagram: {
                url: 'Please enter valid url.'
            },
            twitter: {
                url: 'Please enter valid url.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });

    $("#changePasswordForm").validate({   
        rules:{
            current_password: {
                required: true,
            },
            new_password:{
                required: true,
                minlength: 6,
            },  
            confirm_password:{
                required: true,
                equalTo: '#new_password',
                minlength: 6,
            },  
        },
        messages:{
            current_password: {
                required: 'Please enter your current password.',
            },
            new_password:{
                required: 'Please enter new password.',
                minlength: "Password must be 6 characters long."
            },
            confirm_password: {
                required: 'Please enter confirm password.',
                equalTo: 'Passwords does not match.',                   
                minlength: "Password must be 6 characters long."
            }, 
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
             $(element).next('p').html(error);
        },
        highlight: function(element) { 
        },
        submitHandler: function(form) {
            $('#change-password-button').attr('disabled',true).css('cursor','wait');

            var form_data = $('#changePasswordForm').serialize();
            var url = $('#base_url').val();
            $.ajax({
                type: 'POST',
                url: form.action,
                data: form_data,
                dataType: 'json'
            })
            .done(function(response) {
                $('#change-password-button').attr('disabled',false).css('cursor','pointer');
                $('#growls-default').html('');
                switch($.trim(response.status)) {
                    case 'error': 
                        $('#current-password').find('span').html(response.message).css('display','inline-block');
                    break;
                    case 'success':
                        $('#changePasswordModel').modal('hide');
                        $('#changePasswordForm')[0].reset();
                        $.growl.notice({title: "Success!", message:  response.message});
                    break;
                    default:
                        $('#changePasswordModel').modal('hide');
                        $('#changePasswordForm')[0].reset();
                        $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                    break;
                }
            })
            .fail(function(jqXHR, textStatus ) { 
                $('#change-password-button').attr('disabled',false).css('cursor','pointer');
                if(jqXHR.status == '422') {
                    var errorObj = jqXHR.responseJSON.errors;
                    if(errorObj && typeof errorObj !== 'undefined') {
                        if (errorObj.hasOwnProperty("current_password")) {
                            $('#current-password').find('span').html(errorObj.current_password[0]).css('display','inline-block');
                        }
                        if (errorObj.hasOwnProperty("new_password")) {
                            $('#new-password').find('span').html(errorObj.new_password[0]).css('display','inline-block');
                        }
                        if (errorObj.hasOwnProperty("confirm_password")) {
                            $('#confirm-password').find('span').html(errorObj.confirm_password[0]).css('display','inline-block');
                        }
                   }
                } else {
                    $('#changePasswordModel').modal('hide');
                    $('#changePasswordForm')[0].reset();
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });
    $('.sidebar__toggle').click(function(){
        $('.sidebar__wrapper').toggleClass('open');
    });
    //USERS AJAX
    $("#changePasswordUsersForm").validate({   
        rules:{
            new_password_users:{
                required: true,
                minlength: 6,
            },  
            confirm_password_users:{
                required: true,
                equalTo: '#new_password_users',
                minlength: 6,
            },  
        },
        messages:{           
            new_password_users:{
                required: 'Please enter new password.',
                minlength: "Password must be 6 characters long."
            },
            confirm_password_users: {
                required: 'Please enter confirm password.',
                equalTo: 'Passwords does not match.',                   
                minlength: "Password must be 6 characters long."
            }, 
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
             $(element).next('p').html(error);
        },
        highlight: function(element) { 
        },
        submitHandler: function(form) {
            $('#change-password-button-users').attr('disabled',true).css('cursor','wait');
            var form_data = $('#changePasswordUsersForm').serialize();
            var url = $('#base_url').val();
            $.ajax({
                type: 'POST',
                url: url+ '/changePasswordUsers',
                data: form_data,
                dataType: 'json'
            })
            .done(function(response) {
                $('#change-password-button-users').attr('disabled',false).css('cursor','pointer');
                $('#growls-default').html('');
                switch($.trim(response.status)) {
                   
                    case 'success':
                        $('#changePasswordUsers').modal('hide');
                        $('#changePasswordUsersForm')[0].reset();
                        $.growl.notice({title: "Success!", message:  response.message});
                    break;
                    default:
                        $('#changePasswordUsers').modal('hide');
                        $('#changePasswordUsersForm')[0].reset();
                        $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                    break;
                }
            })
            .fail(function(jqXHR, textStatus ) {
                $('#change-password-button-users').attr('disabled',false).css('cursor','pointer');
                if(jqXHR.status == '422') {
                    var errorObj = jqXHR.responseJSON.errors;
                    if(errorObj && typeof errorObj !== 'undefined') {
                       
                        if (errorObj.hasOwnProperty("new_password_users")) {
                            $('#new-password-users').find('span').html(errorObj.new_password[0]).css('display','inline-block');
                        }
                        if (errorObj.hasOwnProperty("confirm_password_users")) {
                            $('#confirm-password-users').find('span').html(errorObj.confirm_password[0]).css('display','inline-block');
                        }
                   }
                } else {
                    $('#changePasswordUsers').modal('hide');
                    $('#changePasswordUsersForm')[0].reset();
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });
    $('#faq-create-form').validate({
        ignore: [],
        debug: false,
        rules:{
            question: {
                required: true,
            }, 
            answer: {
                check_faq_answer: true,                
            },
            status: {
                required: true,                
            },
            sort_order: {
                check_sortorder_numeric:true,
                required: true,               
            },
             faq_categories_id: {
                required: true,               
            }            
        },
        messages:{
            question: {
                required: 'Please enter question.',
            },                     
            status: {
                required: 'Please select status.',
            },
            sort_order: {
                required: 'Please select sort order.',
            },
             faq_categories_id: {
                required: 'Please select category.',             
            } 

        },
        errorElement:"span",
        errorClass:"invalid-feedback",
         errorPlacement: function(error, element) {
            if(element.attr('id') == 'answer'){
                 $(element).next('div').next('p').next('p').html(error);
            } else {
                $(element).next('p').html(error);
            }
         },     
          
    });
    $.validator.addMethod("check_faq_answer", function (value, element) {
           return check_ck_editor();
    }, 'Please enter answer.'); 
    $.validator.addMethod("check_sortorder_numeric", function (value, element) {
        if(Math.floor(value) == value && $.isNumeric(value)) {
            return !this.optional(element);
        } else {
            return this.optional(element);
        }
    }, 'Please enter integer value.'); 
    $('#terms-edit-form').validate({
        ignore: [],
        debug: false,
        rules:{           
            title: {
                required: true,
            }, 
            content: {
               check_terms_add_method: true, 
            },       
                     
        },
        messages:{
            title: {
                required: 'Please enter title.',
            },
            
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
            if(element.attr('id') == 'content'){
                 $(element).next('div').next('p').next('p').html(error);
            } else {
                $(element).next('p').html(error);
            }
        },
    });
    $('#private-edit-form').validate({
        ignore: [],
        debug: false,
        rules:{            
            title: {
                required: true,
            }, 
            content: {
               check_privatepolicy_add_method: true, 
            },                              
        },
        messages:{
            title: {
                required: 'Please enter title.',
            },          
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
         errorPlacement: function(error, element) {
            if(element.attr('id') == 'content'){
                 $(element).next('div').next('p').next('p').html(error);
            } else {
                $(element).next('p').html(error);
            }
        },
    });
    $.validator.addMethod("check_privatepolicy_add_method", function (value, element) {
        return check_ck_editor();
    }, 'Please enter content.');   
    $.validator.addMethod("check_terms_add_method", function (value, element) {
        return check_ck_editor();
    }, 'Please enter content.'); 
    $('#aboutus-edit-form').validate({
        ignore: [],
        debug: false,
        rules:{            
            title: {
                required: true,
            }, 
            content: {
               check_aboutus_content_add_method: true,
            }   
        },
        messages:{
            title: {
                required: 'Please enter title.',
            }      
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
            if(element.attr('id') == 'content') {
                $(element).next('div').next('p').next('p').html(error);
            } else {
                $(element).next('p').html(error);
            }
        },   
    });
    $.validator.addMethod("check_aboutus_content_add_method", function (value, element) {
        return check_ck_editor();
    }, 'Please enter content.');


    $('#donationVsBid-edit-form').validate({
        ignore: [],
        debug: false,
        rules:{            
            title: {
                required: true,
            }, 
            content: {
               check_donationVsbid_content_add_method: true,
            }   
        },
        messages:{
            title: {
                required: 'Please enter title.',
            }      
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
            if(element.attr('id') == 'content') {
                $(element).next('div').next('p').next('p').html(error);
            } else {
                $(element).next('p').html(error);
            }
        },   
    });
    $.validator.addMethod("check_donationVsbid_content_add_method", function (value, element) {
        return check_ck_editor();
    }, 'Please enter content.');



    $('#terms_edit').click(function(){
        var url = $('#base_url').val() + '/ContentManagement/edit_terms_data';      
        $.ajax ({
            type: 'get',
            url: url,
            datatype: 'json',                 
            success:function(data){
                $("#title").val(data[0]['title']);
                $("#content").val(data[0]['content']);
                CKEDITOR.instances.content.setData( data);
                $(".allTypeError span").remove(); 
            }
        })
    });

    $('#about_edit').click(function(){
        var url = $('#base_url').val() + '/ContentManagement/edit_about_data';      
        $.ajax ({
            type: 'get',
            url: url,
            datatype: 'json',                 
            success:function(data){
                $("#title").val(data[0]['title']);
                $("#content").val(data[0]['content']);
                CKEDITOR.instances.content.setData( data);
                $(".allTypeError span").remove(); 
            }
        })
    });

     $('#donationVsBid_edit').click(function(){
        var url = $('#base_url').val() + '/ContentManagement/edit_donation_vs_Bid';      
        $.ajax ({
            type: 'get',
            url: url,
            datatype: 'json',                 
            success:function(data){
                $("#title").val(data[0]['title']);
                $("#content").val(data[0]['content']);
                CKEDITOR.instances.content.setData( data);
                $(".allTypeError span").remove(); 
            }
        })
    });

    $('#private_edit').click(function(){
        var url = $('#base_url').val() + '/ContentManagement/edit_private_data';      
        $.ajax ({
            type: 'get',
            url: url,
            datatype: 'json',                 
            success:function(data){
               $("#title").val(data[0]['title']);
               $("#content").val(data[0]['content']);
               CKEDITOR.instances.content.setData( data );
               $(".allTypeError span").remove(); 
            }
        })
    });  
    
    contactTable = $('#contact-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: $('#contact-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
            $('.dataTables_filter input[type="search"]').on('keypress', function() {
                $('.dataTables_processing').css('visibility','hidden');  
            });
        },
        columns: [
            { data: 'contact_id', name: 'contact_id', visible: false },
            { data: 'contact_name', name: 'contact_name' },
            { data: 'contact_email', name: 'contact_email' },            
            { data: 'contact_phone', name: 'contact_phone' },           
            { data: 'contact_status', name: 'contact_status', width: 80 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 100 }
        ]
    });
    
    faqTable = $('#faq-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: $('#faq-table-url').val(),
        order: [[ 3, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
            $('.dataTables_filter input[type="search"]').on('keypress', function() {
                $('.dataTables_processing').css('visibility','hidden');  
            }); 

        },
        columns: [
            { data: 'id', name: 'id', visible: false },
            { data: 'question', name: 'question' },
            { data: 'answer', name: 'answer', },
            { data: 'sort_order', name: 'sort_order',  width: 80 },
            { data: 'status', name: 'status',  width: 80  },
            { data: 'created_at', name: 'created_at', width: 100 },
            { data: 'updated_at', name: 'updated_at', width: 100 },
            { data: 'action', name: 'action', width: 80, orderable: false, searchable: false }
        ]
    });
});
function numbersonly(e){
  var unicode=e.charCode? e.charCode : e.keyCode
  if (unicode!=8 || unicode!=46){ 
      if (unicode<48||unicode>57) 
          return false 
  }
}

function restrictSpecialChar(e){
    var unicode=e.charCode? e.charCode : e.keyCode;
    if ( ( unicode >= 48 && unicode < 58 ) || ( unicode >= 64 && unicode < 91 ) || ( unicode >= 97 && unicode < 123 )  || unicode == 109 || unicode==8  || unicode==45) {
        return true;
    } else {
        return false
    }
}
function changefaqStatus(faq_id,status){
    if(faq_id && $.isNumeric(status)) {
        var url = $('#base_url').val() + '/faq/changestatus';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $.ajax({
            type: "POST",
            url: url,
            data: {faq_id: faq_id, status: status},
            dataType: 'json',
            success: function (response) {
                switch($.trim(response.status)) {
                    case 'success':
                        $.growl.notice({title: "Success!", message:  response.message});
                    break;
                    case 'error':
                        $.growl.error({title: "Oops!", message:  response.message});
                    break
                    default:
                        $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                    break;                
                }
                faqTable.ajax.reload();
            },
            error: function (response) {
                $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                faqTable.ajax.reload();
            }
        });
    }
}
function show_loader(){
    $('.show-loader').css('display','block');
}
function hide_loader(){
    $('.show-loader').css('display','none');
}
function deleteFaq(){
    var delete_url = $('#url').val();
    var product_id = $("#DeleteFaq-delete-id").val();
    $("#DeleteFaq-delete-id").val('');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + product_id,
        success: function (data) {
            $('.show-loader').css('display','none');
            faqTable.ajax.reload();
            
            $('#growls-default').html('');
            switch($.trim(data.status)) {
                case 'success':
                    $.growl.notice({title: "Success!", message:  data.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  data.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (data) {
            $('.show-loader').css('display','none');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}

/*** js Lang **/
var langMsgs = {
    networkErr: 'Something went wrong. Please try again.',
    internalErr: 'There is some internal error. Please try after some time.',
    imgErr: 'Please upload image file only.',  
}
function check_ck_editor() {
    if (!$("#cke_1_contents iframe").contents().find("body").text()) {
        return false;
    }
    else {
        $("#error_check_editor").empty();
        return true;
    }
}
function readImageURL(input,preview) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#'+preview).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function rem_error_msg_users() {
    $("#new-password-users").empty();
    $("#confirm-password-users").empty();
}