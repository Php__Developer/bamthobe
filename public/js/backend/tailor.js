var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'id', name: 'id', visible: true,width: 50 },
            { data: 'image_name', name: 'image_name', width: 40, orderable: false, searchable: false },
            { data: 'title', name: 'title',width: 50 },
            { data: 'email', name: 'email',width: 50 },
            { data: 'phone', name: 'phone',width: 50 },
            { data: 'factory', name: 'factory',width: 100 },
            { data: 'monthly_target', name: 'monthly_target', width: 50 },
            { data: 'evaluation', name: 'evaluation',width: 10 },
            { data: 'status', name: 'status', width: 80 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 50 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           title: {
                 required: true
            },
        },
        messages:{
            title: {
                required: 'Please enter title.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });

    $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 
    $('body').on('change','#changeStatus', function() {
        var checked   = $('input', this).is(':checked');
        var id = $(this).attr('module-id');
        if(id && $.isNumeric(id)) {
            checked = ((checked) ? '1': '0');
            var url = $('#base_url').val() + '/tailor/changeStatus';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: url,
                data: {id: id, status: checked},
                dataType: 'json',
                success: function (response) {
                    $('#growls-default').html('');
                    switch($.trim(response.status)) {
                        case 'success':
                            $.growl.notice({title: "Success!", message:  response.message});
                        break;
                        case 'error':
                            $.growl.error({title: "Oops!", message:  response.message});
                        break
                        default:
                            $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                        break;

                    }
                },
                error: function (response) {
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });

    $("#image_name").change(function(){
        var type = $(this).val();
        $('#Feed-image').find('span').html('');
        switch(type.substring(type.lastIndexOf('.') + 1).toLowerCase()) {
            case 'png': case 'x-png': case 'gif': case 'jpeg': case 'pjpeg': case 'jpg': case 'svg': case 'svg+xml': 
                checkImageFile = true;
                readImageURL(this,'show-image-preview');
            break;
            default: 
                checkImageFile = false;
                //$('#service-image').find('span').html(langMsgs['imgErr']).css('display','inline-block');
            break;

        }
    });
})

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/tailor';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}


