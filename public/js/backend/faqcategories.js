var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
               $('.dataTables_paginate').show();
           }
       },
       columns: [
         { data: 'id', name: 'id',visible:false},
         { data: 'name', name: 'name' },
         { data: 'created_at', name: 'created_at', width: 100 },
         { data: 'updated_at', name: 'updated_at', width: 100 },
         { data: 'action', name: 'action', orderable: false, searchable: false, width: 80 }
       ]
   });
    $('#ModuleModal').on('hidden.bs.modal', function () {
    	$(this).find('form').trigger('reset');
    	$('#changeModuleForm').find('textarea').text('');
    	$('.invalid-feedback').html('');
    });
    $('#show-add-form').on('click', function() {
      $('.change-on-edit').html('Add FAQ Category');
      $('#FaqCategoryFormAction').val('add');
      $('#id').val('');
      $('#form-add-button').text('Add');
      $('#ModuleModal').modal();
  });
    $('#changeModuleForm').validate({
        debug: false,
        ignore:":not(:visible)",
        rules:{
            title: {
                required: true,
            }
        },
        messages:{
            title: {
                required: 'Please enter title.',
            }
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
           $(element).next('p').html(error);
       },
       highlight: function(element) { 
       },
       submitHandler: function(form) {
        var FaqCategoryFormAction = $.trim($('#FaqCategoryFormAction').val());
        if((FaqCategoryFormAction === 'add' || FaqCategoryFormAction === 'edit')) {
            $('#form-add-button').attr('disabled',true).css('cursor','wait');
            $('.show-loader').css('display','block');
            var form_data = $('#changeModuleForm').serialize();                
            var url       = $('#base_url').val()+'/faqcategories';
            var id = $('#id').val();
            if(FaqCategoryFormAction === 'add') {
                var type = 'POST';
                var newUrl = url;
            } else {
                var type = 'POST';
                var newUrl = url + '/editFaqCategory/' + id;
            }
            $.ajax({
                type: type,
                url: newUrl,
                data: new FormData($("#changeModuleForm")[0]),
                dataType: 'json',                    
                processData: false,
                contentType: false,
            })
            .done(function(response) {
                $('#form-add-button').attr('disabled',false).css('cursor','pointer');
                $('.show-loader').css('display','none');
                $('#ModuleModal').modal('hide');
                $('#changeModuleForm')[0].reset();
                $('#changeModuleForm').find('textarea').text('');
                $('#growls-default').html('');
                switch($.trim(response.status)) {
                    case 'error': 
                    $.growl.error({title: "Oops!", message:  response.message});
                    break;
                    case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                    break;
                    default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                    break;
                }
            })
            .fail(function(jqXHR, textStatus ) {
                $('#form-add-button').attr('disabled',false).css('cursor','pointer');
                $('.show-loader').css('display','none');
                if(jqXHR.status == '422') {
                    var errorObj = jqXHR.responseJSON.errors;
                    if(errorObj && typeof errorObj !== 'undefined') {
                        if (errorObj.hasOwnProperty("title")) {
                            $('#title').find('span').html(errorObj.title[0]).css('display','inline-block');
                        }
                        
                    }
                } else {
                    $('#ModuleModal').modal('hide');
                    $('#changeModuleForm')[0].reset();
                    $('#changeModuleForm').find('textarea').text('');
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    }
});
$('body').on('click', '.delete-records',function() {
    var id = $(this).val();
    $('#confirm-faqcategory-modal-delete-id').val(id);
    $('#confirm-faqcategory-modal').modal();
});  


$('.dataTables_filter input[type="search"]').on('keypress', function() {
    $('.dataTables_processing').css('visibility','hidden');  
}); 

});
function deleteRecord() {
	var delete_url = $('#base_url').val()+'/faqcategories';
    var id = $("#confirm-faqcategory-modal-delete-id").val();
    $("#confirm-faqcategory-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
        	$('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-faqcategory-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
             case 'success':
             dataTable.ajax.reload();
             $.growl.notice({title: "Success!", message:  response.message});
             break;
             case 'error':
             $.growl.error({title: "Oops!", message:  response.message});
             break
             default:
             $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
             break;

         }
     },
     error: function (response) {
        $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
        $('.show-loader').css('display','none');
        $('#confirm-faqcategory-modal').modal('hide');
        $('#growls-default').html('');
        $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
    }
});
}
function showEditModel(id) {
    show_loader();
    var i =1;
    var edit_url = $('#base_url').val()+'/faqcategories';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: 'post',
        url: edit_url + '/getEditRecord',
        data: {
            id:id
        },
        dataType:'JSON',
        success: function (response) {
          hide_loader();
          switch($.trim(response.status)) {
              case 'error': 
              $.growl.error({title: "Oops!", message:  response.message});
              break;
              case 'success':
              var id = response.result.id;
    					var name = response.result.name;
    					
              $('.change-on-edit').html('Edit FAQ Categories');
              $('#FaqCategoryFormAction').val('edit');
              $('#id').val(id);
              $('#title').val(name);
              $('#form-add-button').text('Update');
              $('#ModuleModal').modal();
              break;
              default:
              $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
              break;
          }   
        }
    });
}