<?php
return [
	'imgTypeError'				=> 'Please upload image file only.',
	'networkErr'                => 'Something went wrong. Please try again.',
	'success'                   => 'Success',
	'error'                     => 'Error',
	'statusUpdated'   		    => 'Status updated successfully.',
	'passwordUpdated'           => 'Password has been updated successfully.',
	'notMatchPassword'          => 'You have entered your current password incorrectly.',
  	'invalidId'                 => 'Something went wrong. Please try again.',
	'AddedMsg'		  	        => ' added successfully.',
	'DeletedMsg'		 	    => ' deleted successfully.',
	'UpdatedMsg'		 	    => ' updated successfully.',
	'InactiveUserLogin'		    => 'Your account has been de-activated. Please contact administrator.',
	'websiteContentMsg'			=>'Website content updated successfully',
];
	