@extends('admin.layouts.app')
@section('title', 'Inventory')
@section('content')
<style type="text/css">
   .buttons .row {
   justify-content: center;
   align-items: center;
   text-align: center;
   display: flex;
   }
   .btn.btn-primary.btn1{
   margin-left: 5px;
   margin-right: 5px;
   }
   .btn.btn-primary.btn2{
   margin-left: 5px;
   margin-right: 5px;
   }
   .btn.btn-primary.btn3{
   margin-left: 5px;
   margin-right: 5px;
   }
</style>
<div class="page-heading">
   <div class="pageheding-inner">
      <h1 class="page-common-head"><span>Inventory</span></h1>
   </div>
</div>

<div class="handi-form">
  <div class="container-2">
    <div id="page-wrapper">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="circle-tile">
            <div class="circle-tile-content red">
              <div class="circle-tile-description text-faded">Summer</div>
              <input type="hidden" name="id" value="1">
              <a href="{{ route('catalogues.home', ['id' => 1]) }}" class="circle-tile-footer">Summer Products <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="circle-tile">
            <div class="circle-tile-content red">
              <div class="circle-tile-description text-faded">Winter</div>
              <a href="{{route('catalogues.home', ['id' => 2]) }}" class="circle-tile-footer">Winter Products <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="circle-tile">
            <div class="circle-tile-content red">
              <div class="circle-tile-description text-faded">Other</div>
              <a href="{{ route('catalogues.home', ['id' => 3]) }}" class="circle-tile-footer">Other Products <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(session()->has('success'))
<script type="text/javascript">
   $.growl.notice({
       title: "Success!",
       message: 'Success'
   });
</script>
@endif
@if(session()->has('error'))
<script type="text/javascript">
   $.growl.error({
       title: "Oops!",
       message: 'Error'
   });
</script>
@endif
<script>
   $(document).ready(function() 
   {
   	 window.history.pushState(null, "", window.location.href); 
   		window.onpopstate = function() 
   		{ 
   			window.history.pushState(null, "", window.location.href); 
   		};
    });
    
</script>
@stop