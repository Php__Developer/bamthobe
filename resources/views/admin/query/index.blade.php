@extends('admin.layouts.app')
@section('title','Users Queries')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Queries</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Queries</span>
        </div>
    </div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Title</th>
                <th>Message</th>
                <th>Date</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('query.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/query.js') }}"></script>

@if(session()->has('success'))
    <script type="text/javascript">
        $.growl.notice({title: "Success!", message:  '{!! session('success') !!}' });
    </script>
@endif
@if(session()->has('error'))
    <script type="text/javascript">
        $.growl.error({title: "Oops!", message:  '{!! session('error') !!}' });
    </script>
@endif
@stop
