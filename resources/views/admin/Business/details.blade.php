@extends('admin.layouts.app')
@section('title','Details')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Business Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','business') }}">Customers</a></span>
                <span>></span>
                <span class="active">{{ (!empty($order->first()->username))?$order->first()->username:$heading }}</span>
            </div>
            <div class="table-responsive">
                <table class="table" id="resorts-table">
                    <thead>
                        <tr>
                            <th>Order number</th>
                            <th>Amount</th>
                            <!-- <th>Order By</th> -->
                            <th>Order Date</th>
                            <th>Completion Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order as $ord)
                        <tr>
                            <td>{!! $ord->order_unique_id !!}</td>
                            <td>0</td>
                            <!-- <td>{!! $customerDetail->username !!}</td> -->
                            <td>{!! $ord->created_at !!}</td>
                            <td>
                                @if($ord->expected_delivery_time == '')
                                <span class="label label-danger">Not Set</span>
                                @else
                                {!! $ord->expected_delivery_time !!}
                                @endif
                            </td>
                            <!-- <td>{!! $ord->expected_delivery_time !!}</td> -->
                            <td>
                                @if($ord->status == 0)
                                <span class="label label-danger">UnAssigned</span>
                                @elseif($ord->status == 1)
                                <span class="label label-primary">Assigned</span>
                                @elseif($ord->status == 2)
                                <span class="label label-primary">Confirmed</span>
                                @elseif($ord->status == 3)
                                <span class="label label-primary">Completed</span>'
                                @elseif($ord->status == 4)
                                <span class="label label-info">Paid</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>      
        </div>
    </div>
    <div class="clearfix"></div></br>
    <script type="text/javascript" src="{{ asset('js/backend/orders.js') }}"></script>
@stop