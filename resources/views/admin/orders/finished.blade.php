@extends('admin.layouts.app')
@section('title','Finished Orders')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Orders</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Finished Orders</span>
        </div>
    </div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Order Id</th>
                <th>Customer Id</th>
                <th>Branch</th>
                <th>Order By</th>
                <th>Order Date</th>
                <th>Completion Date</th>
                <th>Status</th>
                <th>View</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('finished.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/ordersFinished.js') }}"></script>

@if(session()->has('success'))
<script type="text/javascript">
    $.growl.notice({
        title: "Success!",
        message: 'success'
    });
</script>
@endif
@if(session()->has('error'))
<script type="text/javascript">
    $.growl.error({
        title: "Oops!",
        message: 'error'
    });
</script>
@endif
@stop