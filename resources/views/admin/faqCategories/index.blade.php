@extends('admin.layouts.app')
@section('title','FAQ Categories')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>FAQ Categories</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage FAQ Categories</span>
        </div>
        <div class="new-customer-btn single-btn">
            <a class="btn btn-primary pull-right" id="show-add-form"><i class="fa fa-plus" aria-hidden="true"></i> Add FAQ Category</a>
        </div>
    </div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Added On</th>
                <th>Updated On</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('faqcategories.data') !!}">
<!-- Promocode Model -->
<div class="modal fade promocode_modal" id="ModuleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header text-center">
            <h4 class="modal-title change-on-edit">Add FAQ category</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" role="form"  method="POST" id="changeModuleForm" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="form-group col-xs-12 col-sm-12">
                    <div class="row">
                        <label class="col-sm-12 control-label" for="name" > Title <span>*</span></label>
                        <div class="col-sm-12 input-cover-modal">
                            <input type="text" class="form-control" id="title" name="title">
                            <p id="material-name">
                                <span class="invalid-feedback" ></span>
                            </p>
                        </div>
                    </div>
                </div> 
                <div class="clearfix"></div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Close</button>
                    <button type="submit" class="btn btn-primary" id="form-add-button">{{ __('Add') }}</button>
                </div>
                <input type="hidden" name="FaqCategoryFormAction" id="FaqCategoryFormAction" value="add" />
                <input type="hidden" name="id" id="id" />
            </form>
        </div>
    </div>
</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-faqcategory-modal">
    <div class="modal-dialog modal-md modal-delete">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">DELETE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>This category contains questions.Are you sure you want to delete this category?</p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-primary primary deleteRecord" data-dismiss="modal" onclick="deleteRecord();">Yes</button>
                <input type="hidden" id="confirm-faqcategory-modal-delete-id" value="">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/backend/faqcategories.js') }}"></script>
@stop