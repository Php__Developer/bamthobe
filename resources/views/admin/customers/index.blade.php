@extends('admin.layouts.app')
@section('title','Customers')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Customer</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Customer</span>
        </div>
        {{--<div class="new-customer-btn single-btn">
            <a class="btn btn-primary pull-right" href="{{ route('customers.create') }}"><i class="fa fa-plus" aria-hidden="true"></i>Add Customer</a>
        </div>--}}
    </div>
</div>

<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Customer id</th>
                <th>Name</th>
                <th>Contact No.</th>
                <th>Email</th>
                <th>Created By</th>
                <th>Account Type</th>
                <th>Last Order Date</th>
                <th>Total Orders</th>
                <th>Status</th>
                <th><span class="act-block">Action</span></th>
            </tr>
        </thead>
    </table>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-modal">
    <div class="modal-dialog modal-md modal-delete">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">DELETE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete selected customer?</p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-primary primary deletePromotions" data-dismiss="modal" onclick="deleteFinally();">Yes</button>
                <input type="hidden" id="confirm-modal-delete-id" value="">
            </div>
        </div>
    </div>
</div>

<input id="data-table-url" type="hidden" value="{!! route('customers.data') !!}">

<div class="modal fade" id="changePasswordCustomers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="rem_error_msg_customers();">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal pwd-modal" role="form" method="POST" id="changePasswordCustomersForm" action="{{ url('admin','changePasswordCustomers') }}">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="customer_id" name="customer_id">
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 control-label" for="new_password">New Password <span>*</span></label>
                        <div class="col-xs-8 col-sm-9 input-cover-modal"">
                            <input type=" password" class="form-control" id="new_password_customers" name="new_password_customers" placeholder="New Password" />
                        <p id="new-password-customers">
                            <span class="invalid-feedback"></span>
                        </p>
                        <div class="input-icon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-xs-4 col-sm-3 control-label" for="confirm_password">Confirm Password <span>*</span></label>
                <div class="col-xs-8 col-sm-9 input-cover-modal"">
                            <input type=" password" class="form-control" id="confirm_password_customers" name="confirm_password_customers" placeholder="Confirm Password" />
                <p id="confirm-password-customers">
                    <span class="invalid-feedback"></span>
                </p>
                <div class="input-icon"><i class="fa fa-lock" aria-hidden="true"></i></div>
            </div>
        </div>
        <div class="modal-footer text-center">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="rem_error_msg_customers();"> Close</button>
            <button type="submit" class="btn btn-primary" id="change-password-button-customers">{{ __('Save') }}</button>
        </div>
        </form>
    </div>
</div>
</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="DeleteCustomer">
    <div class="modal-dialog modal-md modal-delete">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">DELETE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this customer?</p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-primary primary deletePromotions" data-dismiss="modal" onclick="DeleteCustomer();">Yes</button>
                <input type="hidden" id="DeleteCustomer-delete-id" value="">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/backend/customers.js')}}"></script>

@if(session()->has('success'))
<script type="text/javascript">
    $.growl.notice({
        title: "Success!",
        message: 'Success'
    });
</script>
@endif
@if(session()->has('error'))
<script type="text/javascript">
    $.growl.error({
        title: "Oops!",
        message: 'Error'
    });
</script>
@endif
@stop