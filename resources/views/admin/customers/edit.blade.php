@extends('admin.layouts.app')
@section('title','Customer')
@section('content')
<form method="POST" enctype="multipart/form-data" action="{{ url('admin/customers/'.$customer->id) }}" id="customers-edit-form">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>customer</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','customers') }}">customer</a></span>
                <span>></span>
                <span class="active">{{ (!empty($customer->name))?$customer->name:$heading }}</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','customers') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>Save
            </button>
        </div>
    </div>
    <!-- <div class="handi-form p-l-res"> -->
    <div class="col-xs-12">
        <h4 class="modal-subheading"><span>Basic Details</span></h4>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text" class="form-control has-error" id="name" name="name" value="{{$customer->name}}" />
                    <p></p>
                    @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Contact Number<span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="phone" name = "phone" value="{{$customer->phone}}"/>
                        <p id="existing-email"></p>
                        @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div> -->
    <!-- <div class="clearfix"></div> -->
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Contact Number</label>
                <div class="col-xs-12">
                    <input type="text" class="form-control has-error" id="phone" name="phone" value="{{$customer->phone}}" />
                    <p></p>
                    @if ($errors->has('phone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">E-mail <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text" class="form-control has-error" id="email" name="email" value="{{$customer->email}}" />
                    <p id="existing-email"></p>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Created By <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text" class="form-control has-error" id="created_by" name="created_by" value="{{ ($customer->created_by) ? $customer->created_by : Auth::user()->name}}" readonly />

                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Account Type <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="account_type" name="account_type">
                        <option {{ ($customer->account_type == 1) ? 'selected' : '' }} value="1">Normal</option>
                        <option {{ ($customer->account_type == 2) ? 'selected' : '' }} value="2">VIP</option>
                    </select>
                    <!-- <input type="text"  class="form-control has-error" id="account_type" name = "account_type" value="{{$customer->account_type}}"/> -->
                    <!-- <p id="existing-email"></p> -->

                </div>
            </div>
        </div>
    </div>




    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="status" name="status">
                        <option {{ $customer->status ? 'selected' : '' }} value="1">Active</option>
                        <option {{ !$customer->status ? 'selected' : '' }} value="0">Inactive</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Last Order Date <span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="last_order_date" name = "last_order_date" value="{{$customer->last_order_date}}"/>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Total orders <span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="total_order" name = "total_order" value="{{$customer->total_order}}"/>
                        
                    </div>
                </div>
            </div>
        </div> -->

    <input type="hidden" id="formAction" value="edit" />
    <input type="hidden" id="customer_id" value="{{$customer->id}}" />

</form>
</div>


<link rel="stylesheet" type="text/css" href="{{ asset('css/imgareaselect/imgareaselect-default.css') }}" />

<script type="text/javascript" src="{{ asset('js/backend/customers.js')}}"></script>
@stop