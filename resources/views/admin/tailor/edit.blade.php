@extends('admin.layouts.app')
@section('title','Manager')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/tailor/'.$tailor->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Manager</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','tailor') }}">Manager</a></span>
                <span>></span>
                <span class="active">{{$heading}} Manager</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','tailor') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
@if( Request::segment(3)==='create' )
  <div class="col-xs-12 send_cred">                       
        <input type="checkbox"  id="send_cred" name = "send_cred" value="1" checked="checked" /> 
       <!--  <label for="send_cred"><strong>Send Email (Credentials)</strong></label> -->
    </div>  
@endif
    
</div>
<input type="hidden" name="id" id="id"  value="{{$tailor->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="title" name = "title"  value="{{$tailor->title}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('title'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
     
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Contact Number</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="phone" name = "phone"  value="{{$tailor->phone}}" />
                    
                   
                </div>
            </div>
        </div>
    </div>

   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Email <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="email" name = "email"  value="{{$tailor->email}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>    
         
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Target</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="monthly_target" name = "monthly_target"  value="{{$tailor->monthly_target}}" />
                    
                   
                </div>
            </div>
        </div>
    </div>
         
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="status" name = "status">
                        <?php
                            if($tailor->status==1) 
                            { 
                            echo "<option value='1' selected>Active</option>
                                    <option value='0'>Inactive</option>"; 
                            } 
                            else
                            {
                            echo "<option value='1' >Active</option>
                                   <option value='0' selected>Inactive</option>"; 
                            }
                        ?>                            
                    </select> 
                    <p class="allTypeError"></p>
                    @if ($errors->has('status'))
                    <span class="invalid-tailorback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 img_upload-div ">
                <div class="form-group error">
                <label  class="control-label" for="image_name">Image</label>
                <div class="input-cover-modal">
                    <div class="image_preview error-outer">
                     
                        <img id="show-image-preview" src="{{ (!empty($tailor->image_name) && file_exists(public_path('uploads/tailor/'.$tailor->image_name)))?asset('uploads/tailor/'.$tailor->image_name):asset('images/common/default-image.jpg') }}" alt="" />
                        <div class="choose_file">
                            <input title="" type='file' id="image_name" name="image_name" />
                            <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </div>
                        <input type="hidden" name="image-exist" id="image-exist" val = "no">
                    </div>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('image_name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('image_name') }}</strong>
                    </span>
                    @endif
                </div>
                </div>
            </div>
        </div>
    </div>
     
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/tailor.js') }}"></script>
@stop




