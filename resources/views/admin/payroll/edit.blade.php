@extends('admin.layouts.app')
@section('title','Payroll')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/payroll/'.$payroll->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Payroll</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','payroll') }}">Payroll</a></span>
                <span>></span>
                <span class="active">{{$heading}} Payroll</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','payroll') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$payroll->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Employee Id <span>*</span></label>
                <div class="col-xs-12">
                    <select id="emp_id" name = "emp_id" style="width: 100%;">
                        <option value="">Select Employee Id</option>
                        @foreach($getEmpId as $key => $value)
                            <option value="{{$key}}" {{($payroll->emp_id == $key)?'selected':''}}>{{$value}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="name" name = "name"  value="{{$payroll->name}}" readonly/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Salary <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="salary" name = "salary"  value="{{$payroll->salary}}" readonly/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('salary'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('salary') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Assigned Target <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="assigned_hours" name = "assigned_hours"  value="{{$payroll->assigned_hours}}" readonly/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('assigned_hours'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('assigned_hours') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Acheived Target <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="acheived_hours" name = "acheived_hours"  value="{{$payroll->acheived_hours}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('acheived_hours'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('acheived_hours') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">No Of Defective Order <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="no_defective_order" name = "no_defective_order"  value="{{$payroll->no_defective_order}}" readonly/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('no_defective_order'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('no_defective_order') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Commission <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="commission" name = "commission"  value="{{$payroll->commission}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('commission'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('commission') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Other Deduction <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="other_deduction" name = "other_deduction"  value="{{$payroll->other_deduction}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('other_deduction'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('other_deduction') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Total <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="total" name = "total"  value="{{$payroll->total}}"  readonly/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('total'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('total') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Evaluation <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="evaluation" name = "evaluation"  value="{{$payroll->evaluation}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('evaluation'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('evaluation') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Note <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="note" name = "note"  value="{{$payroll->note}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('note'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('note') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/payroll.js') }}"></script>
@stop




