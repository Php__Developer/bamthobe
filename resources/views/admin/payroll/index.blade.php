@extends('admin.layouts.app')
@section('title','Payroll')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Payroll</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Payroll</span>
        </div>
        <div class="new-customer-btn single-btn">
            <a class="btn btn-primary pull-right" id="show-add-form" href="{{ route('payroll.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add Payroll</a>
        </div>
    </div>
</div>

<div class="handi-form">
    <h5>View Payroll</h5>
    {{--<div class="pull-right">
                <!-- Here is for downloading button for sample csv -->
                <a class="btn btn-primary pull-left" id="importbtn">Export sample csv</a>
                <!-- Here is for the import csv file -->
                <input type="file" name="import" id="import" class="pull-left btn" />
                <a class="btn btn-primary pull-right" id="importbtn">Import</a>
            </div>--}}
    <div class="container-2">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <label for="inputName" class="col-xs-12 control-label">Month </label>
                        <select name="payMonth" id="payMonth" class="form-control form-control-sm">
                            <option>Select month</option>
                        @foreach($payroll as $key => $payMonth)
                            <option value="{{$key}}">{{$payMonth}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <label for="inputName" class="col-xs-12 control-label">Year </label>
                        <select name="payYear" id="payYear" class="form-control form-control-sm">
                            <option>Select year</option>
                        @foreach(range(2015, (int)date("Y")) as $year)
                            <option value="{{$year}}">{{$year}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <label for="inputName" class="col-xs-12 control-label">&nbsp</label>
                        <a class="btn btn-primary pull-right form-control" id="paySearch" href="#"><i class="fa fa-search" aria-hidden="true"></i> View payroll</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Emp Id</th>
                <th>Name</th>
                <th>Salary</th>
                <th>Assigned Hours</th>
                <th>Acheived Hours</th>
                <th>Other Deduction</th>
                <th>Commission</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('payroll.data') !!}">

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-modal">
    <div class="modal-dialog modal-md modal-delete">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">DELETE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Payroll ?</p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-primary primary deleteRecord" data-dismiss="modal" onclick="deleteRecord();">Yes</button>
                <input type="hidden" id="confirm-modal-delete-id" value="">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/backend/payroll.js') }}"></script>

@if(session()->has('success'))
<script type="text/javascript">
    $.growl.notice({
        title: "Success!",
        message: 'success'
    });
</script>
@endif
@if(session()->has('error'))
<script type="text/javascript">
    $.growl.error({
        title: "Oops!",
        message: 'error'
    });
</script>
@endif
@stop