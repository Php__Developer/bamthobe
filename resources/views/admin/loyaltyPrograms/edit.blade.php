@extends('admin.layouts.app')
@section('title','Promo Codes')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/promoCodes/'.$promoCode->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Promo Codes</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','promoCodes') }}">Promo Code</a></span>
                <span>></span>
                <span class="active">{{$heading}} Promo Code</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','promoCodes') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$promoCode->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Promo <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="promo" name = "promo"  value="{{$promoCode->promo}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('promo'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('promo') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Amount <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="amount" name = "amount"  value="{{$promoCode->amount}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('amount'))
                    <span class="invalid-feedback">
                        <strong id="amount-server-err-exists">{{ $errors->first('amount') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="status" name = "status">
                        <?php
                            if($promoCode->status==1) 
                            { 
                            echo "<option value='1' selected>Active</option>
                                    <option value='0'>Inactive</option>"; 
                            } 
                            else
                            {
                            echo "<option value='1' >Active</option>
                                   <option value='0' selected>Inactive</option>"; 
                            }
                        ?>                            
                    </select> 
                    <p class="allTypeError"></p>
                    @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
  </div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/promoCodes.js') }}"></script>
@stop




