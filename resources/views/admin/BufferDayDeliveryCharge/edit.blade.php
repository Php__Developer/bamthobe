@extends('admin.layouts.app')
@section('title','Buffer & Delivery')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/buffer_day_delivery/'.$bufferDelivery->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Manage Buffer & Delivery</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','buffer_day_delivery') }}">Manage Buffer & Delivery</a></span>
                <span>></span>
                <span class="active">{{$heading}} Buffer & Delivery</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','buffer_day_delivery') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$bufferDelivery->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="row">
            <div class="form-group error">
                <label for="inputbuffer_days" class="col-xs-12 control-label">Buffer Days<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorbuffer_days" name = "buffer_days"  value="{{$bufferDelivery->buffer_days}}" required="required" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('buffer_days'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('buffer_days') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-4 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputdelivery_charges" class="col-xs-12 control-label">Delivery Charges(In SAR)<span>*</span></label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="delivery_charges" name = "delivery_charges"  value="{{$bufferDelivery->delivery_charges}}" required="required"/>
                    
                   
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-4 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputtax_rate" class="col-xs-12 control-label">Tax Rate(In %)<span>*</span></label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="tax_rate" name = "tax_rate"  value="{{$bufferDelivery->tax_rate}}" required="required"/>
                    
                   
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/bufferDayDelivery.js') }}"></script>
@stop




