@extends('admin.layouts.app')
@section('title','Purchase Orders')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/payorders/settled') }}">
{{ csrf_field() }}
{{ method_field('POST') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Purchase Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','payorders') }}">Purchase Orders</a></span>
                <span>></span>
                <span class="active">{{$heading}} Purchase Orders</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','payorders') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<div class="handi-form p-l-res">
    <div class="col-xs-6 col-sm-4 col-md-4">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Refrence number<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="refrence_number" name ="refrence_number"  value="{{$payorders->refrence_number}}" readonly />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('refrence_number'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('refrence_number') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-4 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Remaining Ammount<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="remaining_amount" name ="remaining_amount" value="{{$payorders->remaining_amount}}" readonly />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('remaining_amount'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('remaining_amount') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>      
    <div class="col-xs-6 col-sm-4 col-md-4 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Paid amount</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="paid_amount" name = "paid_amount"  value="{{$payorders->paid_amount}}" placeholder="0.00"/>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/payorders.js') }}"></script>

@push('custom-scripts')
<script type="text/javascript">
    // Date Picker
    jQuery('.mydatepicker, #datepicker, .duedatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
@endpush

@stop




