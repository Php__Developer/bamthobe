@extends('admin.layouts.app')
@section('title','Purchase Orders')
@section('content')
<form method="get" id="changeModuleForm" enctype="multipart/form-data" action="{{url('admin/createNextOrder')}}">
{{ csrf_field() }}
{{ method_field('PATCH') }}

<div class="page-heading">

    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Purchase Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','payorders') }}">Purchase Orders</a></span>
                <span>></span>
                <span class="active">{{$heading}} Purchase Orders</span>
            </div>

            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','payorders') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>

            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true" href="{{ url('admin','createNextOrder') }}"></i>Save
            </button>
    </div>

</div>

<input type="hidden" name="id" id="id"  value="{{$payorders->id}}">

<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Date<span>*</span></label>
                    <div class="col-xs-12">
                        <input class="form-control mydatepicker" type="text" id="date" name = "date" value="{{($payorders->date)? \Carbon\Carbon::parse($payorders->date)->format('m/d/Y') :date('m/d/Y')}}">
                        <p class="allTypeError"></p>
                        @if ($errors->has('date'))
                        <span class="invalid-feedback">
                            <strong id="amount-server-err-exists">{{ $errors->first('date')
                            }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Refrence number<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="refrence_number" name ="refrence_number"  value="{{$payorders->refrence_number}}" readonly />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('refrence_number'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('refrence_number') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>       
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Supplier<span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" name="supplier_id" id="supplier_id">
                        <option>Select Supplier</option>
                        @foreach($getSupplier as $Suppliiers => $Supplier)
                        <option value="{{ $Suppliiers }}" {{ ( $Suppliiers == $payorders->supplier_id ) ? 'selected' : '' }}>
                        {{ $Supplier }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/payorders.js') }}"></script>

@push('custom-scripts')
<script type="text/javascript">
    jQuery('.mydatepicker, #datepicker, .duedatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
@endpush

@stop




