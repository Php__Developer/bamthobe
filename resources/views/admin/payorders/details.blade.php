@extends('admin.layouts.app')
@section('title','PO Details')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Purchase Order</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','payorders') }}">Purchase Order</a></span>
                <span>></span>
                <span class="active">Details</span>
            </div>            
        </div>
    </div>
    <!-- Modal -->
    @php $i=1; @endphp

    @foreach($purchaseOrders as $purchaseOrder)

    <div class="row takespace">
        <div class="col-md-12">
            <div class="card card-body printableArea">
                <h3><b>PURCHASE ORDER INVOICE NO. {{$i}} </b> <span class="pull-right">#{{$purchaseOrder->refrence_number}}</span></h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            <address>
                                <h3><b class="text-danger">Bam Admin</b></h3>
                                <p class="text-muted m-l-5">E 104, Dharti-2,
                                    <br/> Nr' Viswakarma Temple,
                                    <br/> Talaja Road,
                                    <br/> Bhavnagar - 364002</p>
                            </address>
                        </div>
                        <div class="pull-right text-right">
                            <address>
                                <h3>To,</h3>
                                <h4 class="font-bold">{{ $purchaseOrder->getSupplier->supplier_name }},</h4>
                                <p class="text-muted m-l-30">{{ $purchaseOrder->getSupplier->company_name }},
                                    <br/> {{ $purchaseOrder->getSupplier->email }},
                                    <br/> {{ $purchaseOrder->getSupplier->primary_phone }}, {{ $purchaseOrder->getSupplier->secondary_phone }},
                                    <br/> {{$purchaseOrder->getSupplier->address}}</p>
                                <p class="m-t-30"><b>PO Date :</b> <i class="fa fa-calendar"></i> {{date('d M Y', strtotime($purchaseOrder->date))}}</p>
                                <p><b>Due Date :</b> <i class="fa fa-calendar"></i> {{date('d M Y', strtotime($purchaseOrder->due_date))}}</p>
                                <p><b>Payment Date :</b> <i class="fa fa-calendar"></i> {{date('d M Y', strtotime($purchaseOrder->created_at))}}</p>
                            </address>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive m-t-40" style="clear: both;">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Product Name</th>
                                        <th class="text-right">Product Code</th>
                                        <th class="text-right">Purchase Cost</th>
                                        <th class="text-right">Quantity</th>
                                        <th class="text-right">Paid Amount</th>
                                        <th class="text-right">Remaining Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td> {{$purchaseOrder->product_name}} </td>
                                        <td class="text-right"> {{$purchaseOrder->product_code}} </td>
                                        <td class="text-right"> {{number_format($purchaseOrder->purchase_unit_price,2, ".", ",")}} </td>
                                        <td class="text-right"> {{$purchaseOrder->quantity}} </td>
                                        <td class="text-right"> {{number_format($purchaseOrder->paid_amount,2, ".", ",")}} </td>
                                        <td class="text-right"> {{number_format($purchaseOrder->remaining_amount,2, ".", ",")}} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right m-t-30 text-right">
                            <p>Sub - Total amount: {{number_format($purchaseOrder->purchase_unit_price*$purchaseOrder->quantity,2, ".", ",")}}</p>
                            <p>tax ({{$purchaseOrder->tax}}%) : SAR {{number_format($purchaseOrder->purchase_amount-($purchaseOrder->purchase_unit_price*$purchaseOrder->quantity),2, ".", ",")}} </p>
                            <hr>
                            <h3><b>Total :</b> SAR {{number_format($purchaseOrder->purchase_amount,2, ".", ",")}}</h3>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php $i++; @endphp

    @endforeach

    <div class="text-right">
        <!-- <button class="btn btn-danger noprint" type="submit"> Proceed to payment </button> -->
        <button id="printme" class="btn btn-default btn-outline noprint" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
    </div>


    <div class="clearfix"></div></br>
    <style type="text/css">
        @media print {
            .page-heading,div#content-3, header.text-right, .noprint, .footertxt{
                display:none;
            }
            .takespace {
                padding: 200px 0 200px 0;
            }
        }
    </style>
    <script type="text/javascript" src="{{ asset('js/backend/payorders.js') }}"></script>
@stop