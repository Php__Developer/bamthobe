@extends('admin.layouts.app')
@section('title','FAQs')
@section('content')

<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>FAQs</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">FAQs</span>
        </div>
        <div class="new-customer-btn single-btn">
            <a class="btn btn-primary pull-right" href="{{ route('faq.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add New FAQ</a>
        </div>
    </div>
</div>

<div class="mng-customer-table">
    <table class="table table-bordered" id="faq-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Question</th>
                <th>Answer</th>
                <th>Order</th>
                <th>Status</th>
                <th>Added On</th>
                <th>Updated On</th>
                <th><span class="act-block">Action</span></th>
            </tr>
        </thead>
    </table>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="DeleteFaq">
    <div class="modal-dialog modal-md modal-delete">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">DELETE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this FAQ?</p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-primary primary deletePromotions" data-dismiss="modal" onclick="deleteFaq();">Yes</button>
                <input type="hidden" id="DeleteFaq-delete-id" value="">
            </div>
        </div>
    </div>
</div>
<input id="url" type="hidden" value="{{Request::url('admin')}}">
<input id="faq-table-url" type="hidden" value="{!! route('faq.data') !!}">


@if(session()->has('success'))
    <script type="text/javascript">
        $.growl.notice({title: "Success!", message:  '{!! session('success') !!}' });
    </script>
@endif
@if(session()->has('error'))
    <script type="text/javascript">
        $.growl.error({title: "Oops!", message:  '{!! session('error') !!}' });
    </script>
@endif
@stop