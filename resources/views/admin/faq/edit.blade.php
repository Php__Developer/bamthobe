@extends('admin.layouts.app')
@section('title','FAQs')
@section('content')
<!-- <?php //print_r($faq); ?>
<?php //print_r($faq_category);die; ?> -->
<form method="POST" action="{{ url('admin/faq/'.$faq->id) }}" id="faq-create-form">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>FAQs</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','faq') }}">FAQs</a></span>
                <span>></span>
                <span class="active">{{$heading}} FAQ</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','faq') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
        </div>
    </div> 
    <input type="hidden" name="id" id="id"  value="{{$faq->id}}">
    <div class="handi-form p-l-res">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Select Category <span>*</span></label>
                    <div class="col-xs-12">

                       <select class="form-control has-error" id="faq_categories_id" name = "faq_categories_id">
                             <option value="">Select Category</option>
                            @foreach ($faq_categories as $faq_category)
                                <option {{ $faq_category->id == $faq->faq_categories_id ? "selected" : "" }} value="{{$faq_category->id}}">{{$faq_category->name}}</option>
                            @endforeach                           
                        </select>
                        
                        <p class="allTypeError"></p>
                        @if ($errors->has('status'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Question <span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="question" name = "question"  value="{{$faq->question}}" />
                        <p class="allTypeError"></p> 
                        @if ($errors->has('question'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('question') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Answer <span>*</span></label>
                    <div class="col-xs-12">
                        <textarea class="form-control has-error" id="answer" name="answer" required="required" ><?php echo $faq->answer;?></textarea>
                        <p class="allTypeError"></p>
                        <p id="existing-answers"></p>
                        @if ($errors->has('answer'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('answer') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
         


        <div class="clearfix"></div>        
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Order <span>*</span></label>
                    <div class="col-xs-12">
                       <input type="number" name="sort_order" id="sort_order" min="1" class="form-control has-error" value="{{$faq->sort_order}}"/>
                         <p class="allTypeError"></p>
                        @if ($errors->has('sort_order'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('sort_order') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                    <div class="col-xs-12">

                       <select class="form-control has-error" id="status" name = "status">
                            <?php
                                if($faq->status==1) 
                                { 
                                echo "<option value='1' selected>Active</option>
                                        <option value='0'>Inactive</option>"; 
                                } 
                                else
                                {
                                echo "<option value='1' >Active</option>
                                        <option value='0' selected>Inactive</option>"; 
                                }
                            ?>                            
                        </select>
                        
                        <p class="allTypeError"></p>
                        @if ($errors->has('status'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>                     
    </div>
</form>
</div>
<script>
    CKEDITOR.replace( 'answer' );
</script>
@stop