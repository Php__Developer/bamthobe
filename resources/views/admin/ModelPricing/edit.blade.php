@extends('admin.layouts.app')
@section('title','Manage Model')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/modelpricing/'.$modelpricing->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Manage Model</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','modelpricing') }}">Manage Model</a></span>
                <span>></span>
                <span class="active">{{$heading}} Manage Model</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','modelpricing') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$modelpricing->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Model Name<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "name"  value="{{$modelpricing->name}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Model Price</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="price" name = "price"  value="{{$modelpricing->price}}" />
                    
                   
                </div>
            </div>
        </div>
    </div>
   
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
            <div class="col-xs-12 col-sm-12 img_upload-div ">
                <div class="form-group error">
                <label  class="control-label" for="image">Model Picture</label>
                <div class="input-cover-modal">
                    <div class="image_preview error-outer">
                     
                        <img id="show-image-preview" src="{{ (!empty($modelpricing->image) && file_exists(public_path('uploads/modelpricing/'.$modelpricing->image)))?asset('uploads/modelpricing/'.$modelpricing->image):asset('images/common/model-default.png') }}" alt="" />
                        <div class="choose_file">
                            <input title="" type='file' id="image" name="image" />
                            <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </div>
                        <input type="hidden" name="image-exist" id="image-exist" val = "no">
                    </div>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('image'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('image') }}</strong>
                    </span>
                    @endif
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/modelpricing.js') }}"></script>
@stop




