<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BAM | Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 93.3vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .top-left {
                position: absolute;
                left: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                width: 50%;
            }

            .title {
                font-size: 40px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 15px;
                margin-top: 15px;
            }
            .footertxt {
                padding: 10px;
                background: #212121;
                color: #fff;
                text-align: center;
            }
            .web-content {
                font-size: 20px;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('admin.auth.login'))
                <div class="top-right links">
                    @auth
                        <a href="{{route('admin.dashboard') }}">Home</a>
                    @else
                        <a href="{{ route('admin.auth.login') }}">Admin Login</a>
                    @endauth
                </div>
            @endif

            <div class="top-left">
                <img src="/images/common/logo.jpg">
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <span class="web-content">{{$getData->content}}</span>
                    </div>
                </div>
                <div class="title m-b-md">
                    Get The App On...
                </div>

                <div class="links">
                    <a href="{{($getData->app_store)? $getData->app_store :'#'}}">
                        <img src="/images/common/app-store.png" style="width: 40%;">
                    </a>
                    <a href="{{($getData->play_store)? $getData->play_store :'#'}}">
                        <img src="/images/common/google-play-store.png" style="width: 40%;">
                    </a>
                </div>

                <div class="title m-b-md">
                    Stay Connect With...
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <span class="web-content">{{$getData->address}}</span>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            @include('admin.includes.footer')
        </footer>       
    </body>
</html>
