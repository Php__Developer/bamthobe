@extends('admin.layouts.app')
@section('title','Manage Uniforms')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/uniform/'.$uniform->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}

<style>
    label.checkbox-input-info {
    display: flex;
    align-items: center;
}
label.checkbox-input-info input{margin-right: 10px;}
</style>

<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Manage order</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span><a href="{{ url('admin','uniform') }}">Manage Orders</a></span>
            <span>></span>
            <span class="active">{{$heading}} Manage Uniform</span>
        </div>
        <div class="new-customer-btn cancel-button">
            <a class="btn btn-primary pull-right" href="{{ url('admin','uniform') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
        </div>
        <button type="submit" class="btn btn-primary save_btn">
            <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
        </button>
    </div>
</div>

<input type="hidden" name="id" id="id"  value="{{$uniform->id}}">

<div class="handi-form p-l-res">

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Shop Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "shopname"  value="{{$uniform->shopname}}" required="" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('shopname'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('shopname') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Username <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "username"  value="{{$uniform->username}}" required=""/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('username'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('username') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Responsible Person <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "responsible_name"  value="{{$uniform->responsible_name}}" required="" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('responsible_name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('responsible_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Contact Number <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "contact_number"  value="{{$uniform->contact_number}}" required="" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('contact_number'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('contact_number') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Shop Email <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "shop_email"  value="{{$uniform->shop_email}}" required=""/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('shop_email'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('shop_email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Shop Location <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "shop_location"  value="{{$uniform->shop_location}}" required=""/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('shop_location'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('shop_location') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!--hidden fields-->

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <label class="col-xs-12 control-label checkbox-input-info"><input type="checkbox" name="colorCheckbox" value="check">Want to add more details ? Check me (Optional fields)</label>
        </div>
    </div>

    <div class="check box" style="display: none;">

    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Quantity<span></span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "quantity"  value="{{$uniform->quantity}}" placeholder="1-999"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('quantity'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('quantity') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

 <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
            <div class="col-xs-12 col-sm-12 img_upload-div ">
                <div class="form-group error">
                <label  class="control-label" for="image">Model</label>
                <div class="input-cover-modal">
                    <div class="image_preview error-outer">
                     
                        <img id="show-image-preview" src="{{ (!empty($uniform->picture) && file_exists(public_path('uploads/uniform/'.$uniform->picture)))?asset('uploads/uniform/'.$uniform->picture):asset('images/common/model-default.png') }}" alt="" />
                        <div class="choose_file">
                            <input title="" type='file' id="picture" name="picture" />
                            <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </div>
                        <input type="hidden" name="image-exist" id="image-exist" val = "no">
                    </div>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('picture'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('picture') }}</strong>
                    </span>
                    @endif
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Completion Date<span></span></label>
                <div class="col-xs-12">
                    <input class="form-control mydatepicker" type="text" id="date" name = "completion_date" value="{{($uniform->completion_date)? \Carbon\Carbon::parse($uniform->completion_date)->format('m/d/Y') :date('m/d/Y')}}">
                    <p class="allTypeError"></p> 
                    @if ($errors->has('completion_date'))
                    <span class="invalid-feedback">
                        <strong id="amount-server-err-exists">{{ $errors->first('completion_date') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">price <span></span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "price"  value="{{$uniform->price}}" placeholder="00.00"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('price'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('price') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">comments <span></span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "comments"  value="{{$uniform->comments}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('comments'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('comments') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">documents<span></span></label>
                <div class="col-xs-12">
                    <input type="file"  class="form-control has-errorname" name = "documents"  value="{{$uniform->documents}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('documents'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('documents') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    

    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/uniform.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="checkbox"]').click(function() {
            var inputValue = $(this).attr("value");
            $("." + inputValue).toggle();
        });
    });
</script>


@push('custom-scripts')
<script type="text/javascript">
    // Date Picker
    jQuery('.mydatepicker, #datepicker, .duedatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
@endpush

@stop




