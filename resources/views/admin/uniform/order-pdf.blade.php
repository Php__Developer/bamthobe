<!doctype html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>Invoice - #123</title>
      <style type="text/css">
         @page {
         margin: 0px;
         }
         body {
         margin: 0px;
         }
         * {
         font-family: Verdana, Arial, sans-serif;
         }
         a {
         color: #fff;
         text-decoration: none;
         }
         table {
         font-size: x-small;
         }
         tfoot tr td {
         font-weight: bold;
         font-size: x-small;
         }
         .invoice table {
         margin: 15px;
         }
         .invoice h3 {
         margin-left: 15px;
         }
         .information {
         background-color: #60A7A6;
         color: #FFF;
         }
         .information .logo {
         margin: 5px;
         }
         .information table {
         padding: 10px;
         }
      </style>
   </head>
   <body>
      <div class="information">
         <table width="100%">
            <center><h1>Bamthobe Invoice</h1></center>
            <tr>
               <td align="left" style="width: 40%;">
                <h2>Customer Name</h2>
                  <pre>{{$orderuniqueID->username}}</pre>
                  <h2>Customer Phone</h2>
                  <h3>{{$orderuniqueID->contact_number}}</h3>
               </td>
               <td align="right" style="width: 40%;">
                  <h3>Shop Name & Address</h3>
                  <pre>
                    <i>{{$orderuniqueID->shopname}}</i>
                </pre>
               </td>
            </tr>
         </table>
      </div>
      <br/>
      <div class="invoice">
         <h3>Invoice specification {{$orderuniqueID['unique_id']}}</h3>
         <table width="100%">
            <thead>
               <tr>
                <th>Total Orders</th>
                <th>Need to be Payed</th>
                <th>Period</th>
                <th>Total</th>
               </tr>
            </thead>
            <tbody>
                <tr><td>4</td></tr>
                <tr><td>5</td></tr>
                <tr><td>6</td></tr>
                <tr><td>$6</td></tr>
            </tbody>
            <tfoot>
               <tr>
               </tr>
            </tfoot>
         </table>
      </div>
      <div class="information" style="position: absolute; bottom: 0;">
         <table width="100%">
            <tr>
               <td align="left" style="width: 50%;">
                  &copy; {{ date('Y') }} {{ config('app.url') }} - All rights reserved.
               </td>
               <td align="right" style="width: 50%;">
                  www.bamthobe.com
               </td>
            </tr>
         </table>
      </div>
   </body>
</html>