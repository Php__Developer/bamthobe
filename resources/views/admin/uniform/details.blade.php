@extends('admin.layouts.app')
@section('title','Details')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Uniform Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','business') }}">Customers</a></span>
                <span>></span>
                <span class="active">{{ (!empty($order->first()->order_unique_id))?$order->first()->order_unique_id:$heading }}</span>
            </div>
            <div class="table-responsive">
    <table class="table" id="resorts-table">
        <thead>
            <tr>
                <th>Order number</th>
                <th>Amount</th>
                <th>Order To</th>
                <th>Order Date</th>
                <th>Completion Date</th>
                <th>Image</th>
                <th>Document</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($order as $ord)
            <tr>
                <td>{!! $ord->order_unique_id !!}</td>
                <td>{!! $ord->price !!}</td>
                <td>{!! $customerDetail->username !!}</td>
                <td>{!! $ord->created_at !!}</td>
                <td>{!! $ord->completion_date !!}</td>
                <td><a href="http://93.188.167.68/projects/bamthobe/public/uploads/uniform/<?= $ord->picture; ?>" target="_blank"><img src="{!! url('/uploads/uniform/'. $ord->picture) !!}" width="72" height="76"></a></td>
                <!-- <td><a href="/bam/public/uploads/{{$ord->picture}}">{{$ord->picture}}</a></td> -->
                <td><a href="http://93.188.167.68/projects/bamthobe/public/uploads/uniform/<?= $ord->documents; ?>" target="_blank">{{$ord->documents}}</a></td>
                <td>
                    @if($ord->order_compete == 1)
                    <span class="label label-danger">Incomplete</span>
                    @elseif($ord->order_compete == 2)
                    <span class="label label-primary">Complete</span>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>      
        </div>
    </div>
    <div class="clearfix"></div></br>
    <script type="text/javascript" src="{{ asset('js/backend/orders.js') }}"></script>
@stop