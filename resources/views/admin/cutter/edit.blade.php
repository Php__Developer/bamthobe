@extends('admin.layouts.app')
@section('title','Manage Cutter')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/cutters/'.$cutter->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Manage Cutter</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','cutter') }}">Manage Cutter</a></span>
                <span>></span>
                <span class="active">{{$heading}} Manage Cutter</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','cutters') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$cutter->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="title" name = "title"  value="{{$cutter->title}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('title'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Email</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="email" name = "email"  value="{{$cutter->email}}" />
                    
                   
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Contact Number</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="phone" name = "phone"  value="{{$cutter->phone}}" />
                    
                   
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Factory Manager</label>
                <div class="col-xs-12">
                <select class="form-control has-error" id="factory_manager" name = "factory_manager">
                    @foreach($factory_assign_manager as $managerKey => $manager)

                    <option value="{{$managerKey}}" {{ ( $managerKey == $cutter->factory_manager) ? 'selected' : '' }}>{{$manager}}</option>

                    @endforeach
                </select> 
                    
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Monthly Target</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="monthly_target" name = "monthly_target"  value="{{$cutter->monthly_target}}" />
                   
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="status" name = "status">
                        <?php
                            if($cutter->status==1) 
                            { 
                            echo "<option value='1' selected>Active</option>
                                    <option value='0'>Inactive</option>"; 
                            } 
                            else
                            {
                            echo "<option value='1' >Active</option>
                                   <option value='0' selected>Inactive</option>"; 
                            }
                        ?>                            
                    </select> 
                    <p class="allTypeError"></p>
                    @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   
   
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
            <div class="col-xs-12 col-sm-12 img_upload-div ">
                <div class="form-group error">
                <label  class="control-label" for="image_name">Image</label>
                <div class="input-cover-modal">
                    <div class="image_preview error-outer">
                     
                        <img id="show-image-preview" src="{{ (!empty($cutter->image_name) && file_exists(public_path('uploads/cutter/'.$cutter->image_name)))?asset('uploads/cutter/'.$cutter->image_name):asset('images/common/default-image.jpg') }}" alt="" />
                        <div class="choose_file">
                            <input title="" type='file' id="image_name" name="image_name" />
                            <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </div>
                        <input type="hidden" name="image-exist" id="image-exist" val = "no">
                    </div>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('image_name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('image_name') }}</strong>
                    </span>
                    @endif
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/cutter.js') }}"></script>
@stop




