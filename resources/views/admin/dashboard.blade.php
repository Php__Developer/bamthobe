@extends('admin.layouts.app')
@section('title', 'Dashboard')
@section('content')
<style type="text/css">
	.buttons .row {
    justify-content: center;
    align-items: center;
    text-align: center;
    display: flex;
}
.btn.btn-primary.btn1{
    margin-left: 5px;
    margin-right: 5px;
}
.btn.btn-primary.btn2{
    margin-left: 5px;
    margin-right: 5px;
}
.btn.btn-primary.btn3{
    margin-left: 5px;
    margin-right: 5px;
}
</style>
<div class="page-heading">
	<div class="pageheding-inner">
		<h1 class="page-common-head"><span>Dashboard</span></h1>
		
		@if(Auth::id() ==1)
		<div class="new-customer-btn single-btn">
            <a class="btn btn-primary pull-right" href="{{ route('admin.create') }}"><i class="fa fa-plus" aria-hidden="true"></i>Add Admin</a>

            <a class="btn btn-primary" id="show-add-form" data-toggle="modal" data-target="#messageModal"><i class="fa fa-bell" aria-hidden="true"></i></a>&nbsp;
        </div>
        @endif
	</div>
</div>

    <!-- Modal -->
    <div class="modal fade" id="messageModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Notification</h4>
            </div>
            <div class="modal-footer">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.notification') }}">
   				{{ csrf_field() }}
                	<select class="col-sm-10" name="type" required>
                    	<option value="">Select for notification</option>
                    	<option value="staff">Staff</option>
                    	<option value="customer">Customer</option>
                    </select>
                    <input type="text" class="col-sm-10" name="message" id="admin-message">
                    <input type="hidden" name="user_id" value="{{Auth::id()}}">
                    <input type="hidden" name="is_admin" value="yes">
                    <!-- <a class="btn btn-primary pull-right" href="{{ route('admin.notification') }}">Post</a> -->
                    <button type="submit" class="btn btn-primary pull-right">Post</button>
<!--                                 <button type="button" class="btn btn-default btn-submit">Post</button> -->
                </form>
            </div>
          </div>
          
        </div>
    </div>
    <!-- End Modal -->

<div class="handi-form">
	    <div class="buttons">
    	<div class="row">
    		<form action="dashboard" method="get">
    			<input type="hidden" name="id" id="id"  value="1">
    			<button type="submit" class="btn btn-primary btn1">Daliy</button>
			</form>
			<form action="dashboard" method="get">
				<input type="hidden" name="id" id="id"  value="2">
				<button type="submit" class="btn btn-primary btn2">Weekly</button>
			</form>
			<form action="dashboard" method="get">
				<input type="hidden" name="id" id="id"  value="3">
			     <button type="submit" class="btn btn-primary btn3">Monthly</button>
			</form>
    	</div>
    </div>
	<div class="container-2">
		<div id="page-wrapper">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading red">
								<img src="{{ asset('images/backend/feed.svg') }}">
							</div>
						<div class="circle-tile-content red">
							<div class="circle-tile-description text-faded">
								Delay order
							</div>
							<div class="circle-tile-number text-faded" id="counter">
								<small>{{$data['delayOrder']}}</small></br>
								<span id="sparklineB"></span>
							</div>
							<!-- <a href="{{ url('admin','feeds') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading dark-blue">
								<img src="{{ asset('images/backend/factory.png') }}" style="width: 31px;">
							</div>
						<div class="circle-tile-content dark-blue">
							<div class="circle-tile-description text-faded">
								In factory
							</div>
							<div class="circle-tile-number text-faded" id="counter">
								<small>{{$data['orderInFactory']}}</small></br>
								<span id="sparklineB"></span>
							</div>
							<!-- <a href="{{ url('admin','feeds') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
						<a href="{{ url('admin','auctions') }}">
							<div class="circle-tile-heading blue">
								<img src="{{ asset('images/backend/auction.svg') }}">
							</div>
						</a>
						<div class="circle-tile-content blue">
							<div class="circle-tile-description text-faded">
							 	Ready to assign
							</div>
							<div class="circle-tile-number text-faded">
								<small>{{$data['totalReadyToAssign']}}</small></br>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading orange">
								<img src="{{ asset('images/backend/termsandcondition.svg') }}">
							</div>
						<div class="circle-tile-content orange">
							<div class="circle-tile-description text-faded">
							 	Completed Order 
							</div>
							<div class="circle-tile-number text-faded">
								<small>{{$data['totalOrderCompleted']}}</small></br>
							</div>
							<!-- <a href="{{ url('admin','currencies') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading red">
								<img src="{{ asset('images/backend/close.png') }}">
							</div>
						<div class="circle-tile-content red">
							<div class="circle-tile-description text-faded">
								Defective order
							</div>
							<div class="circle-tile-number text-faded" id="counter">
								<small>{{$data['totalDefective']}}</small></br>
								<span id="sparklineB"></span>
							</div>
							<!-- <a href="{{ url('admin','feeds') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading green">
								<img src="{{ asset('images/backend/scissors.png') }}">
							</div>
						<div class="circle-tile-content green">
							<div class="circle-tile-description text-faded">
								With cutter
							</div>
							<div class="circle-tile-number text-faded" id="counter">
								<small>{{$data['withCutter']}}</small></br>
								<span id="sparklineB"></span>
							</div>
							<!-- <a href="{{ url('admin','feeds') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading green">
								<img src="{{ asset('images/backend/bank-account.svg') }}">
							</div>
						<div class="circle-tile-content green">
							<div class="circle-tile-description text-faded">
								REVENUE <br>
								{{$data['revenue']}} SAR
							</div>
							<div class="circle-tile-number text-faded" id="counter">
								<small>Cash {{$data['cash']}}</small> &nbsp;&nbsp;
								<small>Credit {{$data['credit']}}</small>
								<span id="sparklineB"></span>
							</div>
							<!-- <a href="{{ url('admin','feeds') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading yellow">
								<img src="{{ asset('images/backend/currency.svg') }}">
							</div>
						<div class="circle-tile-content yellow">
							<div class="circle-tile-description text-faded">
								DAILY BRANCH REVENUE
							</div>
							<div class="circle-tile-number text-faded" id="counter">
								@foreach($data['branchWithOrderRev'] as $value)
									<small>{{$value->branch}} :{{$value->total_branch_rev}}</small></br>
								@endforeach
								<span id="sparklineB"></span>
							</div>
							<!-- <a href="{{ url('admin','feeds') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
							<div class="circle-tile-heading dark-blue">
								<img src="{{ asset('images/backend/order.png') }}" style="width: 31px;">
							</div>
						<div class="circle-tile-content dark-blue">
							<div class="circle-tile-description text-faded">
								Placed order</br><!-- All Users -->
								<small>{{$data['totalPlaceOrders']}}</small>
							</div>
							<div class="circle-tile-number text-faded" id="counter">
								@foreach($data['braPlaceOrder'] as $branch => $order)
									<small>{{$branch}} :{{$order}}</small></br>
								@endforeach
								<span id="sparklineB"></span>
							</div>
							<!-- <a href="{{ url('admin','users') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
						</div>
					</div>
				</div>
				<!-- <div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
						<a href="{{ url('admin','charities') }}">
							<div class="circle-tile-heading dark-blue">
							<img src="{{ asset('images/backend/charity.svg') }}">	
							</div>
						</a>
						<div class="circle-tile-content dark-blue">
							<div class="circle-tile-description text-faded">
								Charities we support
							</div>
							<a href="{{ url('admin','charities') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
						</div>
					</div>
				</div> -->
				
				<!-- <div class="col-xs-12 col-sm-6 col-md-4">
					<div class="circle-tile">
						<a href="{{ url('admin','faq') }}">
							<div class="circle-tile-heading red">
								<img src="{{ asset('images/backend/content-manage.svg') }}">
							</div>
						</a>
						<div class="circle-tile-content red">
							<div class="circle-tile-description text-faded">
								Content Management
							</div>
							<a href="{{ url('admin','faq') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</div>

@if(session()->has('success'))
<script type="text/javascript">
    $.growl.notice({
        title: "Success!",
        message: 'Success'
    });
</script>
@endif

@if(session()->has('error'))
<script type="text/javascript">
    $.growl.error({
        title: "Oops!",
        message: 'Error'
    });
</script>
@endif
<script>
	$(document).ready(function() 
	{
		 window.history.pushState(null, "", window.location.href); 
			window.onpopstate = function() 
			{ 
				window.history.pushState(null, "", window.location.href); 
			};
	 });
	 
</script>
@stop
