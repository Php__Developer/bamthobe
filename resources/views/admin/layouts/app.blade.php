<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <meta name="_token" content="{!! csrf_token() !!}" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>BAM - @yield('title')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" type="image/xicon" href="{{ asset('images/common/favicon.ico') }}" />
        <!-- css files -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome-4.2.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.growl.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.bootstrap.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/admin-styles.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/lightbox.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />

        <!-- scripts --> 
        <!-- <script type="text/javascript" src="{{ asset('js/common/jquery.min.js') }}"></script> -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript" charset="utf-8"></script> -->
        
        <script type="text/javascript" src="{{ asset('js/common/jquery-3.2.1.min.js') }}"></script>        
        <script type="text/javascript" src="{{ asset('js/common/jquery-ui.min.js') }}"></script>  
        <script type="text/javascript" src="{{ asset('js/common/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/common/additional-methods.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/backend/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/backend/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/backend/datatables.bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/backend/jquery.growl.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/backend/lightbox.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/ckeditor/ckeditor/ckeditor.js') }}"></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/web/jquery.datetimepicker.css') }}">
        <script type="text/javascript" src="{{ asset('js/common/jquery.datetimepicker.full.min.js') }}"></script>
        <script src="https://unpkg.com/jquery-input-mask-phone-number@1.0.4/dist/jquery-input-mask-phone-number.js"></script>
        
        <script type="text/javascript" src="{{ asset('js/common/moment.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/common/daterangepicker.js') }}"></script>

        <style type="text/css">  
            .marker
            {
              background-color: yellow;
            }
            strong
            {
                font-weight: bold;
            }           
           

        </style>
    </head>
    <body>
        <div class="wrapper">
            <header class='text-right'>
                @include('admin.includes.header')
            </header>
            <div class="dashboard">
                @include('admin.includes.sidebar')
                <div class="dashboard__content">
                    <div class="dashboard__list">
                        @yield('content') 
                    </div>
                </div>
            </div>
        </div>

         <!-- Change Password Model -->
        <div class="modal fade" id="changePasswordModel" tabindex="-1" role="dialog" 
         aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Change Password</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal pwd-modal" role="form"  method="POST" id="changePasswordForm" action="{{ url('admin','changepassword') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label  class="col-xs-4 col-sm-3 control-label" for="current_password">Current Password <span>*</span></label>
                                <div class="col-xs-8 col-sm-9 input-cover-modal">
                                    <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Current Password"/>
                                    <p id="current-password">
                                        <span class="invalid-feedback"></span>
                                    </p>
                                     <div class="input-icon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 control-label" for="new_password" >New Password <span>*</span></label>
                                <div class="col-xs-8 col-sm-9 input-cover-modal">
                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password"/>
                                    <p id="new-password">
                                        <span class="invalid-feedback" ></span>
                                    </p>
                                    <div class="input-icon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 control-label" for="confirm_password" >Confirm Password <span>*</span></label>
                                <div class="col-xs-8 col-sm-9 input-cover-modal">
                                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password"/>
                                    <p id="confirm-password">
                                        <span class="invalid-feedback"></span>
                                    </p>
                                    <div class="input-icon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="modal-footer text-center">
                                <button type="button" class="btn btn-default" data-dismiss="modal"> Close</button>
                                <button type="submit" class="btn btn-primary" id="change-password-button">{{ __('Save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <input id="base_url" type="hidden" value="{{ URL::to('/admin') }}">
        <input id="assets_url" type="hidden" value="{{ URL::to('/') }}">
        <input id="domain_url" type="hidden" value="{{ URL::to('/') }}">
        <footer>
            @include('admin.includes.footer')
        </footer>
         <div class="show-loader" style="display: none;">
            <img src="{{ asset('images/common/hw_loader.svg') }}">
        </div>
        @stack('custom-scripts')
    </body>
</html>