@extends('admin.layouts.app')
@section('title','Sewing')
@section('content')
    <div class="page-heading"> 
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> sewing Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','defectiveorder') }}"> Defected Orders</a></span>
                <span>></span>
                 <span class="active">Sewing Defected Orders</span>
             </div>
         </div>
     </div>
    <div class="mng-customer-table">
        <table class="table table-bordered" id="data-table">
            <thead>
                <tr>
                    <th>Order Number</th>
                    <th>Item Number</th>
                    <th>Sewer</th>
                    <th>Branch</th>
                    <th>Salesman</th>
                    <th>QC</th>
                    <th>action</th>
                </tr>
            </thead>
        </table>
    </div>
    <input id="data-table-url" type="hidden" value="{!! route('sewingorder.data') !!}">

    <script type="text/javascript" src="{{ asset('js/backend/sewingorder.js') }}"></script>
@stop