@extends('admin.layouts.app')
@section('title','pending')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Defective Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','infactory') }}"> Defective Orders</a></span>
                <span>></span>
                 <span class="active">Manage Defective Orders</span>
              
            </div>
</div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Order Number</th>
                <th>Item Number</th>
                <th>Branch Code</th>
                <th>Salesman</th>
                <th>Sewer</th>
                <th>Cutter</th>
                <th>QC</th>
                <th>Defective</th>
                <th>Accountable</th>
                <th>Assign Time</th>
                <th>Assigned Staff</th>
                <th>Waiting Time</th>
                <th>action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('defective.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/defective.js') }}"></script>
@stop