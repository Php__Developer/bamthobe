@extends('admin.layouts.app')
@section('title','pending')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Pending Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','infactory') }}"> Pending Orders</a></span>
                <span>></span>
                 <span class="active">Manage Pending Orders</span>
              
            </div>
</div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>order_number</th>
                <th>item_number</th>
                <th>cutter_id</th>
                <th>assign_time</th>
                <th>waiting_time</th>
                <th>action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('pending.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/pending.js') }}"></script>
@stop