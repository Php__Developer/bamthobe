@extends('admin.layouts.app')
@section('title','Created Orders')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Created Order</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','orders') }}"> Created Orders</a></span>
                <span>></span>
                 <span class="active">Manage Created Orders</span>
              
            </div>
</div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>order_number</th>
               
                <th>item_number</th>
                  <th> fabric</th>
                <th>action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('finished.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/created.js') }}"></script>
@stop