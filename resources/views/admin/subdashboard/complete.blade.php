@extends('admin.layouts.app')
@section('title','Completed Order') 
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Completed Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','infactory') }}"> Completed Orders</a></span>
                <span>></span>
                 <span class="active">Manage Completed Orders</span>
              
            </div>
</div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>order number</th>
                <th>item number</th>
                <th>completion time</th>
                <th>action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('complete.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/complete.js') }}"></script>
@stop