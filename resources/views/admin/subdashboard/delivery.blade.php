@extends('admin.layouts.app')
@section('title','pending')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Delivered Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','infactory') }}"> In Factory</a></span>
                <span>></span>
                <span class="active">Manage Delivered Orders</span>
            </div>
        </div>
    </div>
    
    <div class="mng-customer-table">
        <table class="table table-bordered" id="data-table">
            <thead>
                <tr>
                    <th>Order Number</th>
                    <th>Item Number</th>
                    <th>Completion Time</th>
                    <th>Delivery Time</th>
                    <th>Waiting Time</th>
                    <th>action</th>
                </tr>
            </thead>
        </table>
    </div>
<input id="data-table-url" type="hidden" value="{!! route('delivery.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/delivery.js') }}"></script>
@stop