@extends('admin.layouts.app')
@section('title', 'Dashboard')
@section('content')
<style type="text/css">
   .buttons .row {
   justify-content: center;
   align-items: center;
   text-align: center;
   display: flex;
   }
   .btn.btn-primary.btn1{
   margin-left: 5px;
   margin-right: 5px;
   }
   .btn.btn-primary.btn2{
   margin-left: 5px;
   margin-right: 5px;
   }
   .btn.btn-primary.btn3{
   margin-left: 5px;
   margin-right: 5px;
   }
   .circle-tile-content.red:hover {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
   }
   .circle-tile-content.red {
       border-radius: 15px;
   }
   a.circle-tile-footer {
       border-radius: 0px 0px 15px 15px;
   }
   .circle-tile-content.dark-blue{
       border-radius: 15px;
   }
   .circle-tile-content.dark-blue:hover {
       box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
   }
   .circle-tile-content.green{
       border-radius: 15px;
   }
   .circle-tile-content.green:hover {
       box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
   }
   .circle-tile-content.yellow{
       border-radius: 15px;
   }
   .circle-tile-content.yellow:hover {
       box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
   }
</style>
<div class="page-heading">
   <div class="pageheding-inner">
      <h1 class="page-common-head"><span>Dashboard</span></h1>
      @if(Auth::id() ==1)
      <div class="new-customer-btn single-btn">
         <a class="btn btn-primary pull-right" href="{{ route('admin.create') }}"><i class="fa fa-plus" aria-hidden="true"></i>Add Admin</a>
         <a class="btn btn-primary" id="show-add-form" data-toggle="modal" data-target="#messageModal"><i class="fa fa-bell" aria-hidden="true"></i></a>&nbsp;
      </div>
      @endif
   </div>
</div>

<div class="modal fade" id="messageModal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Notification</h4>
         </div>
         <div class="modal-footer">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.notification') }}">
               {{ csrf_field() }}
               <select class="col-sm-10" name="type" required>
                  <option value="">Select for notification</option>
                  <option value="staff">Staff</option>
                  <option value="customer">Customer</option>
               </select>
               <input type="text" class="col-sm-10" name="message" id="admin-message">
               <input type="hidden" name="user_id" value="{{Auth::id()}}">
               <input type="hidden" name="is_admin" value="yes">
               <button type="submit" class="btn btn-primary pull-right">Post</button>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="handi-form">
   <div class="buttons">
      <div class="row">
         <form action="dashboard" method="get">
            <input type="hidden" name="id" id="id"  value="1">
            <button type="submit" class="btn btn-primary btn1">Daily</button>
         </form>
         <form action="dashboard" method="get">
            <input type="hidden" name="id" id="id"  value="2">
            <button type="submit" class="btn btn-primary btn2">Weekly</button>
         </form>
         <form action="dashboard" method="get">
            <input type="hidden" name="id" id="id"  value="3">
            <button type="submit" class="btn btn-primary btn3">Monthly</button>
         </form>
      </div>
   </div>

   <div class="container-2">
      <div id="page-wrapper">
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading red">
                     <img src="{{ asset('images/backend/feed.svg') }}">
                  </div>
                  <div class="circle-tile-content red">
                     <div class="circle-tile-description text-faded">
                        Delay Orders
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['delayOrder']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','delay') }}" class="circle-tile-footer">Delayed Orders <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading dark-blue">
                     <img src="{{ asset('images/backend/order.png') }}" style="width: 31px;">
                  </div>
                  <div class="circle-tile-content dark-blue">
                     <div class="circle-tile-description text-faded">
                        Created Orders</br>
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{ $data['braPlaceOrder'] }}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','Created') }}" class="circle-tile-footer">Created Orders <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading red">
                     <img src="{{ asset('images/backend/feed.svg') }}">
                  </div>
                  <div class="circle-tile-content red">
                     <div class="circle-tile-description text-faded">
                        B2B Orders
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['bbOrders']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','b2b') }}" class="circle-tile-footer">B2B Orders <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading red">
                     <img src="{{ asset('images/backend/close.png') }}">
                  </div>
                  <div class="circle-tile-content red">
                     <div class="circle-tile-description text-faded">
                        Defective order
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['totalDefective']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','defectiveorder') }}" class="circle-tile-footer">Defective order <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading green">
                     <img src="{{ asset('images/backend/scissors.png') }}">
                  </div>
                  <div class="circle-tile-content green">
                     <div class="circle-tile-description text-faded">
                        Cutting
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['withCutter']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','cutter') }}" class="circle-tile-footer">Cutting <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading dark-blue">
                     <img src="{{ asset('images/backend/factory.png') }}" style="width: 31px;">
                  </div>
                  <div class="circle-tile-content dark-blue">
                     <div class="circle-tile-description text-faded">
                        In factory
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['orderInFactory']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','infactory') }}" class="circle-tile-footer">factory items <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading green">
                     <img src="{{ asset('images/backend/bank-account.svg') }}">
                  </div>
                  <div class="circle-tile-content green">
                     <div class="circle-tile-description text-faded">
                        Income <br>
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['revenue']}} SAR</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','branchesincome') }}" class="circle-tile-footer">Income Revenue <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="circle-tile">
                  <div class="circle-tile-heading yellow">
                     <img src="{{ asset('images/backend/currency.svg') }}">
                  </div>
                  <div class="circle-tile-content yellow">
                     <div class="circle-tile-description text-faded">
                        Released orders
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['realeased']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','realized') }}" class="circle-tile-footer">Realized order <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@if(session()->has('success'))
<script type="text/javascript">
   $.growl.notice({
       title: "Success!",
       message: 'Success'
   });
</script>
@endif
@if(session()->has('error'))
<script type="text/javascript">
   $.growl.error({
       title: "Oops!",
       message: 'Error'
   });
</script>
@endif
<script>
   $(document).ready(function() 
   {
   	 window.history.pushState(null, "", window.location.href); 
   		window.onpopstate = function() 
   		{ 
   			window.history.pushState(null, "", window.location.href); 
   		};
    });
    
</script>

     
     <script type='text/javascript'>
     $(document).ready(function(){

       // Fetch all records
       $('#id').click(function(){
    fetchRecords(0);
       });

      
       });

     $('.btn1').click(function(e){

      $.ajax({
         url: 'dashboard?id=1',
         type: 'get',
         dataType: 'json',
         success: function(res){

           console.log(res);
           }

        
       });
       });
     // function fetchRecords(id){
     //   $.ajax({
     //     url: 'dashboard/'+id,
     //     type: 'get',
     //     dataType: 'json',
     //     success: function(response){

     //       var len = 0;
     //       $('#userTable tbody').empty(); // Empty <tbody>
     //       if(response['data'] != null){
     //          len = response['data'].length;
     //       }

        
     //   });
     // }
     </script>
@stop