<div class="handi-form">
   <div class="buttons">
      <div class="row">
         <form action="dashboard" method="get">
            <input type="hidden" name="id" id="id"  value="1">
            <span class="btn btn-primary btn1">Daliy</span>
         </form>
         <form action="dashboard" method="get">
            <input type="hidden" name="id" id="id"  value="2">
            <button type="submit" class="btn btn-primary btn2">Weekly</button>
         </form>
         <form action="dashboard" method="get">
            <input type="hidden" name="id" id="id"  value="3">
            <button type="submit" class="btn btn-primary btn3">Monthly</button>
         </form>
      </div>
   </div>
   <div class="container-2">
      <div id="page-wrapper">
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading red">
                     <img src="{{ asset('images/backend/feed.svg') }}">
                  </div>
                  <div class="circle-tile-content red">
                     <div class="circle-tile-description text-faded">
                        Delay order
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['delayOrder']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','delay') }}" class="circle-tile-footer">Delayed Orders <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading dark-blue">
                     <img src="{{ asset('images/backend/order.png') }}" style="width: 31px;">
                  </div>
                  <div class="circle-tile-content dark-blue">
                     <div class="circle-tile-description text-faded">
                        Created Orders</br><!-- All Users -->
                        <small>{{$data['totalPlaceOrders']}}</small>
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        @foreach($data['braPlaceOrder'] as $branch => $order)
                        <small>{{$branch}} :{{$order}}</small></br>
                        @endforeach
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','Created') }}" class="circle-tile-footer">Created Orders <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading red">
                     <img src="{{ asset('images/backend/feed.svg') }}">
                  </div>
                  <div class="circle-tile-content red">
                     <div class="circle-tile-description text-faded">
                        B2B Orders
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['bbOrders']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','uniform') }}" class="circle-tile-footer">B2B Orders <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading dark-blue">
                     <img src="{{ asset('images/backend/factory.png') }}" style="width: 31px;">
                  </div>
                  <div class="circle-tile-content dark-blue">
                     <div class="circle-tile-description text-faded">
                        In factory
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['orderInFactory']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','infactory') }}" class="circle-tile-footer">factory items <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading green">
                     <img src="{{ asset('images/backend/bank-account.svg') }}">
                  </div>
                  <div class="circle-tile-content green">
                     <div class="circle-tile-description text-faded">
                        Income <br>
                        {{$data['revenue']}} SAR
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>Cash {{$data['cash']}}</small> &nbsp;&nbsp;
                        <small>Credit {{$data['credit']}}</small>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','salary') }}" class="circle-tile-footer">Income Revenue <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading red">
                     <img src="{{ asset('images/backend/close.png') }}">
                  </div>
                  <div class="circle-tile-content red">
                     <div class="circle-tile-description text-faded">
                        Defective order
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['totalDefective']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','orders') }}" class="circle-tile-footer">Defective order <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
         	<div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading green">
                     <img src="{{ asset('images/backend/scissors.png') }}">
                  </div>
                  <div class="circle-tile-content green">
                     <div class="circle-tile-description text-faded">
                        Cutting
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        <small>{{$data['withCutter']}}</small></br>
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','cutters') }}" class="circle-tile-footer">Cutting <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading yellow">
                     <img src="{{ asset('images/backend/currency.svg') }}">
                  </div>
                  <div class="circle-tile-content yellow">
                     <div class="circle-tile-description text-faded">
                        Realized order:
                     </div>
                     <div class="circle-tile-number text-faded" id="counter">
                        @foreach($data['branchWithOrderRev'] as $value)
                        <small>{{$value->branch}} :{{$value->total_branch_rev}}</small></br>
                        @endforeach
                        <span id="sparklineB"></span>
                     </div>
                     <a href="{{ url('admin','orders') }}" class="circle-tile-footer">Realized order <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
               <div class="circle-tile">
                  <div class="circle-tile-heading orange">
                     <img src="{{ asset('images/backend/termsandcondition.svg') }}">
                  </div>
                  <div class="circle-tile-content orange">
                     <div class="circle-tile-description text-faded">
                        Completed Order 
                     </div>
                     <div class="circle-tile-number text-faded">
                        <small>{{$data['totalOrderCompleted']}}</small></br>
                     </div>
                     <a href="{{ url('admin','orders') }}" class="circle-tile-footer">Completed Order <i class="fa fa-chevron-circle-right"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>