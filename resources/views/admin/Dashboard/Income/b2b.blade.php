@extends('admin.layouts.app')
@section('title', AL- Order') 
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Branch 01 Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','branchesincome') }}"> Incomes</a></span>
                <span>></span>
                 <span class="active">BAM01 Orders</span>
              
            </div>
</div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Order Number</th>
                <th>Invoice</th>
                <th>Downpayment</th>
                <th>Remaning Amount</th>
                <th>Payment Type</th> 
                <th>action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('income.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/b2bincome.js') }}"></script>
@stop