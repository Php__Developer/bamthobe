@extends('admin.layouts.app')
@section('title','b2b Orders')
@section('content')

<div class="page-heading">
  <div class="pageheding-inner">
    <h1 class="page-common-head"><span> B2B Order</span></h1>
    <div class="breadcrumb">
      <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
      <!-- <span>></span>
      <span><a href="{{ url('admin','orders') }}"> B2B Orders</a></span> -->
      <span>></span>
      <span class="active">Manage B2B Orders</span>
    </div>
  </div>
</div>
<div class="mng-customer-table">
  <table class="table table-bordered" id="data-table">
    <thead>
      <tr>
        <th>Order Number</th>
        <th>Order Type</th>
        <th>Client Name</th>
        <th>Order Date</th>
        <th>Payment</th>
        <th>Order Status</th>
      </tr>
    </thead>
  </table>
</div>

<input id="data-table-url" type="hidden" value="{!! route('b2b.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/b2b.js') }}"></script>
@stop