@extends('admin.layouts.app')
@section('title', 'Dashboard')
@section('content')
<style type="text/css">
   .buttons .row {
   justify-content: center;
   align-items: center;
   text-align: center;
   display: flex;
   }
   .btn.btn-primary.btn1{
   margin-left: 5px;
   margin-right: 5px;
   }
   .btn.btn-primary.btn2{
   margin-left: 5px;
   margin-right: 5px;
   }
   .btn.btn-primary.btn3{
   margin-left: 5px;
   margin-right: 5px;
   }
</style>
<div class="page-heading">
   <div class="pageheding-inner">
      <h1 class="page-common-head"><span>Factory</span></h1>
      @if(Auth::id() ==1)
      <div class="new-customer-btn single-btn">
         <a class="btn btn-primary pull-right" href="{{ route('admin.create') }}"><i class="fa fa-plus" aria-hidden="true"></i>Add Admin</a>
         <a class="btn btn-primary" id="show-add-form" data-toggle="modal" data-target="#messageModal"><i class="fa fa-bell" aria-hidden="true"></i></a>&nbsp;
      </div>
      @endif
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="messageModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Notification</h4>
         </div>
         <div class="modal-footer">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.notification') }}">
               {{ csrf_field() }}
               <select class="col-sm-10" name="type" required>
                  <option value="">Select for notification</option>
                  <option value="staff">Staff</option>
                  <option value="customer">Customer</option>
               </select>
               <input type="text" class="col-sm-10" name="message" id="admin-message">
               <input type="hidden" name="user_id" value="{{Auth::id()}}">
               <input type="hidden" name="is_admin" value="yes">
               <!-- <a class="btn btn-primary pull-right" href="{{ route('admin.notification') }}">Post</a> -->
               <button type="submit" class="btn btn-primary pull-right">Post</button>
               <!--                                 <button type="button" class="btn btn-default btn-submit">Post</button> -->
            </form>
         </div>
      </div>
   </div>
</div>

<div class="handi-form">
   <div class="container-2">
      <div id="page-wrapper">
         <div class="row">

          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="circle-tile">
                <div class="circle-tile-content red">
                  <div class="circle-tile-description text-faded">
                  Pending
                </div>
                <a href="{{ url('admin','pending') }}" class="circle-tile-footer">Pending Orders 
                  <i class="fa fa-chevron-circle-right"></i>
                </a>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4">
             <div class="circle-tile">
                <div class="circle-tile-content dark-blue">
                   <div class="circle-tile-description text-faded">
                      Sewing </br>
                   </div>
                   <a href="{{ url('admin','sewingorder') }}" class="circle-tile-footer">Sewing Orders <i class="fa fa-chevron-circle-right"></i></a>
                </div>
             </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4">
             <div class="circle-tile">
                <div class="circle-tile-content red">
                   <div class="circle-tile-description text-faded">
                      Completed 
                   </div>
                   <a href="{{ url('admin','complete') }}" class="circle-tile-footer">Completed Orders <i class="fa fa-chevron-circle-right"></i></a>
                </div>
             </div>
          </div>

        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="circle-tile">
              <div class="circle-tile-content dark-blue">
                <div class="circle-tile-description text-faded">Delivered</div>
                <div class="circle-tile-number text-faded" id="counter">
                  <span id="sparklineB"></span>
                </div>
                <a href="{{ url('admin','delivery') }}" class="circle-tile-footer">Delivered Orders <i class="fa fa-chevron-circle-right"></i></a>
              </div>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-6 col-md-4">
             <div class="circle-tile">
                <div class="circle-tile-content green">
                   <div class="circle-tile-description text-faded">
                      Defective <br>
                   </div>
                   <div class="circle-tile-number text-faded" id="counter">
                      <span id="sparklineB"></span>
                   </div>
                   <a href="{{ url('admin','defective') }}" class="circle-tile-footer">Defective Orders <i class="fa fa-chevron-circle-right"></i></a>
                </div>
             </div>
          </div>

        </div>
      </div>
    </div>
   </div>
</div>
@if(session()->has('success'))
<script type="text/javascript">
   $.growl.notice({
       title: "Success!",
       message: 'Success'
   });
</script>
@endif
@if(session()->has('error'))
<script type="text/javascript">
   $.growl.error({
       title: "Oops!",
       message: 'Error'
   });
</script>
@endif
<script>
   $(document).ready(function() 
   {
     window.history.pushState(null, "", window.location.href); 
        window.onpopstate = function() 
        { 
            window.history.pushState(null, "", window.location.href); 
        };
    });
    
</script>

     
     <script type='text/javascript'>
     $(document).ready(function(){

       // Fetch all records
       $('#id').click(function(){
    fetchRecords(0);
       });

      
       });

   

     function fetchRecords(id){
       $.ajax({
         url: 'dashboard/'+id,
         type: 'get',
         dataType: 'json',
         success: function(response){

           var len = 0;
           $('#userTable tbody').empty(); // Empty <tbody>
           if(response['data'] != null){
              len = response['data'].length;
           }

        
       });
     }
     </script>
@stop