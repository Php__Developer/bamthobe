<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// To check email template
// Route::get('/mailable', function () {
//     $customer = App\customer::find(22);
//     return new App\Mail\VerifyMail($customer);
// });
// To check email template

Route::group(['namespace' => 'Frontend'], function () {
	
	 Route::get('/getStates', 'HomeController@getStates')->name('getStates');

	 Route::get('/', 'HomeController@index')->name('landing');
	
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});



Route::prefix('admin')->group(function () {
	Route::group(['middleware' => 'auth:admin'], function () {
		Route::post('logout', 'Auth\Admin\LoginController@logout')->name('admin.auth.logout');

		Route::group(['namespace' => 'Admin'], function () {
			Route::post('changepassword', 'AdminController@changePassword')->name('admin.changepassword');

			//Admin create
			Route::get('create', 'CustomersController@createAdmin')->name('admin.create');
			Route::post('addAdmin', 'CustomersController@addAdmin')->name('admin.addAdmin');

			
			//Admin create

			Route::group(['middleware' => 'charityLogin'], function () {
				Route::get('auction-details', 'CharityUserController@index')->name('charity.auctionDetails');
				Route::get('auction-details/table_data','CharityUserController@table_data')->name('charity.auctionData');
			});

			Route::get('dashboard', 'DashBoardController@dashboard')->name('admin.dashboard');
			
			Route::group(['middleware' => 'adminLogin'], function () {

		    	Route::post('changePasswordUsers','AdminController@changePasswordUsers');
		    	Route::any('notification','AdminController@notification')->name('admin.notification');
				
				// Customers
				Route::get('customers/table_data','CustomersController@table_data')->name('customers.data');
				Route::post('customers/changeStatus','CustomersController@changeStatus');
				Route::get('customers/checkEmail','CustomersController@checkEmail');
				Route::get('customers/getcustomerDetailByEmail','CustomersController@getcustomerDetailByEmail');
		    	Route::get('customers/getemailcustomers','CustomersController@getemailcustomers');
		    	Route::get('customers/customer-profile/{customerID}','CustomersController@customerProfile')->name('admin.customerProfile');
				Route::resource('customers','CustomersController');
				Route::post('/customers/delete','CustomersController@delete');
				// Customers

				// FAQ
				Route::post('faq/changestatus','FaqController@changeStatus')->name('faq.changestatus');
				Route::get('faq/table_data','FaqController@table_data')->name('faq.data');
				Route::resource('faq','FaqController');
				// FAQ

				// Factories
				Route::get('factories/table_data','FactoryController@table_data')->name('factories.data');
				Route::post('factories/changeStatus','FactoryController@changeStatus');
				Route::post('factories/getEditRecord','FactoryController@getEditRecord');
				Route::post('factories/editFactory/{factoryID}','FactoryController@update');
				Route::resource('factories','FactoryController');
				// factories

				// Charities
				Route::get('charities/table_data','CharityController@table_data')->name('charities.data');
				// Route::post('charities/getEditRecord','CharityController@getEditRecord');
				Route::post('charities/changeStatus','CharityController@changeStatus');
				Route::resource('charities','CharityController');
				// Charities

				// Terms
				Route::post('ContentManagement/store','ContentManagementController@storeterms');
				Route::get('terms','ContentManagementController@index')->name('admin.terms');
				Route::get('ContentManagement/edit_terms_data','ContentManagementController@editTerms'); 
				// Terms

				// Privacy Policy
				Route::post('ContentManagement/privateUpdate','ContentManagementController@storePrivatePolicy');
				Route::get('privacy-policy','ContentManagementController@privateIndex')->name('admin.privacy_policy');
				Route::get('ContentManagement/edit_private_data','ContentManagementController@editPrivate');
				// Privacy Policy

				// About Us
				Route::post('ContentManagement/aboutUpdate','ContentManagementController@storeAbout');
				Route::get('about-us','ContentManagementController@aboutIndex')->name('admin.about_us');
				Route::get('ContentManagement/edit_about_data','ContentManagementController@editAbout');
				// About Us

				// donationVsBid
				Route::post('ContentManagement/donation_vs_bid_update','ContentManagementController@storedonationVsBid');
				Route::get('donationVsBid','ContentManagementController@donationVsBidIndex')->name('admin.donationVsBid');
				Route::get('ContentManagement/edit_donation_vs_Bid','ContentManagementController@editdonationVsBid');
				// donationVsBid

				
				//Salesmans
				Route::get('salesmans/table_data','SalesmanController@table_data')->name('salesmans.data');
				Route::post('salesmans/changeStatus','SalesmanController@changeStatus');
				Route::resource('salesmans','SalesmanController');
				//Salesmans

				
				//Cutter
				Route::get('cutters/table_data','CutterController@table_data')->name('cutters.data');
				Route::post('cutters/changeStatus','CutterController@changeStatus');
				Route::resource('cutters','CutterController');
				//Cutter

				
				//QA
				Route::get('quality/table_data','QualityController@table_data')->name('quality.data');
				Route::post('quality/changeStatus','QualityController@changeStatus');
				Route::resource('quality','QualityController');
				//QA
				
				//otherUsers
				Route::get('otherUsers/table_data','OtherUsersController@table_data')->name('otherUsers.data');
				Route::post('otherUsers/changeStatus','OtherUsersController@changeStatus');
				Route::resource('otherUsers','OtherUsersController');
				//otherUsers

			

				//Website Content
				Route::post('webpage/website_content_update','WebsiteContentController@storeWebsiteContent');
				Route::get('webpage','WebsiteContentController@index');

				//Website Content

				// Promo Code
				Route::get('promoCodes/table_data','PromoCodeController@table_data')->name('promoCodes.data');
				Route::post('promoCodes/changeStatus','PromoCodeController@changeStatus')->name('promoCodes.changestatus');
				Route::resource('promoCodes','PromoCodeController');
				// Promo Code

				// Referral Code
				Route::get('referralCodes/table_data','ReferralCodeController@table_data')->name('referralCodes.data');
				Route::post('referralCodes/changeStatus','ReferralCodeController@changeStatus')->name('referralCodes.changestatus');
				Route::resource('referralCodes','ReferralCodeController');
				// Referral Code

				// FAQ Category
				Route::get('faqcategories/table_data','FaqCategoryController@table_data')->name('faqcategories.data');
				Route::post('faqcategories/getEditRecord','FaqCategoryController@getEditRecord');
				Route::post('faqcategories/editFaqCategory/{faqcategoryID}','FaqCategoryController@update');
				Route::resource('faqcategories','FaqCategoryController');
				// FAQ Category



				// Tailor
				Route::post('tailor/changeStatus','TailorController@changeStatus')->name('tailor.changestatus');
				Route::get('tailor/table_data','TailorController@table_data')->name('tailor.data');
				Route::resource('tailor','TailorController');
				Route::get('tailor/tailor-profile/{tailorID}','TailorController@tailorProfile')->name('admin.tailorProfile');
				// Tailor


				// Branches
				Route::get('branches/table_data','BranchController@table_data')->name('branches.data');
				Route::post('branches/changeStatus','BranchController@changeStatus');
				Route::get('branches/checkEmail','BranchController@checkEmail');
				Route::get('branches/getbranchDetailByEmail','BranchController@getbranchDetailByEmail');
		    	Route::get('branches/getemailbranches','BranchController@getemailbranches');
		    	Route::get('branches/branch-profile/{branchID}','BranchController@branchProfile')->name('admin.branchProfile');
				Route::resource('branches','BranchController');
				Route::post('/branches/delete','BranchController@delete');
				// Branches

				//suppliers
				Route::get('suppliers/table_data','SupplierController@table_data')->name('suppliers.data');
				Route::post('suppliers/changeStatus','SupplierController@changeStatus');
				Route::resource('suppliers','SupplierController');
				//suppliers

				//Catalogues
				Route::get('inventories','CataloguesController@inventories');
				Route::get('catalogues/home/{id}','CataloguesController@home')->name('catalogues.home');
				Route::get('catalogues/table_data','CataloguesController@table_data')->name('catalogues.data');
				Route::get('catalogues/product-details/{orderId}','CataloguesController@productDetails')->name('catalogues.productDetails');
				Route::post('catalogues/changeStatus','CataloguesController@changeStatus');
				Route::resource('catalogues', 'CataloguesController', ['except' => ['index']]);

				// Route::resource('income','IncomeController');
				Route::get('b2bincome/table_data','IncomebusinessController@table_data')->name('b2bincome.data');
				Route::resource('b2bincome','IncomebusinessController');
				Route::get('branchesincome','IncomeController@branchesincome');
				Route::get('income/main/{id}','IncomeController@main')->name('income.main');
				Route::get('income/home/{id}','IncomeController@home')->name('income.home');
				Route::get('income/table_data','IncomeController@table_data')->name('income.data');
				Route::resource('income', 'IncomeController', ['except' => ['index']]);

				//inventory
				Route::get('inventory/table_data','InventoryController@table_data')->name('inventory.data');
				Route::post('inventory/changeStatus','InventoryController@changeStatus');
				Route::resource('inventory','InventoryController');

				//Payments & Orders
				Route::get('payorders/table_data','PayordersController@table_data')->name('payorders.data');
				Route::get('createOrder','PayordersController@createOrder');
				Route::get('/createNextOrder', 'PayordersController@createNextOrder')->name('admin.createNextOrder');
				// Route::post("/createNextOrder", "PayordersController@createNextOrder")->name('admin.createNextOrder');
				// Route::post('createNextOrder','PayordersController@createNextOrder');
				Route::resource('payorders','PayordersController');
				Route::get('payorders/details/{payId}','PayordersController@details')->name('admin.payOrderDetails');
				Route::get('payorders/settle/{payId}','PayordersController@settle')->name('admin.payOrderSettle');
				Route::post('payorders/settled','PayordersController@settled');
				Route::get('purchaseorder','PayordersController@addorders')->name('admin.purchaseorder');//->name('admin.payOrderSettled');
				//Payments & Orders

				//payroll
				Route::get('payroll/table_data','PayrollController@table_data')->name('payroll.data');
				Route::post('payroll/changeStatus','PayrollController@changeStatus');
				Route::resource('payroll','PayrollController');
				Route::get('ajax/empData','PayrollController@ajaxEmpData');
				Route::get('payroll/generate/{id}','PayrollController@generate')->name('payroll.generate');
				Route::get('payroll/download/{id}','PayrollController@downloadPaySlip')->name('payroll.download');
				//payroll

				//salary
				Route::get('salary/table_data','SalaryController@table_data')->name('salary.data');
				Route::post('salary/changeStatus','SalaryController@changeStatus');
				Route::resource('salary','SalaryController');
				//salary

				//Payments
				Route::resource('payments','PaymentController');
				//Payments

				//Reports
				Route::resource('reports','ReportController');
				//Reports

				// loyalty_programs
				Route::get('loyalty/table_data','LoyaltyController@table_data')->name('loyalty.data');
				Route::post('loyalty/changeStatus','LoyaltyController@changeStatus')->name('loyalty.changestatus');
				Route::resource('loyalty','LoyaltyController');
				// loyalty_programs

				// query
				Route::get('query/table_data','QueryController@table_data')->name('query.data');
				Route::post('query/changeStatus','QueryController@changeStatus')->name('query.changestatus');
				Route::resource('query','QueryController');
				// query
				
				//Orders
				Route::get('orders/table_data','OrdersController@table_data')->name('orders.data');
				Route::resource('orders','OrdersController');
				Route::get('orders/order-details/{orderId}','OrdersController@details')->name('admin.orderDetails');
				Route::post('add/comment', 'OrdersController@addComment');
				
				Route::get('finished/table_data','OrdersFinishedController@table_data')->name('finished.data');
				Route::resource('finished', 'OrdersFinishedController');
				//Orders

				//Accounts
				Route::get('accounts/table_data','AccountsController@table_data')->name('accounts.data');
				Route::get('accounts/summary','AccountsController@summary');
				Route::resource('accounts','AccountsController');
				//Accounts

				//Model Pricing
				Route::get('modelpricing/table_data','ModelPricingController@table_data')->name('modelpricing.data');
				Route::resource('modelpricing','ModelPricingController');
				//Model Pricing

				// Measurements
				Route::get('measurement/table_data','MeasurementController@table_data')->name('measurement.data');
				Route::resource('measurement','MeasurementController');
				// Measurements

				// Measurements
				Route::get('business/table_data','BusinessController@table_data')->name('business.data');
				Route::resource('business','BusinessController');
				Route::post('business/changeStatus','BusinessController@changeStatus');
				Route::get('business/order-details/{orderId}','BusinessController@details')->name('business.orderDetails');
				Route::get('business/order-pay/{orderId}','BusinessController@pay')->name('business.orderPay');
				// Measurements

				//uniform
				Route::get('uniform/table_data','UniformController@table_data')->name('uniform.data');
				Route::get('uniform/order-details/{orderId}','UniformController@details')->name('uniform.orderDetails');
				Route::resource('uniform','UniformController');
				Route::post('uniform/changeStatus','UniformController@changeStatus');
				Route::get('uniform/order-pay/{orderId}','UniformController@pay')->name('uniform.orderPay');
				//uniform
				
				//delay
				Route::get('delay/table_data','DelayController@table_data')->name('delay.data');
				Route::get('delay/order-details/{orderId}','DelayController@orderDetails')->name('delay.orderDetails');
				Route::resource('delay','DelayController');


				Route::get('Created','CreatedController@index');

				Route::get('b2b/table_data','B2BController@table_data')->name('b2b.data');
				Route::resource('b2b','B2BController');
				
				Route::resource('infactory','InfactoryController');

				Route::resource('defectiveorder','DefectiveOrderController');

				Route::resource('cutter','CuttingController');

				Route::resource('realized','RealizedController');

				//delay

				//subdashboard
				Route::get('pending/table_data','PendingController@table_data')->name('pending.data');
				Route::resource('pending','PendingController');
				Route::get('sewing/table_data','SewingController@table_data')->name('sewing.data');
				Route::resource('sewing','SewingController');

				Route::get('complete/table_data','CompletedController@table_data')->name('complete.data');
				Route::resource('complete','CompletedController');

				Route::get('delivery/table_data','DeliveredController@table_data')->name('delivery.data');
				Route::resource('delivery','DeliveredController');

				Route::get('defective/table_data','DefectiveController@table_data')->name('defective.data');
				Route::resource('defective','DefectiveController');

				Route::get('sewingorder/table_data','SewingOrderController@table_data')->name('sewingorder.data');
				Route::resource('sewingorder','SewingOrderController');

				Route::get('measurement/table_data','MeasurementOrderController@table_data')->name('measurement.data');
				Route::resource('measurement','MeasurementOrderController');

				Route::get('cutting/table_data','CuttingOrderController@table_data')->name('cutting.data');
				Route::resource('cutting','CuttingOrderController');

                Route::get('pendingorder/table_data','PendingOrderController@table_data')->name('pendingorder.data');
				Route::resource('pendingorder','PendingOrderController');

				Route::get('Bam01/table_data','Bam01Controller@table_data')->name('Bam01.data');
				Route::resource('Bam01','Bam01Controller');

				Route::get('Bam02/table_data','Bam02Controller@table_data')->name('Bam02.data');
				Route::resource('Bam02','Bam02Controller');

                Route::get('Bam03/table_data','Bam03Controller@table_data')->name('Bam03.data');
				Route::resource('Bam03','Bam03Controller');

				 Route::get('createBam01/table_data','CreateBAm01Controller@table_data')->name('createBam01.data');
				Route::resource('createBam01','CreateBAm01Controller');

				Route::get('createBam02/table_data','CreateBAm02Controller@table_data')->name('createBam02.data');
				Route::resource('createBam02','CreateBAm02Controller');

				Route::get('CreateBam03/table_data','CreateBAm03Controller@table_data')->name('CreateBam03.data');
				Route::resource('CreateBam03','CreateBAm03Controller');
                //subdashboard
                
				//Time Duration & Delivery Charges
				Route::get('buffer_day_delivery/table_data','BufferDayDeliveryChargeController@table_data')->name('buffer_day_delivery.data');
				Route::resource('buffer_day_delivery','BufferDayDeliveryChargeController');
				//Time Duration & Delivery Charges
			});

		});
	});
	Route::get('login', 'Auth\Admin\LoginController@login')->name('admin.auth.login');
	Route::post('login', 'Auth\Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
});
Route::get('privacypolicy','PrivacyController@privacy');
Auth::routes();
