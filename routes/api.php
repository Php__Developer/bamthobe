<?php

use Illuminate\Http\Request;
use Mailgun\Mailgun;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login','Api\AuthController@login');
Route::post('verify','Api\AuthController@checkOtp');
Route::post('register','Api\AuthController@register');
Route::post('dashboard','Api\CustomerController@getimage');
Route::post('appointments','Api\CustomerController@createAppointment');
Route::post('order_history','Api\OrderController@getOrderHistory');
Route::get('products','Api\ProductController@getproducts');
Route::post('productdetails','Api\ProductController@getproductsdetails');
Route::get('terms&condition','Api\WebsiteContentController@termsAndCondition');
Route::get('aboutus','Api\WebsiteContentController@aboutUs');
Route::get('privacypolicy','Api\WebsiteContentController@privacyPolicy');
Route::get('contactus','Api\ContactUsController@ContactUs');
Route::post('updateprofile','Api\CustomerController@updateProfile');
Route::post('update_profile','Api\CustomerController@update_profile');
Route::get('notifications','Api\NotificationController@notification');
Route::get('notifications','Api\NotificationController@teamNotification');
Route::post('loyalty','Api\LoyaltyController@loyalty');
Route::post('editProfile','Api\CustomerController@editProfile');
Route::post('orderdetail','Api\OrderController@orderdetail');
Route::get('promotions','Api\PromotionController@getPromotions');
Route::post('deliverydetails','Api\OrderController@deliverydetails');
Route::post('deliveryadd_address','Api\OrderController@deliveryadd_address');
Route::post('trackorder','Api\TrackorderController@TrackOrder');
Route::post('redeem','Api\LoyaltyController@redeemPoints');
Route::post('cutomer_reorder','Api\OrderController@reorder');
Route::post('send_query','Api\ContactUsController@sendQuery');
Route::post('applyPromo','Api\PromoCodeController@getPromo');
Route::resource('branch','Api\BranchController');



/*TeamApi Routes*/
Route::post('teamlogin','Api\TeamApi\AuthController@login');
Route::post('forgotpassword','Api\TeamApi\AuthController@forgetpassword');
Route::get('getneworder','Api\TeamApi\OrderController@getneworders');
Route::post('serchcustomer','Api\TeamApi\SearchController@serchcustomer');
Route::post('createcustomer','Api\TeamApi\CustomerController@createcustomer');
Route::post('editcustomer','Api\TeamApi\CustomerController@editcustomer');
Route::post('updatecustomer','Api\TeamApi\CustomerController@updatecustomer');
Route::post('getorderhistory','Api\TeamApi\OrderController@orderHistory');
Route::post('trackorder_salesman','Api\TeamApi\OrderController@trackOrder');
Route::post('checkcustomer','Api\TeamApi\OrderController@checkCustomer');
Route::post('orderitem','Api\TeamApi\OrderController@orderitem');
Route::post('editorder','Api\TeamApi\OrderController@editOrder');
Route::get('models','Api\TeamApi\ModelPricingController@modelList');
Route::post('orderacknoledge','Api\TeamApi\OrderController@orderAcknoledgement');
Route::post('orderconfirm','Api\TeamApi\OrderController@orderconfirm');
Route::post('getappointments','Api\TeamApi\AppointmentController@getAppointments');
Route::post('appointmentAccepted','Api\TeamApi\AppointmentController@acceptedAppointment');
Route::post('getorders','Api\TeamApi\OrderController@getorders');
Route::post('getsearchorders','Api\TeamApi\OrderController@getsearchorders');
Route::post('searchorder','Api\TeamApi\OrderController@searchOrder');
Route::post('requestinventory','Api\TeamApi\inventoryController@requestInventory');
Route::post('assignedunassigned','Api\TeamApi\assignmentController@assignedUnassigned');
Route::get('getcutters','Api\TeamApi\cutterController@getCutters');
Route::post('createassignment','Api\TeamApi\assignmentController@createAssignment');
Route::post('addcomment','Api\TeamApi\commentController@addComment');
Route::post('getordercomments','Api\TeamApi\commentController@getComments');
Route::post('visitappointment','Api\TeamApi\AppointmentController@visitToAppointment');
Route::post('cancleappointment','Api\TeamApi\AppointmentController@cancleAppointment');
Route::get('getcutterscompletedorders','Api\TeamApi\OrderController@getcuttersCompletedorders');
Route::post('comleteorderbycutter','Api\TeamApi\OrderController@comleteorderbyCutter');
Route::post('viewsketch','Api\TeamApi\ProductController@viewSketch');
Route::post('senddelivery','Api\TeamApi\OrderController@sendDelivery');
Route::post('getdelivery','Api\TeamApi\OrderController@getDelivery');
Route::post('orderdelivered','Api\TeamApi\OrderController@orderDelivery');
Route::post('getdeliveredorders','Api\TeamApi\OrderController@getDeliveredOrders');
Route::post('addappointment_comments','Api\TeamApi\AppointmentController@addComment');
Route::post('getappointment_comments','Api\TeamApi\AppointmentController@getComments');
Route::post('adddelivery_comments','Api\TeamApi\commentController@addDeliveryComments');
Route::post('getdelivery_comments','Api\TeamApi\commentController@getDeliveryComments');
Route::post('getqcassignedorders','Api\TeamApi\assignmentController@getQcAssignedOrders');
Route::post('passorderbyqc','Api\TeamApi\OrderController@passorderByqc');
Route::post('getqccompletedorders','Api\TeamApi\OrderController@getQcCompletedOrders');
Route::post('managerneworders','Api\TeamApi\OrderController@managerNeworders');
Route::post('managercompletedorders','Api\TeamApi\OrderController@managerCompletedorders');
Route::post('searchCompletedorders','Api\TeamApi\OrderController@searchCompletedorders');
Route::get('gettailorslist','Api\TeamApi\OtherusersController@getTailorsList');
Route::post('assigntailorbymngr','Api\TeamApi\assignmentController@assignTailorbymngr');
Route::get('getqclist','Api\TeamApi\qcController@getQC');
Route::post('assignqcbymngr','Api\TeamApi\assignmentController@assignQcbymngr');
Route::post('getemployeelist','Api\TeamApi\EmployeeController@getEmployeeList');
Route::post('editcutterbymngr','Api\TeamApi\EmployeeController@editCutter');
Route::post('edittailorbymngr','Api\TeamApi\EmployeeController@editTailor');
Route::post('editqcbymngr','Api\TeamApi\EmployeeController@editQC');
Route::post('editpackerbymngr','Api\TeamApi\EmployeeController@editPacker');
Route::get('getemployeerole','Api\TeamApi\EmployeeController@EmployeeRole');
Route::post('createemployee','Api\TeamApi\EmployeeController@createEmployee');
Route::post('searchemployee','Api\TeamApi\EmployeeController@searchEmployee');
Route::post('editsalesmansbymngr','Api\TeamApi\EmployeeController@editSalesmans');
Route::post('searchorderbymngr','Api\TeamApi\SearchController@searchOrders');
Route::post('updateorderstatusbyqc','Api\TeamApi\OrderController@updateOrderStatusbyQC');
Route::post('getdefectiveorders','Api\TeamApi\OrderController@getDefectiveOrdersbyMngr');
Route::post('deliveredordergetbymngr','Api\TeamApi\OrderController@getDeliveredOrdersbyMngr');
Route::post('searchDeliveredOrdersbyMngr','Api\TeamApi\OrderController@searchDeliveredOrdersbyMngr');
Route::post('tailorsdailytarget','Api\TeamApi\TargetController@tailorsDailyTarget');
Route::post('tailormonthlytarget','Api\TeamApi\TargetController@tailorMonthlyTarget');
Route::post('cutterdailytarget','Api\TeamApi\TargetController@cutterDailyTarget');
Route::post('salesmanmonthlytarget','Api\TeamApi\TargetController@salesmanMonthlyTarget');
Route::post('managerdailytarget','Api\TeamApi\TargetController@managerDailyTarget');
Route::post('addcommentorderitem','Api\TeamApi\commentController@addCommentOrderItem');
Route::post('getorderitemscomments','Api\TeamApi\commentController@getOrderItemsComments');
Route::post('confirmreordered','Api\TeamApi\OrderController@confirmReorderbySalesman');
Route::get('getproductslistbysalesman','Api\TeamApi\ProductController@getProductslist');
Route::post('changepasswordteam','Api\TeamApi\AuthController@changePassword');
Route::post('imagetest','Api\TeamApi\OrderController@uploadimagestest');
Route::post('markAsDefective','Api\TeamApi\OrderController@markAsDefective');
Route::get('getSalesmanOrders','Api\TeamApi\OrderController@getSalesmanOrders');

Route::get('getAllmeasurements','Api\TeamApi\MeasurementController@getAllmeasurements');
Route::post('createMeasurement','Api\TeamApi\MeasurementController@createMeasurement');
Route::post('updateMeasurement','Api\TeamApi\MeasurementController@updateMeasurement');
Route::get('customerMeasurement','Api\TeamApi\MeasurementController@customerMeasurement');
Route::post('saveMeasurement','Api\TeamApi\MeasurementController@saveMeasurement');
Route::post('confirmMeasurement','Api\TeamApi\MeasurementController@confirmMeasurement');

/*B2B*/

Route::post('customerLogin','Api\TeamApi\BusinessController@customerLogin');
Route::post('changePassword','Api\TeamApi\BusinessController@changePassword');
Route::post('createBusinessorder','Api\TeamApi\BusinessController@createBusinessorder');
Route::post('businessorderitem','Api\TeamApi\BusinessController@businessorderitem');
Route::post('businesseditOrder','Api\TeamApi\BusinessController@businesseditOrder');
Route::get('businessmodelList','Api\TeamApi\ModelPricingController@businessmodelList');
Route::get('getBusinessProductslist','Api\TeamApi\ProductController@getBusinessProductslist');
Route::post('businesscreateAssignment','Api\TeamApi\assignmentController@businesscreateAssignment');
// Route::post('businessorderconfirm','Api\TeamApi\BusinessController@businessorderconfirm');
// Route::post('businessorderconfirm','Api\TeamApi\BusinessController@businessorderconfirm');
Route::post('businessorderAcknoledgement','Api\TeamApi\BusinessController@businessorderAcknoledgement');
Route::post('businessorderconfirm','Api\TeamApi\BusinessController@businessorderconfirm');
Route::post('businessgetPromo','Api\PromoCodeController@businessgetPromo');
Route::post('businesssendDelivery','Api\TeamApi\BusinessController@businesssendDelivery');
Route::post('businessmanagerNeworders','Api\TeamApi\BusinessController@businessmanagerNeworders');
Route::post('businessStatus','Api\TeamApi\BusinessController@businessStatus');
Route::post('businessorders','Api\TeamApi\BusinessController@businessorders');
Route::post('businessassignedUnassigned','Api\TeamApi\assignmentController@businessassignedUnassigned');
Route::get('businessfmorders','Api\TeamApi\assignmentController@businessfmorders');
Route::post('businesscompletedorders','Api\TeamApi\assignmentController@businesscompletedorders');
Route::post('businesssearchorders','Api\TeamApi\BusinessController@businesssearchorders');
Route::get('getinventry','Api\TeamApi\CataloguesController@getinventry');
Route::post('postinventry','Api\TeamApi\CataloguesController@postinventry');
Route::get('createdInventories','Api\TeamApi\CataloguesController@createdInventories');
Route::post('inventoriesActions','Api\TeamApi\CataloguesController@inventoriesActions');
Route::get('fetchInventory','Api\TeamApi\CataloguesController@fetchInventory');